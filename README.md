# Beispielprojekt für Modellgetriebene Softwareentwicklung

## Ziel

Die in diesem Projekt entwickelte DSL soll die Entwicklung einer OpenAPI-Spezifikation erleichtern und
dabei stets korrekt sein. Aus dem Modell soll dann die richtige OpenAPI-YAML-Datei generiert werden können, 
welche so wiederum an OpenAPI-Code-Generatoren übergeben werden kann.

Der aktuelle DSL-Aufbau spiegelt den Aufbau der OpenAPI-Spezifikation in YAML im Groben wider.

## Test

Der derzeitige Stand der Entwicklung kann im Sandbox-Modell getestet werden. 
Es sind noch nicht alle Features der OpenAPI implementiert.