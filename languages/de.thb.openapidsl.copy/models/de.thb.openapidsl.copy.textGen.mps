<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:0fb7bff9-59f0-44cd-a233-8a32cc7ffb53(de.thb.openapidsl.copy.textGen)">
  <persistence version="9" />
  <languages>
    <use id="b83431fe-5c8f-40bc-8a36-65e25f4dd253" name="jetbrains.mps.lang.textGen" version="1" />
    <devkit ref="fa73d85a-ac7f-447b-846c-fcdc41caa600(jetbrains.mps.devkit.aspect.textgen)" />
  </languages>
  <imports>
    <import index="7thy" ref="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)" implicit="true" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1225271369338" name="jetbrains.mps.baseLanguage.structure.IsEmptyOperation" flags="nn" index="17RlXB" />
      <concept id="1225271408483" name="jetbrains.mps.baseLanguage.structure.IsNotEmptyOperation" flags="nn" index="17RvpY" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="b83431fe-5c8f-40bc-8a36-65e25f4dd253" name="jetbrains.mps.lang.textGen">
      <concept id="45307784116571022" name="jetbrains.mps.lang.textGen.structure.FilenameFunction" flags="ig" index="29tfMY" />
      <concept id="8931911391946696733" name="jetbrains.mps.lang.textGen.structure.ExtensionDeclaration" flags="in" index="9MYSb" />
      <concept id="1237305208784" name="jetbrains.mps.lang.textGen.structure.NewLineAppendPart" flags="ng" index="l8MVK" />
      <concept id="1237305334312" name="jetbrains.mps.lang.textGen.structure.NodeAppendPart" flags="ng" index="l9hG8">
        <child id="1237305790512" name="value" index="lb14g" />
      </concept>
      <concept id="1237305491868" name="jetbrains.mps.lang.textGen.structure.CollectionAppendPart" flags="ng" index="l9S2W">
        <child id="1237305945551" name="list" index="lbANJ" />
      </concept>
      <concept id="1237305557638" name="jetbrains.mps.lang.textGen.structure.ConstantStringAppendPart" flags="ng" index="la8eA">
        <property id="1237305576108" name="value" index="lacIc" />
      </concept>
      <concept id="1237306079178" name="jetbrains.mps.lang.textGen.structure.AppendOperation" flags="nn" index="lc7rE">
        <child id="1237306115446" name="part" index="lcghm" />
      </concept>
      <concept id="1233670071145" name="jetbrains.mps.lang.textGen.structure.ConceptTextGenDeclaration" flags="ig" index="WtQ9Q">
        <reference id="1233670257997" name="conceptDeclaration" index="WuzLi" />
        <child id="45307784116711884" name="filename" index="29tGrW" />
        <child id="1233749296504" name="textGenBlock" index="11c4hB" />
        <child id="7991274449437422201" name="extension" index="33IsuW" />
      </concept>
      <concept id="1233748055915" name="jetbrains.mps.lang.textGen.structure.NodeParameter" flags="nn" index="117lpO" />
      <concept id="1233749247888" name="jetbrains.mps.lang.textGen.structure.GenerateTextDeclaration" flags="in" index="11bSqf" />
      <concept id="1233920501193" name="jetbrains.mps.lang.textGen.structure.IndentBufferOperation" flags="nn" index="1bpajm" />
      <concept id="1236188139846" name="jetbrains.mps.lang.textGen.structure.WithIndentOperation" flags="nn" index="3izx1p">
        <child id="1236188238861" name="list" index="3izTki" />
      </concept>
      <concept id="1234794705341" name="jetbrains.mps.lang.textGen.structure.FoundErrorOperation" flags="nn" index="1ZvZ2y">
        <child id="1237470722868" name="text" index="v0bCk" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
      <concept id="1165530316231" name="jetbrains.mps.baseLanguage.collections.structure.IsEmptyOperation" flags="nn" index="1v1jN8" />
      <concept id="1176501494711" name="jetbrains.mps.baseLanguage.collections.structure.IsNotEmptyOperation" flags="nn" index="3GX2aA" />
    </language>
  </registry>
  <node concept="WtQ9Q" id="3JsE8aiFcA0">
    <ref role="WuzLi" to="7thy:4favA_iBNMM" resolve="StringSchema" />
    <node concept="11bSqf" id="3JsE8aiFcA1" role="11c4hB">
      <node concept="3clFbS" id="3JsE8aiFcA2" role="2VODD2">
        <node concept="1bpajm" id="3JsE8aiHUtU" role="3cqZAp" />
        <node concept="lc7rE" id="3JsE8aiFduz" role="3cqZAp">
          <node concept="l9hG8" id="3JsE8aiFduP" role="lcghm">
            <node concept="2OqwBi" id="3JsE8aiFdCI" role="lb14g">
              <node concept="117lpO" id="3JsE8aiFdvD" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JsE8aiFdFq" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="3JsE8aiHUCF" role="lcghm">
            <property role="lacIc" value=":\n" />
          </node>
        </node>
        <node concept="3izx1p" id="3JsE8aiFdOs" role="3cqZAp">
          <node concept="3clFbS" id="3JsE8aiFdOu" role="3izTki">
            <node concept="1bpajm" id="3JsE8aiHZfz" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiFoz3" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiFoAP" role="lcghm">
                <property role="lacIc" value="type: string" />
              </node>
              <node concept="l8MVK" id="3JsE8aiHUDP" role="lcghm" />
            </node>
            <node concept="1bpajm" id="3JsE8aiHZ9n" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiFemj" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiFeoW" role="lcghm">
                <property role="lacIc" value="description: " />
              </node>
              <node concept="l9hG8" id="3JsE8aiFerU" role="lcghm">
                <node concept="2OqwBi" id="3JsE8aiFe$4" role="lb14g">
                  <node concept="117lpO" id="3JsE8aiFesI" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiFeRl" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                  </node>
                </node>
              </node>
              <node concept="l8MVK" id="3JsE8aiHViW" role="lcghm" />
            </node>
            <node concept="3clFbJ" id="cAH7uXp41m" role="3cqZAp">
              <node concept="3clFbS" id="cAH7uXp41n" role="3clFbx">
                <node concept="1ZvZ2y" id="cAH7uXp41o" role="3cqZAp">
                  <node concept="Xl_RD" id="cAH7uXp41p" role="v0bCk">
                    <property role="Xl_RC" value="description is required" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="cAH7uXp41q" role="3clFbw">
                <node concept="2OqwBi" id="cAH7uXp41r" role="2Oq$k0">
                  <node concept="117lpO" id="cAH7uXp41s" role="2Oq$k0" />
                  <node concept="3TrcHB" id="cAH7uXp41t" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                  </node>
                </node>
                <node concept="17RlXB" id="cAH7uXp41u" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbH" id="cAH7uXp3Tq" role="3cqZAp" />
            <node concept="3clFbJ" id="3JsE8aiHZiG" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiHZiI" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiI0HI" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiFejs" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiFek8" role="lcghm">
                    <property role="lacIc" value="example: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiFdPn" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiFdXx" role="lb14g">
                      <node concept="117lpO" id="3JsE8aiFdQb" role="2Oq$k0" />
                      <node concept="3TrcHB" id="3JsE8aiFegf" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNMJ" resolve="example" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiHVlV" role="lcghm" />
                </node>
              </node>
              <node concept="2OqwBi" id="3JsE8aiI03Y" role="3clFbw">
                <node concept="2OqwBi" id="3JsE8aiHZtW" role="2Oq$k0">
                  <node concept="117lpO" id="3JsE8aiHZlU" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiHZJ1" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMJ" resolve="example" />
                  </node>
                </node>
                <node concept="17RvpY" id="3JsE8aiI0_W" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbJ" id="3JsE8aiI0N4" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiI0N6" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiI24A" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiFeWA" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiFeWB" role="lcghm">
                    <property role="lacIc" value="format: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiFeWD" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiFeWE" role="lb14g">
                      <node concept="117lpO" id="3JsE8aiFeWF" role="2Oq$k0" />
                      <node concept="3TrcHB" id="3JsE8aiFeWG" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:3JsE8ainSvo" resolve="format" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiHVQ4" role="lcghm" />
                </node>
              </node>
              <node concept="2OqwBi" id="3JsE8aiI1uE" role="3clFbw">
                <node concept="2OqwBi" id="3JsE8aiI0Y_" role="2Oq$k0">
                  <node concept="117lpO" id="3JsE8aiI0Qz" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiI19H" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:3JsE8ainSvo" resolve="format" />
                  </node>
                </node>
                <node concept="17RvpY" id="3JsE8aiI243" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbJ" id="3JsE8aiI2hT" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiI2hV" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiIf1T" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiFfb4" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiFfb5" role="lcghm">
                    <property role="lacIc" value="minLength: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiFfb7" role="lcghm">
                    <node concept="3cpWs3" id="3JsE8aiHWVb" role="lb14g">
                      <node concept="Xl_RD" id="3JsE8aiHWzh" role="3uHU7B" />
                      <node concept="2OqwBi" id="3JsE8aiFg5$" role="3uHU7w">
                        <node concept="3TrcHB" id="3JsE8aiFghw" role="2OqNvi">
                          <ref role="3TsBF5" to="7thy:3JsE8aiIcbJ" resolve="minLength" />
                        </node>
                        <node concept="117lpO" id="3JsE8aiHWVY" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiHWy$" role="lcghm" />
                </node>
              </node>
              <node concept="3eOSWO" id="3JsE8aiIeW5" role="3clFbw">
                <node concept="3cmrfG" id="3JsE8aiIeW9" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="2OqwBi" id="3JsE8aiIc_b" role="3uHU7B">
                  <node concept="117lpO" id="3JsE8aiIct9" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiIcRm" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:3JsE8aiIcbJ" resolve="minLength" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="3JsE8aiIfkf" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiIfkh" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiIgTq" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiFjrw" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiFjrx" role="lcghm">
                    <property role="lacIc" value="maxLength: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiFjrz" role="lcghm">
                    <node concept="3cpWs3" id="3JsE8aiHYkw" role="lb14g">
                      <node concept="Xl_RD" id="3JsE8aiHXWA" role="3uHU7B" />
                      <node concept="2OqwBi" id="3JsE8aiFjrA" role="3uHU7w">
                        <node concept="3TrcHB" id="3JsE8aiFjrB" role="2OqNvi">
                          <ref role="3TsBF5" to="7thy:3JsE8aiIcbN" resolve="maxLength" />
                        </node>
                        <node concept="117lpO" id="3JsE8aiHYpA" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiHXVT" role="lcghm" />
                </node>
              </node>
              <node concept="3eOSWO" id="3JsE8aiIgF9" role="3clFbw">
                <node concept="3cmrfG" id="3JsE8aiIgFG" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="2OqwBi" id="3JsE8aiIfvY" role="3uHU7B">
                  <node concept="117lpO" id="3JsE8aiIfod" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiIfVq" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:3JsE8aiIcbN" resolve="maxLength" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="3JsE8aiIh3S" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiIh3U" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiIio6" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiFjDe" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiFjDf" role="lcghm">
                    <property role="lacIc" value="pattern: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiFjDh" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiFjDk" role="lb14g">
                      <node concept="3TrcHB" id="3JsE8aiFjDl" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNS8" resolve="pattern" />
                      </node>
                      <node concept="117lpO" id="3JsE8aiFjDm" role="2Oq$k0" />
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiHYYf" role="lcghm" />
                </node>
              </node>
              <node concept="2OqwBi" id="3JsE8aiIhRT" role="3clFbw">
                <node concept="2OqwBi" id="3JsE8aiIhhk" role="2Oq$k0">
                  <node concept="117lpO" id="3JsE8aiIh9i" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiIhyW" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNS8" resolve="pattern" />
                  </node>
                </node>
                <node concept="17RvpY" id="3JsE8aiIinz" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="3JsE8aiFqkH">
    <ref role="WuzLi" to="7thy:3JW8_axMW7I" resolve="OpenApi" />
    <node concept="11bSqf" id="3JsE8aiFqkJ" role="11c4hB">
      <node concept="3clFbS" id="3JsE8aiFqkK" role="2VODD2">
        <node concept="lc7rE" id="3JsE8aiFql1" role="3cqZAp">
          <node concept="la8eA" id="3JsE8aiFqlj" role="lcghm">
            <property role="lacIc" value="openapi: " />
          </node>
          <node concept="l9hG8" id="7kDFC_u9KtH" role="lcghm">
            <node concept="2OqwBi" id="7kDFC_u9K$H" role="lb14g">
              <node concept="117lpO" id="7kDFC_u9Ku$" role="2Oq$k0" />
              <node concept="3TrcHB" id="7kDFC_u9KNv" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axMYAC" resolve="openapi" />
              </node>
            </node>
          </node>
          <node concept="l8MVK" id="7kDFC_u9XDu" role="lcghm" />
        </node>
        <node concept="lc7rE" id="3JsE8aiFyeB" role="3cqZAp">
          <node concept="la8eA" id="3JsE8aiFyeT" role="lcghm">
            <property role="lacIc" value="info:\n" />
          </node>
        </node>
        <node concept="3izx1p" id="3JsE8aiGRVI" role="3cqZAp">
          <node concept="3clFbS" id="3JsE8aiGRVK" role="3izTki">
            <node concept="lc7rE" id="3JsE8aiFt$V" role="3cqZAp">
              <node concept="l9hG8" id="3JsE8aiFt_f" role="lcghm">
                <node concept="2OqwBi" id="3JsE8aiFtGR" role="lb14g">
                  <node concept="117lpO" id="3JsE8aiFtA3" role="2Oq$k0" />
                  <node concept="3TrEf2" id="3JsE8aiFtW7" role="2OqNvi">
                    <ref role="3Tt5mk" to="7thy:3JW8_axMW7J" resolve="info" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="3JsE8aiFu7r" role="3cqZAp" />
        <node concept="lc7rE" id="3JsE8aiFue2" role="3cqZAp">
          <node concept="la8eA" id="3JsE8aiFufc" role="lcghm">
            <property role="lacIc" value="paths:\n" />
          </node>
        </node>
        <node concept="3izx1p" id="3JsE8aiGWQZ" role="3cqZAp">
          <node concept="3clFbS" id="3JsE8aiGWR1" role="3izTki">
            <node concept="3clFbJ" id="cAH7uXoMfH" role="3cqZAp">
              <node concept="3clFbS" id="cAH7uXoMfJ" role="3clFbx">
                <node concept="1ZvZ2y" id="cAH7uXoRdY" role="3cqZAp">
                  <node concept="Xl_RD" id="cAH7uXoRef" role="v0bCk">
                    <property role="Xl_RC" value="paths may not be empty" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="cAH7uXoO5W" role="3clFbw">
                <node concept="2OqwBi" id="cAH7uXoMnI" role="2Oq$k0">
                  <node concept="117lpO" id="cAH7uXoMgm" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="cAH7uXoM_X" role="2OqNvi">
                    <ref role="3TtcxE" to="7thy:3JW8_axN3xZ" resolve="paths" />
                  </node>
                </node>
                <node concept="1v1jN8" id="cAH7uXoRcZ" role="2OqNvi" />
              </node>
            </node>
            <node concept="lc7rE" id="3JsE8aiGkyH" role="3cqZAp">
              <node concept="l9S2W" id="3JsE8aiGkyZ" role="lcghm">
                <node concept="2OqwBi" id="3JsE8aiGkCZ" role="lbANJ">
                  <node concept="117lpO" id="3JsE8aiGkzj" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="3JsE8aiGkRF" role="2OqNvi">
                    <ref role="3TtcxE" to="7thy:3JW8_axN3xZ" resolve="paths" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="3JsE8aiFuph" role="3cqZAp" />
        <node concept="3clFbJ" id="cAH7uXpfzB" role="3cqZAp">
          <node concept="3clFbS" id="cAH7uXpfzD" role="3clFbx">
            <node concept="lc7rE" id="7kDFC_u8Zuq" role="3cqZAp">
              <node concept="la8eA" id="7kDFC_u8ZvR" role="lcghm">
                <property role="lacIc" value="components:" />
              </node>
              <node concept="l8MVK" id="7kDFC_u8Zxa" role="lcghm" />
            </node>
            <node concept="3izx1p" id="7kDFC_u8ZBR" role="3cqZAp">
              <node concept="3clFbS" id="7kDFC_u8ZBT" role="3izTki">
                <node concept="1bpajm" id="7kDFC_u8ZFM" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiFuqD" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiFuqE" role="lcghm">
                    <property role="lacIc" value="schemas:" />
                  </node>
                  <node concept="l8MVK" id="7kDFC_u8Zy0" role="lcghm" />
                </node>
                <node concept="3izx1p" id="3JsE8aiFuqF" role="3cqZAp">
                  <node concept="3clFbS" id="3JsE8aiFuqG" role="3izTki">
                    <node concept="lc7rE" id="3JsE8aiGkSi" role="3cqZAp">
                      <node concept="l9S2W" id="3JsE8aiGkSL" role="lcghm">
                        <node concept="2OqwBi" id="3JsE8aiGkYJ" role="lbANJ">
                          <node concept="117lpO" id="3JsE8aiGkT3" role="2Oq$k0" />
                          <node concept="3Tsc0h" id="3JsE8aiGldr" role="2OqNvi">
                            <ref role="3TtcxE" to="7thy:4favA_iCQSJ" resolve="schema" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="cAH7uXphk8" role="3clFbw">
            <node concept="2OqwBi" id="cAH7uXpfHY" role="2Oq$k0">
              <node concept="117lpO" id="cAH7uXpfAA" role="2Oq$k0" />
              <node concept="3Tsc0h" id="cAH7uXpfWK" role="2OqNvi">
                <ref role="3TtcxE" to="7thy:4favA_iCQSJ" resolve="schema" />
              </node>
            </node>
            <node concept="3GX2aA" id="cAH7uXpkt0" role="2OqNvi" />
          </node>
        </node>
      </node>
    </node>
    <node concept="9MYSb" id="3JsE8aiFXgV" role="33IsuW">
      <node concept="3clFbS" id="3JsE8aiFXgW" role="2VODD2">
        <node concept="3clFbF" id="3JsE8aiFXnF" role="3cqZAp">
          <node concept="Xl_RD" id="3JsE8aiFXnE" role="3clFbG">
            <property role="Xl_RC" value="yaml" />
          </node>
        </node>
      </node>
    </node>
    <node concept="29tfMY" id="3JsE8aiG43Y" role="29tGrW">
      <node concept="3clFbS" id="3JsE8aiG43Z" role="2VODD2">
        <node concept="3clFbF" id="3JsE8aiG4nJ" role="3cqZAp">
          <node concept="2OqwBi" id="3JsE8aiG5HA" role="3clFbG">
            <node concept="2OqwBi" id="3JsE8aiG57O" role="2Oq$k0">
              <node concept="2OqwBi" id="3JsE8aiG4xT" role="2Oq$k0">
                <node concept="117lpO" id="3JsE8aiG4nI" role="2Oq$k0" />
                <node concept="3TrEf2" id="3JsE8aiG4SA" role="2OqNvi">
                  <ref role="3Tt5mk" to="7thy:3JW8_axMW7J" resolve="info" />
                </node>
              </node>
              <node concept="3TrcHB" id="3JsE8aiG5sy" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axMFqV" resolve="title" />
              </node>
            </node>
            <node concept="liA8E" id="3JsE8aiG6dz" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~String.toLowerCase()" resolve="toLowerCase" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="3JsE8aiFy8c">
    <ref role="WuzLi" to="7thy:3JW8_axMFqS" resolve="OpenApiInfo" />
    <node concept="11bSqf" id="3JsE8aiFyel" role="11c4hB">
      <node concept="3clFbS" id="3JsE8aiFyem" role="2VODD2">
        <node concept="1bpajm" id="3JsE8aiGMX7" role="3cqZAp" />
        <node concept="lc7rE" id="3JsE8aiFyh7" role="3cqZAp">
          <node concept="la8eA" id="3JsE8aiFyMY" role="lcghm">
            <property role="lacIc" value="title: " />
          </node>
          <node concept="l9hG8" id="3JsE8aiFyOV" role="lcghm">
            <node concept="2OqwBi" id="3JsE8aiFyX6" role="lb14g">
              <node concept="117lpO" id="3JsE8aiFyPJ" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JsE8aiFzeI" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axMFqV" resolve="title" />
              </node>
            </node>
          </node>
          <node concept="l8MVK" id="3JsE8aiFzg1" role="lcghm" />
        </node>
        <node concept="3clFbJ" id="cAH7uXpeEB" role="3cqZAp">
          <node concept="3clFbS" id="cAH7uXpeEC" role="3clFbx">
            <node concept="1ZvZ2y" id="cAH7uXpeED" role="3cqZAp">
              <node concept="Xl_RD" id="cAH7uXpeEE" role="v0bCk">
                <property role="Xl_RC" value="title is required" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="cAH7uXpeEF" role="3clFbw">
            <node concept="2OqwBi" id="cAH7uXpeEG" role="2Oq$k0">
              <node concept="117lpO" id="cAH7uXpeEH" role="2Oq$k0" />
              <node concept="3TrcHB" id="cAH7uXpeEI" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axMFqV" resolve="title" />
              </node>
            </node>
            <node concept="17RlXB" id="cAH7uXpeEJ" role="2OqNvi" />
          </node>
        </node>
        <node concept="3clFbH" id="cAH7uXpeCt" role="3cqZAp" />
        <node concept="3clFbJ" id="3JsE8aiGxAo" role="3cqZAp">
          <node concept="3clFbS" id="3JsE8aiGxAq" role="3clFbx">
            <node concept="1bpajm" id="3JsE8aiGMZr" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiFzhh" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiFzhX" role="lcghm">
                <property role="lacIc" value="description: " />
              </node>
              <node concept="l9hG8" id="3JsE8aiFzk4" role="lcghm">
                <node concept="2OqwBi" id="3JsE8aiFzsf" role="lb14g">
                  <node concept="117lpO" id="3JsE8aiFzkS" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiFzIq" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:3JW8_axMFqX" resolve="description" />
                  </node>
                </node>
              </node>
              <node concept="l8MVK" id="3JsE8aiFzLF" role="lcghm" />
            </node>
          </node>
          <node concept="2OqwBi" id="3JsE8aiGyl5" role="3clFbw">
            <node concept="2OqwBi" id="3JsE8aiGxJU" role="2Oq$k0">
              <node concept="117lpO" id="3JsE8aiGxBS" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JsE8aiGy25" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axMFqX" resolve="description" />
              </node>
            </node>
            <node concept="17RvpY" id="3JsE8aiGyOJ" role="2OqNvi" />
          </node>
        </node>
        <node concept="3clFbJ" id="cAH7uXpeYN" role="3cqZAp">
          <node concept="3clFbS" id="cAH7uXpeYO" role="3clFbx">
            <node concept="1ZvZ2y" id="cAH7uXpeYP" role="3cqZAp">
              <node concept="Xl_RD" id="cAH7uXpeYQ" role="v0bCk">
                <property role="Xl_RC" value="version is required" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="cAH7uXpeYR" role="3clFbw">
            <node concept="2OqwBi" id="cAH7uXpeYS" role="2Oq$k0">
              <node concept="117lpO" id="cAH7uXpeYT" role="2Oq$k0" />
              <node concept="3TrcHB" id="cAH7uXpeYU" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axMFr0" resolve="version" />
              </node>
            </node>
            <node concept="17RlXB" id="cAH7uXpeYV" role="2OqNvi" />
          </node>
        </node>
        <node concept="3clFbH" id="cAH7uXpeU9" role="3cqZAp" />
        <node concept="1bpajm" id="3JsE8aiGN1J" role="3cqZAp" />
        <node concept="lc7rE" id="3JsE8aiFzNj" role="3cqZAp">
          <node concept="la8eA" id="3JsE8aiFzOn" role="lcghm">
            <property role="lacIc" value="version: " />
          </node>
          <node concept="l9hG8" id="3JsE8aiFzPo" role="lcghm">
            <node concept="2OqwBi" id="3JsE8aiFzQD" role="lb14g">
              <node concept="117lpO" id="3JsE8aiFzQc" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JsE8aiFzUr" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axMFr0" resolve="version" />
              </node>
            </node>
          </node>
          <node concept="l8MVK" id="3JsE8aiFzVI" role="lcghm" />
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="3JsE8aiF$la">
    <ref role="WuzLi" to="7thy:3JW8_axN3f4" resolve="PathObject" />
    <node concept="11bSqf" id="3JsE8aiF$lb" role="11c4hB">
      <node concept="3clFbS" id="3JsE8aiF$lc" role="2VODD2">
        <node concept="1bpajm" id="3JsE8aiH1_O" role="3cqZAp" />
        <node concept="lc7rE" id="3JsE8aiF$lt" role="3cqZAp">
          <node concept="l9hG8" id="3JsE8aiF$lJ" role="lcghm">
            <node concept="2OqwBi" id="3JsE8aiF$tn" role="lb14g">
              <node concept="117lpO" id="3JsE8aiF$mz" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JsE8aiF$G4" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axN3fb" resolve="path" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="3JsE8aiF$Jc" role="lcghm">
            <property role="lacIc" value=":\n" />
          </node>
        </node>
        <node concept="3izx1p" id="3JsE8aiF$MJ" role="3cqZAp">
          <node concept="3clFbS" id="3JsE8aiF$ML" role="3izTki">
            <node concept="3clFbJ" id="cAH7uXoGJ$" role="3cqZAp">
              <node concept="3clFbS" id="cAH7uXoGJA" role="3clFbx">
                <node concept="1ZvZ2y" id="cAH7uXoLTg" role="3cqZAp">
                  <node concept="Xl_RD" id="cAH7uXoLTx" role="v0bCk">
                    <property role="Xl_RC" value="operation may not be empty" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="cAH7uXoIJp" role="3clFbw">
                <node concept="2OqwBi" id="cAH7uXoGR_" role="2Oq$k0">
                  <node concept="117lpO" id="cAH7uXoGKd" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="cAH7uXoH6U" role="2OqNvi">
                    <ref role="3TtcxE" to="7thy:3JW8_axN3fe" resolve="operation" />
                  </node>
                </node>
                <node concept="1v1jN8" id="cAH7uXoLSh" role="2OqNvi" />
              </node>
            </node>
            <node concept="lc7rE" id="3JsE8aiGzoa" role="3cqZAp">
              <node concept="l9S2W" id="3JsE8aiGzoK" role="lcghm">
                <node concept="2OqwBi" id="3JsE8aiGzuK" role="lbANJ">
                  <node concept="117lpO" id="3JsE8aiGzp4" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="3JsE8aiGzHZ" role="2OqNvi">
                    <ref role="3TtcxE" to="7thy:3JW8_axN3fe" resolve="operation" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="3JsE8aiF__x">
    <ref role="WuzLi" to="7thy:3JW8_axN3Pw" resolve="GetOperationObject" />
    <node concept="11bSqf" id="3JsE8aiF__y" role="11c4hB">
      <node concept="3clFbS" id="3JsE8aiF__z" role="2VODD2">
        <node concept="1bpajm" id="3JsE8aiH6qT" role="3cqZAp" />
        <node concept="lc7rE" id="3JsE8aiFE9s" role="3cqZAp">
          <node concept="la8eA" id="3JsE8aiFE9I" role="lcghm">
            <property role="lacIc" value="get:\n" />
          </node>
        </node>
        <node concept="3izx1p" id="3JsE8aiFEG3" role="3cqZAp">
          <node concept="3clFbS" id="3JsE8aiFEG5" role="3izTki">
            <node concept="1bpajm" id="3JsE8aiHaZv" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiFEaV" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiFEKw" role="lcghm">
                <property role="lacIc" value="summary: " />
              </node>
              <node concept="l9hG8" id="3JsE8aiFEbf" role="lcghm">
                <node concept="2OqwBi" id="3JsE8aiFEjp" role="lb14g">
                  <node concept="117lpO" id="3JsE8aiFEc3" role="2Oq$k0" />
                  <node concept="3TrcHB" id="cAH7uXq3ya" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:3JW8_axN3fB" resolve="summary" />
                  </node>
                </node>
              </node>
              <node concept="l8MVK" id="3JsE8aiFEMh" role="lcghm" />
            </node>
            <node concept="3clFbJ" id="cAH7uXpdFE" role="3cqZAp">
              <node concept="3clFbS" id="cAH7uXpdFF" role="3clFbx">
                <node concept="1ZvZ2y" id="cAH7uXpdFG" role="3cqZAp">
                  <node concept="Xl_RD" id="cAH7uXpdFH" role="v0bCk">
                    <property role="Xl_RC" value="summary is required" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="cAH7uXpdFI" role="3clFbw">
                <node concept="2OqwBi" id="cAH7uXpdFJ" role="2Oq$k0">
                  <node concept="117lpO" id="cAH7uXpdFK" role="2Oq$k0" />
                  <node concept="3TrcHB" id="cAH7uXpdFL" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:3JW8_axN3fB" resolve="summary" />
                  </node>
                </node>
                <node concept="17RlXB" id="cAH7uXpdFM" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbH" id="cAH7uXpdDI" role="3cqZAp" />
            <node concept="3clFbJ" id="3JsE8aiG$pH" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiG$pI" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiHb1z" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiG$pJ" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiG$pK" role="lcghm">
                    <property role="lacIc" value="description: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiG$pL" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiG$pM" role="lb14g">
                      <node concept="117lpO" id="3JsE8aiG$pN" role="2Oq$k0" />
                      <node concept="3TrcHB" id="3JsE8aiG$pO" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:3JW8_axN3fI" resolve="description" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiG$pP" role="lcghm" />
                </node>
              </node>
              <node concept="2OqwBi" id="3JsE8aiG$pQ" role="3clFbw">
                <node concept="2OqwBi" id="3JsE8aiG$pR" role="2Oq$k0">
                  <node concept="117lpO" id="3JsE8aiG$pS" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiG$pT" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:3JW8_axN3fI" resolve="description" />
                  </node>
                </node>
                <node concept="17RvpY" id="3JsE8aiG$pU" role="2OqNvi" />
              </node>
            </node>
            <node concept="1bpajm" id="3JsE8aiHb3B" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiG$pV" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiG$pW" role="lcghm">
                <property role="lacIc" value="responses:\n" />
              </node>
            </node>
            <node concept="3izx1p" id="3JsE8aiG$pX" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiG$pY" role="3izTki">
                <node concept="lc7rE" id="3JsE8aiG$pZ" role="3cqZAp">
                  <node concept="l9S2W" id="3JsE8aiG$q0" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiG$q1" role="lbANJ">
                      <node concept="117lpO" id="3JsE8aiG$q2" role="2Oq$k0" />
                      <node concept="3Tsc0h" id="3JsE8aiG$q3" role="2OqNvi">
                        <ref role="3TtcxE" to="7thy:4favA_iAyf8" resolve="responses" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="3JsE8aiFGfw">
    <ref role="WuzLi" to="7thy:4favA_iBl1f" resolve="PostOperationObject" />
    <node concept="11bSqf" id="3JsE8aiFGfy" role="11c4hB">
      <node concept="3clFbS" id="3JsE8aiFGfz" role="2VODD2">
        <node concept="1bpajm" id="3JsE8aiHfTA" role="3cqZAp" />
        <node concept="lc7rE" id="3JsE8aiFGfP" role="3cqZAp">
          <node concept="la8eA" id="3JsE8aiFGfQ" role="lcghm">
            <property role="lacIc" value="post:\n" />
          </node>
        </node>
        <node concept="3izx1p" id="3JsE8aiFGfR" role="3cqZAp">
          <node concept="3clFbS" id="3JsE8aiFGfS" role="3izTki">
            <node concept="1bpajm" id="3JsE8aiHfWu" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiFGfT" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiFGfU" role="lcghm">
                <property role="lacIc" value="summary: " />
              </node>
              <node concept="l9hG8" id="3JsE8aiFGfV" role="lcghm">
                <node concept="2OqwBi" id="3JsE8aiFGfW" role="lb14g">
                  <node concept="117lpO" id="3JsE8aiFGfX" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiFGfY" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:3JW8_axN3fB" resolve="summary" />
                  </node>
                </node>
              </node>
              <node concept="l8MVK" id="3JsE8aiFGfZ" role="lcghm" />
            </node>
            <node concept="3clFbJ" id="cAH7uXpen8" role="3cqZAp">
              <node concept="3clFbS" id="cAH7uXpen9" role="3clFbx">
                <node concept="1ZvZ2y" id="cAH7uXpena" role="3cqZAp">
                  <node concept="Xl_RD" id="cAH7uXpenb" role="v0bCk">
                    <property role="Xl_RC" value="summary is required" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="cAH7uXpenc" role="3clFbw">
                <node concept="2OqwBi" id="cAH7uXpend" role="2Oq$k0">
                  <node concept="117lpO" id="cAH7uXpene" role="2Oq$k0" />
                  <node concept="3TrcHB" id="cAH7uXpenf" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:3JW8_axN3fB" resolve="summary" />
                  </node>
                </node>
                <node concept="17RlXB" id="cAH7uXpeng" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbH" id="cAH7uXpeje" role="3cqZAp" />
            <node concept="3clFbJ" id="3JsE8aiG$x7" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiG$x8" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiHg0v" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiG$x9" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiG$xa" role="lcghm">
                    <property role="lacIc" value="description: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiG$xb" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiG$xc" role="lb14g">
                      <node concept="117lpO" id="3JsE8aiG$xd" role="2Oq$k0" />
                      <node concept="3TrcHB" id="3JsE8aiG$xe" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:3JW8_axN3fI" resolve="description" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiG$xf" role="lcghm" />
                </node>
              </node>
              <node concept="2OqwBi" id="3JsE8aiG$xg" role="3clFbw">
                <node concept="2OqwBi" id="3JsE8aiG$xh" role="2Oq$k0">
                  <node concept="117lpO" id="3JsE8aiG$xi" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiG$xj" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:3JW8_axN3fI" resolve="description" />
                  </node>
                </node>
                <node concept="17RvpY" id="3JsE8aiG$xk" role="2OqNvi" />
              </node>
            </node>
            <node concept="1bpajm" id="3JsE8aiHg2z" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiG$xl" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiG$xm" role="lcghm">
                <property role="lacIc" value="responses:\n" />
              </node>
            </node>
            <node concept="3izx1p" id="3JsE8aiG$xn" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiG$xo" role="3izTki">
                <node concept="lc7rE" id="3JsE8aiG$xp" role="3cqZAp">
                  <node concept="l9S2W" id="3JsE8aiG$xq" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiG$xr" role="lbANJ">
                      <node concept="117lpO" id="3JsE8aiG$xs" role="2Oq$k0" />
                      <node concept="3Tsc0h" id="3JsE8aiG$xt" role="2OqNvi">
                        <ref role="3TtcxE" to="7thy:4favA_iAyf8" resolve="responses" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="3JsE8aiFGp2">
    <ref role="WuzLi" to="7thy:4favA_iBl1e" resolve="PutOperationObject" />
    <node concept="11bSqf" id="3JsE8aiFGp3" role="11c4hB">
      <node concept="3clFbS" id="3JsE8aiFGp4" role="2VODD2">
        <node concept="1bpajm" id="3JsE8aiHg9l" role="3cqZAp" />
        <node concept="lc7rE" id="3JsE8aiFGpl" role="3cqZAp">
          <node concept="la8eA" id="3JsE8aiFGpm" role="lcghm">
            <property role="lacIc" value="put:\n" />
          </node>
        </node>
        <node concept="3izx1p" id="3JsE8aiFGpn" role="3cqZAp">
          <node concept="3clFbS" id="3JsE8aiFGpo" role="3izTki">
            <node concept="1bpajm" id="3JsE8aiHgcd" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiFGpp" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiFGpq" role="lcghm">
                <property role="lacIc" value="summary: " />
              </node>
              <node concept="l9hG8" id="3JsE8aiFGpr" role="lcghm">
                <node concept="2OqwBi" id="3JsE8aiFGps" role="lb14g">
                  <node concept="117lpO" id="3JsE8aiFGpt" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiFGpu" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:3JW8_axN3fB" resolve="summary" />
                  </node>
                </node>
              </node>
              <node concept="l8MVK" id="3JsE8aiFGpv" role="lcghm" />
            </node>
            <node concept="3clFbJ" id="cAH7uXpeca" role="3cqZAp">
              <node concept="3clFbS" id="cAH7uXpecb" role="3clFbx">
                <node concept="1ZvZ2y" id="cAH7uXpecc" role="3cqZAp">
                  <node concept="Xl_RD" id="cAH7uXpecd" role="v0bCk">
                    <property role="Xl_RC" value="summary is required" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="cAH7uXpece" role="3clFbw">
                <node concept="2OqwBi" id="cAH7uXpecf" role="2Oq$k0">
                  <node concept="117lpO" id="cAH7uXpecg" role="2Oq$k0" />
                  <node concept="3TrcHB" id="cAH7uXpech" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:3JW8_axN3fB" resolve="summary" />
                  </node>
                </node>
                <node concept="17RlXB" id="cAH7uXpeci" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbH" id="cAH7uXpeae" role="3cqZAp" />
            <node concept="3clFbJ" id="3JsE8aiG$Cx" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiG$Cy" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiHgeh" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiG$Cz" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiG$C$" role="lcghm">
                    <property role="lacIc" value="description: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiG$C_" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiG$CA" role="lb14g">
                      <node concept="117lpO" id="3JsE8aiG$CB" role="2Oq$k0" />
                      <node concept="3TrcHB" id="3JsE8aiG$CC" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:3JW8_axN3fI" resolve="description" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiG$CD" role="lcghm" />
                </node>
              </node>
              <node concept="2OqwBi" id="3JsE8aiG$CE" role="3clFbw">
                <node concept="2OqwBi" id="3JsE8aiG$CF" role="2Oq$k0">
                  <node concept="117lpO" id="3JsE8aiG$CG" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiG$CH" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:3JW8_axN3fI" resolve="description" />
                  </node>
                </node>
                <node concept="17RvpY" id="3JsE8aiG$CI" role="2OqNvi" />
              </node>
            </node>
            <node concept="1bpajm" id="3JsE8aiHggN" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiG$CJ" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiG$CK" role="lcghm">
                <property role="lacIc" value="responses:\n" />
              </node>
            </node>
            <node concept="3izx1p" id="3JsE8aiG$CL" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiG$CM" role="3izTki">
                <node concept="lc7rE" id="3JsE8aiG$CN" role="3cqZAp">
                  <node concept="l9S2W" id="3JsE8aiG$CO" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiG$CP" role="lbANJ">
                      <node concept="117lpO" id="3JsE8aiG$CQ" role="2Oq$k0" />
                      <node concept="3Tsc0h" id="3JsE8aiG$CR" role="2OqNvi">
                        <ref role="3TtcxE" to="7thy:4favA_iAyf8" resolve="responses" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="3JsE8aiFGvU">
    <ref role="WuzLi" to="7thy:4favA_iBl1g" resolve="DeleteOperationObject" />
    <node concept="11bSqf" id="3JsE8aiFGvV" role="11c4hB">
      <node concept="3clFbS" id="3JsE8aiFGvW" role="2VODD2">
        <node concept="1bpajm" id="3JsE8aiHgpy" role="3cqZAp" />
        <node concept="lc7rE" id="3JsE8aiFGwd" role="3cqZAp">
          <node concept="la8eA" id="3JsE8aiFGwe" role="lcghm">
            <property role="lacIc" value="delete:\n" />
          </node>
        </node>
        <node concept="3izx1p" id="3JsE8aiFGwf" role="3cqZAp">
          <node concept="3clFbS" id="3JsE8aiFGwg" role="3izTki">
            <node concept="1bpajm" id="3JsE8aiHgsq" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiFGwh" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiFGwi" role="lcghm">
                <property role="lacIc" value="summary: " />
              </node>
              <node concept="l9hG8" id="3JsE8aiFGwj" role="lcghm">
                <node concept="2OqwBi" id="3JsE8aiFGwk" role="lb14g">
                  <node concept="117lpO" id="3JsE8aiFGwl" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiFGwm" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:3JW8_axN3fB" resolve="summary" />
                  </node>
                </node>
              </node>
              <node concept="l8MVK" id="3JsE8aiFGwn" role="lcghm" />
            </node>
            <node concept="3clFbJ" id="cAH7uXpe3a" role="3cqZAp">
              <node concept="3clFbS" id="cAH7uXpe3b" role="3clFbx">
                <node concept="1ZvZ2y" id="cAH7uXpe3c" role="3cqZAp">
                  <node concept="Xl_RD" id="cAH7uXpe3d" role="v0bCk">
                    <property role="Xl_RC" value="summary is required" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="cAH7uXpe3e" role="3clFbw">
                <node concept="2OqwBi" id="cAH7uXpe3f" role="2Oq$k0">
                  <node concept="117lpO" id="cAH7uXpe3g" role="2Oq$k0" />
                  <node concept="3TrcHB" id="cAH7uXpe3h" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:3JW8_axN3fB" resolve="summary" />
                  </node>
                </node>
                <node concept="17RlXB" id="cAH7uXpe3i" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbH" id="cAH7uXpe1e" role="3cqZAp" />
            <node concept="3clFbJ" id="3JsE8aiG$fc" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiG$fd" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiHguu" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiG$fe" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiG$ff" role="lcghm">
                    <property role="lacIc" value="description: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiG$fg" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiG$fh" role="lb14g">
                      <node concept="117lpO" id="3JsE8aiG$fi" role="2Oq$k0" />
                      <node concept="3TrcHB" id="3JsE8aiG$fj" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:3JW8_axN3fI" resolve="description" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiG$fk" role="lcghm" />
                </node>
              </node>
              <node concept="2OqwBi" id="3JsE8aiG$fl" role="3clFbw">
                <node concept="2OqwBi" id="3JsE8aiG$fm" role="2Oq$k0">
                  <node concept="117lpO" id="3JsE8aiG$fn" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiG$fo" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:3JW8_axN3fI" resolve="description" />
                  </node>
                </node>
                <node concept="17RvpY" id="3JsE8aiG$fp" role="2OqNvi" />
              </node>
            </node>
            <node concept="1bpajm" id="3JsE8aiHgwy" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiFGwv" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiFGww" role="lcghm">
                <property role="lacIc" value="responses:\n" />
              </node>
            </node>
            <node concept="3izx1p" id="3JsE8aiFGwx" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiFGwy" role="3izTki">
                <node concept="lc7rE" id="3JsE8aiGzQ5" role="3cqZAp">
                  <node concept="l9S2W" id="3JsE8aiGzQD" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiGzXb" role="lbANJ">
                      <node concept="117lpO" id="3JsE8aiGzQX" role="2Oq$k0" />
                      <node concept="3Tsc0h" id="3JsE8aiG$eM" role="2OqNvi">
                        <ref role="3TtcxE" to="7thy:4favA_iAyf8" resolve="responses" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="3JsE8aiFGBg">
    <ref role="WuzLi" to="7thy:4favA_iAyfb" resolve="Response" />
    <node concept="11bSqf" id="3JsE8aiFGBh" role="11c4hB">
      <node concept="3clFbS" id="3JsE8aiFGBi" role="2VODD2">
        <node concept="1bpajm" id="3JsE8aiHbte" role="3cqZAp" />
        <node concept="lc7rE" id="3JsE8aiFGC3" role="3cqZAp">
          <node concept="l9hG8" id="3JsE8aiFGCl" role="lcghm">
            <node concept="3cpWs3" id="3JsE8aiFHOR" role="lb14g">
              <node concept="Xl_RD" id="3JsE8aiFHt3" role="3uHU7B">
                <property role="Xl_RC" value="'" />
              </node>
              <node concept="2OqwBi" id="3JsE8aiFGJX" role="3uHU7w">
                <node concept="3TrcHB" id="3JsE8aiFGZd" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iAyfc" resolve="httpCode" />
                </node>
                <node concept="117lpO" id="3JsE8aiFHYg" role="2Oq$k0" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="3JsE8aiFHZ0" role="lcghm">
            <property role="lacIc" value="':" />
          </node>
          <node concept="l8MVK" id="3JsE8aiFI0Q" role="lcghm" />
        </node>
        <node concept="3clFbJ" id="cAH7uXohL2" role="3cqZAp">
          <node concept="3clFbS" id="cAH7uXohL4" role="3clFbx">
            <node concept="1ZvZ2y" id="cAH7uXos4w" role="3cqZAp">
              <node concept="Xl_RD" id="cAH7uXos4K" role="v0bCk">
                <property role="Xl_RC" value="At least a description is required" />
              </node>
            </node>
          </node>
          <node concept="1Wc70l" id="cAH7uXopqO" role="3clFbw">
            <node concept="2OqwBi" id="cAH7uXoq9d" role="3uHU7w">
              <node concept="2OqwBi" id="cAH7uXop_K" role="2Oq$k0">
                <node concept="117lpO" id="cAH7uXopu3" role="2Oq$k0" />
                <node concept="3TrcHB" id="cAH7uXopPn" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iAyfe" resolve="description" />
                </node>
              </node>
              <node concept="17RlXB" id="cAH7uXoqDP" role="2OqNvi" />
            </node>
            <node concept="2OqwBi" id="cAH7uXojCE" role="3uHU7B">
              <node concept="2OqwBi" id="cAH7uXoi1J" role="2Oq$k0">
                <node concept="117lpO" id="cAH7uXohUn" role="2Oq$k0" />
                <node concept="3Tsc0h" id="cAH7uXoih4" role="2OqNvi">
                  <ref role="3TtcxE" to="7thy:4favA_iB_5O" resolve="content" />
                </node>
              </node>
              <node concept="1v1jN8" id="cAH7uXomJH" role="2OqNvi" />
            </node>
          </node>
        </node>
        <node concept="3izx1p" id="3JsE8aiFI4K" role="3cqZAp">
          <node concept="3clFbS" id="3JsE8aiFI4M" role="3izTki">
            <node concept="3clFbJ" id="3JsE8aiGzbv" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiGzbw" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiHbw4" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiGzbx" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiGzby" role="lcghm">
                    <property role="lacIc" value="description: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiGzbz" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiGzb$" role="lb14g">
                      <node concept="117lpO" id="3JsE8aiGzb_" role="2Oq$k0" />
                      <node concept="3TrcHB" id="3JsE8aiGzbA" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iAyfe" resolve="description" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiGzbB" role="lcghm" />
                </node>
              </node>
              <node concept="2OqwBi" id="3JsE8aiGzbC" role="3clFbw">
                <node concept="2OqwBi" id="3JsE8aiGzbD" role="2Oq$k0">
                  <node concept="117lpO" id="3JsE8aiGzbE" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiGzbF" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iAyfe" resolve="description" />
                  </node>
                </node>
                <node concept="17RvpY" id="3JsE8aiGzbG" role="2OqNvi" />
              </node>
              <node concept="9aQIb" id="cAH7uXp09q" role="9aQIa">
                <node concept="3clFbS" id="cAH7uXp09r" role="9aQI4">
                  <node concept="1ZvZ2y" id="cAH7uXp0dq" role="3cqZAp">
                    <node concept="Xl_RD" id="cAH7uXp0dE" role="v0bCk">
                      <property role="Xl_RC" value="description is required" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="3JsE8aiHKvv" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiHKvx" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiHbyo" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiFIDg" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiFIFL" role="lcghm">
                    <property role="lacIc" value="content:\n" />
                  </node>
                </node>
                <node concept="3izx1p" id="3JsE8aiFJWs" role="3cqZAp">
                  <node concept="3clFbS" id="3JsE8aiFJWu" role="3izTki">
                    <node concept="lc7rE" id="3JsE8aiHb$j" role="3cqZAp">
                      <node concept="l9S2W" id="3JsE8aiHb$_" role="lcghm">
                        <node concept="2OqwBi" id="3JsE8aiHbE_" role="lbANJ">
                          <node concept="117lpO" id="3JsE8aiHb$T" role="2Oq$k0" />
                          <node concept="3Tsc0h" id="3JsE8aiHbTO" role="2OqNvi">
                            <ref role="3TtcxE" to="7thy:4favA_iB_5O" resolve="content" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="3JsE8aiHMf_" role="3clFbw">
                <node concept="2OqwBi" id="3JsE8aiHKE4" role="2Oq$k0">
                  <node concept="117lpO" id="3JsE8aiHKyw" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="3JsE8aiHKSe" role="2OqNvi">
                    <ref role="3TtcxE" to="7thy:4favA_iB_5O" resolve="content" />
                  </node>
                </node>
                <node concept="3GX2aA" id="3JsE8aiHPmv" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="3JsE8aiFKtJ">
    <ref role="WuzLi" to="7thy:4favA_iB$7K" resolve="Content" />
    <node concept="11bSqf" id="3JsE8aiFKtK" role="11c4hB">
      <node concept="3clFbS" id="3JsE8aiFKtL" role="2VODD2">
        <node concept="1bpajm" id="3JsE8aiHc38" role="3cqZAp" />
        <node concept="lc7rE" id="3JsE8aiFKui" role="3cqZAp">
          <node concept="l9hG8" id="3JsE8aiFKu$" role="lcghm">
            <node concept="2OqwBi" id="3JsE8aiFL20" role="lb14g">
              <node concept="2OqwBi" id="3JsE8aiFKAc" role="2Oq$k0">
                <node concept="117lpO" id="3JsE8aiFKvo" role="2Oq$k0" />
                <node concept="3TrcHB" id="3JsE8aiFKOT" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iB$7L" resolve="mediaType" />
                </node>
              </node>
              <node concept="liA8E" id="3JsE8aiFLhP" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~Object.toString()" resolve="toString" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="3JsE8aiFLlG" role="lcghm">
            <property role="lacIc" value=":" />
          </node>
          <node concept="l8MVK" id="3JsE8aiFLo8" role="lcghm" />
        </node>
        <node concept="3izx1p" id="3JsE8aiFLqi" role="3cqZAp">
          <node concept="3clFbS" id="3JsE8aiFLqk" role="3izTki">
            <node concept="1bpajm" id="3JsE8aiHc5q" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiFLro" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiFLrE" role="lcghm">
                <property role="lacIc" value="schema:" />
              </node>
              <node concept="l8MVK" id="3JsE8aiFLsF" role="lcghm" />
            </node>
            <node concept="3clFbJ" id="cAH7uXq9qD" role="3cqZAp">
              <node concept="3clFbS" id="cAH7uXq9qF" role="3clFbx">
                <node concept="1ZvZ2y" id="cAH7uXqbhc" role="3cqZAp">
                  <node concept="Xl_RD" id="cAH7uXqbht" role="v0bCk">
                    <property role="Xl_RC" value="schema name may not be empty" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="cAH7uXqaLE" role="3clFbw">
                <node concept="2OqwBi" id="cAH7uXqa2C" role="2Oq$k0">
                  <node concept="2OqwBi" id="cAH7uXq9zc" role="2Oq$k0">
                    <node concept="117lpO" id="cAH7uXq9rO" role="2Oq$k0" />
                    <node concept="3TrEf2" id="cAH7uXq9LY" role="2OqNvi">
                      <ref role="3Tt5mk" to="7thy:4favA_iBNSA" resolve="schema" />
                    </node>
                  </node>
                  <node concept="3TrcHB" id="cAH7uXqasK" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                  </node>
                </node>
                <node concept="17RlXB" id="cAH7uXqbgz" role="2OqNvi" />
              </node>
            </node>
            <node concept="3izx1p" id="3JsE8aiFLtD" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiFLtF" role="3izTki">
                <node concept="1bpajm" id="3JsE8aiHc6O" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiFLtU" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiFMnH" role="lcghm">
                    <property role="lacIc" value="$ref: " />
                  </node>
                  <node concept="la8eA" id="3JsE8aiFMti" role="lcghm">
                    <property role="lacIc" value="'#/components/schemas/" />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiFLuc" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiFM2i" role="lb14g">
                      <node concept="2OqwBi" id="3JsE8aiFL_O" role="2Oq$k0">
                        <node concept="117lpO" id="3JsE8aiFLv0" role="2Oq$k0" />
                        <node concept="3TrEf2" id="3JsE8aiFLQa" role="2OqNvi">
                          <ref role="3Tt5mk" to="7thy:4favA_iBNSA" resolve="schema" />
                        </node>
                      </node>
                      <node concept="3TrcHB" id="3JsE8aiFMkr" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                      </node>
                    </node>
                  </node>
                  <node concept="la8eA" id="7kDFC_uawU6" role="lcghm">
                    <property role="lacIc" value="'" />
                  </node>
                  <node concept="l8MVK" id="3JsE8aiFMMS" role="lcghm" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="3JsE8aiIiBQ">
    <ref role="WuzLi" to="7thy:4favA_iBNSc" resolve="IntegerSchema" />
    <node concept="11bSqf" id="3JsE8aiIiBR" role="11c4hB">
      <node concept="3clFbS" id="3JsE8aiIiBS" role="2VODD2">
        <node concept="1bpajm" id="3JsE8aiIiC9" role="3cqZAp" />
        <node concept="lc7rE" id="3JsE8aiIiCa" role="3cqZAp">
          <node concept="l9hG8" id="3JsE8aiIiCb" role="lcghm">
            <node concept="2OqwBi" id="3JsE8aiIiCc" role="lb14g">
              <node concept="117lpO" id="3JsE8aiIiCd" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JsE8aiIiCe" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="3JsE8aiIiCf" role="lcghm">
            <property role="lacIc" value=":\n" />
          </node>
        </node>
        <node concept="3izx1p" id="3JsE8aiIiCg" role="3cqZAp">
          <node concept="3clFbS" id="3JsE8aiIiCh" role="3izTki">
            <node concept="1bpajm" id="3JsE8aiIiCi" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiIiCj" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiIiCk" role="lcghm">
                <property role="lacIc" value="type: integer" />
              </node>
              <node concept="l8MVK" id="3JsE8aiIiCl" role="lcghm" />
            </node>
            <node concept="1bpajm" id="3JsE8aiIiCm" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiIiCn" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiIiCo" role="lcghm">
                <property role="lacIc" value="description: " />
              </node>
              <node concept="l9hG8" id="3JsE8aiIiCp" role="lcghm">
                <node concept="2OqwBi" id="3JsE8aiIiCq" role="lb14g">
                  <node concept="117lpO" id="3JsE8aiIiCr" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiIiCs" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                  </node>
                </node>
              </node>
              <node concept="l8MVK" id="3JsE8aiIiCt" role="lcghm" />
            </node>
            <node concept="3clFbJ" id="cAH7uXp3DK" role="3cqZAp">
              <node concept="3clFbS" id="cAH7uXp3DL" role="3clFbx">
                <node concept="1ZvZ2y" id="cAH7uXp3DM" role="3cqZAp">
                  <node concept="Xl_RD" id="cAH7uXp3DN" role="v0bCk">
                    <property role="Xl_RC" value="description is required" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="cAH7uXp3DO" role="3clFbw">
                <node concept="2OqwBi" id="cAH7uXp3DP" role="2Oq$k0">
                  <node concept="117lpO" id="cAH7uXp3DQ" role="2Oq$k0" />
                  <node concept="3TrcHB" id="cAH7uXp3DR" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                  </node>
                </node>
                <node concept="17RlXB" id="cAH7uXp3DS" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbH" id="cAH7uXp3$H" role="3cqZAp" />
            <node concept="3clFbJ" id="3JsE8aiIiCu" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiIiCv" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiIiCw" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiIiCx" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiIiCy" role="lcghm">
                    <property role="lacIc" value="example: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiIiCz" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiIiC$" role="lb14g">
                      <node concept="117lpO" id="3JsE8aiIiC_" role="2Oq$k0" />
                      <node concept="3TrcHB" id="3JsE8aiIiCA" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNMJ" resolve="example" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiIiCB" role="lcghm" />
                </node>
              </node>
              <node concept="2OqwBi" id="3JsE8aiIiCC" role="3clFbw">
                <node concept="2OqwBi" id="3JsE8aiIiCD" role="2Oq$k0">
                  <node concept="117lpO" id="3JsE8aiIiCE" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiIiCF" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMJ" resolve="example" />
                  </node>
                </node>
                <node concept="17RvpY" id="3JsE8aiIiCG" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbJ" id="3JsE8aiIiCW" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiIiCX" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiIiCY" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiIiCZ" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiIiD0" role="lcghm">
                    <property role="lacIc" value="minimum: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiIiD1" role="lcghm">
                    <node concept="3cpWs3" id="3JsE8aiIiD2" role="lb14g">
                      <node concept="Xl_RD" id="3JsE8aiIiD3" role="3uHU7B" />
                      <node concept="2OqwBi" id="3JsE8aiIiD4" role="3uHU7w">
                        <node concept="3TrcHB" id="3JsE8aiIiD5" role="2OqNvi">
                          <ref role="3TsBF5" to="7thy:4favA_iBNSd" resolve="minimum" />
                        </node>
                        <node concept="117lpO" id="3JsE8aiIiD6" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiIiD7" role="lcghm" />
                </node>
              </node>
              <node concept="3eOSWO" id="3JsE8aiIiD8" role="3clFbw">
                <node concept="3cmrfG" id="3JsE8aiIiD9" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="2OqwBi" id="3JsE8aiIiDa" role="3uHU7B">
                  <node concept="117lpO" id="3JsE8aiIiDb" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiIiDc" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNSd" resolve="minimum" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="3JsE8aiIiDd" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiIiDe" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiIiDf" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiIiDg" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiIiDh" role="lcghm">
                    <property role="lacIc" value="maximum: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiIiDi" role="lcghm">
                    <node concept="3cpWs3" id="3JsE8aiIiDj" role="lb14g">
                      <node concept="Xl_RD" id="3JsE8aiIiDk" role="3uHU7B" />
                      <node concept="2OqwBi" id="3JsE8aiIiDl" role="3uHU7w">
                        <node concept="3TrcHB" id="3JsE8aiIiDm" role="2OqNvi">
                          <ref role="3TsBF5" to="7thy:4favA_iBNSe" resolve="maximum" />
                        </node>
                        <node concept="117lpO" id="3JsE8aiIiDn" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiIiDo" role="lcghm" />
                </node>
              </node>
              <node concept="3eOSWO" id="3JsE8aiIiDp" role="3clFbw">
                <node concept="3cmrfG" id="3JsE8aiIiDq" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="2OqwBi" id="3JsE8aiIiDr" role="3uHU7B">
                  <node concept="117lpO" id="3JsE8aiIiDs" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiIiDt" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNSe" resolve="maximum" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="3JsE8aiIiDu" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiIiDv" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiIiDw" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiIiDx" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiIiDy" role="lcghm">
                    <property role="lacIc" value="pattern: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiIiDz" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiIiD$" role="lb14g">
                      <node concept="3TrcHB" id="3JsE8aiIiD_" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNSf" resolve="pattern" />
                      </node>
                      <node concept="117lpO" id="3JsE8aiIiDA" role="2Oq$k0" />
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiIiDB" role="lcghm" />
                </node>
              </node>
              <node concept="2OqwBi" id="3JsE8aiIiDC" role="3clFbw">
                <node concept="2OqwBi" id="3JsE8aiIiDD" role="2Oq$k0">
                  <node concept="117lpO" id="3JsE8aiIiDE" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiIiDF" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNSf" resolve="pattern" />
                  </node>
                </node>
                <node concept="17RvpY" id="3JsE8aiIiDG" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="3JsE8aiIjZR">
    <ref role="WuzLi" to="7thy:4favA_iBNSQ" resolve="ArraySchema" />
    <node concept="11bSqf" id="3JsE8aiIjZS" role="11c4hB">
      <node concept="3clFbS" id="3JsE8aiIjZT" role="2VODD2">
        <node concept="1bpajm" id="3JsE8aiIk8Q" role="3cqZAp" />
        <node concept="lc7rE" id="3JsE8aiIk8R" role="3cqZAp">
          <node concept="l9hG8" id="3JsE8aiIk8S" role="lcghm">
            <node concept="2OqwBi" id="3JsE8aiIk8T" role="lb14g">
              <node concept="117lpO" id="3JsE8aiIk8U" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JsE8aiIk8V" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="3JsE8aiIk8W" role="lcghm">
            <property role="lacIc" value=":\n" />
          </node>
        </node>
        <node concept="3izx1p" id="3JsE8aiIk8X" role="3cqZAp">
          <node concept="3clFbS" id="3JsE8aiIk8Y" role="3izTki">
            <node concept="1bpajm" id="3JsE8aiIk8Z" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiIk90" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiIk91" role="lcghm">
                <property role="lacIc" value="type: array" />
              </node>
              <node concept="l8MVK" id="3JsE8aiIk92" role="lcghm" />
            </node>
            <node concept="1bpajm" id="3JsE8aiIk93" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiIk94" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiIk95" role="lcghm">
                <property role="lacIc" value="description: " />
              </node>
              <node concept="l9hG8" id="3JsE8aiIk96" role="lcghm">
                <node concept="2OqwBi" id="3JsE8aiIk97" role="lb14g">
                  <node concept="117lpO" id="3JsE8aiIk98" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiIk99" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                  </node>
                </node>
              </node>
              <node concept="l8MVK" id="3JsE8aiIk9a" role="lcghm" />
            </node>
            <node concept="3clFbJ" id="cAH7uXp4vX" role="3cqZAp">
              <node concept="3clFbS" id="cAH7uXp4vY" role="3clFbx">
                <node concept="1ZvZ2y" id="cAH7uXp4vZ" role="3cqZAp">
                  <node concept="Xl_RD" id="cAH7uXp4w0" role="v0bCk">
                    <property role="Xl_RC" value="description is required" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="cAH7uXp4w1" role="3clFbw">
                <node concept="2OqwBi" id="cAH7uXp4w2" role="2Oq$k0">
                  <node concept="117lpO" id="cAH7uXp4w3" role="2Oq$k0" />
                  <node concept="3TrcHB" id="cAH7uXp4w4" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                  </node>
                </node>
                <node concept="17RlXB" id="cAH7uXp4w5" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbH" id="cAH7uXp4r9" role="3cqZAp" />
            <node concept="3clFbJ" id="3JsE8aiIk9b" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiIk9c" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiIk9d" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiIk9e" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiIk9f" role="lcghm">
                    <property role="lacIc" value="example: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiIk9g" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiIk9h" role="lb14g">
                      <node concept="117lpO" id="3JsE8aiIk9i" role="2Oq$k0" />
                      <node concept="3TrcHB" id="3JsE8aiIk9j" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNMJ" resolve="example" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiIk9k" role="lcghm" />
                </node>
              </node>
              <node concept="2OqwBi" id="3JsE8aiIk9l" role="3clFbw">
                <node concept="2OqwBi" id="3JsE8aiIk9m" role="2Oq$k0">
                  <node concept="117lpO" id="3JsE8aiIk9n" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiIk9o" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMJ" resolve="example" />
                  </node>
                </node>
                <node concept="17RvpY" id="3JsE8aiIk9p" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbJ" id="3JsE8aiIk9D" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiIk9E" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiIk9F" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiIk9G" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiIk9H" role="lcghm">
                    <property role="lacIc" value="minItems: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiIk9I" role="lcghm">
                    <node concept="3cpWs3" id="3JsE8aiIk9J" role="lb14g">
                      <node concept="Xl_RD" id="3JsE8aiIk9K" role="3uHU7B" />
                      <node concept="2OqwBi" id="3JsE8aiIk9L" role="3uHU7w">
                        <node concept="3TrcHB" id="3JsE8aiIk9M" role="2OqNvi">
                          <ref role="3TsBF5" to="7thy:4favA_iBNSS" resolve="minItems" />
                        </node>
                        <node concept="117lpO" id="3JsE8aiIk9N" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiIk9O" role="lcghm" />
                </node>
              </node>
              <node concept="3eOSWO" id="3JsE8aiIk9P" role="3clFbw">
                <node concept="3cmrfG" id="3JsE8aiIk9Q" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="2OqwBi" id="3JsE8aiIk9R" role="3uHU7B">
                  <node concept="117lpO" id="3JsE8aiIk9S" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiIk9T" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNSS" resolve="minItems" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="3JsE8aiIk9U" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiIk9V" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiIk9W" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiIk9X" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiIk9Y" role="lcghm">
                    <property role="lacIc" value="maxItems: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiIk9Z" role="lcghm">
                    <node concept="3cpWs3" id="3JsE8aiIka0" role="lb14g">
                      <node concept="Xl_RD" id="3JsE8aiIka1" role="3uHU7B" />
                      <node concept="2OqwBi" id="3JsE8aiIka2" role="3uHU7w">
                        <node concept="3TrcHB" id="3JsE8aiIka3" role="2OqNvi">
                          <ref role="3TsBF5" to="7thy:4favA_iBNSU" resolve="maxItems" />
                        </node>
                        <node concept="117lpO" id="3JsE8aiIka4" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiIka5" role="lcghm" />
                </node>
              </node>
              <node concept="3eOSWO" id="3JsE8aiIka6" role="3clFbw">
                <node concept="3cmrfG" id="3JsE8aiIka7" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="2OqwBi" id="3JsE8aiIka8" role="3uHU7B">
                  <node concept="117lpO" id="3JsE8aiIka9" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiIkaa" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNSU" resolve="maxItems" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="1bpajm" id="3JsE8aiISan" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiISgJ" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiISjZ" role="lcghm">
                <property role="lacIc" value="items:\n" />
              </node>
            </node>
            <node concept="3clFbJ" id="cAH7uXqdiE" role="3cqZAp">
              <node concept="3clFbS" id="cAH7uXqdiF" role="3clFbx">
                <node concept="1ZvZ2y" id="cAH7uXqdiG" role="3cqZAp">
                  <node concept="Xl_RD" id="cAH7uXqdiH" role="v0bCk">
                    <property role="Xl_RC" value="items schema name may not be empty" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="cAH7uXqdiI" role="3clFbw">
                <node concept="2OqwBi" id="cAH7uXqeh1" role="2Oq$k0">
                  <node concept="2OqwBi" id="cAH7uXqdME" role="2Oq$k0">
                    <node concept="117lpO" id="cAH7uXqdGg" role="2Oq$k0" />
                    <node concept="3TrEf2" id="cAH7uXqdXw" role="2OqNvi">
                      <ref role="3Tt5mk" to="7thy:4favA_iBNT3" resolve="items" />
                    </node>
                  </node>
                  <node concept="3TrcHB" id="cAH7uXqesb" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                  </node>
                </node>
                <node concept="17RlXB" id="cAH7uXqdiO" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbH" id="cAH7uXqddG" role="3cqZAp" />
            <node concept="3izx1p" id="3JsE8aiISo8" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiISoa" role="3izTki">
                <node concept="1bpajm" id="3JsE8aiISAH" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiISBc" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiISBw" role="lcghm">
                    <property role="lacIc" value="$ref: " />
                  </node>
                  <node concept="la8eA" id="3JsE8aiISCZ" role="lcghm">
                    <property role="lacIc" value="#/components/schemas/" />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiISFX" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiITke" role="lb14g">
                      <node concept="2OqwBi" id="3JsE8aiISO7" role="2Oq$k0">
                        <node concept="117lpO" id="3JsE8aiISGL" role="2Oq$k0" />
                        <node concept="3TrEf2" id="3JsE8aiIT6i" role="2OqNvi">
                          <ref role="3Tt5mk" to="7thy:4favA_iBNT3" resolve="items" />
                        </node>
                      </node>
                      <node concept="3TrcHB" id="3JsE8aiITCk" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="3JsE8aiIZhU">
    <ref role="WuzLi" to="7thy:4favA_iBNSC" resolve="ObjectSchema" />
    <node concept="11bSqf" id="3JsE8aiIZhV" role="11c4hB">
      <node concept="3clFbS" id="3JsE8aiIZhW" role="2VODD2">
        <node concept="1bpajm" id="3JsE8aiIZ$k" role="3cqZAp" />
        <node concept="lc7rE" id="3JsE8aiIZ$l" role="3cqZAp">
          <node concept="l9hG8" id="3JsE8aiIZ$m" role="lcghm">
            <node concept="2OqwBi" id="3JsE8aiIZ$n" role="lb14g">
              <node concept="117lpO" id="3JsE8aiIZ$o" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JsE8aiIZ$p" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="3JsE8aiIZ$q" role="lcghm">
            <property role="lacIc" value=":\n" />
          </node>
        </node>
        <node concept="3izx1p" id="3JsE8aiIZ$r" role="3cqZAp">
          <node concept="3clFbS" id="3JsE8aiIZ$s" role="3izTki">
            <node concept="1bpajm" id="3JsE8aiIZ$t" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiIZ$u" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiIZ$v" role="lcghm">
                <property role="lacIc" value="type: object" />
              </node>
              <node concept="l8MVK" id="3JsE8aiIZ$w" role="lcghm" />
            </node>
            <node concept="1bpajm" id="3JsE8aiIZ$x" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiIZ$y" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiIZ$z" role="lcghm">
                <property role="lacIc" value="description: " />
              </node>
              <node concept="l9hG8" id="3JsE8aiIZ$$" role="lcghm">
                <node concept="2OqwBi" id="3JsE8aiIZ$_" role="lb14g">
                  <node concept="117lpO" id="3JsE8aiIZ$A" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiIZ$B" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                  </node>
                </node>
              </node>
              <node concept="l8MVK" id="3JsE8aiIZ$C" role="lcghm" />
            </node>
            <node concept="3clFbJ" id="cAH7uXp0Ko" role="3cqZAp">
              <node concept="3clFbS" id="cAH7uXp0Kq" role="3clFbx">
                <node concept="1ZvZ2y" id="cAH7uXp25h" role="3cqZAp">
                  <node concept="Xl_RD" id="cAH7uXp25i" role="v0bCk">
                    <property role="Xl_RC" value="description is required" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="cAH7uXp1$6" role="3clFbw">
                <node concept="2OqwBi" id="cAH7uXp0Yi" role="2Oq$k0">
                  <node concept="117lpO" id="cAH7uXp0Qg" role="2Oq$k0" />
                  <node concept="3TrcHB" id="cAH7uXp1h5" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                  </node>
                </node>
                <node concept="17RlXB" id="cAH7uXp24H" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbJ" id="3JsE8aiIZ$D" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiIZ$E" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiIZ$F" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiIZ$G" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiIZ$H" role="lcghm">
                    <property role="lacIc" value="example: " />
                  </node>
                  <node concept="l9hG8" id="3JsE8aiIZ$I" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiIZ$J" role="lb14g">
                      <node concept="117lpO" id="3JsE8aiIZ$K" role="2Oq$k0" />
                      <node concept="3TrcHB" id="3JsE8aiIZ$L" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNMJ" resolve="example" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="3JsE8aiIZ$M" role="lcghm" />
                </node>
              </node>
              <node concept="2OqwBi" id="3JsE8aiIZ$N" role="3clFbw">
                <node concept="2OqwBi" id="3JsE8aiIZ$O" role="2Oq$k0">
                  <node concept="117lpO" id="3JsE8aiIZ$P" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3JsE8aiIZ$Q" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMJ" resolve="example" />
                  </node>
                </node>
                <node concept="17RvpY" id="3JsE8aiIZ$R" role="2OqNvi" />
              </node>
            </node>
            <node concept="1bpajm" id="3JsE8aiIZY2" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiJ00E" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiJ022" role="lcghm">
                <property role="lacIc" value="properties:\n" />
              </node>
            </node>
            <node concept="3izx1p" id="3JsE8aiJ050" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiJ052" role="3izTki">
                <node concept="lc7rE" id="3JsE8aiJ06l" role="3cqZAp">
                  <node concept="l9S2W" id="3JsE8aiJ06B" role="lcghm">
                    <node concept="2OqwBi" id="3JsE8aiJ0d9" role="lbANJ">
                      <node concept="117lpO" id="3JsE8aiJ06V" role="2Oq$k0" />
                      <node concept="3Tsc0h" id="3JsE8aiJ0vj" role="2OqNvi">
                        <ref role="3TtcxE" to="7thy:4favA_iBNSJ" resolve="properties" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="3JsE8aiJ0$w" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiJ0$y" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiJwUI" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiJ1a2" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiJ1am" role="lcghm">
                    <property role="lacIc" value="required:\n" />
                  </node>
                </node>
                <node concept="3izx1p" id="3JsE8aiJ1c4" role="3cqZAp">
                  <node concept="3clFbS" id="3JsE8aiJ1c6" role="3izTki">
                    <node concept="2Gpval" id="3JsE8aiJ1cl" role="3cqZAp">
                      <node concept="2GrKxI" id="3JsE8aiJ1cm" role="2Gsz3X">
                        <property role="TrG5h" value="prop" />
                      </node>
                      <node concept="2OqwBi" id="3JsE8aiJ1lu" role="2GsD0m">
                        <node concept="117lpO" id="3JsE8aiJ1dg" role="2Oq$k0" />
                        <node concept="3Tsc0h" id="3JsE8aiJ6au" role="2OqNvi">
                          <ref role="3TtcxE" to="7thy:3JsE8aitKU3" resolve="required" />
                        </node>
                      </node>
                      <node concept="3clFbS" id="3JsE8aiJ1co" role="2LFqv$">
                        <node concept="1bpajm" id="3JsE8aiJ6bI" role="3cqZAp" />
                        <node concept="lc7rE" id="3JsE8aiJ6be" role="3cqZAp">
                          <node concept="la8eA" id="3JsE8aiJ6c0" role="lcghm">
                            <property role="lacIc" value="- " />
                          </node>
                          <node concept="l9hG8" id="3JsE8aiJ6cM" role="lcghm">
                            <node concept="2OqwBi" id="3JsE8aiJ7qS" role="lb14g">
                              <node concept="2OqwBi" id="3JsE8aiJ6Hg" role="2Oq$k0">
                                <node concept="2GrUjf" id="3JsE8aiJ6dA" role="2Oq$k0">
                                  <ref role="2Gs0qQ" node="3JsE8aiJ1cm" resolve="prop" />
                                </node>
                                <node concept="3TrEf2" id="3JsE8aiJ7df" role="2OqNvi">
                                  <ref role="3Tt5mk" to="7thy:3JsE8aiu$43" resolve="property" />
                                </node>
                              </node>
                              <node concept="3TrcHB" id="3JsE8aiJ7Hu" role="2OqNvi">
                                <ref role="3TsBF5" to="7thy:4favA_iBNSM" resolve="name" />
                              </node>
                            </node>
                          </node>
                          <node concept="l8MVK" id="3JsE8aiJpUQ" role="lcghm" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="3JsE8aiJ0LK" role="3clFbw">
                <node concept="2OqwBi" id="3JsE8aiJ0C8" role="2Oq$k0">
                  <node concept="117lpO" id="3JsE8aiJ0A4" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="3JsE8aiJ1RF" role="2OqNvi">
                    <ref role="3TtcxE" to="7thy:3JsE8aitKU3" resolve="required" />
                  </node>
                </node>
                <node concept="3GX2aA" id="3JsE8aiJ65r" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbJ" id="3JsE8aiJ7N3" role="3cqZAp">
              <node concept="3clFbS" id="3JsE8aiJ7N5" role="3clFbx">
                <node concept="1bpajm" id="3JsE8aiJwJe" role="3cqZAp" />
                <node concept="lc7rE" id="3JsE8aiJ916" role="3cqZAp">
                  <node concept="la8eA" id="3JsE8aiJ91q" role="lcghm">
                    <property role="lacIc" value="additionalProperties:\n" />
                  </node>
                </node>
                <node concept="3izx1p" id="3JsE8aiJ93z" role="3cqZAp">
                  <node concept="3clFbS" id="3JsE8aiJ93_" role="3izTki">
                    <node concept="1bpajm" id="3JsE8aiJayb" role="3cqZAp" />
                    <node concept="lc7rE" id="3JsE8aiJ93O" role="3cqZAp">
                      <node concept="la8eA" id="3JsE8aiJava" role="lcghm">
                        <property role="lacIc" value="type: " />
                      </node>
                      <node concept="l9hG8" id="3JsE8aiJ94_" role="lcghm">
                        <node concept="2OqwBi" id="3JsE8aiJab8" role="lb14g">
                          <node concept="2OqwBi" id="3JsE8aiJ9FF" role="2Oq$k0">
                            <node concept="2OqwBi" id="3JsE8aiJ9cH" role="2Oq$k0">
                              <node concept="117lpO" id="3JsE8aiJ95n" role="2Oq$k0" />
                              <node concept="3TrEf2" id="3JsE8aiJ9vr" role="2OqNvi">
                                <ref role="3Tt5mk" to="7thy:3JsE8ainS_0" resolve="additionalProperties" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="3JsE8aiJ9XO" role="2OqNvi">
                              <ref role="3TsBF5" to="7thy:3JsE8ainS_4" resolve="type" />
                            </node>
                          </node>
                          <node concept="liA8E" id="3JsE8aiJar9" role="2OqNvi">
                            <ref role="37wK5l" to="wyt6:~Object.toString()" resolve="toString" />
                          </node>
                        </node>
                      </node>
                      <node concept="l8MVK" id="3JsE8aiJpWm" role="lcghm" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="3JsE8aiJ8EL" role="3clFbw">
                <node concept="2OqwBi" id="3JsE8aiJ8dP" role="2Oq$k0">
                  <node concept="117lpO" id="3JsE8aiJ85N" role="2Oq$k0" />
                  <node concept="3TrEf2" id="3JsE8aiJ8yJ" role="2OqNvi">
                    <ref role="3Tt5mk" to="7thy:3JsE8ainS_0" resolve="additionalProperties" />
                  </node>
                </node>
                <node concept="3x8VRR" id="3JsE8aiJ8X2" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="3JsE8aiJh1g">
    <ref role="WuzLi" to="7thy:4favA_iBNSL" resolve="ObjectSchemaProperty" />
    <node concept="11bSqf" id="3JsE8aiJh1h" role="11c4hB">
      <node concept="3clFbS" id="3JsE8aiJh1i" role="2VODD2">
        <node concept="1bpajm" id="3JsE8aiJh1z" role="3cqZAp" />
        <node concept="lc7rE" id="3JsE8aiJh22" role="3cqZAp">
          <node concept="l9hG8" id="3JsE8aiJh2m" role="lcghm">
            <node concept="2OqwBi" id="3JsE8aiJh9Y" role="lb14g">
              <node concept="117lpO" id="3JsE8aiJh3a" role="2Oq$k0" />
              <node concept="3TrcHB" id="3JsE8aiJhpe" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNSM" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="3JsE8aiJhsm" role="lcghm">
            <property role="lacIc" value=":" />
          </node>
          <node concept="l8MVK" id="3JsE8aiJiiv" role="lcghm" />
        </node>
        <node concept="3clFbJ" id="cAH7uXqeMN" role="3cqZAp">
          <node concept="3clFbS" id="cAH7uXqeMO" role="3clFbx">
            <node concept="1ZvZ2y" id="cAH7uXqeMP" role="3cqZAp">
              <node concept="Xl_RD" id="cAH7uXqeMQ" role="v0bCk">
                <property role="Xl_RC" value="property schema name may not be empty" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="cAH7uXqeMR" role="3clFbw">
            <node concept="2OqwBi" id="cAH7uXqeMS" role="2Oq$k0">
              <node concept="2OqwBi" id="cAH7uXqeMT" role="2Oq$k0">
                <node concept="117lpO" id="cAH7uXqeMU" role="2Oq$k0" />
                <node concept="3TrEf2" id="cAH7uXqeMV" role="2OqNvi">
                  <ref role="3Tt5mk" to="7thy:4favA_iBNSO" resolve="schema" />
                </node>
              </node>
              <node concept="3TrcHB" id="cAH7uXqeMW" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
              </node>
            </node>
            <node concept="17RlXB" id="cAH7uXqeMX" role="2OqNvi" />
          </node>
        </node>
        <node concept="3clFbH" id="cAH7uXqeMq" role="3cqZAp" />
        <node concept="3izx1p" id="3JsE8aiJilj" role="3cqZAp">
          <node concept="3clFbS" id="3JsE8aiJill" role="3izTki">
            <node concept="1bpajm" id="3JsE8aiJwBR" role="3cqZAp" />
            <node concept="lc7rE" id="3JsE8aiJiom" role="3cqZAp">
              <node concept="la8eA" id="3JsE8aiJioC" role="lcghm">
                <property role="lacIc" value="$ref: " />
              </node>
              <node concept="la8eA" id="3JsE8aiJipS" role="lcghm">
                <property role="lacIc" value="'#/components/schemas/" />
              </node>
              <node concept="l9hG8" id="3JsE8aiJiqT" role="lcghm">
                <node concept="2OqwBi" id="3JsE8aiJiTM" role="lb14g">
                  <node concept="2OqwBi" id="3JsE8aiJiyx" role="2Oq$k0">
                    <node concept="117lpO" id="3JsE8aiJirH" role="2Oq$k0" />
                    <node concept="3TrEf2" id="3JsE8aiJiMk" role="2OqNvi">
                      <ref role="3Tt5mk" to="7thy:4favA_iBNSO" resolve="schema" />
                    </node>
                  </node>
                  <node concept="3TrcHB" id="3JsE8aiJjbV" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                  </node>
                </node>
              </node>
              <node concept="la8eA" id="7kDFC_uao3S" role="lcghm">
                <property role="lacIc" value="'" />
              </node>
              <node concept="l8MVK" id="3JsE8aiJpES" role="lcghm" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="3JsE8aiJBwW">
    <ref role="WuzLi" to="7thy:3JsE8aiB2q4" resolve="AnyOfSchema" />
    <node concept="11bSqf" id="3JsE8aiJBwX" role="11c4hB">
      <node concept="3clFbS" id="3JsE8aiJBwY" role="2VODD2">
        <node concept="1bpajm" id="7kDFC_u9uuH" role="3cqZAp" />
        <node concept="lc7rE" id="7kDFC_u9uuI" role="3cqZAp">
          <node concept="l9hG8" id="7kDFC_u9uuJ" role="lcghm">
            <node concept="2OqwBi" id="7kDFC_u9uuK" role="lb14g">
              <node concept="117lpO" id="7kDFC_u9uuL" role="2Oq$k0" />
              <node concept="3TrcHB" id="7kDFC_u9uuM" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="7kDFC_u9uuN" role="lcghm">
            <property role="lacIc" value=":" />
          </node>
          <node concept="l8MVK" id="7kDFC_u9uuO" role="lcghm" />
        </node>
        <node concept="3izx1p" id="7kDFC_u9uuP" role="3cqZAp">
          <node concept="3clFbS" id="7kDFC_u9uuQ" role="3izTki">
            <node concept="3clFbJ" id="7kDFC_u9uuR" role="3cqZAp">
              <node concept="3clFbS" id="7kDFC_u9uuS" role="3clFbx">
                <node concept="1bpajm" id="7kDFC_u9uuT" role="3cqZAp" />
                <node concept="lc7rE" id="7kDFC_u9uuU" role="3cqZAp">
                  <node concept="la8eA" id="7kDFC_u9uuV" role="lcghm">
                    <property role="lacIc" value="description: " />
                  </node>
                  <node concept="l9hG8" id="7kDFC_u9uuW" role="lcghm">
                    <node concept="2OqwBi" id="7kDFC_u9uuX" role="lb14g">
                      <node concept="117lpO" id="7kDFC_u9uuY" role="2Oq$k0" />
                      <node concept="3TrcHB" id="7kDFC_u9uuZ" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="7kDFC_udmg_" role="lcghm" />
                </node>
              </node>
              <node concept="2OqwBi" id="7kDFC_u9uv0" role="3clFbw">
                <node concept="2OqwBi" id="7kDFC_u9uv1" role="2Oq$k0">
                  <node concept="117lpO" id="7kDFC_u9uv2" role="2Oq$k0" />
                  <node concept="3TrcHB" id="7kDFC_u9uv3" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                  </node>
                </node>
                <node concept="17RvpY" id="7kDFC_u9uv4" role="2OqNvi" />
              </node>
            </node>
            <node concept="1bpajm" id="7kDFC_u9uv7" role="3cqZAp" />
            <node concept="lc7rE" id="7kDFC_u9uv8" role="3cqZAp">
              <node concept="la8eA" id="7kDFC_u9uv9" role="lcghm">
                <property role="lacIc" value="anyOf:" />
              </node>
              <node concept="l8MVK" id="7kDFC_u9uva" role="lcghm" />
            </node>
            <node concept="2Gpval" id="7kDFC_u9uvb" role="3cqZAp">
              <node concept="2GrKxI" id="7kDFC_u9uvc" role="2Gsz3X">
                <property role="TrG5h" value="elem" />
              </node>
              <node concept="2OqwBi" id="7kDFC_u9uvd" role="2GsD0m">
                <node concept="117lpO" id="7kDFC_u9uve" role="2Oq$k0" />
                <node concept="3Tsc0h" id="7kDFC_u9uvf" role="2OqNvi">
                  <ref role="3TtcxE" to="7thy:3JsE8aiB2q5" resolve="anyOf" />
                </node>
              </node>
              <node concept="3clFbS" id="7kDFC_u9uvg" role="2LFqv$">
                <node concept="3clFbJ" id="cAH7uXqcUQ" role="3cqZAp">
                  <node concept="3clFbS" id="cAH7uXqcUR" role="3clFbx">
                    <node concept="1ZvZ2y" id="cAH7uXqcUS" role="3cqZAp">
                      <node concept="Xl_RD" id="cAH7uXqcUT" role="v0bCk">
                        <property role="Xl_RC" value="schema name may not be empty" />
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="cAH7uXqcUU" role="3clFbw">
                    <node concept="2OqwBi" id="cAH7uXqcUV" role="2Oq$k0">
                      <node concept="2OqwBi" id="cAH7uXqcUW" role="2Oq$k0">
                        <node concept="2GrUjf" id="cAH7uXqcUX" role="2Oq$k0">
                          <ref role="2Gs0qQ" node="7kDFC_u9uvc" resolve="elem" />
                        </node>
                        <node concept="3TrEf2" id="cAH7uXqcUY" role="2OqNvi">
                          <ref role="3Tt5mk" to="7thy:3JsE8aiBPe0" resolve="schema" />
                        </node>
                      </node>
                      <node concept="3TrcHB" id="cAH7uXqcUZ" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                      </node>
                    </node>
                    <node concept="17RlXB" id="cAH7uXqcV0" role="2OqNvi" />
                  </node>
                </node>
                <node concept="3clFbH" id="cAH7uXqcUp" role="3cqZAp" />
                <node concept="3izx1p" id="7kDFC_u9uvh" role="3cqZAp">
                  <node concept="3clFbS" id="7kDFC_u9uvi" role="3izTki">
                    <node concept="1bpajm" id="7kDFC_u9uvj" role="3cqZAp" />
                    <node concept="lc7rE" id="7kDFC_u9uvk" role="3cqZAp">
                      <node concept="la8eA" id="7kDFC_u9uvl" role="lcghm">
                        <property role="lacIc" value="- $ref: " />
                      </node>
                      <node concept="la8eA" id="7kDFC_u9uvm" role="lcghm">
                        <property role="lacIc" value="'#/components/schemas/" />
                      </node>
                      <node concept="l9hG8" id="7kDFC_u9uvn" role="lcghm">
                        <node concept="2OqwBi" id="7kDFC_u9uvo" role="lb14g">
                          <node concept="2OqwBi" id="7kDFC_u9uvp" role="2Oq$k0">
                            <node concept="2GrUjf" id="7kDFC_u9uvq" role="2Oq$k0">
                              <ref role="2Gs0qQ" node="7kDFC_u9uvc" resolve="elem" />
                            </node>
                            <node concept="3TrEf2" id="7kDFC_u9uvr" role="2OqNvi">
                              <ref role="3Tt5mk" to="7thy:3JsE8aiBPe0" resolve="schema" />
                            </node>
                          </node>
                          <node concept="3TrcHB" id="7kDFC_u9uvs" role="2OqNvi">
                            <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                          </node>
                        </node>
                      </node>
                      <node concept="la8eA" id="7kDFC_uanVB" role="lcghm">
                        <property role="lacIc" value="'" />
                      </node>
                      <node concept="l8MVK" id="7kDFC_udmhK" role="lcghm" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="cAH7uXoE04" role="3cqZAp">
              <node concept="3clFbS" id="cAH7uXoE05" role="3clFbx">
                <node concept="1ZvZ2y" id="cAH7uXoE06" role="3cqZAp">
                  <node concept="Xl_RD" id="cAH7uXoE07" role="v0bCk">
                    <property role="Xl_RC" value="anyOf may not be empty" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="cAH7uXoE08" role="3clFbw">
                <node concept="2OqwBi" id="cAH7uXoE09" role="2Oq$k0">
                  <node concept="117lpO" id="cAH7uXoE0a" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="cAH7uXoE0b" role="2OqNvi">
                    <ref role="3TtcxE" to="7thy:3JsE8aiB2q5" resolve="anyOf" />
                  </node>
                </node>
                <node concept="1v1jN8" id="cAH7uXoE0c" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="7kDFC_u8Vrl">
    <ref role="WuzLi" to="7thy:3JsE8aiB2oY" resolve="OneOfSchema" />
    <node concept="11bSqf" id="7kDFC_u8Vrm" role="11c4hB">
      <node concept="3clFbS" id="7kDFC_u8Vrn" role="2VODD2">
        <node concept="1bpajm" id="7kDFC_u9v06" role="3cqZAp" />
        <node concept="lc7rE" id="7kDFC_u9v07" role="3cqZAp">
          <node concept="l9hG8" id="7kDFC_u9v08" role="lcghm">
            <node concept="2OqwBi" id="7kDFC_u9v09" role="lb14g">
              <node concept="117lpO" id="7kDFC_u9v0a" role="2Oq$k0" />
              <node concept="3TrcHB" id="7kDFC_u9v0b" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="7kDFC_u9v0c" role="lcghm">
            <property role="lacIc" value=":" />
          </node>
          <node concept="l8MVK" id="7kDFC_u9v0d" role="lcghm" />
        </node>
        <node concept="3izx1p" id="7kDFC_u9v0e" role="3cqZAp">
          <node concept="3clFbS" id="7kDFC_u9v0f" role="3izTki">
            <node concept="3clFbJ" id="7kDFC_u9v0g" role="3cqZAp">
              <node concept="3clFbS" id="7kDFC_u9v0h" role="3clFbx">
                <node concept="1bpajm" id="7kDFC_u9v0i" role="3cqZAp" />
                <node concept="lc7rE" id="7kDFC_u9v0j" role="3cqZAp">
                  <node concept="la8eA" id="7kDFC_u9v0k" role="lcghm">
                    <property role="lacIc" value="description: " />
                  </node>
                  <node concept="l9hG8" id="7kDFC_u9v0l" role="lcghm">
                    <node concept="2OqwBi" id="7kDFC_u9v0m" role="lb14g">
                      <node concept="117lpO" id="7kDFC_u9v0n" role="2Oq$k0" />
                      <node concept="3TrcHB" id="7kDFC_u9v0o" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="7kDFC_udm9q" role="lcghm" />
                </node>
              </node>
              <node concept="2OqwBi" id="7kDFC_u9v0p" role="3clFbw">
                <node concept="2OqwBi" id="7kDFC_u9v0q" role="2Oq$k0">
                  <node concept="117lpO" id="7kDFC_u9v0r" role="2Oq$k0" />
                  <node concept="3TrcHB" id="7kDFC_u9v0s" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                  </node>
                </node>
                <node concept="17RvpY" id="7kDFC_u9v0t" role="2OqNvi" />
              </node>
            </node>
            <node concept="1bpajm" id="7kDFC_u9v0w" role="3cqZAp" />
            <node concept="lc7rE" id="7kDFC_u9v0x" role="3cqZAp">
              <node concept="la8eA" id="7kDFC_u9v0y" role="lcghm">
                <property role="lacIc" value="oneOf:" />
              </node>
              <node concept="l8MVK" id="7kDFC_u9v0z" role="lcghm" />
            </node>
            <node concept="2Gpval" id="7kDFC_u9v0$" role="3cqZAp">
              <node concept="2GrKxI" id="7kDFC_u9v0_" role="2Gsz3X">
                <property role="TrG5h" value="elem" />
              </node>
              <node concept="2OqwBi" id="7kDFC_u9v0A" role="2GsD0m">
                <node concept="117lpO" id="7kDFC_u9v0B" role="2Oq$k0" />
                <node concept="3Tsc0h" id="7kDFC_u9v0C" role="2OqNvi">
                  <ref role="3TtcxE" to="7thy:3JsE8aiB2oZ" resolve="oneOf" />
                </node>
              </node>
              <node concept="3clFbS" id="7kDFC_u9v0D" role="2LFqv$">
                <node concept="3clFbJ" id="cAH7uXqbwB" role="3cqZAp">
                  <node concept="3clFbS" id="cAH7uXqbwC" role="3clFbx">
                    <node concept="1ZvZ2y" id="cAH7uXqbwD" role="3cqZAp">
                      <node concept="Xl_RD" id="cAH7uXqbwE" role="v0bCk">
                        <property role="Xl_RC" value="schema name may not be empty" />
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="cAH7uXqbwF" role="3clFbw">
                    <node concept="2OqwBi" id="cAH7uXqci0" role="2Oq$k0">
                      <node concept="2OqwBi" id="cAH7uXqbLH" role="2Oq$k0">
                        <node concept="2GrUjf" id="cAH7uXqbIY" role="2Oq$k0">
                          <ref role="2Gs0qQ" node="7kDFC_u9v0_" resolve="elem" />
                        </node>
                        <node concept="3TrEf2" id="cAH7uXqc5S" role="2OqNvi">
                          <ref role="3Tt5mk" to="7thy:3JsE8aiBPe0" resolve="schema" />
                        </node>
                      </node>
                      <node concept="3TrcHB" id="cAH7uXqcEp" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                      </node>
                    </node>
                    <node concept="17RlXB" id="cAH7uXqbwL" role="2OqNvi" />
                  </node>
                </node>
                <node concept="3clFbH" id="cAH7uXqbwa" role="3cqZAp" />
                <node concept="3izx1p" id="7kDFC_u9v0E" role="3cqZAp">
                  <node concept="3clFbS" id="7kDFC_u9v0F" role="3izTki">
                    <node concept="1bpajm" id="7kDFC_u9v0G" role="3cqZAp" />
                    <node concept="lc7rE" id="7kDFC_u9v0H" role="3cqZAp">
                      <node concept="la8eA" id="7kDFC_u9v0I" role="lcghm">
                        <property role="lacIc" value="- $ref: " />
                      </node>
                      <node concept="la8eA" id="7kDFC_u9v0J" role="lcghm">
                        <property role="lacIc" value="'#/components/schemas/" />
                      </node>
                      <node concept="l9hG8" id="7kDFC_u9v0K" role="lcghm">
                        <node concept="2OqwBi" id="7kDFC_u9v0L" role="lb14g">
                          <node concept="2OqwBi" id="7kDFC_u9v0M" role="2Oq$k0">
                            <node concept="2GrUjf" id="7kDFC_u9v0N" role="2Oq$k0">
                              <ref role="2Gs0qQ" node="7kDFC_u9v0_" resolve="elem" />
                            </node>
                            <node concept="3TrEf2" id="7kDFC_u9v0O" role="2OqNvi">
                              <ref role="3Tt5mk" to="7thy:3JsE8aiBPe0" resolve="schema" />
                            </node>
                          </node>
                          <node concept="3TrcHB" id="7kDFC_u9v0P" role="2OqNvi">
                            <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                          </node>
                        </node>
                      </node>
                      <node concept="la8eA" id="7kDFC_uanWU" role="lcghm">
                        <property role="lacIc" value="'" />
                      </node>
                      <node concept="l8MVK" id="7kDFC_udma_" role="lcghm" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="cAH7uXoEDI" role="3cqZAp">
              <node concept="3clFbS" id="cAH7uXoEDJ" role="3clFbx">
                <node concept="1ZvZ2y" id="cAH7uXoEDK" role="3cqZAp">
                  <node concept="Xl_RD" id="cAH7uXoEDL" role="v0bCk">
                    <property role="Xl_RC" value="oneOf may not be empty" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="cAH7uXoEDM" role="3clFbw">
                <node concept="2OqwBi" id="cAH7uXoEDN" role="2Oq$k0">
                  <node concept="117lpO" id="cAH7uXoEDO" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="cAH7uXoEDP" role="2OqNvi">
                    <ref role="3TtcxE" to="7thy:3JsE8aiB2oZ" resolve="oneOf" />
                  </node>
                </node>
                <node concept="1v1jN8" id="cAH7uXoEDQ" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="7kDFC_u8VzK">
    <ref role="WuzLi" to="7thy:3JsE8aiB2pG" resolve="AllOfSchema" />
    <node concept="11bSqf" id="7kDFC_u8VzL" role="11c4hB">
      <node concept="3clFbS" id="7kDFC_u8VzM" role="2VODD2">
        <node concept="1bpajm" id="7kDFC_u8VzN" role="3cqZAp" />
        <node concept="lc7rE" id="7kDFC_u9aKw" role="3cqZAp">
          <node concept="l9hG8" id="7kDFC_u9aM6" role="lcghm">
            <node concept="2OqwBi" id="7kDFC_u9aTA" role="lb14g">
              <node concept="117lpO" id="7kDFC_u9aMW" role="2Oq$k0" />
              <node concept="3TrcHB" id="7kDFC_u9bbj" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="7kDFC_u9beD" role="lcghm">
            <property role="lacIc" value=":" />
          </node>
          <node concept="l8MVK" id="7kDFC_u9bgm" role="lcghm" />
        </node>
        <node concept="3izx1p" id="7kDFC_u9dXa" role="3cqZAp">
          <node concept="3clFbS" id="7kDFC_u9dXc" role="3izTki">
            <node concept="3clFbJ" id="7kDFC_u9crC" role="3cqZAp">
              <node concept="3clFbS" id="7kDFC_u9crE" role="3clFbx">
                <node concept="1bpajm" id="7kDFC_u9b$d" role="3cqZAp" />
                <node concept="lc7rE" id="7kDFC_u9bCi" role="3cqZAp">
                  <node concept="la8eA" id="7kDFC_u9dMJ" role="lcghm">
                    <property role="lacIc" value="description: " />
                  </node>
                  <node concept="l9hG8" id="7kDFC_u9bEq" role="lcghm">
                    <node concept="2OqwBi" id="7kDFC_u9bLU" role="lb14g">
                      <node concept="117lpO" id="7kDFC_u9bFg" role="2Oq$k0" />
                      <node concept="3TrcHB" id="7kDFC_u9ciR" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                      </node>
                    </node>
                  </node>
                  <node concept="l8MVK" id="7kDFC_udmpH" role="lcghm" />
                </node>
              </node>
              <node concept="2OqwBi" id="7kDFC_u9d2V" role="3clFbw">
                <node concept="2OqwBi" id="7kDFC_u9cAm" role="2Oq$k0">
                  <node concept="117lpO" id="7kDFC_u9cuj" role="2Oq$k0" />
                  <node concept="3TrcHB" id="7kDFC_u9cJU" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                  </node>
                </node>
                <node concept="17RvpY" id="7kDFC_u9dyI" role="2OqNvi" />
              </node>
            </node>
            <node concept="1bpajm" id="7kDFC_u9mmp" role="3cqZAp" />
            <node concept="lc7rE" id="7kDFC_u8VzO" role="3cqZAp">
              <node concept="la8eA" id="7kDFC_u8VzP" role="lcghm">
                <property role="lacIc" value="allOf:" />
              </node>
              <node concept="l8MVK" id="7kDFC_u8VzQ" role="lcghm" />
            </node>
            <node concept="2Gpval" id="7kDFC_u8VzR" role="3cqZAp">
              <node concept="2GrKxI" id="7kDFC_u8VzS" role="2Gsz3X">
                <property role="TrG5h" value="elem" />
              </node>
              <node concept="2OqwBi" id="7kDFC_u8VzT" role="2GsD0m">
                <node concept="117lpO" id="7kDFC_u8VzU" role="2Oq$k0" />
                <node concept="3Tsc0h" id="7kDFC_u8VzV" role="2OqNvi">
                  <ref role="3TtcxE" to="7thy:3JsE8aiB2pH" resolve="allOf" />
                </node>
              </node>
              <node concept="3clFbS" id="7kDFC_u8VzW" role="2LFqv$">
                <node concept="3clFbJ" id="cAH7uXqcLg" role="3cqZAp">
                  <node concept="3clFbS" id="cAH7uXqcLh" role="3clFbx">
                    <node concept="1ZvZ2y" id="cAH7uXqcLi" role="3cqZAp">
                      <node concept="Xl_RD" id="cAH7uXqcLj" role="v0bCk">
                        <property role="Xl_RC" value="schema name may not be empty" />
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="cAH7uXqcLk" role="3clFbw">
                    <node concept="2OqwBi" id="cAH7uXqcLl" role="2Oq$k0">
                      <node concept="2OqwBi" id="cAH7uXqcLm" role="2Oq$k0">
                        <node concept="2GrUjf" id="cAH7uXqcLn" role="2Oq$k0">
                          <ref role="2Gs0qQ" node="7kDFC_u8VzS" resolve="elem" />
                        </node>
                        <node concept="3TrEf2" id="cAH7uXqcLo" role="2OqNvi">
                          <ref role="3Tt5mk" to="7thy:3JsE8aiBPe0" resolve="schema" />
                        </node>
                      </node>
                      <node concept="3TrcHB" id="cAH7uXqcLp" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                      </node>
                    </node>
                    <node concept="17RlXB" id="cAH7uXqcLq" role="2OqNvi" />
                  </node>
                </node>
                <node concept="3clFbH" id="cAH7uXqcKN" role="3cqZAp" />
                <node concept="3izx1p" id="7kDFC_u8VzX" role="3cqZAp">
                  <node concept="3clFbS" id="7kDFC_u8VzY" role="3izTki">
                    <node concept="1bpajm" id="7kDFC_u8VzZ" role="3cqZAp" />
                    <node concept="lc7rE" id="7kDFC_u8V$0" role="3cqZAp">
                      <node concept="la8eA" id="7kDFC_u8V$1" role="lcghm">
                        <property role="lacIc" value="- $ref: " />
                      </node>
                      <node concept="la8eA" id="7kDFC_u8V$2" role="lcghm">
                        <property role="lacIc" value="'#/components/schemas/" />
                      </node>
                      <node concept="l9hG8" id="7kDFC_u8V$3" role="lcghm">
                        <node concept="2OqwBi" id="7kDFC_u8V$4" role="lb14g">
                          <node concept="2OqwBi" id="7kDFC_u8V$5" role="2Oq$k0">
                            <node concept="2GrUjf" id="7kDFC_u8V$6" role="2Oq$k0">
                              <ref role="2Gs0qQ" node="7kDFC_u8VzS" resolve="elem" />
                            </node>
                            <node concept="3TrEf2" id="7kDFC_u8V$7" role="2OqNvi">
                              <ref role="3Tt5mk" to="7thy:3JsE8aiBPe0" resolve="schema" />
                            </node>
                          </node>
                          <node concept="3TrcHB" id="7kDFC_u8V$8" role="2OqNvi">
                            <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                          </node>
                        </node>
                      </node>
                      <node concept="la8eA" id="7kDFC_uanPz" role="lcghm">
                        <property role="lacIc" value="'" />
                      </node>
                      <node concept="l8MVK" id="7kDFC_udmqS" role="lcghm" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="cAH7uXo$Sr" role="3cqZAp">
              <node concept="3clFbS" id="cAH7uXo$St" role="3clFbx">
                <node concept="1ZvZ2y" id="cAH7uXoDMM" role="3cqZAp">
                  <node concept="Xl_RD" id="cAH7uXoDN3" role="v0bCk">
                    <property role="Xl_RC" value="allOf may not be empty" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="cAH7uXoAEK" role="3clFbw">
                <node concept="2OqwBi" id="cAH7uXo_8I" role="2Oq$k0">
                  <node concept="117lpO" id="cAH7uXo_0G" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="cAH7uXo_jo" role="2OqNvi">
                    <ref role="3TtcxE" to="7thy:3JsE8aiB2pH" resolve="allOf" />
                  </node>
                </node>
                <node concept="1v1jN8" id="cAH7uXoDLN" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="7kDFC_u8YwN">
    <ref role="WuzLi" to="7thy:3JsE8aiAEqP" resolve="BooleanSchema" />
    <node concept="11bSqf" id="7kDFC_u8YwO" role="11c4hB">
      <node concept="3clFbS" id="7kDFC_u8YwP" role="2VODD2">
        <node concept="1bpajm" id="7kDFC_u8Yx6" role="3cqZAp" />
        <node concept="lc7rE" id="7kDFC_u8YxC" role="3cqZAp">
          <node concept="l9hG8" id="7kDFC_u8Yyv" role="lcghm">
            <node concept="2OqwBi" id="7kDFC_u8YDY" role="lb14g">
              <node concept="117lpO" id="7kDFC_u8Yzj" role="2Oq$k0" />
              <node concept="3TrcHB" id="7kDFC_u8YWe" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="7kDFC_u8YZ$" role="lcghm">
            <property role="lacIc" value=":" />
          </node>
          <node concept="l8MVK" id="7kDFC_u8Z1h" role="lcghm" />
        </node>
        <node concept="3izx1p" id="7kDFC_u8Z4b" role="3cqZAp">
          <node concept="3clFbS" id="7kDFC_u8Z4d" role="3izTki">
            <node concept="1bpajm" id="7kDFC_u8Z4Y" role="3cqZAp" />
            <node concept="lc7rE" id="7kDFC_u8Z5w" role="3cqZAp">
              <node concept="la8eA" id="7kDFC_u8Z5R" role="lcghm">
                <property role="lacIc" value="type: boolean" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

