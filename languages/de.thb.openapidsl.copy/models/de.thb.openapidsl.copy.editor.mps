<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:d8fb28b9-f6ea-4cf9-9827-770d53bda47d(de.thb.openapidsl.copy.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="14" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="7thy" ref="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="5991739802479784073" name="jetbrains.mps.lang.editor.structure.MenuTypeDefault" flags="ng" index="22hDWj" />
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="8954657570917870539" name="jetbrains.mps.lang.editor.structure.TransformationLocation_ContextAssistant" flags="ng" index="2j_NTm" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="1237308012275" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineStyleClassItem" flags="ln" index="ljvvj" />
      <concept id="1237375020029" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineChildrenStyleClassItem" flags="ln" index="pj6Ft" />
      <concept id="1237385578942" name="jetbrains.mps.lang.editor.structure.IndentLayoutOnNewLineStyleClassItem" flags="ln" index="pVoyu" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="6718020819487620876" name="jetbrains.mps.lang.editor.structure.TransformationMenuReference_Default" flags="ng" index="A1WHr" />
      <concept id="2816844678677370764" name="jetbrains.mps.lang.editor.structure.TransformationMenuPart_PropertyMenu" flags="ng" index="2V5er3">
        <reference id="2816844678677370765" name="property" index="2V5er2" />
      </concept>
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1630016958697718209" name="jetbrains.mps.lang.editor.structure.IMenuReference_Default" flags="ng" index="2Z_bC8">
        <reference id="1630016958698373342" name="concept" index="2ZyFGn" />
      </concept>
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
        <child id="5991739802479788259" name="type" index="22hAXT" />
      </concept>
      <concept id="3360401466585705291" name="jetbrains.mps.lang.editor.structure.CellModel_ContextAssistant" flags="ng" index="18a60v" />
      <concept id="7342352913006985500" name="jetbrains.mps.lang.editor.structure.TransformationLocation_Completion" flags="ng" index="3eGOoe" />
      <concept id="5692353713941573329" name="jetbrains.mps.lang.editor.structure.QueryFunction_TransformationMenu_ActionLabelText" flags="ig" index="1hCUdq" />
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="701160265283677816" name="jetbrains.mps.lang.editor.structure.TransparentStyleSheetItem" flags="ln" index="3noiJN" />
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <property id="1140114345053" name="allowEmptyText" index="1O74Pk" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389214265" name="jetbrains.mps.lang.editor.structure.EditorCellModel" flags="ng" index="3EYTF0">
        <child id="4202667662392416064" name="transformationMenu" index="3vIgyS" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="5624877018226900666" name="jetbrains.mps.lang.editor.structure.TransformationMenu" flags="ng" index="3ICUPy" />
      <concept id="5624877018228267058" name="jetbrains.mps.lang.editor.structure.ITransformationMenu" flags="ng" index="3INCJE">
        <child id="1638911550608572412" name="sections" index="IW6Ez" />
      </concept>
      <concept id="6918029743850363447" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_targetNode" flags="ng" index="1NM5Ph" />
      <concept id="422708224287891156" name="jetbrains.mps.lang.editor.structure.TransformationMenuPart_ReferenceMenu" flags="ng" index="3PzhKR">
        <reference id="422708224287891157" name="referenceLink" index="3PzhKQ" />
        <child id="8537008540390643508" name="textFunction" index="w35GX" />
        <child id="7136626533202861118" name="visibleTextFunction" index="1t9nwV" />
      </concept>
      <concept id="7980428675268276156" name="jetbrains.mps.lang.editor.structure.TransformationMenuSection" flags="ng" index="1Qtc8_">
        <child id="7980428675268276157" name="locations" index="1Qtc8$" />
        <child id="7980428675268276159" name="parts" index="1Qtc8A" />
      </concept>
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="3JW8_axMFr4">
    <ref role="1XX52x" to="7thy:3JW8_axMFqS" resolve="OpenApiInfo" />
    <node concept="3EZMnI" id="3JW8_axMRKO" role="2wV5jI">
      <node concept="l2Vlx" id="3JW8_axMRKP" role="2iSdaV" />
      <node concept="3F0ifn" id="3JW8_axMRKM" role="3EZMnx">
        <property role="3F0ifm" value="title:" />
      </node>
      <node concept="3F0A7n" id="3JW8_axMRKX" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:3JW8_axMFqV" resolve="title" />
        <node concept="ljvvj" id="3JW8_axMTX0" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="3JW8_axMRLm" role="3EZMnx">
        <property role="3F0ifm" value="description:" />
      </node>
      <node concept="3F0A7n" id="3JW8_axMRLw" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <ref role="1NtTu8" to="7thy:3JW8_axMFqX" resolve="description" />
        <node concept="ljvvj" id="3JW8_axMTX4" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="3JW8_axMRLN" role="3EZMnx">
        <property role="3F0ifm" value="version:" />
      </node>
      <node concept="3F0A7n" id="3JW8_axMRM1" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:3JW8_axMFr0" resolve="version" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="3JW8_axN6CB">
    <ref role="1XX52x" to="7thy:3JW8_axN3f4" resolve="PathObject" />
    <node concept="3EZMnI" id="3JW8_axN6CD" role="2wV5jI">
      <node concept="3F0A7n" id="3JW8_axN6CK" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:3JW8_axN3fb" resolve="path" />
        <node concept="ljvvj" id="3JW8_axNgJr" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="3JW8_axN6CG" role="2iSdaV" />
      <node concept="3F2HdR" id="3JW8_axN9pu" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:3JW8_axN3fe" resolve="operation" />
        <node concept="l2Vlx" id="3JW8_axN9pw" role="2czzBx" />
        <node concept="ljvvj" id="3JW8_axN9p$" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pj6Ft" id="3JW8_axN9pA" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="3JW8_axNl5O" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="3JW8_axNcFr">
    <ref role="1XX52x" to="7thy:3JW8_axN3Pw" resolve="GetOperationObject" />
    <node concept="3EZMnI" id="3JW8_axNcFt" role="2wV5jI">
      <node concept="3F0ifn" id="3JW8_axNcFB" role="3EZMnx">
        <property role="3F0ifm" value="GET" />
        <node concept="ljvvj" id="3JW8_axNcFN" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="3JW8_axNcFw" role="2iSdaV" />
      <node concept="3F0ifn" id="3JW8_axNcGa" role="3EZMnx">
        <property role="3F0ifm" value="summary:" />
        <node concept="lj46D" id="3JW8_axNp8B" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="3JW8_axNcGk" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:3JW8_axN3fB" resolve="summary" />
        <node concept="ljvvj" id="3JW8_axNcGq" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="3JW8_axNcGF" role="3EZMnx">
        <property role="3F0ifm" value="description:" />
        <node concept="lj46D" id="3JW8_axNp8D" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="3JW8_axNcGV" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <ref role="1NtTu8" to="7thy:3JW8_axN3fI" resolve="description" />
        <node concept="ljvvj" id="4favA_iAT$a" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4favA_iAT$q" role="3EZMnx">
        <property role="3F0ifm" value="responses:" />
        <node concept="lj46D" id="4favA_iAT_j" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="4favA_iB27X" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="4favA_iB28w" role="3EZMnx">
        <node concept="l2Vlx" id="4favA_iB28x" role="2iSdaV" />
        <node concept="3F2HdR" id="4favA_iAT_3" role="3EZMnx">
          <ref role="1NtTu8" to="7thy:4favA_iAyf8" resolve="responses" />
          <node concept="l2Vlx" id="4favA_iAT_5" role="2czzBx" />
          <node concept="ljvvj" id="4favA_iAXQg" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pj6Ft" id="4favA_iAXQi" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="4favA_iB28s" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="lj46D" id="4favA_iB28V" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4favA_iAyfl">
    <ref role="1XX52x" to="7thy:4favA_iAyfb" resolve="Response" />
    <node concept="3EZMnI" id="4favA_iAyfn" role="2wV5jI">
      <node concept="l2Vlx" id="4favA_iAyfq" role="2iSdaV" />
      <node concept="3F0A7n" id="4favA_iAyfW" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iAyfc" resolve="httpCode" />
        <node concept="ljvvj" id="4favA_iAyg2" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4favA_iAygb" role="3EZMnx">
        <property role="3F0ifm" value="description:" />
        <node concept="lj46D" id="4favA_iBgI_" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="4favA_iAygr" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iAyfe" resolve="description" />
        <node concept="ljvvj" id="4favA_iBcro" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4favA_iBcre" role="3EZMnx">
        <property role="3F0ifm" value="content:" />
        <node concept="lj46D" id="4favA_iBgIB" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="4favA_iB_Ae" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="4favA_iB__O" role="3EZMnx">
        <node concept="l2Vlx" id="4favA_iB__P" role="2iSdaV" />
        <node concept="3F2HdR" id="4favA_iB__$" role="3EZMnx">
          <ref role="1NtTu8" to="7thy:4favA_iB_5O" resolve="content" />
          <node concept="l2Vlx" id="4favA_iB__B" role="2czzBx" />
          <node concept="ljvvj" id="4favA_iB__M" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="4favA_iB_A6" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pj6Ft" id="cAH7uXnRdO" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="ljvvj" id="4favA_iB_A9" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pj6Ft" id="4favA_iB_Ab" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="4favA_iB_Ah" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4favA_iBl1h">
    <ref role="1XX52x" to="7thy:4favA_iBl1f" resolve="PostOperationObject" />
    <node concept="3EZMnI" id="4favA_iBl1i" role="2wV5jI">
      <node concept="3F0ifn" id="4favA_iBl1j" role="3EZMnx">
        <property role="3F0ifm" value="POST" />
        <node concept="ljvvj" id="4favA_iBl1k" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="4favA_iBl1l" role="2iSdaV" />
      <node concept="3F0ifn" id="4favA_iBl1m" role="3EZMnx">
        <property role="3F0ifm" value="summary:" />
        <node concept="lj46D" id="4favA_iBl1n" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="4favA_iBl1o" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:3JW8_axN3fB" resolve="summary" />
        <node concept="ljvvj" id="4favA_iBl1p" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4favA_iBl1q" role="3EZMnx">
        <property role="3F0ifm" value="description:" />
        <node concept="lj46D" id="4favA_iBl1r" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="4favA_iBl1s" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <ref role="1NtTu8" to="7thy:3JW8_axN3fI" resolve="description" />
        <node concept="ljvvj" id="4favA_iBl1t" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4favA_iBl1u" role="3EZMnx">
        <property role="3F0ifm" value="responses:" />
        <node concept="lj46D" id="4favA_iBl1v" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="4favA_iBl1w" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="4favA_iBl1x" role="3EZMnx">
        <node concept="l2Vlx" id="4favA_iBl1y" role="2iSdaV" />
        <node concept="3F2HdR" id="4favA_iBl1z" role="3EZMnx">
          <ref role="1NtTu8" to="7thy:4favA_iAyf8" resolve="responses" />
          <node concept="l2Vlx" id="4favA_iBl1$" role="2czzBx" />
          <node concept="ljvvj" id="4favA_iBl1_" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pj6Ft" id="4favA_iBl1A" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="4favA_iBl1B" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="lj46D" id="4favA_iBl1C" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4favA_iBl1D">
    <ref role="1XX52x" to="7thy:4favA_iBl1e" resolve="PutOperationObject" />
    <node concept="3EZMnI" id="4favA_iBl1E" role="2wV5jI">
      <node concept="3F0ifn" id="4favA_iBl1F" role="3EZMnx">
        <property role="3F0ifm" value="PUT" />
        <node concept="ljvvj" id="4favA_iBl1G" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="4favA_iBl1H" role="2iSdaV" />
      <node concept="3F0ifn" id="4favA_iBl1I" role="3EZMnx">
        <property role="3F0ifm" value="summary:" />
        <node concept="lj46D" id="4favA_iBl1J" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="4favA_iBl1K" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:3JW8_axN3fB" resolve="summary" />
        <node concept="ljvvj" id="4favA_iBl1L" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4favA_iBl1M" role="3EZMnx">
        <property role="3F0ifm" value="description:" />
        <node concept="lj46D" id="4favA_iBl1N" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="4favA_iBl1O" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <ref role="1NtTu8" to="7thy:3JW8_axN3fI" resolve="description" />
        <node concept="ljvvj" id="4favA_iBl1P" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4favA_iBl1Q" role="3EZMnx">
        <property role="3F0ifm" value="responses:" />
        <node concept="lj46D" id="4favA_iBl1R" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="4favA_iBl1S" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="4favA_iBl1T" role="3EZMnx">
        <node concept="l2Vlx" id="4favA_iBl1U" role="2iSdaV" />
        <node concept="3F2HdR" id="4favA_iBl1V" role="3EZMnx">
          <ref role="1NtTu8" to="7thy:4favA_iAyf8" resolve="responses" />
          <node concept="l2Vlx" id="4favA_iBl1W" role="2czzBx" />
          <node concept="ljvvj" id="4favA_iBl1X" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pj6Ft" id="4favA_iBl1Y" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="4favA_iBl1Z" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="lj46D" id="4favA_iBl20" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4favA_iBl21">
    <ref role="1XX52x" to="7thy:4favA_iBl1g" resolve="DeleteOperationObject" />
    <node concept="3EZMnI" id="4favA_iBl22" role="2wV5jI">
      <node concept="3F0ifn" id="4favA_iBl23" role="3EZMnx">
        <property role="3F0ifm" value="DELETE" />
        <node concept="ljvvj" id="4favA_iBl24" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="4favA_iBl25" role="2iSdaV" />
      <node concept="3F0ifn" id="4favA_iBl26" role="3EZMnx">
        <property role="3F0ifm" value="summary:" />
        <node concept="lj46D" id="4favA_iBl27" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="4favA_iBl28" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:3JW8_axN3fB" resolve="summary" />
        <node concept="ljvvj" id="4favA_iBl29" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4favA_iBl2a" role="3EZMnx">
        <property role="3F0ifm" value="description:" />
        <node concept="lj46D" id="4favA_iBl2b" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="4favA_iBl2c" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:3JW8_axN3fI" resolve="description" />
        <node concept="ljvvj" id="4favA_iBl2d" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4favA_iBl2e" role="3EZMnx">
        <property role="3F0ifm" value="responses:" />
        <node concept="lj46D" id="4favA_iBl2f" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="4favA_iBl2g" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="4favA_iBl2h" role="3EZMnx">
        <node concept="l2Vlx" id="4favA_iBl2i" role="2iSdaV" />
        <node concept="3F2HdR" id="4favA_iBl2j" role="3EZMnx">
          <ref role="1NtTu8" to="7thy:4favA_iAyf8" resolve="responses" />
          <node concept="l2Vlx" id="4favA_iBl2k" role="2czzBx" />
          <node concept="ljvvj" id="4favA_iBl2l" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pj6Ft" id="4favA_iBl2m" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="4favA_iBl2n" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="lj46D" id="4favA_iBl2o" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4favA_iBGva">
    <ref role="1XX52x" to="7thy:4favA_iB$7K" resolve="Content" />
    <node concept="3EZMnI" id="4favA_iBGvc" role="2wV5jI">
      <node concept="3F0A7n" id="4favA_iBGvj" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iB$7L" resolve="mediaType" />
        <node concept="ljvvj" id="4favA_iC3vl" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="4favA_iBGvf" role="2iSdaV" />
      <node concept="3F0ifn" id="4favA_iChdU" role="3EZMnx">
        <property role="3F0ifm" value="schema:" />
        <node concept="pVoyu" id="cAH7uXna5O" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="cAH7uXna5T" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="1iCGBv" id="6TJtlvV8e92" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iBNSA" resolve="schema" />
        <node concept="lj46D" id="6TJtlvV8e94" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="1sVBvm" id="6TJtlvV8e95" role="1sWHZn">
          <node concept="3F0A7n" id="6TJtlvV8R1Z" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="7thy:4favA_iBNMN" resolve="name" />
          </node>
        </node>
        <node concept="A1WHr" id="6TJtlvV8DFx" role="3vIgyS">
          <ref role="2ZyFGn" to="7thy:4favA_iB$7K" resolve="Content" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4favA_iCtM9">
    <ref role="1XX52x" to="7thy:3JW8_axMW7I" resolve="OpenApi" />
    <node concept="3EZMnI" id="4favA_iCtMb" role="2wV5jI">
      <node concept="3F0ifn" id="4favA_iCtMl" role="3EZMnx">
        <property role="3F0ifm" value="openapi:" />
      </node>
      <node concept="l2Vlx" id="4favA_iCtMe" role="2iSdaV" />
      <node concept="3F0A7n" id="4favA_iCtMr" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:3JW8_axMYAC" resolve="openapi" />
        <node concept="ljvvj" id="4favA_iCtMs" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4favA_iCtMF" role="3EZMnx">
        <property role="3F0ifm" value="info:" />
        <node concept="ljvvj" id="4favA_iCElY" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F1sOY" id="4favA_iCtMR" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:3JW8_axMW7J" resolve="info" />
        <node concept="ljvvj" id="4favA_iCtNf" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="4favA_iCEm0" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4favA_iCtN5" role="3EZMnx">
        <property role="3F0ifm" value="paths:" />
        <node concept="ljvvj" id="4favA_iCtNA" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="4favA_iCtNq" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:3JW8_axN3xZ" resolve="paths" />
        <node concept="l2Vlx" id="4favA_iCtNs" role="2czzBx" />
        <node concept="lj46D" id="4favA_iCtNC" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="4favA_iCQTk" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4favA_iCQT2" role="3EZMnx">
        <property role="3F0ifm" value="schemas:" />
        <node concept="ljvvj" id="4favA_iCQTW" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="18a60v" id="3JsE8aiEl4Z" role="3EZMnx">
        <node concept="VPM3Z" id="3JsE8aiEl52" role="3F10Kt" />
        <node concept="3noiJN" id="3JsE8aiEl53" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="3JsE8aiEl5n" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="3JsE8aiEl5N" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iCQSJ" resolve="schema" />
        <node concept="lj46D" id="3JsE8aiEl5O" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="3JsE8aiEl5P" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pj6Ft" id="3JsE8aiEl5Q" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="l2Vlx" id="3JsE8aiEl5R" role="2czzBx" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4favA_iDFCj">
    <ref role="1XX52x" to="7thy:4favA_iBNMM" resolve="StringSchema" />
    <node concept="3EZMnI" id="4favA_iBNNN" role="2wV5jI">
      <node concept="3F0A7n" id="4favA_iBNNU" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iBNMN" resolve="name" />
        <node concept="ljvvj" id="4favA_iBNOf" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="4favA_iBNNQ" role="2iSdaV" />
      <node concept="3F0ifn" id="6TJtlvV94N6" role="3EZMnx">
        <property role="3F0ifm" value="type: string" />
        <node concept="lj46D" id="6TJtlvV94Nb" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="6TJtlvV94NY" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4favA_iBNO0" role="3EZMnx">
        <property role="3F0ifm" value="description:" />
        <node concept="lj46D" id="4favA_iBNOh" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="4favA_iBNO8" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iBNMH" resolve="description" />
        <node concept="ljvvj" id="4favA_iBNOE" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4favA_iBNOW" role="3EZMnx">
        <property role="3F0ifm" value="example:" />
        <node concept="lj46D" id="4favA_iBNP5" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="4favA_iBNPh" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <ref role="1NtTu8" to="7thy:4favA_iBNMJ" resolve="example" />
        <node concept="ljvvj" id="4favA_iBNPR" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4favA_iBNQi" role="3EZMnx">
        <property role="3F0ifm" value="minLength:" />
        <node concept="lj46D" id="4favA_iBNQv" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="4favA_iBNQJ" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <ref role="1NtTu8" to="7thy:3JsE8aiIcbJ" resolve="minLength" />
        <node concept="ljvvj" id="4favA_iBNRv" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4favA_iBNRd" role="3EZMnx">
        <property role="3F0ifm" value="maxLength:" />
        <node concept="lj46D" id="4favA_iBNRx" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="4favA_iBNRP" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <ref role="1NtTu8" to="7thy:3JsE8aiIcbN" resolve="maxLength" />
        <node concept="ljvvj" id="6TJtlvV94Lm" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6TJtlvV94LL" role="3EZMnx">
        <property role="3F0ifm" value="pattern:" />
        <node concept="lj46D" id="6TJtlvV94M7" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="6TJtlvV94Mw" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <ref role="1NtTu8" to="7thy:4favA_iBNS8" resolve="pattern" />
        <node concept="ljvvj" id="3JsE8ainSyJ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="3JsE8ainSx6" role="3EZMnx">
        <property role="3F0ifm" value="format:" />
        <node concept="lj46D" id="3JsE8ainSyL" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="3JsE8ainSzI" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <ref role="1NtTu8" to="7thy:3JsE8ainSvo" resolve="format" />
      </node>
    </node>
  </node>
  <node concept="3ICUPy" id="6TJtlvV7bWE">
    <ref role="aqKnT" to="7thy:4favA_iB$7K" resolve="Content" />
    <node concept="1Qtc8_" id="6TJtlvV7bWH" role="IW6Ez">
      <node concept="3eGOoe" id="6TJtlvV8rX3" role="1Qtc8$" />
      <node concept="3PzhKR" id="6TJtlvV7bWR" role="1Qtc8A">
        <ref role="3PzhKQ" to="7thy:4favA_iBNSA" resolve="schema" />
        <node concept="1hCUdq" id="6TJtlvV7bWT" role="w35GX">
          <node concept="3clFbS" id="6TJtlvV7bWU" role="2VODD2">
            <node concept="3clFbF" id="6TJtlvV7c1$" role="3cqZAp">
              <node concept="2OqwBi" id="6TJtlvV7cdv" role="3clFbG">
                <node concept="1NM5Ph" id="6TJtlvV7c1z" role="2Oq$k0" />
                <node concept="3TrcHB" id="6TJtlvV7cwo" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1hCUdq" id="6TJtlvV7NBQ" role="1t9nwV">
          <node concept="3clFbS" id="6TJtlvV7NBR" role="2VODD2">
            <node concept="3clFbF" id="6TJtlvV7NCR" role="3cqZAp">
              <node concept="2OqwBi" id="6TJtlvV7NON" role="3clFbG">
                <node concept="1NM5Ph" id="6TJtlvV7NCQ" role="2Oq$k0" />
                <node concept="3TrcHB" id="6TJtlvV7O4N" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="22hDWj" id="6TJtlvV7bWL" role="22hAXT" />
  </node>
  <node concept="24kQdi" id="6TJtlvV94O1">
    <ref role="1XX52x" to="7thy:4favA_iBNSc" resolve="IntegerSchema" />
    <node concept="3EZMnI" id="6TJtlvV94O2" role="2wV5jI">
      <node concept="3F0A7n" id="6TJtlvV94O3" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iBNMN" resolve="name" />
        <node concept="ljvvj" id="6TJtlvV94O4" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="6TJtlvV94O5" role="2iSdaV" />
      <node concept="3F0ifn" id="6TJtlvV94O6" role="3EZMnx">
        <property role="3F0ifm" value="type: integer" />
        <node concept="lj46D" id="6TJtlvV94O7" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="6TJtlvV94O8" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6TJtlvV94O9" role="3EZMnx">
        <property role="3F0ifm" value="description:" />
        <node concept="lj46D" id="6TJtlvV94Oa" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="6TJtlvV94Ob" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iBNMH" resolve="description" />
        <node concept="ljvvj" id="6TJtlvV94Oc" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6TJtlvV94Od" role="3EZMnx">
        <property role="3F0ifm" value="example:" />
        <node concept="lj46D" id="6TJtlvV94Oe" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="6TJtlvV94Of" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <ref role="1NtTu8" to="7thy:4favA_iBNMJ" resolve="example" />
        <node concept="ljvvj" id="6TJtlvV94Og" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6TJtlvV94Oh" role="3EZMnx">
        <property role="3F0ifm" value="minimum" />
        <node concept="lj46D" id="6TJtlvV94Oi" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="6TJtlvV94Oj" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <ref role="1NtTu8" to="7thy:4favA_iBNSd" resolve="minimum" />
        <node concept="ljvvj" id="6TJtlvV94Ok" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6TJtlvV94Ol" role="3EZMnx">
        <property role="3F0ifm" value="maximum" />
        <node concept="lj46D" id="6TJtlvV94Om" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="6TJtlvV94On" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <ref role="1NtTu8" to="7thy:4favA_iBNSe" resolve="maximum" />
        <node concept="ljvvj" id="6TJtlvV94Oo" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6TJtlvV94Py">
    <ref role="1XX52x" to="7thy:4favA_iBNSQ" resolve="ArraySchema" />
    <node concept="3EZMnI" id="6TJtlvV94Pz" role="2wV5jI">
      <node concept="3F0A7n" id="6TJtlvV94P$" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iBNMN" resolve="name" />
        <node concept="ljvvj" id="6TJtlvV94P_" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="6TJtlvV94PA" role="2iSdaV" />
      <node concept="3F0ifn" id="6TJtlvV94PB" role="3EZMnx">
        <property role="3F0ifm" value="type: array" />
        <node concept="lj46D" id="6TJtlvV94PC" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="6TJtlvV94PD" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6TJtlvV94PE" role="3EZMnx">
        <property role="3F0ifm" value="description:" />
        <node concept="lj46D" id="6TJtlvV94PF" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="6TJtlvV94PG" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iBNMH" resolve="description" />
        <node concept="ljvvj" id="6TJtlvV94PH" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6TJtlvV94PI" role="3EZMnx">
        <property role="3F0ifm" value="example:" />
        <node concept="lj46D" id="6TJtlvV94PJ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="6TJtlvV94PK" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <ref role="1NtTu8" to="7thy:4favA_iBNMJ" resolve="example" />
        <node concept="ljvvj" id="6TJtlvV94PL" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6TJtlvV94PM" role="3EZMnx">
        <property role="3F0ifm" value="minItems:" />
        <node concept="lj46D" id="6TJtlvV94PN" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="6TJtlvV94PO" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <ref role="1NtTu8" to="7thy:4favA_iBNSS" resolve="minItems" />
        <node concept="ljvvj" id="6TJtlvV94PP" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6TJtlvV94PQ" role="3EZMnx">
        <property role="3F0ifm" value="maxItems:" />
        <node concept="lj46D" id="6TJtlvV94PR" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="6TJtlvV94PS" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <ref role="1NtTu8" to="7thy:4favA_iBNSU" resolve="maxItems" />
        <node concept="ljvvj" id="6TJtlvV94PT" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6TJtlvV94PU" role="3EZMnx">
        <property role="3F0ifm" value="items:" />
        <node concept="lj46D" id="6TJtlvV94PV" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="6TJtlvV94RY" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="6TJtlvV94QH" role="3EZMnx">
        <node concept="VPM3Z" id="6TJtlvV94QJ" role="3F10Kt" />
        <node concept="1iCGBv" id="6TJtlvV94Rf" role="3EZMnx">
          <ref role="1NtTu8" to="7thy:4favA_iBNT3" resolve="items" />
          <node concept="ljvvj" id="6TJtlvV94Rg" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="6TJtlvV94Rh" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="1sVBvm" id="6TJtlvV94Ri" role="1sWHZn">
            <node concept="3F0A7n" id="3JsE8aiIlLH" role="2wV5jI">
              <property role="1Intyy" value="true" />
              <ref role="1NtTu8" to="7thy:4favA_iBNMN" resolve="name" />
            </node>
          </node>
        </node>
        <node concept="l2Vlx" id="6TJtlvV94QM" role="2iSdaV" />
        <node concept="pj6Ft" id="6TJtlvV94RR" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="6TJtlvV94RU" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="6TJtlvV94S1" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="3ICUPy" id="6TJtlvV94S6">
    <ref role="aqKnT" to="7thy:4favA_iBNSQ" resolve="ArraySchema" />
    <node concept="1Qtc8_" id="6TJtlvV94S9" role="IW6Ez">
      <node concept="3PzhKR" id="6TJtlvV9540" role="1Qtc8A">
        <ref role="3PzhKQ" to="7thy:4favA_iBNT3" resolve="items" />
        <node concept="1hCUdq" id="6TJtlvV9543" role="w35GX">
          <node concept="3clFbS" id="6TJtlvV9544" role="2VODD2">
            <node concept="3clFbF" id="6TJtlvV954r" role="3cqZAp">
              <node concept="2OqwBi" id="6TJtlvV95gm" role="3clFbG">
                <node concept="1NM5Ph" id="6TJtlvV954q" role="2Oq$k0" />
                <node concept="3TrcHB" id="6TJtlvV95Ch" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1hCUdq" id="3JsE8aiIlSy" role="1t9nwV">
          <node concept="3clFbS" id="3JsE8aiIlSz" role="2VODD2">
            <node concept="3clFbF" id="3JsE8aiIlTx" role="3cqZAp">
              <node concept="2OqwBi" id="3JsE8aiIm4N" role="3clFbG">
                <node concept="1NM5Ph" id="3JsE8aiIlTw" role="2Oq$k0" />
                <node concept="3TrcHB" id="3JsE8aiImk$" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3eGOoe" id="6TJtlvV953V" role="1Qtc8$" />
    </node>
    <node concept="22hDWj" id="6TJtlvV94S7" role="22hAXT" />
  </node>
  <node concept="24kQdi" id="6TJtlvV95J7">
    <ref role="1XX52x" to="7thy:4favA_iBNSC" resolve="ObjectSchema" />
    <node concept="3EZMnI" id="6TJtlvV95J8" role="2wV5jI">
      <node concept="3F0A7n" id="6TJtlvV95J9" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iBNMN" resolve="name" />
        <node concept="ljvvj" id="6TJtlvV95Ja" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="6TJtlvV95Jb" role="2iSdaV" />
      <node concept="3F0ifn" id="6TJtlvV95Jc" role="3EZMnx">
        <property role="3F0ifm" value="type: object" />
        <node concept="lj46D" id="6TJtlvV95Jd" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="6TJtlvV95Je" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6TJtlvV95Jf" role="3EZMnx">
        <property role="3F0ifm" value="description:" />
        <node concept="lj46D" id="6TJtlvV95Jg" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="6TJtlvV95Jh" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iBNMH" resolve="description" />
        <node concept="ljvvj" id="6TJtlvV95Ji" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6TJtlvV95Jj" role="3EZMnx">
        <property role="3F0ifm" value="example:" />
        <node concept="lj46D" id="6TJtlvV95Jk" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="6TJtlvV95Jl" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <ref role="1NtTu8" to="7thy:4favA_iBNMJ" resolve="example" />
        <node concept="ljvvj" id="6TJtlvV95Jm" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6TJtlvV95Jv" role="3EZMnx">
        <property role="3F0ifm" value="properties:" />
        <node concept="lj46D" id="6TJtlvV95Jw" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="6TJtlvV95Jx" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="6TJtlvV95Ml" role="3EZMnx">
        <node concept="l2Vlx" id="6TJtlvV95Mm" role="2iSdaV" />
        <node concept="3F2HdR" id="6TJtlvV95LU" role="3EZMnx">
          <ref role="1NtTu8" to="7thy:4favA_iBNSJ" resolve="properties" />
          <node concept="l2Vlx" id="6TJtlvV95LW" role="2czzBx" />
          <node concept="lj46D" id="6TJtlvV95Mj" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pj6Ft" id="6TJtlvV95MP" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="lj46D" id="6TJtlvV95MN" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="3JsE8aioBP9" role="3EZMnx">
        <property role="3F0ifm" value="additionalProperties:" />
        <node concept="lj46D" id="3JsE8aioBPz" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="3JsE8aioXL5" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="3JsE8aioBQ0" role="3EZMnx">
        <node concept="VPM3Z" id="3JsE8aioBQ2" role="3F10Kt" />
        <node concept="3F1sOY" id="3JsE8aioBQ$" role="3EZMnx">
          <ref role="1NtTu8" to="7thy:3JsE8ainS_0" resolve="additionalProperties" />
          <node concept="lj46D" id="3JsE8aioXLD" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="A1WHr" id="3JsE8aipkh6" role="3vIgyS">
            <ref role="2ZyFGn" to="7thy:3JsE8ainS_3" resolve="ObjectSchemaAdditionalProperty" />
          </node>
        </node>
        <node concept="l2Vlx" id="3JsE8aioBQ5" role="2iSdaV" />
        <node concept="lj46D" id="3JsE8aioXLy" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pj6Ft" id="3JsE8aioXL_" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="3JsE8aipj$Q" role="3EZMnx">
        <property role="3F0ifm" value="required:" />
        <node concept="lj46D" id="3JsE8aipjAO" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="3JsE8aipjAQ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="3JsE8aix6Hd" role="3EZMnx">
        <node concept="VPM3Z" id="3JsE8aix6Hg" role="3F10Kt" />
        <node concept="3F2HdR" id="3JsE8aix6IR" role="3EZMnx">
          <ref role="1NtTu8" to="7thy:3JsE8aitKU3" resolve="required" />
          <node concept="l2Vlx" id="3JsE8aix6IU" role="2czzBx" />
          <node concept="lj46D" id="3JsE8aix6J2" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pj6Ft" id="3JsE8aizi5N" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="l2Vlx" id="3JsE8aix6Hk" role="2iSdaV" />
        <node concept="lj46D" id="3JsE8aix6IX" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6TJtlvV9pqk">
    <ref role="1XX52x" to="7thy:4favA_iBNSL" resolve="ObjectSchemaProperty" />
    <node concept="3EZMnI" id="6TJtlvV9pqm" role="2wV5jI">
      <node concept="3F0A7n" id="6TJtlvV9pqt" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iBNSM" resolve="name" />
      </node>
      <node concept="l2Vlx" id="6TJtlvV9pqp" role="2iSdaV" />
      <node concept="3F0ifn" id="6TJtlvV9pq_" role="3EZMnx">
        <property role="3F0ifm" value=":" />
      </node>
      <node concept="1iCGBv" id="6TJtlvV9pqH" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iBNSO" resolve="schema" />
        <node concept="ljvvj" id="6TJtlvV9pqI" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="6TJtlvV9pqJ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="1sVBvm" id="6TJtlvV9pqK" role="1sWHZn">
          <node concept="3F0A7n" id="6TJtlvV9Ikh" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="7thy:4favA_iBNMN" resolve="name" />
          </node>
        </node>
        <node concept="A1WHr" id="3JsE8ais8OA" role="3vIgyS">
          <ref role="2ZyFGn" to="7thy:4favA_iBNSL" resolve="ObjectSchemaProperty" />
        </node>
      </node>
    </node>
  </node>
  <node concept="3ICUPy" id="6TJtlvV9pqX">
    <ref role="aqKnT" to="7thy:4favA_iBNSL" resolve="ObjectSchemaProperty" />
    <node concept="1Qtc8_" id="6TJtlvV9pqY" role="IW6Ez">
      <node concept="3eGOoe" id="6TJtlvV9pqZ" role="1Qtc8$" />
      <node concept="3PzhKR" id="6TJtlvV9pr0" role="1Qtc8A">
        <ref role="3PzhKQ" to="7thy:4favA_iBNSO" resolve="schema" />
        <node concept="1hCUdq" id="6TJtlvV9pr1" role="w35GX">
          <node concept="3clFbS" id="6TJtlvV9pr2" role="2VODD2">
            <node concept="3clFbF" id="6TJtlvV9pr3" role="3cqZAp">
              <node concept="2OqwBi" id="6TJtlvV9pr4" role="3clFbG">
                <node concept="1NM5Ph" id="6TJtlvV9pr5" role="2Oq$k0" />
                <node concept="3TrcHB" id="6TJtlvV9pr6" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1hCUdq" id="6TJtlvV9pr7" role="1t9nwV">
          <node concept="3clFbS" id="6TJtlvV9pr8" role="2VODD2">
            <node concept="3clFbF" id="6TJtlvV9pr9" role="3cqZAp">
              <node concept="2OqwBi" id="6TJtlvV9pra" role="3clFbG">
                <node concept="1NM5Ph" id="3JsE8airqRK" role="2Oq$k0" />
                <node concept="3TrcHB" id="6TJtlvV9prc" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="22hDWj" id="6TJtlvV9prd" role="22hAXT" />
  </node>
  <node concept="24kQdi" id="3JsE8ainS_6">
    <ref role="1XX52x" to="7thy:3JsE8ainS_3" resolve="ObjectSchemaAdditionalProperty" />
    <node concept="3EZMnI" id="3JsE8ainS_8" role="2wV5jI">
      <node concept="3F0ifn" id="3JsE8ainS_i" role="3EZMnx">
        <property role="3F0ifm" value="type:" />
      </node>
      <node concept="l2Vlx" id="3JsE8ainS_b" role="2iSdaV" />
      <node concept="3F0A7n" id="3JsE8ainS_o" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:3JsE8ainS_4" resolve="type" />
      </node>
    </node>
  </node>
  <node concept="3ICUPy" id="3JsE8ainS_s">
    <ref role="aqKnT" to="7thy:3JsE8ainS_3" resolve="ObjectSchemaAdditionalProperty" />
    <node concept="1Qtc8_" id="3JsE8ainS_v" role="IW6Ez">
      <node concept="2V5er3" id="3JsE8ainSA7" role="1Qtc8A">
        <ref role="2V5er2" to="7thy:3JsE8ainS_4" resolve="type" />
      </node>
      <node concept="2j_NTm" id="3JsE8ainS_z" role="1Qtc8$" />
    </node>
    <node concept="22hDWj" id="3JsE8ainS_t" role="22hAXT" />
  </node>
  <node concept="3ICUPy" id="3JsE8aipkhm">
    <ref role="aqKnT" to="7thy:3JsE8aiu$41" resolve="ObjectSchemaRequiredProperty" />
    <node concept="1Qtc8_" id="3JsE8aipkhp" role="IW6Ez">
      <node concept="3eGOoe" id="3JsE8aipkwx" role="1Qtc8$" />
      <node concept="3PzhKR" id="3JsE8aiu$3Z" role="1Qtc8A">
        <ref role="3PzhKQ" to="7thy:3JsE8aiu$43" resolve="property" />
        <node concept="1hCUdq" id="3JsE8aiu$fS" role="w35GX">
          <node concept="3clFbS" id="3JsE8aiu$fT" role="2VODD2">
            <node concept="3clFbF" id="3JsE8aiu$gh" role="3cqZAp">
              <node concept="2OqwBi" id="3JsE8ai$tN3" role="3clFbG">
                <node concept="1NM5Ph" id="3JsE8aiu$gg" role="2Oq$k0" />
                <node concept="3TrcHB" id="3JsE8ai$QkN" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNSM" resolve="name" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1hCUdq" id="3JsE8aizFLi" role="1t9nwV">
          <node concept="3clFbS" id="3JsE8aizFLj" role="2VODD2">
            <node concept="3clFbF" id="3JsE8aizFMh" role="3cqZAp">
              <node concept="2OqwBi" id="3JsE8ai$uv8" role="3clFbG">
                <node concept="1NM5Ph" id="3JsE8aizFMg" role="2Oq$k0" />
                <node concept="3TrcHB" id="3JsE8ai$Qn1" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNSM" resolve="name" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="22hDWj" id="3JsE8aiu$44" role="22hAXT" />
  </node>
  <node concept="24kQdi" id="3JsE8aisRwa">
    <ref role="1XX52x" to="7thy:3JsE8aiu$41" resolve="ObjectSchemaRequiredProperty" />
    <node concept="1iCGBv" id="3JsE8aiwJE9" role="2wV5jI">
      <ref role="1NtTu8" to="7thy:3JsE8aiu$43" resolve="property" />
      <node concept="1sVBvm" id="3JsE8aiwJEc" role="1sWHZn">
        <node concept="3F0A7n" id="3JsE8aiwJEn" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="7thy:4favA_iBNSM" resolve="name" />
        </node>
      </node>
      <node concept="A1WHr" id="3JsE8ai_W6K" role="3vIgyS">
        <ref role="2ZyFGn" to="7thy:3JsE8aiu$41" resolve="ObjectSchemaRequiredProperty" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="3JsE8aiAEqQ">
    <ref role="1XX52x" to="7thy:3JsE8aiAEqP" resolve="BooleanSchema" />
    <node concept="3EZMnI" id="3JsE8aiAEqS" role="2wV5jI">
      <node concept="3F0A7n" id="3JsE8aiAEqZ" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iBNMN" resolve="name" />
        <node concept="ljvvj" id="3JsE8aiAEr2" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="3JsE8aiAEqV" role="2iSdaV" />
      <node concept="3F0ifn" id="3JsE8aiAErd" role="3EZMnx">
        <property role="3F0ifm" value="type: boolean" />
        <node concept="lj46D" id="3JsE8aiAEri" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="3JsE8aiB2p1">
    <ref role="1XX52x" to="7thy:3JsE8aiB2oY" resolve="OneOfSchema" />
    <node concept="3EZMnI" id="3JsE8aiB2p3" role="2wV5jI">
      <node concept="3F0A7n" id="3JsE8aiB2pa" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iBNMN" resolve="name" />
        <node concept="ljvvj" id="3JsE8aiB2pd" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="3JsE8aiB2p6" role="2iSdaV" />
      <node concept="3F0ifn" id="3JsE8aiB2pj" role="3EZMnx">
        <property role="3F0ifm" value="oneOf:" />
        <node concept="lj46D" id="3JsE8aiB2pE" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="3JsE8aiDuMa" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="3JsE8aiDuMd" role="3EZMnx">
        <node concept="l2Vlx" id="3JsE8aiDuMe" role="2iSdaV" />
        <node concept="3F2HdR" id="3JsE8aiB2pt" role="3EZMnx">
          <ref role="1NtTu8" to="7thy:3JsE8aiB2oZ" resolve="oneOf" />
          <node concept="l2Vlx" id="3JsE8aiB2pv" role="2czzBx" />
          <node concept="ljvvj" id="3JsE8aiB2p_" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pj6Ft" id="3JsE8aiB2pB" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="3JsE8aiDuMw" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="lj46D" id="3JsE8aiDuMu" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="3JsE8aiB2pI">
    <ref role="1XX52x" to="7thy:3JsE8aiB2pG" resolve="AllOfSchema" />
    <node concept="3EZMnI" id="3JsE8aiB2pK" role="2wV5jI">
      <node concept="3F0A7n" id="3JsE8aiB2pL" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iBNMN" resolve="name" />
        <node concept="ljvvj" id="3JsE8aiB2pM" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="3JsE8aiB2pN" role="2iSdaV" />
      <node concept="3F0ifn" id="3JsE8aiB2pO" role="3EZMnx">
        <property role="3F0ifm" value="allOf:" />
        <node concept="lj46D" id="3JsE8aiB2pP" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="3JsE8aiCDZ7" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="3JsE8aiBs3H" role="3EZMnx">
        <node concept="l2Vlx" id="3JsE8aiBs3I" role="2iSdaV" />
        <node concept="3F2HdR" id="3JsE8aiB2pQ" role="3EZMnx">
          <ref role="1NtTu8" to="7thy:3JsE8aiB2pH" resolve="allOf" />
          <node concept="l2Vlx" id="3JsE8aiB2pR" role="2czzBx" />
          <node concept="ljvvj" id="3JsE8aiB2pS" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pj6Ft" id="3JsE8aiB2pT" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="3JsE8aiBs3X" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="pj6Ft" id="3JsE8aiCg5s" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="3JsE8aiCDZa" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="3JsE8aiB2q6">
    <ref role="1XX52x" to="7thy:3JsE8aiB2q4" resolve="AnyOfSchema" />
    <node concept="3EZMnI" id="3JsE8aiB2q8" role="2wV5jI">
      <node concept="3F0A7n" id="3JsE8aiB2q9" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:4favA_iBNMN" resolve="name" />
        <node concept="ljvvj" id="3JsE8aiB2qa" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="3JsE8aiB2qb" role="2iSdaV" />
      <node concept="3F0ifn" id="3JsE8aiB2qc" role="3EZMnx">
        <property role="3F0ifm" value="anyOf:" />
        <node concept="lj46D" id="3JsE8aiB2qd" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="3JsE8aiDuLI" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="3JsE8aiDuLP" role="3EZMnx">
        <node concept="l2Vlx" id="3JsE8aiDuLQ" role="2iSdaV" />
        <node concept="3F2HdR" id="3JsE8aiB2qe" role="3EZMnx">
          <ref role="1NtTu8" to="7thy:3JsE8aiB2q5" resolve="anyOf" />
          <node concept="l2Vlx" id="3JsE8aiB2qf" role="2czzBx" />
          <node concept="ljvvj" id="3JsE8aiB2qg" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pj6Ft" id="3JsE8aiB2qh" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="3JsE8aiDuLL" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="A1WHr" id="3JsE8aiDT0t" role="3vIgyS">
            <ref role="2ZyFGn" to="7thy:3JsE8aiBPdZ" resolve="SchemaRef" />
          </node>
        </node>
        <node concept="lj46D" id="3JsE8aiDuM8" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="3JsE8aiBQc8">
    <ref role="1XX52x" to="7thy:3JsE8aiBPdZ" resolve="SchemaRef" />
    <node concept="3EZMnI" id="3JsE8aiBQca" role="2wV5jI">
      <node concept="1iCGBv" id="3JsE8aiBQcg" role="3EZMnx">
        <ref role="1NtTu8" to="7thy:3JsE8aiBPe0" resolve="schema" />
        <node concept="1sVBvm" id="3JsE8aiBQci" role="1sWHZn">
          <node concept="3F0A7n" id="3JsE8aiBQcp" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="7thy:4favA_iBNMN" resolve="name" />
          </node>
        </node>
        <node concept="A1WHr" id="3JsE8aiD4yJ" role="3vIgyS">
          <ref role="2ZyFGn" to="7thy:3JsE8aiBPdZ" resolve="SchemaRef" />
        </node>
      </node>
      <node concept="l2Vlx" id="3JsE8aiBQcc" role="2iSdaV" />
    </node>
  </node>
  <node concept="3ICUPy" id="3JsE8aiD3Rt">
    <ref role="aqKnT" to="7thy:3JsE8aiBPdZ" resolve="SchemaRef" />
    <node concept="1Qtc8_" id="3JsE8aiD3Rw" role="IW6Ez">
      <node concept="3eGOoe" id="3JsE8aiD3R$" role="1Qtc8$" />
      <node concept="3PzhKR" id="3JsE8aiD3RB" role="1Qtc8A">
        <ref role="3PzhKQ" to="7thy:3JsE8aiBPe0" resolve="schema" />
        <node concept="1hCUdq" id="3JsE8aiD3RD" role="w35GX">
          <node concept="3clFbS" id="3JsE8aiD3RE" role="2VODD2">
            <node concept="3clFbF" id="3JsE8aiD3Wk" role="3cqZAp">
              <node concept="2OqwBi" id="3JsE8aiD48f" role="3clFbG">
                <node concept="1NM5Ph" id="3JsE8aiD3Wj" role="2Oq$k0" />
                <node concept="3TrcHB" id="3JsE8aiD4rR" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="22hDWj" id="3JsE8aiD3Ru" role="22hAXT" />
  </node>
</model>

