<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="9" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="3348158742936976480" name="jetbrains.mps.lang.structure.structure.EnumerationMemberDeclaration" flags="ng" index="25R33">
        <property id="1421157252384165432" name="memberId" index="3tVfz5" />
        <property id="672037151186491528" name="presentation" index="1L1pqM" />
      </concept>
      <concept id="3348158742936976479" name="jetbrains.mps.lang.structure.structure.EnumerationDeclaration" flags="ng" index="25R3W">
        <reference id="1075010451642646892" name="defaultMember" index="1H5jkz" />
        <child id="3348158742936976577" name="members" index="25R1y" />
      </concept>
      <concept id="1082978164218" name="jetbrains.mps.lang.structure.structure.DataTypeDeclaration" flags="ng" index="AxPO6">
        <property id="7791109065626895363" name="datatypeId" index="3F6X1D" />
      </concept>
      <concept id="1082978499127" name="jetbrains.mps.lang.structure.structure.ConstrainedDataTypeDeclaration" flags="ng" index="Az7Fb">
        <property id="1083066089218" name="constraint" index="FLfZY" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="3JW8_axMFqS">
    <property role="EcuMT" value="4322367480665913016" />
    <property role="TrG5h" value="OpenApiInfo" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyi" id="3JW8_axMFqV" role="1TKVEl">
      <property role="IQ2nx" value="4322367480665913019" />
      <property role="TrG5h" value="title" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3JW8_axMFqX" role="1TKVEl">
      <property role="IQ2nx" value="4322367480665913021" />
      <property role="TrG5h" value="description" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3JW8_axMFr0" role="1TKVEl">
      <property role="IQ2nx" value="4322367480665913024" />
      <property role="TrG5h" value="version" />
      <ref role="AX2Wp" node="3JW8_axMYB5" resolve="Version" />
    </node>
    <node concept="PrWs8" id="3JW8_axMHHk" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="3JW8_axMW7I">
    <property role="EcuMT" value="4322367480665981422" />
    <property role="TrG5h" value="OpenApi" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyj" id="3JW8_axMW7J" role="1TKVEi">
      <property role="IQ2ns" value="4322367480665981423" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="info" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="3JW8_axMFqS" resolve="OpenApiInfo" />
    </node>
    <node concept="1TJgyj" id="3JW8_axN3xZ" role="1TKVEi">
      <property role="IQ2ns" value="4322367480666011775" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="paths" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="3JW8_axN3f4" resolve="PathObject" />
    </node>
    <node concept="1TJgyj" id="4favA_iCQSJ" role="1TKVEi">
      <property role="IQ2ns" value="4884855736659832367" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="schema" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="4favA_iBNMk" resolve="Schema" />
    </node>
    <node concept="1TJgyi" id="3JW8_axMYAC" role="1TKVEl">
      <property role="IQ2nx" value="4322367480665991592" />
      <property role="TrG5h" value="openapi" />
      <ref role="AX2Wp" node="3JW8_axMYB5" resolve="Version" />
    </node>
  </node>
  <node concept="Az7Fb" id="3JW8_axMYB5">
    <property role="3F6X1D" value="4322367480665991621" />
    <property role="TrG5h" value="Version" />
    <property role="FLfZY" value="[0-9]+\\.[0-9]+\\.[0-9]+" />
  </node>
  <node concept="1TIwiD" id="3JW8_axN3f4">
    <property role="EcuMT" value="4322367480666010564" />
    <property role="TrG5h" value="PathObject" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyi" id="3JW8_axN3fb" role="1TKVEl">
      <property role="IQ2nx" value="4322367480666010571" />
      <property role="TrG5h" value="path" />
      <ref role="AX2Wp" node="3JW8_axN3fd" resolve="Path" />
    </node>
    <node concept="1TJgyj" id="3JW8_axN3fe" role="1TKVEi">
      <property role="IQ2ns" value="4322367480666010574" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="operation" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="3JW8_axN3fg" resolve="OperationObject" />
    </node>
  </node>
  <node concept="Az7Fb" id="3JW8_axN3fd">
    <property role="3F6X1D" value="4322367480666010573" />
    <property role="TrG5h" value="Path" />
    <property role="FLfZY" value="/[a-z0-9]+" />
  </node>
  <node concept="1TIwiD" id="3JW8_axN3fg">
    <property role="EcuMT" value="4322367480666010576" />
    <property role="TrG5h" value="OperationObject" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyi" id="3JW8_axN3fB" role="1TKVEl">
      <property role="IQ2nx" value="4322367480666010599" />
      <property role="TrG5h" value="summary" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3JW8_axN3fI" role="1TKVEl">
      <property role="IQ2nx" value="4322367480666010606" />
      <property role="TrG5h" value="description" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="4favA_iAyf8" role="1TKVEi">
      <property role="IQ2ns" value="4884855736659223496" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="responses" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="4favA_iAyfb" resolve="Response" />
    </node>
  </node>
  <node concept="25R3W" id="3JW8_axN3fj">
    <property role="3F6X1D" value="4322367480666010579" />
    <property role="TrG5h" value="Operation" />
    <node concept="25R33" id="3JW8_axN3fk" role="25R1y">
      <property role="3tVfz5" value="4322367480666010580" />
      <property role="TrG5h" value="GET" />
    </node>
    <node concept="25R33" id="3JW8_axN3fl" role="25R1y">
      <property role="3tVfz5" value="4322367480666010581" />
      <property role="TrG5h" value="PUT" />
    </node>
    <node concept="25R33" id="3JW8_axN3fo" role="25R1y">
      <property role="3tVfz5" value="4322367480666010584" />
      <property role="TrG5h" value="POST" />
    </node>
    <node concept="25R33" id="3JW8_axN3fs" role="25R1y">
      <property role="3tVfz5" value="4322367480666010588" />
      <property role="TrG5h" value="DELETE" />
    </node>
    <node concept="25R33" id="3JW8_axN3fx" role="25R1y">
      <property role="3tVfz5" value="4322367480666010593" />
      <property role="TrG5h" value="UPDATE" />
    </node>
  </node>
  <node concept="1TIwiD" id="3JW8_axN3Pw">
    <property role="EcuMT" value="4322367480666013024" />
    <property role="TrG5h" value="GetOperationObject" />
    <property role="34LRSv" value="get" />
    <ref role="1TJDcQ" node="3JW8_axN3fg" resolve="OperationObject" />
  </node>
  <node concept="1TIwiD" id="4favA_iAyfb">
    <property role="EcuMT" value="4884855736659223499" />
    <property role="TrG5h" value="Response" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyi" id="4favA_iAyfc" role="1TKVEl">
      <property role="IQ2nx" value="4884855736659223500" />
      <property role="TrG5h" value="httpCode" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="4favA_iAyfe" role="1TKVEl">
      <property role="IQ2nx" value="4884855736659223502" />
      <property role="TrG5h" value="description" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="4favA_iB_5O" role="1TKVEi">
      <property role="IQ2ns" value="4884855736659497332" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="content" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="4favA_iB$7K" resolve="Content" />
    </node>
  </node>
  <node concept="1TIwiD" id="4favA_iBl1e">
    <property role="EcuMT" value="4884855736659431502" />
    <property role="TrG5h" value="PutOperationObject" />
    <property role="34LRSv" value="put" />
    <ref role="1TJDcQ" node="3JW8_axN3fg" resolve="OperationObject" />
  </node>
  <node concept="1TIwiD" id="4favA_iBl1f">
    <property role="EcuMT" value="4884855736659431503" />
    <property role="TrG5h" value="PostOperationObject" />
    <property role="34LRSv" value="post" />
    <ref role="1TJDcQ" node="3JW8_axN3fg" resolve="OperationObject" />
  </node>
  <node concept="1TIwiD" id="4favA_iBl1g">
    <property role="EcuMT" value="4884855736659431504" />
    <property role="TrG5h" value="DeleteOperationObject" />
    <property role="34LRSv" value="delete" />
    <ref role="1TJDcQ" node="3JW8_axN3fg" resolve="OperationObject" />
  </node>
  <node concept="1TIwiD" id="4favA_iB$7K">
    <property role="EcuMT" value="4884855736659493360" />
    <property role="TrG5h" value="Content" />
    <property role="34LRSv" value="content" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyi" id="4favA_iB$7L" role="1TKVEl">
      <property role="IQ2nx" value="4884855736659493361" />
      <property role="TrG5h" value="mediaType" />
      <ref role="AX2Wp" node="4favA_iB$7N" resolve="MediaType" />
    </node>
    <node concept="1TJgyj" id="4favA_iBNSA" role="1TKVEi">
      <property role="IQ2ns" value="4884855736659557926" />
      <property role="20kJfa" value="schema" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4favA_iBNMk" resolve="Schema" />
    </node>
  </node>
  <node concept="25R3W" id="4favA_iB$7N">
    <property role="3F6X1D" value="4884855736659493363" />
    <property role="TrG5h" value="MediaType" />
    <ref role="1H5jkz" node="4favA_iB$7O" resolve="APPLICATION_JSON" />
    <node concept="25R33" id="4favA_iB$7O" role="25R1y">
      <property role="3tVfz5" value="4884855736659493364" />
      <property role="TrG5h" value="APPLICATION_JSON" />
      <property role="1L1pqM" value="application/json" />
    </node>
    <node concept="25R33" id="4favA_iB$7P" role="25R1y">
      <property role="3tVfz5" value="4884855736659493365" />
      <property role="TrG5h" value="TEXT_HTML" />
      <property role="1L1pqM" value="text/html" />
    </node>
    <node concept="25R33" id="4favA_iB$7S" role="25R1y">
      <property role="3tVfz5" value="4884855736659493368" />
      <property role="TrG5h" value="TEXT_PLAIN" />
      <property role="1L1pqM" value="text/plain" />
    </node>
  </node>
  <node concept="1TIwiD" id="4favA_iBNMk">
    <property role="EcuMT" value="4884855736659557524" />
    <property role="TrG5h" value="Schema" />
    <property role="R5$K7" value="true" />
    <property role="34LRSv" value="schema" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyi" id="4favA_iBNMN" role="1TKVEl">
      <property role="IQ2nx" value="4884855736659557555" />
      <property role="TrG5h" value="name" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="4favA_iBNMH" role="1TKVEl">
      <property role="IQ2nx" value="4884855736659557549" />
      <property role="TrG5h" value="description" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="4favA_iBNMJ" role="1TKVEl">
      <property role="IQ2nx" value="4884855736659557551" />
      <property role="TrG5h" value="example" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="4favA_iBNMM">
    <property role="EcuMT" value="4884855736659557554" />
    <property role="TrG5h" value="StringSchema" />
    <property role="34LRSv" value="string" />
    <ref role="1TJDcQ" node="4favA_iBNMk" resolve="Schema" />
    <node concept="1TJgyi" id="3JsE8aiIcbJ" role="1TKVEl">
      <property role="IQ2nx" value="4313507821874889455" />
      <property role="TrG5h" value="minLength" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="3JsE8aiIcbN" role="1TKVEl">
      <property role="IQ2nx" value="4313507821874889459" />
      <property role="TrG5h" value="maxLength" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="4favA_iBNS8" role="1TKVEl">
      <property role="IQ2nx" value="4884855736659557896" />
      <property role="TrG5h" value="pattern" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="3JsE8ainSvo" role="1TKVEl">
      <property role="IQ2nx" value="4313507821869041624" />
      <property role="TrG5h" value="format" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="4favA_iBNSc">
    <property role="EcuMT" value="4884855736659557900" />
    <property role="TrG5h" value="IntegerSchema" />
    <property role="34LRSv" value="integer" />
    <ref role="1TJDcQ" node="4favA_iBNMk" resolve="Schema" />
    <node concept="1TJgyi" id="4favA_iBNSd" role="1TKVEl">
      <property role="IQ2nx" value="4884855736659557901" />
      <property role="TrG5h" value="minimum" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="4favA_iBNSe" role="1TKVEl">
      <property role="IQ2nx" value="4884855736659557902" />
      <property role="TrG5h" value="maximum" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="4favA_iBNSf" role="1TKVEl">
      <property role="IQ2nx" value="4884855736659557903" />
      <property role="TrG5h" value="pattern" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="4favA_iBNSC">
    <property role="EcuMT" value="4884855736659557928" />
    <property role="TrG5h" value="ObjectSchema" />
    <property role="34LRSv" value="object" />
    <ref role="1TJDcQ" node="4favA_iBNMk" resolve="Schema" />
    <node concept="1TJgyj" id="3JsE8aitKU3" role="1TKVEi">
      <property role="IQ2ns" value="4313507821870583427" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="required" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="3JsE8aiu$41" resolve="ObjectSchemaRequiredProperty" />
    </node>
    <node concept="1TJgyj" id="4favA_iBNSJ" role="1TKVEi">
      <property role="IQ2ns" value="4884855736659557935" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="properties" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="4favA_iBNSL" resolve="ObjectSchemaProperty" />
    </node>
    <node concept="1TJgyj" id="3JsE8ainS_0" role="1TKVEi">
      <property role="IQ2ns" value="4313507821869041984" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="additionalProperties" />
      <ref role="20lvS9" node="3JsE8ainS_3" resolve="ObjectSchemaAdditionalProperty" />
    </node>
  </node>
  <node concept="1TIwiD" id="4favA_iBNSL">
    <property role="EcuMT" value="4884855736659557937" />
    <property role="TrG5h" value="ObjectSchemaProperty" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyi" id="4favA_iBNSM" role="1TKVEl">
      <property role="IQ2nx" value="4884855736659557938" />
      <property role="TrG5h" value="name" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="4favA_iBNSO" role="1TKVEi">
      <property role="IQ2ns" value="4884855736659557940" />
      <property role="20kJfa" value="schema" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4favA_iBNMk" resolve="Schema" />
    </node>
  </node>
  <node concept="1TIwiD" id="4favA_iBNSQ">
    <property role="EcuMT" value="4884855736659557942" />
    <property role="TrG5h" value="ArraySchema" />
    <property role="34LRSv" value="array" />
    <ref role="1TJDcQ" node="4favA_iBNMk" resolve="Schema" />
    <node concept="1TJgyi" id="4favA_iBNSS" role="1TKVEl">
      <property role="IQ2nx" value="4884855736659557944" />
      <property role="TrG5h" value="minItems" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="4favA_iBNSU" role="1TKVEl">
      <property role="IQ2nx" value="4884855736659557946" />
      <property role="TrG5h" value="maxItems" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyj" id="4favA_iBNT3" role="1TKVEi">
      <property role="IQ2ns" value="4884855736659557955" />
      <property role="20kJfa" value="items" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4favA_iBNMk" resolve="Schema" />
    </node>
  </node>
  <node concept="1TIwiD" id="3JsE8ainS_3">
    <property role="EcuMT" value="4313507821869041987" />
    <property role="TrG5h" value="ObjectSchemaAdditionalProperty" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="3JsE8ainS_4" role="1TKVEl">
      <property role="IQ2nx" value="4313507821869041988" />
      <property role="TrG5h" value="type" />
      <ref role="AX2Wp" node="3JsE8ainS_I" resolve="DataType" />
    </node>
    <node concept="1TJgyj" id="3JsE8ainS_5" role="1TKVEi">
      <property role="IQ2ns" value="4313507821869041989" />
      <property role="20kJfa" value="schema" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4favA_iBNMk" resolve="Schema" />
    </node>
  </node>
  <node concept="25R3W" id="3JsE8ainS_I">
    <property role="3F6X1D" value="4313507821869042030" />
    <property role="TrG5h" value="DataType" />
    <node concept="25R33" id="3JsE8ainS_J" role="25R1y">
      <property role="3tVfz5" value="4313507821869042031" />
      <property role="TrG5h" value="STRING" />
      <property role="1L1pqM" value="string" />
    </node>
    <node concept="25R33" id="3JsE8ainS_K" role="25R1y">
      <property role="3tVfz5" value="4313507821869042032" />
      <property role="TrG5h" value="INTEGER" />
      <property role="1L1pqM" value="integer" />
    </node>
    <node concept="25R33" id="3JsE8ainS_N" role="25R1y">
      <property role="3tVfz5" value="4313507821869042035" />
      <property role="TrG5h" value="NUMBER" />
      <property role="1L1pqM" value="number" />
    </node>
    <node concept="25R33" id="3JsE8ainS_R" role="25R1y">
      <property role="3tVfz5" value="4313507821869042039" />
      <property role="TrG5h" value="BOOLEAN" />
      <property role="1L1pqM" value="boolean" />
    </node>
  </node>
  <node concept="1TIwiD" id="3JsE8aiu$41">
    <property role="EcuMT" value="4313507821870792961" />
    <property role="TrG5h" value="ObjectSchemaRequiredProperty" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="3JsE8aiu$43" role="1TKVEi">
      <property role="IQ2ns" value="4313507821870792963" />
      <property role="20kJfa" value="property" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4favA_iBNSL" resolve="ObjectSchemaProperty" />
    </node>
  </node>
  <node concept="1TIwiD" id="3JsE8aiAEqP">
    <property role="EcuMT" value="4313507821872916149" />
    <property role="TrG5h" value="BooleanSchema" />
    <property role="34LRSv" value="boolean" />
    <ref role="1TJDcQ" node="4favA_iBNMk" resolve="Schema" />
  </node>
  <node concept="1TIwiD" id="3JsE8aiB2oY">
    <property role="EcuMT" value="4313507821873014334" />
    <property role="TrG5h" value="OneOfSchema" />
    <property role="34LRSv" value="oneOf" />
    <ref role="1TJDcQ" node="4favA_iBNMk" resolve="Schema" />
    <node concept="1TJgyj" id="3JsE8aiB2oZ" role="1TKVEi">
      <property role="IQ2ns" value="4313507821873014335" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="oneOf" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="3JsE8aiBPdZ" resolve="SchemaRef" />
    </node>
  </node>
  <node concept="1TIwiD" id="3JsE8aiB2pG">
    <property role="EcuMT" value="4313507821873014380" />
    <property role="TrG5h" value="AllOfSchema" />
    <property role="34LRSv" value="allOf" />
    <ref role="1TJDcQ" node="4favA_iBNMk" resolve="Schema" />
    <node concept="1TJgyj" id="3JsE8aiB2pH" role="1TKVEi">
      <property role="IQ2ns" value="4313507821873014381" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="allOf" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="3JsE8aiBPdZ" resolve="SchemaRef" />
    </node>
  </node>
  <node concept="1TIwiD" id="3JsE8aiB2q4">
    <property role="EcuMT" value="4313507821873014404" />
    <property role="TrG5h" value="AnyOfSchema" />
    <property role="34LRSv" value="anyOf" />
    <ref role="1TJDcQ" node="4favA_iBNMk" resolve="Schema" />
    <node concept="1TJgyj" id="3JsE8aiB2q5" role="1TKVEi">
      <property role="IQ2ns" value="4313507821873014405" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="anyOf" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="3JsE8aiBPdZ" resolve="SchemaRef" />
    </node>
  </node>
  <node concept="1TIwiD" id="3JsE8aiBPdZ">
    <property role="EcuMT" value="4313507821873222527" />
    <property role="TrG5h" value="SchemaRef" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyj" id="3JsE8aiBPe0" role="1TKVEi">
      <property role="IQ2ns" value="4313507821873222528" />
      <property role="20kJfa" value="schema" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4favA_iBNMk" resolve="Schema" />
    </node>
  </node>
</model>

