<?xml version="1.0" encoding="UTF-8"?>
<solution name="de.thb.openapidsl.copy.sandbox" uuid="8575d7f8-41e5-416f-9ed8-13f66480e885" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <facets>
    <facet type="java">
      <classes generated="true" path="${module}/classes_gen" />
    </facet>
  </facets>
  <sourcePath />
  <languageVersions>
    <language slang="l:29d429a2-0531-4919-8e80-6706b89fb3b7:de.thb.openapidsl.copy" version="0" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="2" />
  </languageVersions>
  <dependencyVersions>
    <module reference="8575d7f8-41e5-416f-9ed8-13f66480e885(de.thb.openapidsl.copy.sandbox)" version="0" />
  </dependencyVersions>
</solution>

