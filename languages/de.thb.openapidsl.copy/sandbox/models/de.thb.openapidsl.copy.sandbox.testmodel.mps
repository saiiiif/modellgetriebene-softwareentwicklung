<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:2c801383-eca5-4e7a-aa05-83124f325a47(de.thb.openapidsl.copy.sandbox.testmodel)">
  <persistence version="9" />
  <languages>
    <use id="29d429a2-0531-4919-8e80-6706b89fb3b7" name="de.thb.openapidsl.copy" version="0" />
  </languages>
  <imports />
  <registry>
    <language id="29d429a2-0531-4919-8e80-6706b89fb3b7" name="de.thb.openapidsl.copy">
      <concept id="4884855736659223499" name="de.thb.openapidsl.copy.structure.Response" flags="ng" index="hk5Qy">
        <property id="4884855736659223500" name="httpCode" index="hk5Q_" />
        <property id="4884855736659223502" name="description" index="hk5QB" />
        <child id="4884855736659497332" name="content" index="hl2Wt" />
      </concept>
      <concept id="4884855736659493360" name="de.thb.openapidsl.copy.structure.Content" flags="ng" index="hl3Yp">
        <property id="4884855736659493361" name="mediaType" index="hl3Yo" />
        <reference id="4884855736659557926" name="schema" index="hlk1f" />
      </concept>
      <concept id="4884855736659557928" name="de.thb.openapidsl.copy.structure.ObjectSchema" flags="ng" index="hlk11">
        <child id="4884855736659557935" name="properties" index="hlk16" />
        <child id="4313507821870583427" name="required" index="2DEUo1" />
      </concept>
      <concept id="4884855736659557937" name="de.thb.openapidsl.copy.structure.ObjectSchemaProperty" flags="ng" index="hlk1o">
        <property id="4884855736659557938" name="name" index="hlk1r" />
        <reference id="4884855736659557940" name="schema" index="hlk1t" />
      </concept>
      <concept id="4884855736659557900" name="de.thb.openapidsl.copy.structure.IntegerSchema" flags="ng" index="hlk1_">
        <property id="4884855736659557901" name="minimum" index="hlk1$" />
        <property id="4884855736659557902" name="maximum" index="hlk1B" />
      </concept>
      <concept id="4884855736659557554" name="de.thb.openapidsl.copy.structure.StringSchema" flags="ng" index="hlkbr" />
      <concept id="4884855736659557524" name="de.thb.openapidsl.copy.structure.Schema" flags="ng" index="hlkbX">
        <property id="4884855736659557549" name="description" index="hlkb4" />
        <property id="4884855736659557555" name="name" index="hlkbq" />
      </concept>
      <concept id="4884855736659431503" name="de.thb.openapidsl.copy.structure.PostOperationObject" flags="ng" index="hlMSA" />
      <concept id="4322367480666010576" name="de.thb.openapidsl.copy.structure.OperationObject" flags="ng" index="2qc162">
        <property id="4322367480666010599" name="summary" index="2qc16P" />
        <child id="4884855736659223496" name="responses" index="hk5Qx" />
      </concept>
      <concept id="4322367480666010564" name="de.thb.openapidsl.copy.structure.PathObject" flags="ng" index="2qc16m">
        <property id="4322367480666010571" name="path" index="2qc16p" />
        <child id="4322367480666010574" name="operation" index="2qc16s" />
      </concept>
      <concept id="4322367480666013024" name="de.thb.openapidsl.copy.structure.GetOperationObject" flags="ng" index="2qc1WM" />
      <concept id="4322367480665913016" name="de.thb.openapidsl.copy.structure.OpenApiInfo" flags="ng" index="2qdDjE">
        <property id="4322367480665913024" name="version" index="2qdDii" />
        <property id="4322367480665913019" name="title" index="2qdDjD" />
      </concept>
      <concept id="4322367480665981422" name="de.thb.openapidsl.copy.structure.OpenApi" flags="ng" index="2qdYeW">
        <property id="4322367480665991592" name="openapi" index="2qdWJU" />
        <child id="4884855736659832367" name="schema" index="hqh16" />
        <child id="4322367480666011775" name="paths" index="2qc1CH" />
        <child id="4322367480665981423" name="info" index="2qdYeX" />
      </concept>
      <concept id="4313507821873014334" name="de.thb.openapidsl.copy.structure.OneOfSchema" flags="ng" index="2Dg8UW">
        <child id="4313507821873014335" name="oneOf" index="2Dg8UX" />
      </concept>
      <concept id="4313507821873014380" name="de.thb.openapidsl.copy.structure.AllOfSchema" flags="ng" index="2Dg8VI">
        <child id="4313507821873014381" name="allOf" index="2Dg8VJ" />
      </concept>
      <concept id="4313507821873222527" name="de.thb.openapidsl.copy.structure.SchemaRef" flags="ng" index="2DgZJX">
        <reference id="4313507821873222528" name="schema" index="2DgZG2" />
      </concept>
      <concept id="4313507821870792961" name="de.thb.openapidsl.copy.structure.ObjectSchemaRequiredProperty" flags="ng" index="2DDIA3">
        <reference id="4313507821870792963" name="property" index="2DDIA1" />
      </concept>
    </language>
  </registry>
  <node concept="2qdYeW" id="3JW8_axMWhR">
    <property role="2qdWJU" value="3.0.1" />
    <node concept="2qc16m" id="3JsE8aiFDUc" role="2qc1CH">
      <property role="2qc16p" value="/api" />
      <node concept="2qc1WM" id="3JsE8aiFDUk" role="2qc16s">
        <property role="2qc16P" value="t" />
        <node concept="hk5Qy" id="cAH7uXn4Jm" role="hk5Qx">
          <property role="hk5Q_" value="200" />
          <property role="hk5QB" value="jop" />
          <node concept="hl3Yp" id="cAH7uXn4Jt" role="hl2Wt">
            <property role="hl3Yo" value="4favA_iB$7P/TEXT_HTML" />
            <ref role="hlk1f" node="4favA_iEoVV" />
          </node>
        </node>
        <node concept="hk5Qy" id="cAH7uXn4K5" role="hk5Qx">
          <property role="hk5Q_" value="404" />
          <property role="hk5QB" value="asd" />
        </node>
      </node>
    </node>
    <node concept="hlkbr" id="9odBvb2do2" role="hqh16">
      <property role="hlkbq" value="testString" />
      <property role="hlkb4" value="Result of /test" />
    </node>
    <node concept="2qc16m" id="9odBvb2dnG" role="2qc1CH">
      <property role="2qc16p" value="/test" />
      <node concept="hlMSA" id="9odBvb2dnP" role="2qc16s">
        <property role="2qc16P" value="a" />
        <node concept="hk5Qy" id="9odBvb2dnR" role="hk5Qx">
          <property role="hk5Q_" value="200" />
          <property role="hk5QB" value="Ok" />
          <node concept="hl3Yp" id="9odBvb2dnS" role="hl2Wt">
            <ref role="hlk1f" node="4favA_iEoVV" />
          </node>
        </node>
      </node>
    </node>
    <node concept="hlkbr" id="4favA_iEoVV" role="hqh16">
      <property role="hlkbq" value="someString" />
      <property role="hlkb4" value="This is a string" />
    </node>
    <node concept="hlkbr" id="4favA_iEoW0" role="hqh16">
      <property role="hlkbq" value="anotherString" />
      <property role="hlkb4" value="This is a string too" />
    </node>
    <node concept="hlk1_" id="4favA_iEoW5" role="hqh16">
      <property role="hlkbq" value="anInteger" />
      <property role="hlkb4" value="This is an integer" />
      <property role="hlk1$" value="0" />
      <property role="hlk1B" value="10" />
    </node>
    <node concept="hlk11" id="3JsE8ai_W6$" role="hqh16">
      <property role="hlkbq" value="anObject" />
      <property role="hlkb4" value="test object" />
      <node concept="hlk1o" id="3JsE8ai_W6A" role="hlk16">
        <property role="hlk1r" value="x" />
        <ref role="hlk1t" node="4favA_iEoW5" />
      </node>
      <node concept="hlk1o" id="3JsE8aiFDU9" role="hlk16">
        <property role="hlk1r" value="str" />
        <ref role="hlk1t" node="4favA_iEoVV" />
      </node>
      <node concept="2DDIA3" id="3JsE8aiCDYG" role="2DEUo1">
        <ref role="2DDIA1" node="3JsE8ai_W6A" />
      </node>
    </node>
    <node concept="2Dg8UW" id="7kDFC_udlXw" role="hqh16">
      <property role="hlkbq" value="union" />
      <node concept="2DgZJX" id="7kDFC_udlYA" role="2Dg8UX">
        <ref role="2DgZG2" node="3JsE8ai_W6$" />
      </node>
      <node concept="2DgZJX" id="7kDFC_udlXy" role="2Dg8UX">
        <ref role="2DgZG2" node="4favA_iEoW5" />
      </node>
    </node>
    <node concept="2Dg8VI" id="7kDFC_udlZd" role="hqh16">
      <property role="hlkbq" value="intersection" />
      <node concept="2DgZJX" id="7kDFC_udlZX" role="2Dg8VJ">
        <ref role="2DgZG2" node="4favA_iEoW0" />
      </node>
      <node concept="2DgZJX" id="7kDFC_udlZf" role="2Dg8VJ">
        <ref role="2DgZG2" node="3JsE8ai_W6$" />
      </node>
    </node>
    <node concept="2qdDjE" id="3JW8_axMWhS" role="2qdYeX">
      <property role="2qdDii" value="0.1.0" />
      <property role="2qdDjD" value="Test" />
    </node>
  </node>
</model>

