<?xml version="1.0" encoding="UTF-8"?>
<model ref="00000000-0000-4000-5f02-5beb5f025beb/i:fc06d81(checkpoints/de.thb.openapidsl.copy.structure@descriptorclasses)">
  <persistence version="9" />
  <attribute name="checkpoint" value="DescriptorClasses" />
  <attribute name="generation-plan" value="AspectCPS" />
  <attribute name="user-objects" value="true" />
  <languages />
  <imports>
    <import index="7thy" ref="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)" />
    <import index="ksn4" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.lang.smodel(MPS.Core/)" />
    <import index="c17a" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.language(MPS.OpenAPI/)" />
    <import index="ze1i" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.runtime(MPS.Core/)" />
    <import index="e8bb" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.adapter.ids(MPS.Core/)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="vndm" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.language(MPS.Core/)" />
    <import index="bzg8" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.runtime.impl(MPS.Core/)" />
    <import index="mhfm" ref="3f233e7f-b8a6-46d2-a57f-795d56775243/java:org.jetbrains.annotations(Annotations/)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="nn" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1083260308424" name="jetbrains.mps.baseLanguage.structure.EnumConstantReference" flags="nn" index="Rm8GO">
        <reference id="1083260308426" name="enumConstantDeclaration" index="Rm8GQ" />
        <reference id="1144432896254" name="enumClass" index="1Px2BO" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070462154015" name="jetbrains.mps.baseLanguage.structure.StaticFieldDeclaration" flags="ig" index="Wx3nA">
        <property id="6468716278899126575" name="isVolatile" index="2dlcS1" />
        <property id="6468716278899125786" name="isTransient" index="2dld4O" />
      </concept>
      <concept id="1070475587102" name="jetbrains.mps.baseLanguage.structure.SuperConstructorInvocation" flags="nn" index="XkiVB" />
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg">
        <property id="8606350594693632173" name="isTransient" index="eg7rD" />
        <property id="1240249534625" name="isVolatile" index="34CwA1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <property id="1221565133444" name="isFinal" index="1EXbeo" />
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="4269842503726207156" name="jetbrains.mps.baseLanguage.structure.LongLiteral" flags="nn" index="1adDum">
        <property id="4269842503726207157" name="value" index="1adDun" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242867" name="jetbrains.mps.baseLanguage.structure.LongType" flags="in" index="3cpWsb" />
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
        <child id="1109201940907" name="parameter" index="11_B2D" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1163670490218" name="jetbrains.mps.baseLanguage.structure.SwitchStatement" flags="nn" index="3KaCP$">
        <child id="1163670592366" name="defaultBlock" index="3Kb1Dw" />
        <child id="1163670766145" name="expression" index="3KbGdf" />
        <child id="1163670772911" name="case" index="3KbHQx" />
      </concept>
      <concept id="1163670641947" name="jetbrains.mps.baseLanguage.structure.SwitchCase" flags="ng" index="3KbdKl">
        <child id="1163670677455" name="expression" index="3Kbmr1" />
        <child id="1163670683720" name="body" index="3Kbo56" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1116615150612" name="jetbrains.mps.baseLanguage.structure.ClassifierClassExpression" flags="nn" index="3VsKOn">
        <reference id="1116615189566" name="classifier" index="3VsUkX" />
      </concept>
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="5808518347809715508" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_InputNode" flags="nn" index="385nmt">
        <property id="5808518347809748738" name="presentation" index="385vuF" />
        <child id="5808518347809747118" name="node" index="385v07" />
      </concept>
      <concept id="3864140621129707969" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_Mappings" flags="nn" index="39dXUE">
        <child id="3864140621129713349" name="labels" index="39e2AI" />
      </concept>
      <concept id="3864140621129713351" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_NodeMapEntry" flags="nn" index="39e2AG">
        <property id="5843998055530255671" name="isNewRoot" index="2mV_xN" />
        <reference id="3864140621129713371" name="inputOrigin" index="39e2AK" />
        <child id="5808518347809748862" name="inputNode" index="385vvn" />
        <child id="3864140621129713365" name="outputNode" index="39e2AY" />
      </concept>
      <concept id="3864140621129713348" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_LabelEntry" flags="nn" index="39e2AJ">
        <property id="3864140621129715945" name="label" index="39e3Y2" />
        <child id="3864140621129715947" name="entries" index="39e3Y0" />
      </concept>
      <concept id="3864140621129713362" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_NodeRef" flags="nn" index="39e2AT">
        <reference id="3864140621129713363" name="node" index="39e2AS" />
      </concept>
      <concept id="3637169702552512264" name="jetbrains.mps.lang.generator.structure.ElementaryNodeId" flags="ng" index="3u3nmq">
        <property id="3637169702552512269" name="nodeId" index="3u3nmv" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="0">
    <property role="TrG5h" value="ConceptPresentationAspectImpl" />
    <node concept="3uibUv" id="1" role="1zkMxy">
      <ref role="3uigEE" to="ze1i:~ConceptPresentationAspectBase" resolve="ConceptPresentationAspectBase" />
    </node>
    <node concept="312cEg" id="2" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_AllOfSchema" />
      <node concept="3uibUv" id="s" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="t" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="3" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_AnyOfSchema" />
      <node concept="3uibUv" id="u" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="v" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="4" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_ArraySchema" />
      <node concept="3uibUv" id="w" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="x" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="5" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_BooleanSchema" />
      <node concept="3uibUv" id="y" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="z" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="6" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_Content" />
      <node concept="3uibUv" id="$" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="_" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="7" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_DeleteOperationObject" />
      <node concept="3uibUv" id="A" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="B" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="8" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_GetOperationObject" />
      <node concept="3uibUv" id="C" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="D" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="9" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_IntegerSchema" />
      <node concept="3uibUv" id="E" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="F" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="a" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_ObjectSchema" />
      <node concept="3uibUv" id="G" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="H" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="b" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_ObjectSchemaAdditionalProperty" />
      <node concept="3uibUv" id="I" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="J" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="c" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_ObjectSchemaProperty" />
      <node concept="3uibUv" id="K" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="L" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="d" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_ObjectSchemaRequiredProperty" />
      <node concept="3uibUv" id="M" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="N" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="e" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_OneOfSchema" />
      <node concept="3uibUv" id="O" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="P" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="f" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_OpenApi" />
      <node concept="3uibUv" id="Q" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="R" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="g" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_OpenApiInfo" />
      <node concept="3uibUv" id="S" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="T" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="h" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_OperationObject" />
      <node concept="3uibUv" id="U" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="V" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="i" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_PathObject" />
      <node concept="3uibUv" id="W" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="X" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="j" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_PostOperationObject" />
      <node concept="3uibUv" id="Y" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="Z" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="k" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_PutOperationObject" />
      <node concept="3uibUv" id="10" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="11" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="l" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_Response" />
      <node concept="3uibUv" id="12" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="13" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="m" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_Schema" />
      <node concept="3uibUv" id="14" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="15" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="n" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_SchemaRef" />
      <node concept="3uibUv" id="16" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="17" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="o" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_StringSchema" />
      <node concept="3uibUv" id="18" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="19" role="1B3o_S" />
    </node>
    <node concept="3Tm1VV" id="p" role="1B3o_S" />
    <node concept="2tJIrI" id="q" role="jymVt" />
    <node concept="3clFb_" id="r" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getDescriptor" />
      <property role="DiZV1" value="false" />
      <node concept="3Tm1VV" id="1a" role="1B3o_S" />
      <node concept="37vLTG" id="1b" role="3clF46">
        <property role="TrG5h" value="c" />
        <node concept="3uibUv" id="1g" role="1tU5fm">
          <ref role="3uigEE" to="c17a:~SAbstractConcept" resolve="SAbstractConcept" />
        </node>
      </node>
      <node concept="3clFbS" id="1c" role="3clF47">
        <node concept="3cpWs8" id="1h" role="3cqZAp">
          <node concept="3cpWsn" id="1k" role="3cpWs9">
            <property role="TrG5h" value="structureDescriptor" />
            <node concept="3uibUv" id="1l" role="1tU5fm">
              <ref role="3uigEE" node="o3" resolve="StructureAspectDescriptor" />
            </node>
            <node concept="10QFUN" id="1m" role="33vP2m">
              <node concept="3uibUv" id="1n" role="10QFUM">
                <ref role="3uigEE" node="o3" resolve="StructureAspectDescriptor" />
              </node>
              <node concept="2OqwBi" id="1o" role="10QFUP">
                <node concept="37vLTw" id="1p" role="2Oq$k0">
                  <ref role="3cqZAo" to="ze1i:~ConceptPresentationAspectBase.myLanguageRuntime" resolve="myLanguageRuntime" />
                </node>
                <node concept="liA8E" id="1q" role="2OqNvi">
                  <ref role="37wK5l" to="vndm:~LanguageRuntime.getAspect(java.lang.Class)" resolve="getAspect" />
                  <node concept="3VsKOn" id="1r" role="37wK5m">
                    <ref role="3VsUkX" to="ze1i:~StructureAspectDescriptor" resolve="StructureAspectDescriptor" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3KaCP$" id="1i" role="3cqZAp">
          <node concept="2OqwBi" id="1s" role="3KbGdf">
            <node concept="37vLTw" id="1O" role="2Oq$k0">
              <ref role="3cqZAo" node="1k" resolve="structureDescriptor" />
            </node>
            <node concept="liA8E" id="1P" role="2OqNvi">
              <ref role="37wK5l" node="oJ" resolve="internalIndex" />
              <node concept="37vLTw" id="1Q" role="37wK5m">
                <ref role="3cqZAo" node="1b" resolve="c" />
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="1t" role="3KbHQx">
            <node concept="3clFbS" id="1R" role="3Kbo56">
              <node concept="3clFbJ" id="1T" role="3cqZAp">
                <node concept="3clFbS" id="1V" role="3clFbx">
                  <node concept="3cpWs8" id="1X" role="3cqZAp">
                    <node concept="3cpWsn" id="20" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="21" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="22" role="33vP2m">
                        <node concept="1pGfFk" id="23" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="1Y" role="3cqZAp">
                    <node concept="2OqwBi" id="24" role="3clFbG">
                      <node concept="37vLTw" id="25" role="2Oq$k0">
                        <ref role="3cqZAo" node="20" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="26" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String)" resolve="rawPresentation" />
                        <uo k="s:originTrace" v="n:4313507821873014380" />
                        <node concept="Xl_RD" id="27" role="37wK5m">
                          <property role="Xl_RC" value="allOf" />
                          <uo k="s:originTrace" v="n:4313507821873014380" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="1Z" role="3cqZAp">
                    <node concept="37vLTI" id="28" role="3clFbG">
                      <node concept="2OqwBi" id="29" role="37vLTx">
                        <node concept="37vLTw" id="2b" role="2Oq$k0">
                          <ref role="3cqZAo" node="20" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="2c" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="2a" role="37vLTJ">
                        <ref role="3cqZAo" node="2" resolve="props_AllOfSchema" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="1W" role="3clFbw">
                  <node concept="10Nm6u" id="2d" role="3uHU7w" />
                  <node concept="37vLTw" id="2e" role="3uHU7B">
                    <ref role="3cqZAo" node="2" resolve="props_AllOfSchema" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="1U" role="3cqZAp">
                <node concept="37vLTw" id="2f" role="3cqZAk">
                  <ref role="3cqZAo" node="2" resolve="props_AllOfSchema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="1S" role="3Kbmr1">
              <ref role="3cqZAo" node="jL" resolve="AllOfSchema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1u" role="3KbHQx">
            <node concept="3clFbS" id="2g" role="3Kbo56">
              <node concept="3clFbJ" id="2i" role="3cqZAp">
                <node concept="3clFbS" id="2k" role="3clFbx">
                  <node concept="3cpWs8" id="2m" role="3cqZAp">
                    <node concept="3cpWsn" id="2p" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="2q" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="2r" role="33vP2m">
                        <node concept="1pGfFk" id="2s" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="2n" role="3cqZAp">
                    <node concept="2OqwBi" id="2t" role="3clFbG">
                      <node concept="37vLTw" id="2u" role="2Oq$k0">
                        <ref role="3cqZAo" node="2p" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="2v" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String)" resolve="rawPresentation" />
                        <uo k="s:originTrace" v="n:4313507821873014404" />
                        <node concept="Xl_RD" id="2w" role="37wK5m">
                          <property role="Xl_RC" value="anyOf" />
                          <uo k="s:originTrace" v="n:4313507821873014404" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="2o" role="3cqZAp">
                    <node concept="37vLTI" id="2x" role="3clFbG">
                      <node concept="2OqwBi" id="2y" role="37vLTx">
                        <node concept="37vLTw" id="2$" role="2Oq$k0">
                          <ref role="3cqZAo" node="2p" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="2_" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="2z" role="37vLTJ">
                        <ref role="3cqZAo" node="3" resolve="props_AnyOfSchema" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="2l" role="3clFbw">
                  <node concept="10Nm6u" id="2A" role="3uHU7w" />
                  <node concept="37vLTw" id="2B" role="3uHU7B">
                    <ref role="3cqZAo" node="3" resolve="props_AnyOfSchema" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="2j" role="3cqZAp">
                <node concept="37vLTw" id="2C" role="3cqZAk">
                  <ref role="3cqZAo" node="3" resolve="props_AnyOfSchema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="2h" role="3Kbmr1">
              <ref role="3cqZAo" node="jM" resolve="AnyOfSchema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1v" role="3KbHQx">
            <node concept="3clFbS" id="2D" role="3Kbo56">
              <node concept="3clFbJ" id="2F" role="3cqZAp">
                <node concept="3clFbS" id="2H" role="3clFbx">
                  <node concept="3cpWs8" id="2J" role="3cqZAp">
                    <node concept="3cpWsn" id="2M" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="2N" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="2O" role="33vP2m">
                        <node concept="1pGfFk" id="2P" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="2K" role="3cqZAp">
                    <node concept="2OqwBi" id="2Q" role="3clFbG">
                      <node concept="37vLTw" id="2R" role="2Oq$k0">
                        <ref role="3cqZAo" node="2M" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="2S" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String)" resolve="rawPresentation" />
                        <uo k="s:originTrace" v="n:4884855736659557942" />
                        <node concept="Xl_RD" id="2T" role="37wK5m">
                          <property role="Xl_RC" value="array" />
                          <uo k="s:originTrace" v="n:4884855736659557942" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="2L" role="3cqZAp">
                    <node concept="37vLTI" id="2U" role="3clFbG">
                      <node concept="2OqwBi" id="2V" role="37vLTx">
                        <node concept="37vLTw" id="2X" role="2Oq$k0">
                          <ref role="3cqZAo" node="2M" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="2Y" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="2W" role="37vLTJ">
                        <ref role="3cqZAo" node="4" resolve="props_ArraySchema" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="2I" role="3clFbw">
                  <node concept="10Nm6u" id="2Z" role="3uHU7w" />
                  <node concept="37vLTw" id="30" role="3uHU7B">
                    <ref role="3cqZAo" node="4" resolve="props_ArraySchema" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="2G" role="3cqZAp">
                <node concept="37vLTw" id="31" role="3cqZAk">
                  <ref role="3cqZAo" node="4" resolve="props_ArraySchema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="2E" role="3Kbmr1">
              <ref role="3cqZAo" node="jN" resolve="ArraySchema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1w" role="3KbHQx">
            <node concept="3clFbS" id="32" role="3Kbo56">
              <node concept="3clFbJ" id="34" role="3cqZAp">
                <node concept="3clFbS" id="36" role="3clFbx">
                  <node concept="3cpWs8" id="38" role="3cqZAp">
                    <node concept="3cpWsn" id="3b" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="3c" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="3d" role="33vP2m">
                        <node concept="1pGfFk" id="3e" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="39" role="3cqZAp">
                    <node concept="2OqwBi" id="3f" role="3clFbG">
                      <node concept="37vLTw" id="3g" role="2Oq$k0">
                        <ref role="3cqZAo" node="3b" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="3h" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String)" resolve="rawPresentation" />
                        <uo k="s:originTrace" v="n:4313507821872916149" />
                        <node concept="Xl_RD" id="3i" role="37wK5m">
                          <property role="Xl_RC" value="boolean" />
                          <uo k="s:originTrace" v="n:4313507821872916149" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="3a" role="3cqZAp">
                    <node concept="37vLTI" id="3j" role="3clFbG">
                      <node concept="2OqwBi" id="3k" role="37vLTx">
                        <node concept="37vLTw" id="3m" role="2Oq$k0">
                          <ref role="3cqZAo" node="3b" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="3n" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="3l" role="37vLTJ">
                        <ref role="3cqZAo" node="5" resolve="props_BooleanSchema" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="37" role="3clFbw">
                  <node concept="10Nm6u" id="3o" role="3uHU7w" />
                  <node concept="37vLTw" id="3p" role="3uHU7B">
                    <ref role="3cqZAo" node="5" resolve="props_BooleanSchema" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="35" role="3cqZAp">
                <node concept="37vLTw" id="3q" role="3cqZAk">
                  <ref role="3cqZAo" node="5" resolve="props_BooleanSchema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="33" role="3Kbmr1">
              <ref role="3cqZAo" node="jO" resolve="BooleanSchema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1x" role="3KbHQx">
            <node concept="3clFbS" id="3r" role="3Kbo56">
              <node concept="3clFbJ" id="3t" role="3cqZAp">
                <node concept="3clFbS" id="3v" role="3clFbx">
                  <node concept="3cpWs8" id="3x" role="3cqZAp">
                    <node concept="3cpWsn" id="3$" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="3_" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="3A" role="33vP2m">
                        <node concept="1pGfFk" id="3B" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="3y" role="3cqZAp">
                    <node concept="2OqwBi" id="3C" role="3clFbG">
                      <node concept="37vLTw" id="3D" role="2Oq$k0">
                        <ref role="3cqZAo" node="3$" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="3E" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String)" resolve="rawPresentation" />
                        <uo k="s:originTrace" v="n:4884855736659493360" />
                        <node concept="Xl_RD" id="3F" role="37wK5m">
                          <property role="Xl_RC" value="content" />
                          <uo k="s:originTrace" v="n:4884855736659493360" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="3z" role="3cqZAp">
                    <node concept="37vLTI" id="3G" role="3clFbG">
                      <node concept="2OqwBi" id="3H" role="37vLTx">
                        <node concept="37vLTw" id="3J" role="2Oq$k0">
                          <ref role="3cqZAo" node="3$" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="3K" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="3I" role="37vLTJ">
                        <ref role="3cqZAo" node="6" resolve="props_Content" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="3w" role="3clFbw">
                  <node concept="10Nm6u" id="3L" role="3uHU7w" />
                  <node concept="37vLTw" id="3M" role="3uHU7B">
                    <ref role="3cqZAo" node="6" resolve="props_Content" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="3u" role="3cqZAp">
                <node concept="37vLTw" id="3N" role="3cqZAk">
                  <ref role="3cqZAo" node="6" resolve="props_Content" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="3s" role="3Kbmr1">
              <ref role="3cqZAo" node="jP" resolve="Content" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1y" role="3KbHQx">
            <node concept="3clFbS" id="3O" role="3Kbo56">
              <node concept="3clFbJ" id="3Q" role="3cqZAp">
                <node concept="3clFbS" id="3S" role="3clFbx">
                  <node concept="3cpWs8" id="3U" role="3cqZAp">
                    <node concept="3cpWsn" id="3X" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="3Y" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="3Z" role="33vP2m">
                        <node concept="1pGfFk" id="40" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="3V" role="3cqZAp">
                    <node concept="2OqwBi" id="41" role="3clFbG">
                      <node concept="37vLTw" id="42" role="2Oq$k0">
                        <ref role="3cqZAo" node="3X" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="43" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String)" resolve="rawPresentation" />
                        <uo k="s:originTrace" v="n:4884855736659431504" />
                        <node concept="Xl_RD" id="44" role="37wK5m">
                          <property role="Xl_RC" value="delete" />
                          <uo k="s:originTrace" v="n:4884855736659431504" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="3W" role="3cqZAp">
                    <node concept="37vLTI" id="45" role="3clFbG">
                      <node concept="2OqwBi" id="46" role="37vLTx">
                        <node concept="37vLTw" id="48" role="2Oq$k0">
                          <ref role="3cqZAo" node="3X" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="49" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="47" role="37vLTJ">
                        <ref role="3cqZAo" node="7" resolve="props_DeleteOperationObject" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="3T" role="3clFbw">
                  <node concept="10Nm6u" id="4a" role="3uHU7w" />
                  <node concept="37vLTw" id="4b" role="3uHU7B">
                    <ref role="3cqZAo" node="7" resolve="props_DeleteOperationObject" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="3R" role="3cqZAp">
                <node concept="37vLTw" id="4c" role="3cqZAk">
                  <ref role="3cqZAo" node="7" resolve="props_DeleteOperationObject" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="3P" role="3Kbmr1">
              <ref role="3cqZAo" node="jQ" resolve="DeleteOperationObject" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1z" role="3KbHQx">
            <node concept="3clFbS" id="4d" role="3Kbo56">
              <node concept="3clFbJ" id="4f" role="3cqZAp">
                <node concept="3clFbS" id="4h" role="3clFbx">
                  <node concept="3cpWs8" id="4j" role="3cqZAp">
                    <node concept="3cpWsn" id="4m" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="4n" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="4o" role="33vP2m">
                        <node concept="1pGfFk" id="4p" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="4k" role="3cqZAp">
                    <node concept="2OqwBi" id="4q" role="3clFbG">
                      <node concept="37vLTw" id="4r" role="2Oq$k0">
                        <ref role="3cqZAo" node="4m" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="4s" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String)" resolve="rawPresentation" />
                        <uo k="s:originTrace" v="n:4322367480666013024" />
                        <node concept="Xl_RD" id="4t" role="37wK5m">
                          <property role="Xl_RC" value="get" />
                          <uo k="s:originTrace" v="n:4322367480666013024" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="4l" role="3cqZAp">
                    <node concept="37vLTI" id="4u" role="3clFbG">
                      <node concept="2OqwBi" id="4v" role="37vLTx">
                        <node concept="37vLTw" id="4x" role="2Oq$k0">
                          <ref role="3cqZAo" node="4m" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="4y" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="4w" role="37vLTJ">
                        <ref role="3cqZAo" node="8" resolve="props_GetOperationObject" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="4i" role="3clFbw">
                  <node concept="10Nm6u" id="4z" role="3uHU7w" />
                  <node concept="37vLTw" id="4$" role="3uHU7B">
                    <ref role="3cqZAo" node="8" resolve="props_GetOperationObject" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="4g" role="3cqZAp">
                <node concept="37vLTw" id="4_" role="3cqZAk">
                  <ref role="3cqZAo" node="8" resolve="props_GetOperationObject" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="4e" role="3Kbmr1">
              <ref role="3cqZAo" node="jR" resolve="GetOperationObject" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1$" role="3KbHQx">
            <node concept="3clFbS" id="4A" role="3Kbo56">
              <node concept="3clFbJ" id="4C" role="3cqZAp">
                <node concept="3clFbS" id="4E" role="3clFbx">
                  <node concept="3cpWs8" id="4G" role="3cqZAp">
                    <node concept="3cpWsn" id="4J" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="4K" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="4L" role="33vP2m">
                        <node concept="1pGfFk" id="4M" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="4H" role="3cqZAp">
                    <node concept="2OqwBi" id="4N" role="3clFbG">
                      <node concept="37vLTw" id="4O" role="2Oq$k0">
                        <ref role="3cqZAo" node="4J" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="4P" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String)" resolve="rawPresentation" />
                        <uo k="s:originTrace" v="n:4884855736659557900" />
                        <node concept="Xl_RD" id="4Q" role="37wK5m">
                          <property role="Xl_RC" value="integer" />
                          <uo k="s:originTrace" v="n:4884855736659557900" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="4I" role="3cqZAp">
                    <node concept="37vLTI" id="4R" role="3clFbG">
                      <node concept="2OqwBi" id="4S" role="37vLTx">
                        <node concept="37vLTw" id="4U" role="2Oq$k0">
                          <ref role="3cqZAo" node="4J" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="4V" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="4T" role="37vLTJ">
                        <ref role="3cqZAo" node="9" resolve="props_IntegerSchema" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="4F" role="3clFbw">
                  <node concept="10Nm6u" id="4W" role="3uHU7w" />
                  <node concept="37vLTw" id="4X" role="3uHU7B">
                    <ref role="3cqZAo" node="9" resolve="props_IntegerSchema" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="4D" role="3cqZAp">
                <node concept="37vLTw" id="4Y" role="3cqZAk">
                  <ref role="3cqZAo" node="9" resolve="props_IntegerSchema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="4B" role="3Kbmr1">
              <ref role="3cqZAo" node="jS" resolve="IntegerSchema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1_" role="3KbHQx">
            <node concept="3clFbS" id="4Z" role="3Kbo56">
              <node concept="3clFbJ" id="51" role="3cqZAp">
                <node concept="3clFbS" id="53" role="3clFbx">
                  <node concept="3cpWs8" id="55" role="3cqZAp">
                    <node concept="3cpWsn" id="58" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="59" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="5a" role="33vP2m">
                        <node concept="1pGfFk" id="5b" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="56" role="3cqZAp">
                    <node concept="2OqwBi" id="5c" role="3clFbG">
                      <node concept="37vLTw" id="5d" role="2Oq$k0">
                        <ref role="3cqZAo" node="58" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="5e" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String)" resolve="rawPresentation" />
                        <uo k="s:originTrace" v="n:4884855736659557928" />
                        <node concept="Xl_RD" id="5f" role="37wK5m">
                          <property role="Xl_RC" value="object" />
                          <uo k="s:originTrace" v="n:4884855736659557928" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="57" role="3cqZAp">
                    <node concept="37vLTI" id="5g" role="3clFbG">
                      <node concept="2OqwBi" id="5h" role="37vLTx">
                        <node concept="37vLTw" id="5j" role="2Oq$k0">
                          <ref role="3cqZAo" node="58" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="5k" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="5i" role="37vLTJ">
                        <ref role="3cqZAo" node="a" resolve="props_ObjectSchema" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="54" role="3clFbw">
                  <node concept="10Nm6u" id="5l" role="3uHU7w" />
                  <node concept="37vLTw" id="5m" role="3uHU7B">
                    <ref role="3cqZAo" node="a" resolve="props_ObjectSchema" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="52" role="3cqZAp">
                <node concept="37vLTw" id="5n" role="3cqZAk">
                  <ref role="3cqZAo" node="a" resolve="props_ObjectSchema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="50" role="3Kbmr1">
              <ref role="3cqZAo" node="jT" resolve="ObjectSchema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1A" role="3KbHQx">
            <node concept="3clFbS" id="5o" role="3Kbo56">
              <node concept="3clFbJ" id="5q" role="3cqZAp">
                <node concept="3clFbS" id="5s" role="3clFbx">
                  <node concept="3cpWs8" id="5u" role="3cqZAp">
                    <node concept="3cpWsn" id="5x" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="5y" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="5z" role="33vP2m">
                        <node concept="1pGfFk" id="5$" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="5v" role="3cqZAp">
                    <node concept="2OqwBi" id="5_" role="3clFbG">
                      <node concept="37vLTw" id="5A" role="2Oq$k0">
                        <ref role="3cqZAo" node="5x" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="5B" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.presentationByReference(long,long,long,long,java.lang.String,java.lang.String,java.lang.String)" resolve="presentationByReference" />
                        <uo k="s:originTrace" v="n:4313507821869041987" />
                        <node concept="1adDum" id="5C" role="37wK5m">
                          <property role="1adDun" value="0x29d429a205314919L" />
                          <uo k="s:originTrace" v="n:4313507821869041987" />
                        </node>
                        <node concept="1adDum" id="5D" role="37wK5m">
                          <property role="1adDun" value="0x8e806706b89fb3b7L" />
                          <uo k="s:originTrace" v="n:4313507821869041987" />
                        </node>
                        <node concept="1adDum" id="5E" role="37wK5m">
                          <property role="1adDun" value="0x3bdca882925f8943L" />
                          <uo k="s:originTrace" v="n:4313507821869041987" />
                        </node>
                        <node concept="1adDum" id="5F" role="37wK5m">
                          <property role="1adDun" value="0x3bdca882925f8945L" />
                          <uo k="s:originTrace" v="n:4313507821869041987" />
                        </node>
                        <node concept="Xl_RD" id="5G" role="37wK5m">
                          <property role="Xl_RC" value="schema" />
                          <uo k="s:originTrace" v="n:4313507821869041987" />
                        </node>
                        <node concept="Xl_RD" id="5H" role="37wK5m">
                          <property role="Xl_RC" value="" />
                          <uo k="s:originTrace" v="n:4313507821869041987" />
                        </node>
                        <node concept="Xl_RD" id="5I" role="37wK5m">
                          <property role="Xl_RC" value="" />
                          <uo k="s:originTrace" v="n:4313507821869041987" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="5w" role="3cqZAp">
                    <node concept="37vLTI" id="5J" role="3clFbG">
                      <node concept="2OqwBi" id="5K" role="37vLTx">
                        <node concept="37vLTw" id="5M" role="2Oq$k0">
                          <ref role="3cqZAo" node="5x" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="5N" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="5L" role="37vLTJ">
                        <ref role="3cqZAo" node="b" resolve="props_ObjectSchemaAdditionalProperty" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="5t" role="3clFbw">
                  <node concept="10Nm6u" id="5O" role="3uHU7w" />
                  <node concept="37vLTw" id="5P" role="3uHU7B">
                    <ref role="3cqZAo" node="b" resolve="props_ObjectSchemaAdditionalProperty" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="5r" role="3cqZAp">
                <node concept="37vLTw" id="5Q" role="3cqZAk">
                  <ref role="3cqZAo" node="b" resolve="props_ObjectSchemaAdditionalProperty" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="5p" role="3Kbmr1">
              <ref role="3cqZAo" node="jU" resolve="ObjectSchemaAdditionalProperty" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1B" role="3KbHQx">
            <node concept="3clFbS" id="5R" role="3Kbo56">
              <node concept="3clFbJ" id="5T" role="3cqZAp">
                <node concept="3clFbS" id="5V" role="3clFbx">
                  <node concept="3cpWs8" id="5X" role="3cqZAp">
                    <node concept="3cpWsn" id="60" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="61" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="62" role="33vP2m">
                        <node concept="1pGfFk" id="63" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="5Y" role="3cqZAp">
                    <node concept="2OqwBi" id="64" role="3clFbG">
                      <node concept="37vLTw" id="65" role="2Oq$k0">
                        <ref role="3cqZAo" node="60" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="66" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.presentationByReference(long,long,long,long,java.lang.String,java.lang.String,java.lang.String)" resolve="presentationByReference" />
                        <uo k="s:originTrace" v="n:4884855736659557937" />
                        <node concept="1adDum" id="67" role="37wK5m">
                          <property role="1adDun" value="0x29d429a205314919L" />
                          <uo k="s:originTrace" v="n:4884855736659557937" />
                        </node>
                        <node concept="1adDum" id="68" role="37wK5m">
                          <property role="1adDun" value="0x8e806706b89fb3b7L" />
                          <uo k="s:originTrace" v="n:4884855736659557937" />
                        </node>
                        <node concept="1adDum" id="69" role="37wK5m">
                          <property role="1adDun" value="0x43ca7e69529f3e31L" />
                          <uo k="s:originTrace" v="n:4884855736659557937" />
                        </node>
                        <node concept="1adDum" id="6a" role="37wK5m">
                          <property role="1adDun" value="0x43ca7e69529f3e34L" />
                          <uo k="s:originTrace" v="n:4884855736659557937" />
                        </node>
                        <node concept="Xl_RD" id="6b" role="37wK5m">
                          <property role="Xl_RC" value="schema" />
                          <uo k="s:originTrace" v="n:4884855736659557937" />
                        </node>
                        <node concept="Xl_RD" id="6c" role="37wK5m">
                          <property role="Xl_RC" value="" />
                          <uo k="s:originTrace" v="n:4884855736659557937" />
                        </node>
                        <node concept="Xl_RD" id="6d" role="37wK5m">
                          <property role="Xl_RC" value="" />
                          <uo k="s:originTrace" v="n:4884855736659557937" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="5Z" role="3cqZAp">
                    <node concept="37vLTI" id="6e" role="3clFbG">
                      <node concept="2OqwBi" id="6f" role="37vLTx">
                        <node concept="37vLTw" id="6h" role="2Oq$k0">
                          <ref role="3cqZAo" node="60" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="6i" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="6g" role="37vLTJ">
                        <ref role="3cqZAo" node="c" resolve="props_ObjectSchemaProperty" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="5W" role="3clFbw">
                  <node concept="10Nm6u" id="6j" role="3uHU7w" />
                  <node concept="37vLTw" id="6k" role="3uHU7B">
                    <ref role="3cqZAo" node="c" resolve="props_ObjectSchemaProperty" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="5U" role="3cqZAp">
                <node concept="37vLTw" id="6l" role="3cqZAk">
                  <ref role="3cqZAo" node="c" resolve="props_ObjectSchemaProperty" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="5S" role="3Kbmr1">
              <ref role="3cqZAo" node="jV" resolve="ObjectSchemaProperty" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1C" role="3KbHQx">
            <node concept="3clFbS" id="6m" role="3Kbo56">
              <node concept="3clFbJ" id="6o" role="3cqZAp">
                <node concept="3clFbS" id="6q" role="3clFbx">
                  <node concept="3cpWs8" id="6s" role="3cqZAp">
                    <node concept="3cpWsn" id="6v" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="6w" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="6x" role="33vP2m">
                        <node concept="1pGfFk" id="6y" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="6t" role="3cqZAp">
                    <node concept="2OqwBi" id="6z" role="3clFbG">
                      <node concept="37vLTw" id="6$" role="2Oq$k0">
                        <ref role="3cqZAo" node="6v" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="6_" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.presentationByReference(long,long,long,long,java.lang.String,java.lang.String,java.lang.String)" resolve="presentationByReference" />
                        <uo k="s:originTrace" v="n:4313507821870792961" />
                        <node concept="1adDum" id="6A" role="37wK5m">
                          <property role="1adDun" value="0x29d429a205314919L" />
                          <uo k="s:originTrace" v="n:4313507821870792961" />
                        </node>
                        <node concept="1adDum" id="6B" role="37wK5m">
                          <property role="1adDun" value="0x8e806706b89fb3b7L" />
                          <uo k="s:originTrace" v="n:4313507821870792961" />
                        </node>
                        <node concept="1adDum" id="6C" role="37wK5m">
                          <property role="1adDun" value="0x3bdca882927a4101L" />
                          <uo k="s:originTrace" v="n:4313507821870792961" />
                        </node>
                        <node concept="1adDum" id="6D" role="37wK5m">
                          <property role="1adDun" value="0x3bdca882927a4103L" />
                          <uo k="s:originTrace" v="n:4313507821870792961" />
                        </node>
                        <node concept="Xl_RD" id="6E" role="37wK5m">
                          <property role="Xl_RC" value="property" />
                          <uo k="s:originTrace" v="n:4313507821870792961" />
                        </node>
                        <node concept="Xl_RD" id="6F" role="37wK5m">
                          <property role="Xl_RC" value="" />
                          <uo k="s:originTrace" v="n:4313507821870792961" />
                        </node>
                        <node concept="Xl_RD" id="6G" role="37wK5m">
                          <property role="Xl_RC" value="" />
                          <uo k="s:originTrace" v="n:4313507821870792961" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="6u" role="3cqZAp">
                    <node concept="37vLTI" id="6H" role="3clFbG">
                      <node concept="2OqwBi" id="6I" role="37vLTx">
                        <node concept="37vLTw" id="6K" role="2Oq$k0">
                          <ref role="3cqZAo" node="6v" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="6L" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="6J" role="37vLTJ">
                        <ref role="3cqZAo" node="d" resolve="props_ObjectSchemaRequiredProperty" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="6r" role="3clFbw">
                  <node concept="10Nm6u" id="6M" role="3uHU7w" />
                  <node concept="37vLTw" id="6N" role="3uHU7B">
                    <ref role="3cqZAo" node="d" resolve="props_ObjectSchemaRequiredProperty" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="6p" role="3cqZAp">
                <node concept="37vLTw" id="6O" role="3cqZAk">
                  <ref role="3cqZAo" node="d" resolve="props_ObjectSchemaRequiredProperty" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="6n" role="3Kbmr1">
              <ref role="3cqZAo" node="jW" resolve="ObjectSchemaRequiredProperty" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1D" role="3KbHQx">
            <node concept="3clFbS" id="6P" role="3Kbo56">
              <node concept="3clFbJ" id="6R" role="3cqZAp">
                <node concept="3clFbS" id="6T" role="3clFbx">
                  <node concept="3cpWs8" id="6V" role="3cqZAp">
                    <node concept="3cpWsn" id="6Y" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="6Z" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="70" role="33vP2m">
                        <node concept="1pGfFk" id="71" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="6W" role="3cqZAp">
                    <node concept="2OqwBi" id="72" role="3clFbG">
                      <node concept="37vLTw" id="73" role="2Oq$k0">
                        <ref role="3cqZAo" node="6Y" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="74" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String)" resolve="rawPresentation" />
                        <uo k="s:originTrace" v="n:4313507821873014334" />
                        <node concept="Xl_RD" id="75" role="37wK5m">
                          <property role="Xl_RC" value="oneOf" />
                          <uo k="s:originTrace" v="n:4313507821873014334" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="6X" role="3cqZAp">
                    <node concept="37vLTI" id="76" role="3clFbG">
                      <node concept="2OqwBi" id="77" role="37vLTx">
                        <node concept="37vLTw" id="79" role="2Oq$k0">
                          <ref role="3cqZAo" node="6Y" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="7a" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="78" role="37vLTJ">
                        <ref role="3cqZAo" node="e" resolve="props_OneOfSchema" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="6U" role="3clFbw">
                  <node concept="10Nm6u" id="7b" role="3uHU7w" />
                  <node concept="37vLTw" id="7c" role="3uHU7B">
                    <ref role="3cqZAo" node="e" resolve="props_OneOfSchema" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="6S" role="3cqZAp">
                <node concept="37vLTw" id="7d" role="3cqZAk">
                  <ref role="3cqZAo" node="e" resolve="props_OneOfSchema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="6Q" role="3Kbmr1">
              <ref role="3cqZAo" node="jX" resolve="OneOfSchema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1E" role="3KbHQx">
            <node concept="3clFbS" id="7e" role="3Kbo56">
              <node concept="3clFbJ" id="7g" role="3cqZAp">
                <node concept="3clFbS" id="7i" role="3clFbx">
                  <node concept="3cpWs8" id="7k" role="3cqZAp">
                    <node concept="3cpWsn" id="7n" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="7o" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="7p" role="33vP2m">
                        <node concept="1pGfFk" id="7q" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="7l" role="3cqZAp">
                    <node concept="2OqwBi" id="7r" role="3clFbG">
                      <node concept="37vLTw" id="7s" role="2Oq$k0">
                        <ref role="3cqZAo" node="7n" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="7t" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String)" resolve="rawPresentation" />
                        <uo k="s:originTrace" v="n:4322367480665981422" />
                        <node concept="Xl_RD" id="7u" role="37wK5m">
                          <property role="Xl_RC" value="OpenApi" />
                          <uo k="s:originTrace" v="n:4322367480665981422" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="7m" role="3cqZAp">
                    <node concept="37vLTI" id="7v" role="3clFbG">
                      <node concept="2OqwBi" id="7w" role="37vLTx">
                        <node concept="37vLTw" id="7y" role="2Oq$k0">
                          <ref role="3cqZAo" node="7n" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="7z" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="7x" role="37vLTJ">
                        <ref role="3cqZAo" node="f" resolve="props_OpenApi" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="7j" role="3clFbw">
                  <node concept="10Nm6u" id="7$" role="3uHU7w" />
                  <node concept="37vLTw" id="7_" role="3uHU7B">
                    <ref role="3cqZAo" node="f" resolve="props_OpenApi" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="7h" role="3cqZAp">
                <node concept="37vLTw" id="7A" role="3cqZAk">
                  <ref role="3cqZAo" node="f" resolve="props_OpenApi" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="7f" role="3Kbmr1">
              <ref role="3cqZAo" node="jY" resolve="OpenApi" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1F" role="3KbHQx">
            <node concept="3clFbS" id="7B" role="3Kbo56">
              <node concept="3clFbJ" id="7D" role="3cqZAp">
                <node concept="3clFbS" id="7F" role="3clFbx">
                  <node concept="3cpWs8" id="7H" role="3cqZAp">
                    <node concept="3cpWsn" id="7K" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="7L" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="7M" role="33vP2m">
                        <node concept="1pGfFk" id="7N" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="7I" role="3cqZAp">
                    <node concept="2OqwBi" id="7O" role="3clFbG">
                      <node concept="37vLTw" id="7P" role="2Oq$k0">
                        <ref role="3cqZAo" node="7K" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="7Q" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.presentationByName()" resolve="presentationByName" />
                        <uo k="s:originTrace" v="n:4322367480665913016" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="7J" role="3cqZAp">
                    <node concept="37vLTI" id="7R" role="3clFbG">
                      <node concept="2OqwBi" id="7S" role="37vLTx">
                        <node concept="37vLTw" id="7U" role="2Oq$k0">
                          <ref role="3cqZAo" node="7K" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="7V" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="7T" role="37vLTJ">
                        <ref role="3cqZAo" node="g" resolve="props_OpenApiInfo" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="7G" role="3clFbw">
                  <node concept="10Nm6u" id="7W" role="3uHU7w" />
                  <node concept="37vLTw" id="7X" role="3uHU7B">
                    <ref role="3cqZAo" node="g" resolve="props_OpenApiInfo" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="7E" role="3cqZAp">
                <node concept="37vLTw" id="7Y" role="3cqZAk">
                  <ref role="3cqZAo" node="g" resolve="props_OpenApiInfo" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="7C" role="3Kbmr1">
              <ref role="3cqZAo" node="jZ" resolve="OpenApiInfo" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1G" role="3KbHQx">
            <node concept="3clFbS" id="7Z" role="3Kbo56">
              <node concept="3clFbJ" id="81" role="3cqZAp">
                <node concept="3clFbS" id="83" role="3clFbx">
                  <node concept="3cpWs8" id="85" role="3cqZAp">
                    <node concept="3cpWsn" id="87" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="88" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="89" role="33vP2m">
                        <node concept="1pGfFk" id="8a" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="86" role="3cqZAp">
                    <node concept="37vLTI" id="8b" role="3clFbG">
                      <node concept="2OqwBi" id="8c" role="37vLTx">
                        <node concept="37vLTw" id="8e" role="2Oq$k0">
                          <ref role="3cqZAo" node="87" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="8f" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="8d" role="37vLTJ">
                        <ref role="3cqZAo" node="h" resolve="props_OperationObject" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="84" role="3clFbw">
                  <node concept="10Nm6u" id="8g" role="3uHU7w" />
                  <node concept="37vLTw" id="8h" role="3uHU7B">
                    <ref role="3cqZAo" node="h" resolve="props_OperationObject" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="82" role="3cqZAp">
                <node concept="37vLTw" id="8i" role="3cqZAk">
                  <ref role="3cqZAo" node="h" resolve="props_OperationObject" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="80" role="3Kbmr1">
              <ref role="3cqZAo" node="k0" resolve="OperationObject" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1H" role="3KbHQx">
            <node concept="3clFbS" id="8j" role="3Kbo56">
              <node concept="3clFbJ" id="8l" role="3cqZAp">
                <node concept="3clFbS" id="8n" role="3clFbx">
                  <node concept="3cpWs8" id="8p" role="3cqZAp">
                    <node concept="3cpWsn" id="8s" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="8t" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="8u" role="33vP2m">
                        <node concept="1pGfFk" id="8v" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="8q" role="3cqZAp">
                    <node concept="2OqwBi" id="8w" role="3clFbG">
                      <node concept="37vLTw" id="8x" role="2Oq$k0">
                        <ref role="3cqZAo" node="8s" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="8y" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String)" resolve="rawPresentation" />
                        <uo k="s:originTrace" v="n:4322367480666010564" />
                        <node concept="Xl_RD" id="8z" role="37wK5m">
                          <property role="Xl_RC" value="PathObject" />
                          <uo k="s:originTrace" v="n:4322367480666010564" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="8r" role="3cqZAp">
                    <node concept="37vLTI" id="8$" role="3clFbG">
                      <node concept="2OqwBi" id="8_" role="37vLTx">
                        <node concept="37vLTw" id="8B" role="2Oq$k0">
                          <ref role="3cqZAo" node="8s" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="8C" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="8A" role="37vLTJ">
                        <ref role="3cqZAo" node="i" resolve="props_PathObject" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="8o" role="3clFbw">
                  <node concept="10Nm6u" id="8D" role="3uHU7w" />
                  <node concept="37vLTw" id="8E" role="3uHU7B">
                    <ref role="3cqZAo" node="i" resolve="props_PathObject" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="8m" role="3cqZAp">
                <node concept="37vLTw" id="8F" role="3cqZAk">
                  <ref role="3cqZAo" node="i" resolve="props_PathObject" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="8k" role="3Kbmr1">
              <ref role="3cqZAo" node="k1" resolve="PathObject" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1I" role="3KbHQx">
            <node concept="3clFbS" id="8G" role="3Kbo56">
              <node concept="3clFbJ" id="8I" role="3cqZAp">
                <node concept="3clFbS" id="8K" role="3clFbx">
                  <node concept="3cpWs8" id="8M" role="3cqZAp">
                    <node concept="3cpWsn" id="8P" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="8Q" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="8R" role="33vP2m">
                        <node concept="1pGfFk" id="8S" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="8N" role="3cqZAp">
                    <node concept="2OqwBi" id="8T" role="3clFbG">
                      <node concept="37vLTw" id="8U" role="2Oq$k0">
                        <ref role="3cqZAo" node="8P" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="8V" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String)" resolve="rawPresentation" />
                        <uo k="s:originTrace" v="n:4884855736659431503" />
                        <node concept="Xl_RD" id="8W" role="37wK5m">
                          <property role="Xl_RC" value="post" />
                          <uo k="s:originTrace" v="n:4884855736659431503" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="8O" role="3cqZAp">
                    <node concept="37vLTI" id="8X" role="3clFbG">
                      <node concept="2OqwBi" id="8Y" role="37vLTx">
                        <node concept="37vLTw" id="90" role="2Oq$k0">
                          <ref role="3cqZAo" node="8P" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="91" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="8Z" role="37vLTJ">
                        <ref role="3cqZAo" node="j" resolve="props_PostOperationObject" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="8L" role="3clFbw">
                  <node concept="10Nm6u" id="92" role="3uHU7w" />
                  <node concept="37vLTw" id="93" role="3uHU7B">
                    <ref role="3cqZAo" node="j" resolve="props_PostOperationObject" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="8J" role="3cqZAp">
                <node concept="37vLTw" id="94" role="3cqZAk">
                  <ref role="3cqZAo" node="j" resolve="props_PostOperationObject" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="8H" role="3Kbmr1">
              <ref role="3cqZAo" node="k2" resolve="PostOperationObject" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1J" role="3KbHQx">
            <node concept="3clFbS" id="95" role="3Kbo56">
              <node concept="3clFbJ" id="97" role="3cqZAp">
                <node concept="3clFbS" id="99" role="3clFbx">
                  <node concept="3cpWs8" id="9b" role="3cqZAp">
                    <node concept="3cpWsn" id="9e" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="9f" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="9g" role="33vP2m">
                        <node concept="1pGfFk" id="9h" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="9c" role="3cqZAp">
                    <node concept="2OqwBi" id="9i" role="3clFbG">
                      <node concept="37vLTw" id="9j" role="2Oq$k0">
                        <ref role="3cqZAo" node="9e" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="9k" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String)" resolve="rawPresentation" />
                        <uo k="s:originTrace" v="n:4884855736659431502" />
                        <node concept="Xl_RD" id="9l" role="37wK5m">
                          <property role="Xl_RC" value="put" />
                          <uo k="s:originTrace" v="n:4884855736659431502" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="9d" role="3cqZAp">
                    <node concept="37vLTI" id="9m" role="3clFbG">
                      <node concept="2OqwBi" id="9n" role="37vLTx">
                        <node concept="37vLTw" id="9p" role="2Oq$k0">
                          <ref role="3cqZAo" node="9e" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="9q" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="9o" role="37vLTJ">
                        <ref role="3cqZAo" node="k" resolve="props_PutOperationObject" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="9a" role="3clFbw">
                  <node concept="10Nm6u" id="9r" role="3uHU7w" />
                  <node concept="37vLTw" id="9s" role="3uHU7B">
                    <ref role="3cqZAo" node="k" resolve="props_PutOperationObject" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="98" role="3cqZAp">
                <node concept="37vLTw" id="9t" role="3cqZAk">
                  <ref role="3cqZAo" node="k" resolve="props_PutOperationObject" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="96" role="3Kbmr1">
              <ref role="3cqZAo" node="k3" resolve="PutOperationObject" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1K" role="3KbHQx">
            <node concept="3clFbS" id="9u" role="3Kbo56">
              <node concept="3clFbJ" id="9w" role="3cqZAp">
                <node concept="3clFbS" id="9y" role="3clFbx">
                  <node concept="3cpWs8" id="9$" role="3cqZAp">
                    <node concept="3cpWsn" id="9B" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="9C" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="9D" role="33vP2m">
                        <node concept="1pGfFk" id="9E" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="9_" role="3cqZAp">
                    <node concept="2OqwBi" id="9F" role="3clFbG">
                      <node concept="37vLTw" id="9G" role="2Oq$k0">
                        <ref role="3cqZAo" node="9B" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="9H" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String)" resolve="rawPresentation" />
                        <uo k="s:originTrace" v="n:4884855736659223499" />
                        <node concept="Xl_RD" id="9I" role="37wK5m">
                          <property role="Xl_RC" value="Response" />
                          <uo k="s:originTrace" v="n:4884855736659223499" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="9A" role="3cqZAp">
                    <node concept="37vLTI" id="9J" role="3clFbG">
                      <node concept="2OqwBi" id="9K" role="37vLTx">
                        <node concept="37vLTw" id="9M" role="2Oq$k0">
                          <ref role="3cqZAo" node="9B" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="9N" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="9L" role="37vLTJ">
                        <ref role="3cqZAo" node="l" resolve="props_Response" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="9z" role="3clFbw">
                  <node concept="10Nm6u" id="9O" role="3uHU7w" />
                  <node concept="37vLTw" id="9P" role="3uHU7B">
                    <ref role="3cqZAo" node="l" resolve="props_Response" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="9x" role="3cqZAp">
                <node concept="37vLTw" id="9Q" role="3cqZAk">
                  <ref role="3cqZAo" node="l" resolve="props_Response" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="9v" role="3Kbmr1">
              <ref role="3cqZAo" node="k4" resolve="Response" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1L" role="3KbHQx">
            <node concept="3clFbS" id="9R" role="3Kbo56">
              <node concept="3clFbJ" id="9T" role="3cqZAp">
                <node concept="3clFbS" id="9V" role="3clFbx">
                  <node concept="3cpWs8" id="9X" role="3cqZAp">
                    <node concept="3cpWsn" id="9Z" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="a0" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="a1" role="33vP2m">
                        <node concept="1pGfFk" id="a2" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="9Y" role="3cqZAp">
                    <node concept="37vLTI" id="a3" role="3clFbG">
                      <node concept="2OqwBi" id="a4" role="37vLTx">
                        <node concept="37vLTw" id="a6" role="2Oq$k0">
                          <ref role="3cqZAo" node="9Z" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="a7" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="a5" role="37vLTJ">
                        <ref role="3cqZAo" node="m" resolve="props_Schema" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="9W" role="3clFbw">
                  <node concept="10Nm6u" id="a8" role="3uHU7w" />
                  <node concept="37vLTw" id="a9" role="3uHU7B">
                    <ref role="3cqZAo" node="m" resolve="props_Schema" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="9U" role="3cqZAp">
                <node concept="37vLTw" id="aa" role="3cqZAk">
                  <ref role="3cqZAo" node="m" resolve="props_Schema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="9S" role="3Kbmr1">
              <ref role="3cqZAo" node="k5" resolve="Schema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1M" role="3KbHQx">
            <node concept="3clFbS" id="ab" role="3Kbo56">
              <node concept="3clFbJ" id="ad" role="3cqZAp">
                <node concept="3clFbS" id="af" role="3clFbx">
                  <node concept="3cpWs8" id="ah" role="3cqZAp">
                    <node concept="3cpWsn" id="ak" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="al" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="am" role="33vP2m">
                        <node concept="1pGfFk" id="an" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="ai" role="3cqZAp">
                    <node concept="2OqwBi" id="ao" role="3clFbG">
                      <node concept="37vLTw" id="ap" role="2Oq$k0">
                        <ref role="3cqZAo" node="ak" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="aq" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.presentationByReference(long,long,long,long,java.lang.String,java.lang.String,java.lang.String)" resolve="presentationByReference" />
                        <uo k="s:originTrace" v="n:4313507821873222527" />
                        <node concept="1adDum" id="ar" role="37wK5m">
                          <property role="1adDun" value="0x29d429a205314919L" />
                          <uo k="s:originTrace" v="n:4313507821873222527" />
                        </node>
                        <node concept="1adDum" id="as" role="37wK5m">
                          <property role="1adDun" value="0x8e806706b89fb3b7L" />
                          <uo k="s:originTrace" v="n:4313507821873222527" />
                        </node>
                        <node concept="1adDum" id="at" role="37wK5m">
                          <property role="1adDun" value="0x3bdca882929f537fL" />
                          <uo k="s:originTrace" v="n:4313507821873222527" />
                        </node>
                        <node concept="1adDum" id="au" role="37wK5m">
                          <property role="1adDun" value="0x3bdca882929f5380L" />
                          <uo k="s:originTrace" v="n:4313507821873222527" />
                        </node>
                        <node concept="Xl_RD" id="av" role="37wK5m">
                          <property role="Xl_RC" value="schema" />
                          <uo k="s:originTrace" v="n:4313507821873222527" />
                        </node>
                        <node concept="Xl_RD" id="aw" role="37wK5m">
                          <property role="Xl_RC" value="" />
                          <uo k="s:originTrace" v="n:4313507821873222527" />
                        </node>
                        <node concept="Xl_RD" id="ax" role="37wK5m">
                          <property role="Xl_RC" value="" />
                          <uo k="s:originTrace" v="n:4313507821873222527" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="aj" role="3cqZAp">
                    <node concept="37vLTI" id="ay" role="3clFbG">
                      <node concept="2OqwBi" id="az" role="37vLTx">
                        <node concept="37vLTw" id="a_" role="2Oq$k0">
                          <ref role="3cqZAo" node="ak" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="aA" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="a$" role="37vLTJ">
                        <ref role="3cqZAo" node="n" resolve="props_SchemaRef" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="ag" role="3clFbw">
                  <node concept="10Nm6u" id="aB" role="3uHU7w" />
                  <node concept="37vLTw" id="aC" role="3uHU7B">
                    <ref role="3cqZAo" node="n" resolve="props_SchemaRef" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="ae" role="3cqZAp">
                <node concept="37vLTw" id="aD" role="3cqZAk">
                  <ref role="3cqZAo" node="n" resolve="props_SchemaRef" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="ac" role="3Kbmr1">
              <ref role="3cqZAo" node="k6" resolve="SchemaRef" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="1N" role="3KbHQx">
            <node concept="3clFbS" id="aE" role="3Kbo56">
              <node concept="3clFbJ" id="aG" role="3cqZAp">
                <node concept="3clFbS" id="aI" role="3clFbx">
                  <node concept="3cpWs8" id="aK" role="3cqZAp">
                    <node concept="3cpWsn" id="aN" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="aO" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="aP" role="33vP2m">
                        <node concept="1pGfFk" id="aQ" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="aL" role="3cqZAp">
                    <node concept="2OqwBi" id="aR" role="3clFbG">
                      <node concept="37vLTw" id="aS" role="2Oq$k0">
                        <ref role="3cqZAo" node="aN" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="aT" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String)" resolve="rawPresentation" />
                        <uo k="s:originTrace" v="n:4884855736659557554" />
                        <node concept="Xl_RD" id="aU" role="37wK5m">
                          <property role="Xl_RC" value="string" />
                          <uo k="s:originTrace" v="n:4884855736659557554" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="aM" role="3cqZAp">
                    <node concept="37vLTI" id="aV" role="3clFbG">
                      <node concept="2OqwBi" id="aW" role="37vLTx">
                        <node concept="37vLTw" id="aY" role="2Oq$k0">
                          <ref role="3cqZAo" node="aN" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="aZ" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create()" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="aX" role="37vLTJ">
                        <ref role="3cqZAo" node="o" resolve="props_StringSchema" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="aJ" role="3clFbw">
                  <node concept="10Nm6u" id="b0" role="3uHU7w" />
                  <node concept="37vLTw" id="b1" role="3uHU7B">
                    <ref role="3cqZAo" node="o" resolve="props_StringSchema" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="aH" role="3cqZAp">
                <node concept="37vLTw" id="b2" role="3cqZAk">
                  <ref role="3cqZAo" node="o" resolve="props_StringSchema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="aF" role="3Kbmr1">
              <ref role="3cqZAo" node="k7" resolve="StringSchema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="1j" role="3cqZAp">
          <node concept="10Nm6u" id="b3" role="3cqZAk" />
        </node>
      </node>
      <node concept="3uibUv" id="1d" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="2AHcQZ" id="1e" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="2AHcQZ" id="1f" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="b4">
    <property role="TrG5h" value="EnumerationDescriptor_DataType" />
    <uo k="s:originTrace" v="n:4313507821869042030" />
    <node concept="2tJIrI" id="b5" role="jymVt">
      <uo k="s:originTrace" v="n:4313507821869042030" />
    </node>
    <node concept="3clFbW" id="b6" role="jymVt">
      <uo k="s:originTrace" v="n:4313507821869042030" />
      <node concept="3cqZAl" id="bp" role="3clF45">
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="3Tm1VV" id="bq" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="3clFbS" id="br" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821869042030" />
        <node concept="XkiVB" id="bs" role="3cqZAp">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptorBase.&lt;init&gt;(long,long,long,java.lang.String,java.lang.String)" resolve="EnumerationDescriptorBase" />
          <uo k="s:originTrace" v="n:4313507821869042030" />
          <node concept="1adDum" id="bt" role="37wK5m">
            <property role="1adDun" value="0x29d429a205314919L" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="1adDum" id="bu" role="37wK5m">
            <property role="1adDun" value="0x8e806706b89fb3b7L" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="1adDum" id="bv" role="37wK5m">
            <property role="1adDun" value="0x3bdca882925f896eL" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="Xl_RD" id="bw" role="37wK5m">
            <property role="Xl_RC" value="DataType" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="Xl_RD" id="bx" role="37wK5m">
            <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4313507821869042030" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="b7" role="jymVt">
      <uo k="s:originTrace" v="n:4313507821869042030" />
    </node>
    <node concept="312cEg" id="b8" role="jymVt">
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myMember_STRING_0" />
      <uo k="s:originTrace" v="n:4313507821869042030" />
      <node concept="3Tm6S6" id="by" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="3uibUv" id="bz" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="2ShNRf" id="b$" role="33vP2m">
        <uo k="s:originTrace" v="n:4313507821869042030" />
        <node concept="1pGfFk" id="b_" role="2ShVmc">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptor$MemberDescriptor.&lt;init&gt;(java.lang.String,java.lang.String,long,java.lang.String,java.lang.String,java.lang.String)" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4313507821869042030" />
          <node concept="Xl_RD" id="bA" role="37wK5m">
            <property role="Xl_RC" value="STRING" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="Xl_RD" id="bB" role="37wK5m">
            <property role="Xl_RC" value="string" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="1adDum" id="bC" role="37wK5m">
            <property role="1adDun" value="0x3bdca882925f896fL" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="Xl_RD" id="bD" role="37wK5m">
            <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4313507821869042031" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
        </node>
      </node>
    </node>
    <node concept="312cEg" id="b9" role="jymVt">
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myMember_INTEGER_0" />
      <uo k="s:originTrace" v="n:4313507821869042030" />
      <node concept="3Tm6S6" id="bE" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="3uibUv" id="bF" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="2ShNRf" id="bG" role="33vP2m">
        <uo k="s:originTrace" v="n:4313507821869042030" />
        <node concept="1pGfFk" id="bH" role="2ShVmc">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptor$MemberDescriptor.&lt;init&gt;(java.lang.String,java.lang.String,long,java.lang.String,java.lang.String,java.lang.String)" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4313507821869042030" />
          <node concept="Xl_RD" id="bI" role="37wK5m">
            <property role="Xl_RC" value="INTEGER" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="Xl_RD" id="bJ" role="37wK5m">
            <property role="Xl_RC" value="integer" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="1adDum" id="bK" role="37wK5m">
            <property role="1adDun" value="0x3bdca882925f8970L" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="Xl_RD" id="bL" role="37wK5m">
            <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4313507821869042032" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
        </node>
      </node>
    </node>
    <node concept="312cEg" id="ba" role="jymVt">
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myMember_NUMBER_0" />
      <uo k="s:originTrace" v="n:4313507821869042030" />
      <node concept="3Tm6S6" id="bM" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="3uibUv" id="bN" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="2ShNRf" id="bO" role="33vP2m">
        <uo k="s:originTrace" v="n:4313507821869042030" />
        <node concept="1pGfFk" id="bP" role="2ShVmc">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptor$MemberDescriptor.&lt;init&gt;(java.lang.String,java.lang.String,long,java.lang.String,java.lang.String,java.lang.String)" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4313507821869042030" />
          <node concept="Xl_RD" id="bQ" role="37wK5m">
            <property role="Xl_RC" value="NUMBER" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="Xl_RD" id="bR" role="37wK5m">
            <property role="Xl_RC" value="number" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="1adDum" id="bS" role="37wK5m">
            <property role="1adDun" value="0x3bdca882925f8973L" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="Xl_RD" id="bT" role="37wK5m">
            <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4313507821869042035" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
        </node>
      </node>
    </node>
    <node concept="312cEg" id="bb" role="jymVt">
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myMember_BOOLEAN_0" />
      <uo k="s:originTrace" v="n:4313507821869042030" />
      <node concept="3Tm6S6" id="bU" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="3uibUv" id="bV" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="2ShNRf" id="bW" role="33vP2m">
        <uo k="s:originTrace" v="n:4313507821869042030" />
        <node concept="1pGfFk" id="bX" role="2ShVmc">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptor$MemberDescriptor.&lt;init&gt;(java.lang.String,java.lang.String,long,java.lang.String,java.lang.String,java.lang.String)" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4313507821869042030" />
          <node concept="Xl_RD" id="bY" role="37wK5m">
            <property role="Xl_RC" value="BOOLEAN" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="Xl_RD" id="bZ" role="37wK5m">
            <property role="Xl_RC" value="boolean" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="1adDum" id="c0" role="37wK5m">
            <property role="1adDun" value="0x3bdca882925f8977L" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="Xl_RD" id="c1" role="37wK5m">
            <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4313507821869042039" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3Tm1VV" id="bc" role="1B3o_S">
      <uo k="s:originTrace" v="n:4313507821869042030" />
    </node>
    <node concept="3uibUv" id="bd" role="1zkMxy">
      <ref role="3uigEE" to="ze1i:~EnumerationDescriptorBase" resolve="EnumerationDescriptorBase" />
      <uo k="s:originTrace" v="n:4313507821869042030" />
    </node>
    <node concept="2tJIrI" id="be" role="jymVt">
      <uo k="s:originTrace" v="n:4313507821869042030" />
    </node>
    <node concept="312cEg" id="bf" role="jymVt">
      <property role="TrG5h" value="myIndex" />
      <property role="3TUv4t" value="true" />
      <uo k="s:originTrace" v="n:4313507821869042030" />
      <node concept="3Tm6S6" id="c2" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="3uibUv" id="c3" role="1tU5fm">
        <ref role="3uigEE" to="ksn4:~EnumerationLiteralsIndex" resolve="EnumerationLiteralsIndex" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="2YIFZM" id="c4" role="33vP2m">
        <ref role="37wK5l" to="ksn4:~EnumerationLiteralsIndex.build(long,long,long,long...):jetbrains.mps.lang.smodel.EnumerationLiteralsIndex" resolve="build" />
        <ref role="1Pybhc" to="ksn4:~EnumerationLiteralsIndex" resolve="EnumerationLiteralsIndex" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
        <node concept="1adDum" id="c5" role="37wK5m">
          <property role="1adDun" value="0x29d429a205314919L" />
          <uo k="s:originTrace" v="n:4313507821869042030" />
        </node>
        <node concept="1adDum" id="c6" role="37wK5m">
          <property role="1adDun" value="0x8e806706b89fb3b7L" />
          <uo k="s:originTrace" v="n:4313507821869042030" />
        </node>
        <node concept="1adDum" id="c7" role="37wK5m">
          <property role="1adDun" value="0x3bdca882925f896eL" />
          <uo k="s:originTrace" v="n:4313507821869042030" />
        </node>
        <node concept="1adDum" id="c8" role="37wK5m">
          <property role="1adDun" value="0x3bdca882925f896fL" />
          <uo k="s:originTrace" v="n:4313507821869042030" />
        </node>
        <node concept="1adDum" id="c9" role="37wK5m">
          <property role="1adDun" value="0x3bdca882925f8970L" />
          <uo k="s:originTrace" v="n:4313507821869042030" />
        </node>
        <node concept="1adDum" id="ca" role="37wK5m">
          <property role="1adDun" value="0x3bdca882925f8973L" />
          <uo k="s:originTrace" v="n:4313507821869042030" />
        </node>
        <node concept="1adDum" id="cb" role="37wK5m">
          <property role="1adDun" value="0x3bdca882925f8977L" />
          <uo k="s:originTrace" v="n:4313507821869042030" />
        </node>
      </node>
    </node>
    <node concept="312cEg" id="bg" role="jymVt">
      <property role="TrG5h" value="myMembers" />
      <property role="3TUv4t" value="true" />
      <uo k="s:originTrace" v="n:4313507821869042030" />
      <node concept="3Tm6S6" id="cc" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="3uibUv" id="cd" role="1tU5fm">
        <ref role="3uigEE" to="33ny:~List" resolve="List" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
        <node concept="3uibUv" id="cf" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4313507821869042030" />
        </node>
      </node>
      <node concept="2ShNRf" id="ce" role="33vP2m">
        <uo k="s:originTrace" v="n:4313507821869042030" />
        <node concept="1pGfFk" id="cg" role="2ShVmc">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptorBase$MembersList.&lt;init&gt;(jetbrains.mps.smodel.runtime.EnumerationDescriptorBase,jetbrains.mps.lang.smodel.EnumerationLiteralsIndex,jetbrains.mps.smodel.runtime.EnumerationDescriptor$MemberDescriptor...)" resolve="EnumerationDescriptorBase.MembersList" />
          <uo k="s:originTrace" v="n:4313507821869042030" />
          <node concept="37vLTw" id="ch" role="37wK5m">
            <ref role="3cqZAo" node="bf" resolve="myIndex" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="37vLTw" id="ci" role="37wK5m">
            <ref role="3cqZAo" node="b8" resolve="myMember_STRING_0" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="37vLTw" id="cj" role="37wK5m">
            <ref role="3cqZAo" node="b9" resolve="myMember_INTEGER_0" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="37vLTw" id="ck" role="37wK5m">
            <ref role="3cqZAo" node="ba" resolve="myMember_NUMBER_0" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="37vLTw" id="cl" role="37wK5m">
            <ref role="3cqZAo" node="bb" resolve="myMember_BOOLEAN_0" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="bh" role="jymVt">
      <uo k="s:originTrace" v="n:4313507821869042030" />
    </node>
    <node concept="3clFb_" id="bi" role="jymVt">
      <property role="TrG5h" value="getDefault" />
      <uo k="s:originTrace" v="n:4313507821869042030" />
      <node concept="3Tm1VV" id="cm" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="2AHcQZ" id="cn" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="3uibUv" id="co" role="3clF45">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="3clFbS" id="cp" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821869042030" />
        <node concept="3clFbF" id="cr" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821869042030" />
          <node concept="10Nm6u" id="cs" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="cq" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
    </node>
    <node concept="2tJIrI" id="bj" role="jymVt">
      <uo k="s:originTrace" v="n:4313507821869042030" />
    </node>
    <node concept="3clFb_" id="bk" role="jymVt">
      <property role="TrG5h" value="getMembers" />
      <uo k="s:originTrace" v="n:4313507821869042030" />
      <node concept="3Tm1VV" id="ct" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="2AHcQZ" id="cu" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="3uibUv" id="cv" role="3clF45">
        <ref role="3uigEE" to="33ny:~List" resolve="List" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
        <node concept="3uibUv" id="cy" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4313507821869042030" />
        </node>
      </node>
      <node concept="3clFbS" id="cw" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821869042030" />
        <node concept="3cpWs6" id="cz" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821869042030" />
          <node concept="37vLTw" id="c$" role="3cqZAk">
            <ref role="3cqZAo" node="bg" resolve="myMembers" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="cx" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
    </node>
    <node concept="2tJIrI" id="bl" role="jymVt">
      <uo k="s:originTrace" v="n:4313507821869042030" />
    </node>
    <node concept="3clFb_" id="bm" role="jymVt">
      <property role="TrG5h" value="getMember" />
      <uo k="s:originTrace" v="n:4313507821869042030" />
      <node concept="3Tm1VV" id="c_" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="2AHcQZ" id="cA" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="3uibUv" id="cB" role="3clF45">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="37vLTG" id="cC" role="3clF46">
        <property role="TrG5h" value="memberName" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
        <node concept="3uibUv" id="cF" role="1tU5fm">
          <ref role="3uigEE" to="wyt6:~String" resolve="String" />
          <uo k="s:originTrace" v="n:4313507821869042030" />
        </node>
        <node concept="2AHcQZ" id="cG" role="2AJF6D">
          <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
          <uo k="s:originTrace" v="n:4313507821869042030" />
        </node>
      </node>
      <node concept="3clFbS" id="cD" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821869042030" />
        <node concept="3clFbJ" id="cH" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821869042030" />
          <node concept="3clFbS" id="cK" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821869042030" />
            <node concept="3cpWs6" id="cM" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821869042030" />
              <node concept="10Nm6u" id="cN" role="3cqZAk">
                <uo k="s:originTrace" v="n:4313507821869042030" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="cL" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821869042030" />
            <node concept="10Nm6u" id="cO" role="3uHU7w">
              <uo k="s:originTrace" v="n:4313507821869042030" />
            </node>
            <node concept="37vLTw" id="cP" role="3uHU7B">
              <ref role="3cqZAo" node="cC" resolve="memberName" />
              <uo k="s:originTrace" v="n:4313507821869042030" />
            </node>
          </node>
        </node>
        <node concept="3KaCP$" id="cI" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821869042030" />
          <node concept="37vLTw" id="cQ" role="3KbGdf">
            <ref role="3cqZAo" node="cC" resolve="memberName" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
          <node concept="3KbdKl" id="cR" role="3KbHQx">
            <uo k="s:originTrace" v="n:4313507821869042030" />
            <node concept="Xl_RD" id="cV" role="3Kbmr1">
              <property role="Xl_RC" value="STRING" />
              <uo k="s:originTrace" v="n:4313507821869042030" />
            </node>
            <node concept="3clFbS" id="cW" role="3Kbo56">
              <uo k="s:originTrace" v="n:4313507821869042030" />
              <node concept="3cpWs6" id="cX" role="3cqZAp">
                <uo k="s:originTrace" v="n:4313507821869042030" />
                <node concept="37vLTw" id="cY" role="3cqZAk">
                  <ref role="3cqZAo" node="b8" resolve="myMember_STRING_0" />
                  <uo k="s:originTrace" v="n:4313507821869042030" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="cS" role="3KbHQx">
            <uo k="s:originTrace" v="n:4313507821869042030" />
            <node concept="Xl_RD" id="cZ" role="3Kbmr1">
              <property role="Xl_RC" value="INTEGER" />
              <uo k="s:originTrace" v="n:4313507821869042030" />
            </node>
            <node concept="3clFbS" id="d0" role="3Kbo56">
              <uo k="s:originTrace" v="n:4313507821869042030" />
              <node concept="3cpWs6" id="d1" role="3cqZAp">
                <uo k="s:originTrace" v="n:4313507821869042030" />
                <node concept="37vLTw" id="d2" role="3cqZAk">
                  <ref role="3cqZAo" node="b9" resolve="myMember_INTEGER_0" />
                  <uo k="s:originTrace" v="n:4313507821869042030" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="cT" role="3KbHQx">
            <uo k="s:originTrace" v="n:4313507821869042030" />
            <node concept="Xl_RD" id="d3" role="3Kbmr1">
              <property role="Xl_RC" value="NUMBER" />
              <uo k="s:originTrace" v="n:4313507821869042030" />
            </node>
            <node concept="3clFbS" id="d4" role="3Kbo56">
              <uo k="s:originTrace" v="n:4313507821869042030" />
              <node concept="3cpWs6" id="d5" role="3cqZAp">
                <uo k="s:originTrace" v="n:4313507821869042030" />
                <node concept="37vLTw" id="d6" role="3cqZAk">
                  <ref role="3cqZAo" node="ba" resolve="myMember_NUMBER_0" />
                  <uo k="s:originTrace" v="n:4313507821869042030" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="cU" role="3KbHQx">
            <uo k="s:originTrace" v="n:4313507821869042030" />
            <node concept="Xl_RD" id="d7" role="3Kbmr1">
              <property role="Xl_RC" value="BOOLEAN" />
              <uo k="s:originTrace" v="n:4313507821869042030" />
            </node>
            <node concept="3clFbS" id="d8" role="3Kbo56">
              <uo k="s:originTrace" v="n:4313507821869042030" />
              <node concept="3cpWs6" id="d9" role="3cqZAp">
                <uo k="s:originTrace" v="n:4313507821869042030" />
                <node concept="37vLTw" id="da" role="3cqZAk">
                  <ref role="3cqZAo" node="bb" resolve="myMember_BOOLEAN_0" />
                  <uo k="s:originTrace" v="n:4313507821869042030" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="cJ" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821869042030" />
          <node concept="10Nm6u" id="db" role="3cqZAk">
            <uo k="s:originTrace" v="n:4313507821869042030" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="cE" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
    </node>
    <node concept="2tJIrI" id="bn" role="jymVt">
      <uo k="s:originTrace" v="n:4313507821869042030" />
    </node>
    <node concept="3clFb_" id="bo" role="jymVt">
      <property role="TrG5h" value="getMember" />
      <uo k="s:originTrace" v="n:4313507821869042030" />
      <node concept="3Tm1VV" id="dc" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="2AHcQZ" id="dd" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="3uibUv" id="de" role="3clF45">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
      <node concept="37vLTG" id="df" role="3clF46">
        <property role="TrG5h" value="idValue" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
        <node concept="3cpWsb" id="di" role="1tU5fm">
          <uo k="s:originTrace" v="n:4313507821869042030" />
        </node>
      </node>
      <node concept="3clFbS" id="dg" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821869042030" />
        <node concept="3cpWs8" id="dj" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821869042030" />
          <node concept="3cpWsn" id="dm" role="3cpWs9">
            <property role="TrG5h" value="index" />
            <uo k="s:originTrace" v="n:4313507821869042030" />
            <node concept="10Oyi0" id="dn" role="1tU5fm">
              <uo k="s:originTrace" v="n:4313507821869042030" />
            </node>
            <node concept="2OqwBi" id="do" role="33vP2m">
              <uo k="s:originTrace" v="n:4313507821869042030" />
              <node concept="37vLTw" id="dp" role="2Oq$k0">
                <ref role="3cqZAo" node="bf" resolve="myIndex" />
                <uo k="s:originTrace" v="n:4313507821869042030" />
              </node>
              <node concept="liA8E" id="dq" role="2OqNvi">
                <ref role="37wK5l" to="ksn4:~EnumerationLiteralsIndex.index(long):int" resolve="index" />
                <uo k="s:originTrace" v="n:4313507821869042030" />
                <node concept="37vLTw" id="dr" role="37wK5m">
                  <ref role="3cqZAo" node="df" resolve="idValue" />
                  <uo k="s:originTrace" v="n:4313507821869042030" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="dk" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821869042030" />
          <node concept="3clFbS" id="ds" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821869042030" />
            <node concept="3cpWs6" id="du" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821869042030" />
              <node concept="10Nm6u" id="dv" role="3cqZAk">
                <uo k="s:originTrace" v="n:4313507821869042030" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="dt" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821869042030" />
            <node concept="3cmrfG" id="dw" role="3uHU7w">
              <property role="3cmrfH" value="-1" />
              <uo k="s:originTrace" v="n:4313507821869042030" />
            </node>
            <node concept="37vLTw" id="dx" role="3uHU7B">
              <ref role="3cqZAo" node="dm" resolve="index" />
              <uo k="s:originTrace" v="n:4313507821869042030" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="dl" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821869042030" />
          <node concept="2OqwBi" id="dy" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821869042030" />
            <node concept="37vLTw" id="dz" role="2Oq$k0">
              <ref role="3cqZAo" node="bg" resolve="myMembers" />
              <uo k="s:originTrace" v="n:4313507821869042030" />
            </node>
            <node concept="liA8E" id="d$" role="2OqNvi">
              <ref role="37wK5l" to="33ny:~List.get(int):java.lang.Object" resolve="get" />
              <uo k="s:originTrace" v="n:4313507821869042030" />
              <node concept="37vLTw" id="d_" role="37wK5m">
                <ref role="3cqZAo" node="dm" resolve="index" />
                <uo k="s:originTrace" v="n:4313507821869042030" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="dh" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821869042030" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="dA">
    <property role="TrG5h" value="EnumerationDescriptor_MediaType" />
    <uo k="s:originTrace" v="n:4884855736659493363" />
    <node concept="2tJIrI" id="dB" role="jymVt">
      <uo k="s:originTrace" v="n:4884855736659493363" />
    </node>
    <node concept="3clFbW" id="dC" role="jymVt">
      <uo k="s:originTrace" v="n:4884855736659493363" />
      <node concept="3cqZAl" id="dU" role="3clF45">
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="3Tm1VV" id="dV" role="1B3o_S">
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="3clFbS" id="dW" role="3clF47">
        <uo k="s:originTrace" v="n:4884855736659493363" />
        <node concept="XkiVB" id="dX" role="3cqZAp">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptorBase.&lt;init&gt;(long,long,long,java.lang.String,java.lang.String)" resolve="EnumerationDescriptorBase" />
          <uo k="s:originTrace" v="n:4884855736659493363" />
          <node concept="1adDum" id="dY" role="37wK5m">
            <property role="1adDun" value="0x29d429a205314919L" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
          <node concept="1adDum" id="dZ" role="37wK5m">
            <property role="1adDun" value="0x8e806706b89fb3b7L" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
          <node concept="1adDum" id="e0" role="37wK5m">
            <property role="1adDun" value="0x43ca7e69529e41f3L" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
          <node concept="Xl_RD" id="e1" role="37wK5m">
            <property role="Xl_RC" value="MediaType" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
          <node concept="Xl_RD" id="e2" role="37wK5m">
            <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4884855736659493363" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="dD" role="jymVt">
      <uo k="s:originTrace" v="n:4884855736659493363" />
    </node>
    <node concept="312cEg" id="dE" role="jymVt">
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myMember_APPLICATION_JSON_0" />
      <uo k="s:originTrace" v="n:4884855736659493363" />
      <node concept="3Tm6S6" id="e3" role="1B3o_S">
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="3uibUv" id="e4" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="2ShNRf" id="e5" role="33vP2m">
        <uo k="s:originTrace" v="n:4884855736659493363" />
        <node concept="1pGfFk" id="e6" role="2ShVmc">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptor$MemberDescriptor.&lt;init&gt;(java.lang.String,java.lang.String,long,java.lang.String,java.lang.String,java.lang.String)" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4884855736659493363" />
          <node concept="Xl_RD" id="e7" role="37wK5m">
            <property role="Xl_RC" value="APPLICATION_JSON" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
          <node concept="Xl_RD" id="e8" role="37wK5m">
            <property role="Xl_RC" value="application/json" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
          <node concept="1adDum" id="e9" role="37wK5m">
            <property role="1adDun" value="0x43ca7e69529e41f4L" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
          <node concept="Xl_RD" id="ea" role="37wK5m">
            <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4884855736659493364" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
        </node>
      </node>
    </node>
    <node concept="312cEg" id="dF" role="jymVt">
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myMember_TEXT_HTML_0" />
      <uo k="s:originTrace" v="n:4884855736659493363" />
      <node concept="3Tm6S6" id="eb" role="1B3o_S">
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="3uibUv" id="ec" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="2ShNRf" id="ed" role="33vP2m">
        <uo k="s:originTrace" v="n:4884855736659493363" />
        <node concept="1pGfFk" id="ee" role="2ShVmc">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptor$MemberDescriptor.&lt;init&gt;(java.lang.String,java.lang.String,long,java.lang.String,java.lang.String,java.lang.String)" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4884855736659493363" />
          <node concept="Xl_RD" id="ef" role="37wK5m">
            <property role="Xl_RC" value="TEXT_HTML" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
          <node concept="Xl_RD" id="eg" role="37wK5m">
            <property role="Xl_RC" value="text/html" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
          <node concept="1adDum" id="eh" role="37wK5m">
            <property role="1adDun" value="0x43ca7e69529e41f5L" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
          <node concept="Xl_RD" id="ei" role="37wK5m">
            <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4884855736659493365" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
        </node>
      </node>
    </node>
    <node concept="312cEg" id="dG" role="jymVt">
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myMember_TEXT_PLAIN_0" />
      <uo k="s:originTrace" v="n:4884855736659493363" />
      <node concept="3Tm6S6" id="ej" role="1B3o_S">
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="3uibUv" id="ek" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="2ShNRf" id="el" role="33vP2m">
        <uo k="s:originTrace" v="n:4884855736659493363" />
        <node concept="1pGfFk" id="em" role="2ShVmc">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptor$MemberDescriptor.&lt;init&gt;(java.lang.String,java.lang.String,long,java.lang.String,java.lang.String,java.lang.String)" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4884855736659493363" />
          <node concept="Xl_RD" id="en" role="37wK5m">
            <property role="Xl_RC" value="TEXT_PLAIN" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
          <node concept="Xl_RD" id="eo" role="37wK5m">
            <property role="Xl_RC" value="text/plain" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
          <node concept="1adDum" id="ep" role="37wK5m">
            <property role="1adDun" value="0x43ca7e69529e41f8L" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
          <node concept="Xl_RD" id="eq" role="37wK5m">
            <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4884855736659493368" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3Tm1VV" id="dH" role="1B3o_S">
      <uo k="s:originTrace" v="n:4884855736659493363" />
    </node>
    <node concept="3uibUv" id="dI" role="1zkMxy">
      <ref role="3uigEE" to="ze1i:~EnumerationDescriptorBase" resolve="EnumerationDescriptorBase" />
      <uo k="s:originTrace" v="n:4884855736659493363" />
    </node>
    <node concept="2tJIrI" id="dJ" role="jymVt">
      <uo k="s:originTrace" v="n:4884855736659493363" />
    </node>
    <node concept="312cEg" id="dK" role="jymVt">
      <property role="TrG5h" value="myIndex" />
      <property role="3TUv4t" value="true" />
      <uo k="s:originTrace" v="n:4884855736659493363" />
      <node concept="3Tm6S6" id="er" role="1B3o_S">
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="3uibUv" id="es" role="1tU5fm">
        <ref role="3uigEE" to="ksn4:~EnumerationLiteralsIndex" resolve="EnumerationLiteralsIndex" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="2YIFZM" id="et" role="33vP2m">
        <ref role="37wK5l" to="ksn4:~EnumerationLiteralsIndex.build(long,long,long,long...):jetbrains.mps.lang.smodel.EnumerationLiteralsIndex" resolve="build" />
        <ref role="1Pybhc" to="ksn4:~EnumerationLiteralsIndex" resolve="EnumerationLiteralsIndex" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
        <node concept="1adDum" id="eu" role="37wK5m">
          <property role="1adDun" value="0x29d429a205314919L" />
          <uo k="s:originTrace" v="n:4884855736659493363" />
        </node>
        <node concept="1adDum" id="ev" role="37wK5m">
          <property role="1adDun" value="0x8e806706b89fb3b7L" />
          <uo k="s:originTrace" v="n:4884855736659493363" />
        </node>
        <node concept="1adDum" id="ew" role="37wK5m">
          <property role="1adDun" value="0x43ca7e69529e41f3L" />
          <uo k="s:originTrace" v="n:4884855736659493363" />
        </node>
        <node concept="1adDum" id="ex" role="37wK5m">
          <property role="1adDun" value="0x43ca7e69529e41f4L" />
          <uo k="s:originTrace" v="n:4884855736659493363" />
        </node>
        <node concept="1adDum" id="ey" role="37wK5m">
          <property role="1adDun" value="0x43ca7e69529e41f5L" />
          <uo k="s:originTrace" v="n:4884855736659493363" />
        </node>
        <node concept="1adDum" id="ez" role="37wK5m">
          <property role="1adDun" value="0x43ca7e69529e41f8L" />
          <uo k="s:originTrace" v="n:4884855736659493363" />
        </node>
      </node>
    </node>
    <node concept="312cEg" id="dL" role="jymVt">
      <property role="TrG5h" value="myMembers" />
      <property role="3TUv4t" value="true" />
      <uo k="s:originTrace" v="n:4884855736659493363" />
      <node concept="3Tm6S6" id="e$" role="1B3o_S">
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="3uibUv" id="e_" role="1tU5fm">
        <ref role="3uigEE" to="33ny:~List" resolve="List" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
        <node concept="3uibUv" id="eB" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4884855736659493363" />
        </node>
      </node>
      <node concept="2ShNRf" id="eA" role="33vP2m">
        <uo k="s:originTrace" v="n:4884855736659493363" />
        <node concept="1pGfFk" id="eC" role="2ShVmc">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptorBase$MembersList.&lt;init&gt;(jetbrains.mps.smodel.runtime.EnumerationDescriptorBase,jetbrains.mps.lang.smodel.EnumerationLiteralsIndex,jetbrains.mps.smodel.runtime.EnumerationDescriptor$MemberDescriptor...)" resolve="EnumerationDescriptorBase.MembersList" />
          <uo k="s:originTrace" v="n:4884855736659493363" />
          <node concept="37vLTw" id="eD" role="37wK5m">
            <ref role="3cqZAo" node="dK" resolve="myIndex" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
          <node concept="37vLTw" id="eE" role="37wK5m">
            <ref role="3cqZAo" node="dE" resolve="myMember_APPLICATION_JSON_0" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
          <node concept="37vLTw" id="eF" role="37wK5m">
            <ref role="3cqZAo" node="dF" resolve="myMember_TEXT_HTML_0" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
          <node concept="37vLTw" id="eG" role="37wK5m">
            <ref role="3cqZAo" node="dG" resolve="myMember_TEXT_PLAIN_0" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="dM" role="jymVt">
      <uo k="s:originTrace" v="n:4884855736659493363" />
    </node>
    <node concept="3clFb_" id="dN" role="jymVt">
      <property role="TrG5h" value="getDefault" />
      <uo k="s:originTrace" v="n:4884855736659493363" />
      <node concept="3Tm1VV" id="eH" role="1B3o_S">
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="2AHcQZ" id="eI" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="3uibUv" id="eJ" role="3clF45">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="3clFbS" id="eK" role="3clF47">
        <uo k="s:originTrace" v="n:4884855736659493363" />
        <node concept="3clFbF" id="eM" role="3cqZAp">
          <uo k="s:originTrace" v="n:4884855736659493363" />
          <node concept="37vLTw" id="eN" role="3clFbG">
            <ref role="3cqZAo" node="dE" resolve="myMember_APPLICATION_JSON_0" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="eL" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
    </node>
    <node concept="2tJIrI" id="dO" role="jymVt">
      <uo k="s:originTrace" v="n:4884855736659493363" />
    </node>
    <node concept="3clFb_" id="dP" role="jymVt">
      <property role="TrG5h" value="getMembers" />
      <uo k="s:originTrace" v="n:4884855736659493363" />
      <node concept="3Tm1VV" id="eO" role="1B3o_S">
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="2AHcQZ" id="eP" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="3uibUv" id="eQ" role="3clF45">
        <ref role="3uigEE" to="33ny:~List" resolve="List" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
        <node concept="3uibUv" id="eT" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4884855736659493363" />
        </node>
      </node>
      <node concept="3clFbS" id="eR" role="3clF47">
        <uo k="s:originTrace" v="n:4884855736659493363" />
        <node concept="3cpWs6" id="eU" role="3cqZAp">
          <uo k="s:originTrace" v="n:4884855736659493363" />
          <node concept="37vLTw" id="eV" role="3cqZAk">
            <ref role="3cqZAo" node="dL" resolve="myMembers" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="eS" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
    </node>
    <node concept="2tJIrI" id="dQ" role="jymVt">
      <uo k="s:originTrace" v="n:4884855736659493363" />
    </node>
    <node concept="3clFb_" id="dR" role="jymVt">
      <property role="TrG5h" value="getMember" />
      <uo k="s:originTrace" v="n:4884855736659493363" />
      <node concept="3Tm1VV" id="eW" role="1B3o_S">
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="2AHcQZ" id="eX" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="3uibUv" id="eY" role="3clF45">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="37vLTG" id="eZ" role="3clF46">
        <property role="TrG5h" value="memberName" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
        <node concept="3uibUv" id="f2" role="1tU5fm">
          <ref role="3uigEE" to="wyt6:~String" resolve="String" />
          <uo k="s:originTrace" v="n:4884855736659493363" />
        </node>
        <node concept="2AHcQZ" id="f3" role="2AJF6D">
          <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
          <uo k="s:originTrace" v="n:4884855736659493363" />
        </node>
      </node>
      <node concept="3clFbS" id="f0" role="3clF47">
        <uo k="s:originTrace" v="n:4884855736659493363" />
        <node concept="3clFbJ" id="f4" role="3cqZAp">
          <uo k="s:originTrace" v="n:4884855736659493363" />
          <node concept="3clFbS" id="f7" role="3clFbx">
            <uo k="s:originTrace" v="n:4884855736659493363" />
            <node concept="3cpWs6" id="f9" role="3cqZAp">
              <uo k="s:originTrace" v="n:4884855736659493363" />
              <node concept="10Nm6u" id="fa" role="3cqZAk">
                <uo k="s:originTrace" v="n:4884855736659493363" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="f8" role="3clFbw">
            <uo k="s:originTrace" v="n:4884855736659493363" />
            <node concept="10Nm6u" id="fb" role="3uHU7w">
              <uo k="s:originTrace" v="n:4884855736659493363" />
            </node>
            <node concept="37vLTw" id="fc" role="3uHU7B">
              <ref role="3cqZAo" node="eZ" resolve="memberName" />
              <uo k="s:originTrace" v="n:4884855736659493363" />
            </node>
          </node>
        </node>
        <node concept="3KaCP$" id="f5" role="3cqZAp">
          <uo k="s:originTrace" v="n:4884855736659493363" />
          <node concept="37vLTw" id="fd" role="3KbGdf">
            <ref role="3cqZAo" node="eZ" resolve="memberName" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
          <node concept="3KbdKl" id="fe" role="3KbHQx">
            <uo k="s:originTrace" v="n:4884855736659493363" />
            <node concept="Xl_RD" id="fh" role="3Kbmr1">
              <property role="Xl_RC" value="APPLICATION_JSON" />
              <uo k="s:originTrace" v="n:4884855736659493363" />
            </node>
            <node concept="3clFbS" id="fi" role="3Kbo56">
              <uo k="s:originTrace" v="n:4884855736659493363" />
              <node concept="3cpWs6" id="fj" role="3cqZAp">
                <uo k="s:originTrace" v="n:4884855736659493363" />
                <node concept="37vLTw" id="fk" role="3cqZAk">
                  <ref role="3cqZAo" node="dE" resolve="myMember_APPLICATION_JSON_0" />
                  <uo k="s:originTrace" v="n:4884855736659493363" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="ff" role="3KbHQx">
            <uo k="s:originTrace" v="n:4884855736659493363" />
            <node concept="Xl_RD" id="fl" role="3Kbmr1">
              <property role="Xl_RC" value="TEXT_HTML" />
              <uo k="s:originTrace" v="n:4884855736659493363" />
            </node>
            <node concept="3clFbS" id="fm" role="3Kbo56">
              <uo k="s:originTrace" v="n:4884855736659493363" />
              <node concept="3cpWs6" id="fn" role="3cqZAp">
                <uo k="s:originTrace" v="n:4884855736659493363" />
                <node concept="37vLTw" id="fo" role="3cqZAk">
                  <ref role="3cqZAo" node="dF" resolve="myMember_TEXT_HTML_0" />
                  <uo k="s:originTrace" v="n:4884855736659493363" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="fg" role="3KbHQx">
            <uo k="s:originTrace" v="n:4884855736659493363" />
            <node concept="Xl_RD" id="fp" role="3Kbmr1">
              <property role="Xl_RC" value="TEXT_PLAIN" />
              <uo k="s:originTrace" v="n:4884855736659493363" />
            </node>
            <node concept="3clFbS" id="fq" role="3Kbo56">
              <uo k="s:originTrace" v="n:4884855736659493363" />
              <node concept="3cpWs6" id="fr" role="3cqZAp">
                <uo k="s:originTrace" v="n:4884855736659493363" />
                <node concept="37vLTw" id="fs" role="3cqZAk">
                  <ref role="3cqZAo" node="dG" resolve="myMember_TEXT_PLAIN_0" />
                  <uo k="s:originTrace" v="n:4884855736659493363" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="f6" role="3cqZAp">
          <uo k="s:originTrace" v="n:4884855736659493363" />
          <node concept="10Nm6u" id="ft" role="3cqZAk">
            <uo k="s:originTrace" v="n:4884855736659493363" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="f1" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
    </node>
    <node concept="2tJIrI" id="dS" role="jymVt">
      <uo k="s:originTrace" v="n:4884855736659493363" />
    </node>
    <node concept="3clFb_" id="dT" role="jymVt">
      <property role="TrG5h" value="getMember" />
      <uo k="s:originTrace" v="n:4884855736659493363" />
      <node concept="3Tm1VV" id="fu" role="1B3o_S">
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="2AHcQZ" id="fv" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="3uibUv" id="fw" role="3clF45">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
      <node concept="37vLTG" id="fx" role="3clF46">
        <property role="TrG5h" value="idValue" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
        <node concept="3cpWsb" id="f$" role="1tU5fm">
          <uo k="s:originTrace" v="n:4884855736659493363" />
        </node>
      </node>
      <node concept="3clFbS" id="fy" role="3clF47">
        <uo k="s:originTrace" v="n:4884855736659493363" />
        <node concept="3cpWs8" id="f_" role="3cqZAp">
          <uo k="s:originTrace" v="n:4884855736659493363" />
          <node concept="3cpWsn" id="fC" role="3cpWs9">
            <property role="TrG5h" value="index" />
            <uo k="s:originTrace" v="n:4884855736659493363" />
            <node concept="10Oyi0" id="fD" role="1tU5fm">
              <uo k="s:originTrace" v="n:4884855736659493363" />
            </node>
            <node concept="2OqwBi" id="fE" role="33vP2m">
              <uo k="s:originTrace" v="n:4884855736659493363" />
              <node concept="37vLTw" id="fF" role="2Oq$k0">
                <ref role="3cqZAo" node="dK" resolve="myIndex" />
                <uo k="s:originTrace" v="n:4884855736659493363" />
              </node>
              <node concept="liA8E" id="fG" role="2OqNvi">
                <ref role="37wK5l" to="ksn4:~EnumerationLiteralsIndex.index(long):int" resolve="index" />
                <uo k="s:originTrace" v="n:4884855736659493363" />
                <node concept="37vLTw" id="fH" role="37wK5m">
                  <ref role="3cqZAo" node="fx" resolve="idValue" />
                  <uo k="s:originTrace" v="n:4884855736659493363" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="fA" role="3cqZAp">
          <uo k="s:originTrace" v="n:4884855736659493363" />
          <node concept="3clFbS" id="fI" role="3clFbx">
            <uo k="s:originTrace" v="n:4884855736659493363" />
            <node concept="3cpWs6" id="fK" role="3cqZAp">
              <uo k="s:originTrace" v="n:4884855736659493363" />
              <node concept="10Nm6u" id="fL" role="3cqZAk">
                <uo k="s:originTrace" v="n:4884855736659493363" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="fJ" role="3clFbw">
            <uo k="s:originTrace" v="n:4884855736659493363" />
            <node concept="3cmrfG" id="fM" role="3uHU7w">
              <property role="3cmrfH" value="-1" />
              <uo k="s:originTrace" v="n:4884855736659493363" />
            </node>
            <node concept="37vLTw" id="fN" role="3uHU7B">
              <ref role="3cqZAo" node="fC" resolve="index" />
              <uo k="s:originTrace" v="n:4884855736659493363" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="fB" role="3cqZAp">
          <uo k="s:originTrace" v="n:4884855736659493363" />
          <node concept="2OqwBi" id="fO" role="3clFbG">
            <uo k="s:originTrace" v="n:4884855736659493363" />
            <node concept="37vLTw" id="fP" role="2Oq$k0">
              <ref role="3cqZAo" node="dL" resolve="myMembers" />
              <uo k="s:originTrace" v="n:4884855736659493363" />
            </node>
            <node concept="liA8E" id="fQ" role="2OqNvi">
              <ref role="37wK5l" to="33ny:~List.get(int):java.lang.Object" resolve="get" />
              <uo k="s:originTrace" v="n:4884855736659493363" />
              <node concept="37vLTw" id="fR" role="37wK5m">
                <ref role="3cqZAo" node="fC" resolve="index" />
                <uo k="s:originTrace" v="n:4884855736659493363" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="fz" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4884855736659493363" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="fS">
    <property role="TrG5h" value="EnumerationDescriptor_Operation" />
    <uo k="s:originTrace" v="n:4322367480666010579" />
    <node concept="2tJIrI" id="fT" role="jymVt">
      <uo k="s:originTrace" v="n:4322367480666010579" />
    </node>
    <node concept="3clFbW" id="fU" role="jymVt">
      <uo k="s:originTrace" v="n:4322367480666010579" />
      <node concept="3cqZAl" id="ge" role="3clF45">
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="3Tm1VV" id="gf" role="1B3o_S">
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="3clFbS" id="gg" role="3clF47">
        <uo k="s:originTrace" v="n:4322367480666010579" />
        <node concept="XkiVB" id="gh" role="3cqZAp">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptorBase.&lt;init&gt;(long,long,long,java.lang.String,java.lang.String)" resolve="EnumerationDescriptorBase" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
          <node concept="1adDum" id="gi" role="37wK5m">
            <property role="1adDun" value="0x29d429a205314919L" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="1adDum" id="gj" role="37wK5m">
            <property role="1adDun" value="0x8e806706b89fb3b7L" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="1adDum" id="gk" role="37wK5m">
            <property role="1adDun" value="0x3bfc2252a1cc33d3L" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="Xl_RD" id="gl" role="37wK5m">
            <property role="Xl_RC" value="Operation" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="Xl_RD" id="gm" role="37wK5m">
            <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4322367480666010579" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="fV" role="jymVt">
      <uo k="s:originTrace" v="n:4322367480666010579" />
    </node>
    <node concept="312cEg" id="fW" role="jymVt">
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myMember_GET_0" />
      <uo k="s:originTrace" v="n:4322367480666010579" />
      <node concept="3Tm6S6" id="gn" role="1B3o_S">
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="3uibUv" id="go" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="2ShNRf" id="gp" role="33vP2m">
        <uo k="s:originTrace" v="n:4322367480666010579" />
        <node concept="1pGfFk" id="gq" role="2ShVmc">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptor$MemberDescriptor.&lt;init&gt;(java.lang.String,java.lang.String,long,java.lang.String,java.lang.String,java.lang.String)" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
          <node concept="Xl_RD" id="gr" role="37wK5m">
            <property role="Xl_RC" value="GET" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="Xl_RD" id="gs" role="37wK5m">
            <property role="Xl_RC" value="GET" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="1adDum" id="gt" role="37wK5m">
            <property role="1adDun" value="0x3bfc2252a1cc33d4L" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="Xl_RD" id="gu" role="37wK5m">
            <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4322367480666010580" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
        </node>
      </node>
    </node>
    <node concept="312cEg" id="fX" role="jymVt">
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myMember_PUT_0" />
      <uo k="s:originTrace" v="n:4322367480666010579" />
      <node concept="3Tm6S6" id="gv" role="1B3o_S">
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="3uibUv" id="gw" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="2ShNRf" id="gx" role="33vP2m">
        <uo k="s:originTrace" v="n:4322367480666010579" />
        <node concept="1pGfFk" id="gy" role="2ShVmc">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptor$MemberDescriptor.&lt;init&gt;(java.lang.String,java.lang.String,long,java.lang.String,java.lang.String,java.lang.String)" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
          <node concept="Xl_RD" id="gz" role="37wK5m">
            <property role="Xl_RC" value="PUT" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="Xl_RD" id="g$" role="37wK5m">
            <property role="Xl_RC" value="PUT" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="1adDum" id="g_" role="37wK5m">
            <property role="1adDun" value="0x3bfc2252a1cc33d5L" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="Xl_RD" id="gA" role="37wK5m">
            <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4322367480666010581" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
        </node>
      </node>
    </node>
    <node concept="312cEg" id="fY" role="jymVt">
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myMember_POST_0" />
      <uo k="s:originTrace" v="n:4322367480666010579" />
      <node concept="3Tm6S6" id="gB" role="1B3o_S">
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="3uibUv" id="gC" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="2ShNRf" id="gD" role="33vP2m">
        <uo k="s:originTrace" v="n:4322367480666010579" />
        <node concept="1pGfFk" id="gE" role="2ShVmc">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptor$MemberDescriptor.&lt;init&gt;(java.lang.String,java.lang.String,long,java.lang.String,java.lang.String,java.lang.String)" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
          <node concept="Xl_RD" id="gF" role="37wK5m">
            <property role="Xl_RC" value="POST" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="Xl_RD" id="gG" role="37wK5m">
            <property role="Xl_RC" value="POST" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="1adDum" id="gH" role="37wK5m">
            <property role="1adDun" value="0x3bfc2252a1cc33d8L" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="Xl_RD" id="gI" role="37wK5m">
            <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4322367480666010584" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
        </node>
      </node>
    </node>
    <node concept="312cEg" id="fZ" role="jymVt">
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myMember_DELETE_0" />
      <uo k="s:originTrace" v="n:4322367480666010579" />
      <node concept="3Tm6S6" id="gJ" role="1B3o_S">
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="3uibUv" id="gK" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="2ShNRf" id="gL" role="33vP2m">
        <uo k="s:originTrace" v="n:4322367480666010579" />
        <node concept="1pGfFk" id="gM" role="2ShVmc">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptor$MemberDescriptor.&lt;init&gt;(java.lang.String,java.lang.String,long,java.lang.String,java.lang.String,java.lang.String)" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
          <node concept="Xl_RD" id="gN" role="37wK5m">
            <property role="Xl_RC" value="DELETE" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="Xl_RD" id="gO" role="37wK5m">
            <property role="Xl_RC" value="DELETE" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="1adDum" id="gP" role="37wK5m">
            <property role="1adDun" value="0x3bfc2252a1cc33dcL" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="Xl_RD" id="gQ" role="37wK5m">
            <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4322367480666010588" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
        </node>
      </node>
    </node>
    <node concept="312cEg" id="g0" role="jymVt">
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myMember_UPDATE_0" />
      <uo k="s:originTrace" v="n:4322367480666010579" />
      <node concept="3Tm6S6" id="gR" role="1B3o_S">
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="3uibUv" id="gS" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="2ShNRf" id="gT" role="33vP2m">
        <uo k="s:originTrace" v="n:4322367480666010579" />
        <node concept="1pGfFk" id="gU" role="2ShVmc">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptor$MemberDescriptor.&lt;init&gt;(java.lang.String,java.lang.String,long,java.lang.String,java.lang.String,java.lang.String)" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
          <node concept="Xl_RD" id="gV" role="37wK5m">
            <property role="Xl_RC" value="UPDATE" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="Xl_RD" id="gW" role="37wK5m">
            <property role="Xl_RC" value="UPDATE" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="1adDum" id="gX" role="37wK5m">
            <property role="1adDun" value="0x3bfc2252a1cc33e1L" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="Xl_RD" id="gY" role="37wK5m">
            <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4322367480666010593" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3Tm1VV" id="g1" role="1B3o_S">
      <uo k="s:originTrace" v="n:4322367480666010579" />
    </node>
    <node concept="3uibUv" id="g2" role="1zkMxy">
      <ref role="3uigEE" to="ze1i:~EnumerationDescriptorBase" resolve="EnumerationDescriptorBase" />
      <uo k="s:originTrace" v="n:4322367480666010579" />
    </node>
    <node concept="2tJIrI" id="g3" role="jymVt">
      <uo k="s:originTrace" v="n:4322367480666010579" />
    </node>
    <node concept="312cEg" id="g4" role="jymVt">
      <property role="TrG5h" value="myIndex" />
      <property role="3TUv4t" value="true" />
      <uo k="s:originTrace" v="n:4322367480666010579" />
      <node concept="3Tm6S6" id="gZ" role="1B3o_S">
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="3uibUv" id="h0" role="1tU5fm">
        <ref role="3uigEE" to="ksn4:~EnumerationLiteralsIndex" resolve="EnumerationLiteralsIndex" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="2YIFZM" id="h1" role="33vP2m">
        <ref role="37wK5l" to="ksn4:~EnumerationLiteralsIndex.build(long,long,long,long...):jetbrains.mps.lang.smodel.EnumerationLiteralsIndex" resolve="build" />
        <ref role="1Pybhc" to="ksn4:~EnumerationLiteralsIndex" resolve="EnumerationLiteralsIndex" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
        <node concept="1adDum" id="h2" role="37wK5m">
          <property role="1adDun" value="0x29d429a205314919L" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
        </node>
        <node concept="1adDum" id="h3" role="37wK5m">
          <property role="1adDun" value="0x8e806706b89fb3b7L" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
        </node>
        <node concept="1adDum" id="h4" role="37wK5m">
          <property role="1adDun" value="0x3bfc2252a1cc33d3L" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
        </node>
        <node concept="1adDum" id="h5" role="37wK5m">
          <property role="1adDun" value="0x3bfc2252a1cc33d4L" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
        </node>
        <node concept="1adDum" id="h6" role="37wK5m">
          <property role="1adDun" value="0x3bfc2252a1cc33d5L" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
        </node>
        <node concept="1adDum" id="h7" role="37wK5m">
          <property role="1adDun" value="0x3bfc2252a1cc33d8L" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
        </node>
        <node concept="1adDum" id="h8" role="37wK5m">
          <property role="1adDun" value="0x3bfc2252a1cc33dcL" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
        </node>
        <node concept="1adDum" id="h9" role="37wK5m">
          <property role="1adDun" value="0x3bfc2252a1cc33e1L" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
        </node>
      </node>
    </node>
    <node concept="312cEg" id="g5" role="jymVt">
      <property role="TrG5h" value="myMembers" />
      <property role="3TUv4t" value="true" />
      <uo k="s:originTrace" v="n:4322367480666010579" />
      <node concept="3Tm6S6" id="ha" role="1B3o_S">
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="3uibUv" id="hb" role="1tU5fm">
        <ref role="3uigEE" to="33ny:~List" resolve="List" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
        <node concept="3uibUv" id="hd" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
        </node>
      </node>
      <node concept="2ShNRf" id="hc" role="33vP2m">
        <uo k="s:originTrace" v="n:4322367480666010579" />
        <node concept="1pGfFk" id="he" role="2ShVmc">
          <ref role="37wK5l" to="ze1i:~EnumerationDescriptorBase$MembersList.&lt;init&gt;(jetbrains.mps.smodel.runtime.EnumerationDescriptorBase,jetbrains.mps.lang.smodel.EnumerationLiteralsIndex,jetbrains.mps.smodel.runtime.EnumerationDescriptor$MemberDescriptor...)" resolve="EnumerationDescriptorBase.MembersList" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
          <node concept="37vLTw" id="hf" role="37wK5m">
            <ref role="3cqZAo" node="g4" resolve="myIndex" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="37vLTw" id="hg" role="37wK5m">
            <ref role="3cqZAo" node="fW" resolve="myMember_GET_0" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="37vLTw" id="hh" role="37wK5m">
            <ref role="3cqZAo" node="fX" resolve="myMember_PUT_0" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="37vLTw" id="hi" role="37wK5m">
            <ref role="3cqZAo" node="fY" resolve="myMember_POST_0" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="37vLTw" id="hj" role="37wK5m">
            <ref role="3cqZAo" node="fZ" resolve="myMember_DELETE_0" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="37vLTw" id="hk" role="37wK5m">
            <ref role="3cqZAo" node="g0" resolve="myMember_UPDATE_0" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="g6" role="jymVt">
      <uo k="s:originTrace" v="n:4322367480666010579" />
    </node>
    <node concept="3clFb_" id="g7" role="jymVt">
      <property role="TrG5h" value="getDefault" />
      <uo k="s:originTrace" v="n:4322367480666010579" />
      <node concept="3Tm1VV" id="hl" role="1B3o_S">
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="2AHcQZ" id="hm" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="3uibUv" id="hn" role="3clF45">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="3clFbS" id="ho" role="3clF47">
        <uo k="s:originTrace" v="n:4322367480666010579" />
        <node concept="3clFbF" id="hq" role="3cqZAp">
          <uo k="s:originTrace" v="n:4322367480666010579" />
          <node concept="10Nm6u" id="hr" role="3clFbG">
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="hp" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
    </node>
    <node concept="2tJIrI" id="g8" role="jymVt">
      <uo k="s:originTrace" v="n:4322367480666010579" />
    </node>
    <node concept="3clFb_" id="g9" role="jymVt">
      <property role="TrG5h" value="getMembers" />
      <uo k="s:originTrace" v="n:4322367480666010579" />
      <node concept="3Tm1VV" id="hs" role="1B3o_S">
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="2AHcQZ" id="ht" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="3uibUv" id="hu" role="3clF45">
        <ref role="3uigEE" to="33ny:~List" resolve="List" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
        <node concept="3uibUv" id="hx" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
        </node>
      </node>
      <node concept="3clFbS" id="hv" role="3clF47">
        <uo k="s:originTrace" v="n:4322367480666010579" />
        <node concept="3cpWs6" id="hy" role="3cqZAp">
          <uo k="s:originTrace" v="n:4322367480666010579" />
          <node concept="37vLTw" id="hz" role="3cqZAk">
            <ref role="3cqZAo" node="g5" resolve="myMembers" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="hw" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
    </node>
    <node concept="2tJIrI" id="ga" role="jymVt">
      <uo k="s:originTrace" v="n:4322367480666010579" />
    </node>
    <node concept="3clFb_" id="gb" role="jymVt">
      <property role="TrG5h" value="getMember" />
      <uo k="s:originTrace" v="n:4322367480666010579" />
      <node concept="3Tm1VV" id="h$" role="1B3o_S">
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="2AHcQZ" id="h_" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="3uibUv" id="hA" role="3clF45">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="37vLTG" id="hB" role="3clF46">
        <property role="TrG5h" value="memberName" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
        <node concept="3uibUv" id="hE" role="1tU5fm">
          <ref role="3uigEE" to="wyt6:~String" resolve="String" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
        </node>
        <node concept="2AHcQZ" id="hF" role="2AJF6D">
          <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
          <uo k="s:originTrace" v="n:4322367480666010579" />
        </node>
      </node>
      <node concept="3clFbS" id="hC" role="3clF47">
        <uo k="s:originTrace" v="n:4322367480666010579" />
        <node concept="3clFbJ" id="hG" role="3cqZAp">
          <uo k="s:originTrace" v="n:4322367480666010579" />
          <node concept="3clFbS" id="hJ" role="3clFbx">
            <uo k="s:originTrace" v="n:4322367480666010579" />
            <node concept="3cpWs6" id="hL" role="3cqZAp">
              <uo k="s:originTrace" v="n:4322367480666010579" />
              <node concept="10Nm6u" id="hM" role="3cqZAk">
                <uo k="s:originTrace" v="n:4322367480666010579" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="hK" role="3clFbw">
            <uo k="s:originTrace" v="n:4322367480666010579" />
            <node concept="10Nm6u" id="hN" role="3uHU7w">
              <uo k="s:originTrace" v="n:4322367480666010579" />
            </node>
            <node concept="37vLTw" id="hO" role="3uHU7B">
              <ref role="3cqZAo" node="hB" resolve="memberName" />
              <uo k="s:originTrace" v="n:4322367480666010579" />
            </node>
          </node>
        </node>
        <node concept="3KaCP$" id="hH" role="3cqZAp">
          <uo k="s:originTrace" v="n:4322367480666010579" />
          <node concept="37vLTw" id="hP" role="3KbGdf">
            <ref role="3cqZAo" node="hB" resolve="memberName" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
          <node concept="3KbdKl" id="hQ" role="3KbHQx">
            <uo k="s:originTrace" v="n:4322367480666010579" />
            <node concept="Xl_RD" id="hV" role="3Kbmr1">
              <property role="Xl_RC" value="GET" />
              <uo k="s:originTrace" v="n:4322367480666010579" />
            </node>
            <node concept="3clFbS" id="hW" role="3Kbo56">
              <uo k="s:originTrace" v="n:4322367480666010579" />
              <node concept="3cpWs6" id="hX" role="3cqZAp">
                <uo k="s:originTrace" v="n:4322367480666010579" />
                <node concept="37vLTw" id="hY" role="3cqZAk">
                  <ref role="3cqZAo" node="fW" resolve="myMember_GET_0" />
                  <uo k="s:originTrace" v="n:4322367480666010579" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="hR" role="3KbHQx">
            <uo k="s:originTrace" v="n:4322367480666010579" />
            <node concept="Xl_RD" id="hZ" role="3Kbmr1">
              <property role="Xl_RC" value="PUT" />
              <uo k="s:originTrace" v="n:4322367480666010579" />
            </node>
            <node concept="3clFbS" id="i0" role="3Kbo56">
              <uo k="s:originTrace" v="n:4322367480666010579" />
              <node concept="3cpWs6" id="i1" role="3cqZAp">
                <uo k="s:originTrace" v="n:4322367480666010579" />
                <node concept="37vLTw" id="i2" role="3cqZAk">
                  <ref role="3cqZAo" node="fX" resolve="myMember_PUT_0" />
                  <uo k="s:originTrace" v="n:4322367480666010579" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="hS" role="3KbHQx">
            <uo k="s:originTrace" v="n:4322367480666010579" />
            <node concept="Xl_RD" id="i3" role="3Kbmr1">
              <property role="Xl_RC" value="POST" />
              <uo k="s:originTrace" v="n:4322367480666010579" />
            </node>
            <node concept="3clFbS" id="i4" role="3Kbo56">
              <uo k="s:originTrace" v="n:4322367480666010579" />
              <node concept="3cpWs6" id="i5" role="3cqZAp">
                <uo k="s:originTrace" v="n:4322367480666010579" />
                <node concept="37vLTw" id="i6" role="3cqZAk">
                  <ref role="3cqZAo" node="fY" resolve="myMember_POST_0" />
                  <uo k="s:originTrace" v="n:4322367480666010579" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="hT" role="3KbHQx">
            <uo k="s:originTrace" v="n:4322367480666010579" />
            <node concept="Xl_RD" id="i7" role="3Kbmr1">
              <property role="Xl_RC" value="DELETE" />
              <uo k="s:originTrace" v="n:4322367480666010579" />
            </node>
            <node concept="3clFbS" id="i8" role="3Kbo56">
              <uo k="s:originTrace" v="n:4322367480666010579" />
              <node concept="3cpWs6" id="i9" role="3cqZAp">
                <uo k="s:originTrace" v="n:4322367480666010579" />
                <node concept="37vLTw" id="ia" role="3cqZAk">
                  <ref role="3cqZAo" node="fZ" resolve="myMember_DELETE_0" />
                  <uo k="s:originTrace" v="n:4322367480666010579" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="hU" role="3KbHQx">
            <uo k="s:originTrace" v="n:4322367480666010579" />
            <node concept="Xl_RD" id="ib" role="3Kbmr1">
              <property role="Xl_RC" value="UPDATE" />
              <uo k="s:originTrace" v="n:4322367480666010579" />
            </node>
            <node concept="3clFbS" id="ic" role="3Kbo56">
              <uo k="s:originTrace" v="n:4322367480666010579" />
              <node concept="3cpWs6" id="id" role="3cqZAp">
                <uo k="s:originTrace" v="n:4322367480666010579" />
                <node concept="37vLTw" id="ie" role="3cqZAk">
                  <ref role="3cqZAo" node="g0" resolve="myMember_UPDATE_0" />
                  <uo k="s:originTrace" v="n:4322367480666010579" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="hI" role="3cqZAp">
          <uo k="s:originTrace" v="n:4322367480666010579" />
          <node concept="10Nm6u" id="if" role="3cqZAk">
            <uo k="s:originTrace" v="n:4322367480666010579" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="hD" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
    </node>
    <node concept="2tJIrI" id="gc" role="jymVt">
      <uo k="s:originTrace" v="n:4322367480666010579" />
    </node>
    <node concept="3clFb_" id="gd" role="jymVt">
      <property role="TrG5h" value="getMember" />
      <uo k="s:originTrace" v="n:4322367480666010579" />
      <node concept="3Tm1VV" id="ig" role="1B3o_S">
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="2AHcQZ" id="ih" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="3uibUv" id="ii" role="3clF45">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor$MemberDescriptor" resolve="EnumerationDescriptor.MemberDescriptor" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
      <node concept="37vLTG" id="ij" role="3clF46">
        <property role="TrG5h" value="idValue" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
        <node concept="3cpWsb" id="im" role="1tU5fm">
          <uo k="s:originTrace" v="n:4322367480666010579" />
        </node>
      </node>
      <node concept="3clFbS" id="ik" role="3clF47">
        <uo k="s:originTrace" v="n:4322367480666010579" />
        <node concept="3cpWs8" id="in" role="3cqZAp">
          <uo k="s:originTrace" v="n:4322367480666010579" />
          <node concept="3cpWsn" id="iq" role="3cpWs9">
            <property role="TrG5h" value="index" />
            <uo k="s:originTrace" v="n:4322367480666010579" />
            <node concept="10Oyi0" id="ir" role="1tU5fm">
              <uo k="s:originTrace" v="n:4322367480666010579" />
            </node>
            <node concept="2OqwBi" id="is" role="33vP2m">
              <uo k="s:originTrace" v="n:4322367480666010579" />
              <node concept="37vLTw" id="it" role="2Oq$k0">
                <ref role="3cqZAo" node="g4" resolve="myIndex" />
                <uo k="s:originTrace" v="n:4322367480666010579" />
              </node>
              <node concept="liA8E" id="iu" role="2OqNvi">
                <ref role="37wK5l" to="ksn4:~EnumerationLiteralsIndex.index(long):int" resolve="index" />
                <uo k="s:originTrace" v="n:4322367480666010579" />
                <node concept="37vLTw" id="iv" role="37wK5m">
                  <ref role="3cqZAo" node="ij" resolve="idValue" />
                  <uo k="s:originTrace" v="n:4322367480666010579" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="io" role="3cqZAp">
          <uo k="s:originTrace" v="n:4322367480666010579" />
          <node concept="3clFbS" id="iw" role="3clFbx">
            <uo k="s:originTrace" v="n:4322367480666010579" />
            <node concept="3cpWs6" id="iy" role="3cqZAp">
              <uo k="s:originTrace" v="n:4322367480666010579" />
              <node concept="10Nm6u" id="iz" role="3cqZAk">
                <uo k="s:originTrace" v="n:4322367480666010579" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="ix" role="3clFbw">
            <uo k="s:originTrace" v="n:4322367480666010579" />
            <node concept="3cmrfG" id="i$" role="3uHU7w">
              <property role="3cmrfH" value="-1" />
              <uo k="s:originTrace" v="n:4322367480666010579" />
            </node>
            <node concept="37vLTw" id="i_" role="3uHU7B">
              <ref role="3cqZAo" node="iq" resolve="index" />
              <uo k="s:originTrace" v="n:4322367480666010579" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ip" role="3cqZAp">
          <uo k="s:originTrace" v="n:4322367480666010579" />
          <node concept="2OqwBi" id="iA" role="3clFbG">
            <uo k="s:originTrace" v="n:4322367480666010579" />
            <node concept="37vLTw" id="iB" role="2Oq$k0">
              <ref role="3cqZAo" node="g5" resolve="myMembers" />
              <uo k="s:originTrace" v="n:4322367480666010579" />
            </node>
            <node concept="liA8E" id="iC" role="2OqNvi">
              <ref role="37wK5l" to="33ny:~List.get(int):java.lang.Object" resolve="get" />
              <uo k="s:originTrace" v="n:4322367480666010579" />
              <node concept="37vLTw" id="iD" role="37wK5m">
                <ref role="3cqZAo" node="iq" resolve="index" />
                <uo k="s:originTrace" v="n:4322367480666010579" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="il" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4322367480666010579" />
      </node>
    </node>
  </node>
  <node concept="39dXUE" id="iE">
    <node concept="39e2AJ" id="iF" role="39e2AI">
      <property role="39e3Y2" value="EnumerationDescriptorCons" />
      <node concept="39e2AG" id="iJ" role="39e3Y0">
        <ref role="39e2AK" to="7thy:3JsE8ainS_I" resolve="DataType" />
        <node concept="385nmt" id="iM" role="385vvn">
          <property role="385vuF" value="DataType" />
          <node concept="3u3nmq" id="iO" role="385v07">
            <property role="3u3nmv" value="4313507821869042030" />
          </node>
        </node>
        <node concept="39e2AT" id="iN" role="39e2AY">
          <ref role="39e2AS" node="b6" resolve="EnumerationDescriptor_DataType" />
        </node>
      </node>
      <node concept="39e2AG" id="iK" role="39e3Y0">
        <ref role="39e2AK" to="7thy:4favA_iB$7N" resolve="MediaType" />
        <node concept="385nmt" id="iP" role="385vvn">
          <property role="385vuF" value="MediaType" />
          <node concept="3u3nmq" id="iR" role="385v07">
            <property role="3u3nmv" value="4884855736659493363" />
          </node>
        </node>
        <node concept="39e2AT" id="iQ" role="39e2AY">
          <ref role="39e2AS" node="dC" resolve="EnumerationDescriptor_MediaType" />
        </node>
      </node>
      <node concept="39e2AG" id="iL" role="39e3Y0">
        <ref role="39e2AK" to="7thy:3JW8_axN3fj" resolve="Operation" />
        <node concept="385nmt" id="iS" role="385vvn">
          <property role="385vuF" value="Operation" />
          <node concept="3u3nmq" id="iU" role="385v07">
            <property role="3u3nmv" value="4322367480666010579" />
          </node>
        </node>
        <node concept="39e2AT" id="iT" role="39e2AY">
          <ref role="39e2AS" node="fU" resolve="EnumerationDescriptor_Operation" />
        </node>
      </node>
    </node>
    <node concept="39e2AJ" id="iG" role="39e2AI">
      <property role="39e3Y2" value="EnumerationMember" />
      <node concept="39e2AG" id="iV" role="39e3Y0">
        <ref role="39e2AK" to="7thy:4favA_iB$7O" resolve="APPLICATION_JSON" />
        <node concept="385nmt" id="j7" role="385vvn">
          <property role="385vuF" value="APPLICATION_JSON" />
          <node concept="3u3nmq" id="j9" role="385v07">
            <property role="3u3nmv" value="4884855736659493364" />
          </node>
        </node>
        <node concept="39e2AT" id="j8" role="39e2AY">
          <ref role="39e2AS" node="dE" resolve="myMember_APPLICATION_JSON_0" />
        </node>
      </node>
      <node concept="39e2AG" id="iW" role="39e3Y0">
        <ref role="39e2AK" to="7thy:3JsE8ainS_R" resolve="BOOLEAN" />
        <node concept="385nmt" id="ja" role="385vvn">
          <property role="385vuF" value="BOOLEAN" />
          <node concept="3u3nmq" id="jc" role="385v07">
            <property role="3u3nmv" value="4313507821869042039" />
          </node>
        </node>
        <node concept="39e2AT" id="jb" role="39e2AY">
          <ref role="39e2AS" node="bb" resolve="myMember_BOOLEAN_0" />
        </node>
      </node>
      <node concept="39e2AG" id="iX" role="39e3Y0">
        <ref role="39e2AK" to="7thy:3JW8_axN3fs" resolve="DELETE" />
        <node concept="385nmt" id="jd" role="385vvn">
          <property role="385vuF" value="DELETE" />
          <node concept="3u3nmq" id="jf" role="385v07">
            <property role="3u3nmv" value="4322367480666010588" />
          </node>
        </node>
        <node concept="39e2AT" id="je" role="39e2AY">
          <ref role="39e2AS" node="fZ" resolve="myMember_DELETE_0" />
        </node>
      </node>
      <node concept="39e2AG" id="iY" role="39e3Y0">
        <ref role="39e2AK" to="7thy:3JW8_axN3fk" resolve="GET" />
        <node concept="385nmt" id="jg" role="385vvn">
          <property role="385vuF" value="GET" />
          <node concept="3u3nmq" id="ji" role="385v07">
            <property role="3u3nmv" value="4322367480666010580" />
          </node>
        </node>
        <node concept="39e2AT" id="jh" role="39e2AY">
          <ref role="39e2AS" node="fW" resolve="myMember_GET_0" />
        </node>
      </node>
      <node concept="39e2AG" id="iZ" role="39e3Y0">
        <ref role="39e2AK" to="7thy:3JsE8ainS_K" resolve="INTEGER" />
        <node concept="385nmt" id="jj" role="385vvn">
          <property role="385vuF" value="INTEGER" />
          <node concept="3u3nmq" id="jl" role="385v07">
            <property role="3u3nmv" value="4313507821869042032" />
          </node>
        </node>
        <node concept="39e2AT" id="jk" role="39e2AY">
          <ref role="39e2AS" node="b9" resolve="myMember_INTEGER_0" />
        </node>
      </node>
      <node concept="39e2AG" id="j0" role="39e3Y0">
        <ref role="39e2AK" to="7thy:3JsE8ainS_N" resolve="NUMBER" />
        <node concept="385nmt" id="jm" role="385vvn">
          <property role="385vuF" value="NUMBER" />
          <node concept="3u3nmq" id="jo" role="385v07">
            <property role="3u3nmv" value="4313507821869042035" />
          </node>
        </node>
        <node concept="39e2AT" id="jn" role="39e2AY">
          <ref role="39e2AS" node="ba" resolve="myMember_NUMBER_0" />
        </node>
      </node>
      <node concept="39e2AG" id="j1" role="39e3Y0">
        <ref role="39e2AK" to="7thy:3JW8_axN3fo" resolve="POST" />
        <node concept="385nmt" id="jp" role="385vvn">
          <property role="385vuF" value="POST" />
          <node concept="3u3nmq" id="jr" role="385v07">
            <property role="3u3nmv" value="4322367480666010584" />
          </node>
        </node>
        <node concept="39e2AT" id="jq" role="39e2AY">
          <ref role="39e2AS" node="fY" resolve="myMember_POST_0" />
        </node>
      </node>
      <node concept="39e2AG" id="j2" role="39e3Y0">
        <ref role="39e2AK" to="7thy:3JW8_axN3fl" resolve="PUT" />
        <node concept="385nmt" id="js" role="385vvn">
          <property role="385vuF" value="PUT" />
          <node concept="3u3nmq" id="ju" role="385v07">
            <property role="3u3nmv" value="4322367480666010581" />
          </node>
        </node>
        <node concept="39e2AT" id="jt" role="39e2AY">
          <ref role="39e2AS" node="fX" resolve="myMember_PUT_0" />
        </node>
      </node>
      <node concept="39e2AG" id="j3" role="39e3Y0">
        <ref role="39e2AK" to="7thy:3JsE8ainS_J" resolve="STRING" />
        <node concept="385nmt" id="jv" role="385vvn">
          <property role="385vuF" value="STRING" />
          <node concept="3u3nmq" id="jx" role="385v07">
            <property role="3u3nmv" value="4313507821869042031" />
          </node>
        </node>
        <node concept="39e2AT" id="jw" role="39e2AY">
          <ref role="39e2AS" node="b8" resolve="myMember_STRING_0" />
        </node>
      </node>
      <node concept="39e2AG" id="j4" role="39e3Y0">
        <ref role="39e2AK" to="7thy:4favA_iB$7P" resolve="TEXT_HTML" />
        <node concept="385nmt" id="jy" role="385vvn">
          <property role="385vuF" value="TEXT_HTML" />
          <node concept="3u3nmq" id="j$" role="385v07">
            <property role="3u3nmv" value="4884855736659493365" />
          </node>
        </node>
        <node concept="39e2AT" id="jz" role="39e2AY">
          <ref role="39e2AS" node="dF" resolve="myMember_TEXT_HTML_0" />
        </node>
      </node>
      <node concept="39e2AG" id="j5" role="39e3Y0">
        <ref role="39e2AK" to="7thy:4favA_iB$7S" resolve="TEXT_PLAIN" />
        <node concept="385nmt" id="j_" role="385vvn">
          <property role="385vuF" value="TEXT_PLAIN" />
          <node concept="3u3nmq" id="jB" role="385v07">
            <property role="3u3nmv" value="4884855736659493368" />
          </node>
        </node>
        <node concept="39e2AT" id="jA" role="39e2AY">
          <ref role="39e2AS" node="dG" resolve="myMember_TEXT_PLAIN_0" />
        </node>
      </node>
      <node concept="39e2AG" id="j6" role="39e3Y0">
        <ref role="39e2AK" to="7thy:3JW8_axN3fx" resolve="UPDATE" />
        <node concept="385nmt" id="jC" role="385vvn">
          <property role="385vuF" value="UPDATE" />
          <node concept="3u3nmq" id="jE" role="385v07">
            <property role="3u3nmv" value="4322367480666010593" />
          </node>
        </node>
        <node concept="39e2AT" id="jD" role="39e2AY">
          <ref role="39e2AS" node="g0" resolve="myMember_UPDATE_0" />
        </node>
      </node>
    </node>
    <node concept="39e2AJ" id="iH" role="39e2AI">
      <property role="39e3Y2" value="ConceptPresentationAspectClass" />
      <node concept="39e2AG" id="jF" role="39e3Y0">
        <property role="2mV_xN" value="true" />
        <node concept="39e2AT" id="jG" role="39e2AY">
          <ref role="39e2AS" node="0" resolve="ConceptPresentationAspectImpl" />
        </node>
      </node>
    </node>
    <node concept="39e2AJ" id="iI" role="39e2AI">
      <property role="39e3Y2" value="StructureAspectDescriptorCons" />
      <node concept="39e2AG" id="jH" role="39e3Y0">
        <property role="2mV_xN" value="true" />
        <node concept="39e2AT" id="jI" role="39e2AY">
          <ref role="39e2AS" node="o$" resolve="StructureAspectDescriptor" />
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="jJ">
    <property role="TrG5h" value="LanguageConceptSwitch" />
    <property role="1EXbeo" value="true" />
    <node concept="312cEg" id="jK" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="myIndex" />
      <property role="3TUv4t" value="true" />
      <node concept="3Tm6S6" id="kf" role="1B3o_S" />
      <node concept="3uibUv" id="kg" role="1tU5fm">
        <ref role="3uigEE" to="ksn4:~LanguageConceptIndex" resolve="LanguageConceptIndex" />
      </node>
    </node>
    <node concept="Wx3nA" id="jL" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="AllOfSchema" />
      <node concept="3Tm1VV" id="kh" role="1B3o_S" />
      <node concept="10Oyi0" id="ki" role="1tU5fm" />
      <node concept="3cmrfG" id="kj" role="33vP2m">
        <property role="3cmrfH" value="0" />
      </node>
    </node>
    <node concept="Wx3nA" id="jM" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="AnyOfSchema" />
      <node concept="3Tm1VV" id="kk" role="1B3o_S" />
      <node concept="10Oyi0" id="kl" role="1tU5fm" />
      <node concept="3cmrfG" id="km" role="33vP2m">
        <property role="3cmrfH" value="1" />
      </node>
    </node>
    <node concept="Wx3nA" id="jN" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="ArraySchema" />
      <node concept="3Tm1VV" id="kn" role="1B3o_S" />
      <node concept="10Oyi0" id="ko" role="1tU5fm" />
      <node concept="3cmrfG" id="kp" role="33vP2m">
        <property role="3cmrfH" value="2" />
      </node>
    </node>
    <node concept="Wx3nA" id="jO" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="BooleanSchema" />
      <node concept="3Tm1VV" id="kq" role="1B3o_S" />
      <node concept="10Oyi0" id="kr" role="1tU5fm" />
      <node concept="3cmrfG" id="ks" role="33vP2m">
        <property role="3cmrfH" value="3" />
      </node>
    </node>
    <node concept="Wx3nA" id="jP" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="Content" />
      <node concept="3Tm1VV" id="kt" role="1B3o_S" />
      <node concept="10Oyi0" id="ku" role="1tU5fm" />
      <node concept="3cmrfG" id="kv" role="33vP2m">
        <property role="3cmrfH" value="4" />
      </node>
    </node>
    <node concept="Wx3nA" id="jQ" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="DeleteOperationObject" />
      <node concept="3Tm1VV" id="kw" role="1B3o_S" />
      <node concept="10Oyi0" id="kx" role="1tU5fm" />
      <node concept="3cmrfG" id="ky" role="33vP2m">
        <property role="3cmrfH" value="5" />
      </node>
    </node>
    <node concept="Wx3nA" id="jR" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="GetOperationObject" />
      <node concept="3Tm1VV" id="kz" role="1B3o_S" />
      <node concept="10Oyi0" id="k$" role="1tU5fm" />
      <node concept="3cmrfG" id="k_" role="33vP2m">
        <property role="3cmrfH" value="6" />
      </node>
    </node>
    <node concept="Wx3nA" id="jS" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="IntegerSchema" />
      <node concept="3Tm1VV" id="kA" role="1B3o_S" />
      <node concept="10Oyi0" id="kB" role="1tU5fm" />
      <node concept="3cmrfG" id="kC" role="33vP2m">
        <property role="3cmrfH" value="7" />
      </node>
    </node>
    <node concept="Wx3nA" id="jT" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="ObjectSchema" />
      <node concept="3Tm1VV" id="kD" role="1B3o_S" />
      <node concept="10Oyi0" id="kE" role="1tU5fm" />
      <node concept="3cmrfG" id="kF" role="33vP2m">
        <property role="3cmrfH" value="8" />
      </node>
    </node>
    <node concept="Wx3nA" id="jU" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="ObjectSchemaAdditionalProperty" />
      <node concept="3Tm1VV" id="kG" role="1B3o_S" />
      <node concept="10Oyi0" id="kH" role="1tU5fm" />
      <node concept="3cmrfG" id="kI" role="33vP2m">
        <property role="3cmrfH" value="9" />
      </node>
    </node>
    <node concept="Wx3nA" id="jV" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="ObjectSchemaProperty" />
      <node concept="3Tm1VV" id="kJ" role="1B3o_S" />
      <node concept="10Oyi0" id="kK" role="1tU5fm" />
      <node concept="3cmrfG" id="kL" role="33vP2m">
        <property role="3cmrfH" value="10" />
      </node>
    </node>
    <node concept="Wx3nA" id="jW" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="ObjectSchemaRequiredProperty" />
      <node concept="3Tm1VV" id="kM" role="1B3o_S" />
      <node concept="10Oyi0" id="kN" role="1tU5fm" />
      <node concept="3cmrfG" id="kO" role="33vP2m">
        <property role="3cmrfH" value="11" />
      </node>
    </node>
    <node concept="Wx3nA" id="jX" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="OneOfSchema" />
      <node concept="3Tm1VV" id="kP" role="1B3o_S" />
      <node concept="10Oyi0" id="kQ" role="1tU5fm" />
      <node concept="3cmrfG" id="kR" role="33vP2m">
        <property role="3cmrfH" value="12" />
      </node>
    </node>
    <node concept="Wx3nA" id="jY" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="OpenApi" />
      <node concept="3Tm1VV" id="kS" role="1B3o_S" />
      <node concept="10Oyi0" id="kT" role="1tU5fm" />
      <node concept="3cmrfG" id="kU" role="33vP2m">
        <property role="3cmrfH" value="13" />
      </node>
    </node>
    <node concept="Wx3nA" id="jZ" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="OpenApiInfo" />
      <node concept="3Tm1VV" id="kV" role="1B3o_S" />
      <node concept="10Oyi0" id="kW" role="1tU5fm" />
      <node concept="3cmrfG" id="kX" role="33vP2m">
        <property role="3cmrfH" value="14" />
      </node>
    </node>
    <node concept="Wx3nA" id="k0" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="OperationObject" />
      <node concept="3Tm1VV" id="kY" role="1B3o_S" />
      <node concept="10Oyi0" id="kZ" role="1tU5fm" />
      <node concept="3cmrfG" id="l0" role="33vP2m">
        <property role="3cmrfH" value="15" />
      </node>
    </node>
    <node concept="Wx3nA" id="k1" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="PathObject" />
      <node concept="3Tm1VV" id="l1" role="1B3o_S" />
      <node concept="10Oyi0" id="l2" role="1tU5fm" />
      <node concept="3cmrfG" id="l3" role="33vP2m">
        <property role="3cmrfH" value="16" />
      </node>
    </node>
    <node concept="Wx3nA" id="k2" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="PostOperationObject" />
      <node concept="3Tm1VV" id="l4" role="1B3o_S" />
      <node concept="10Oyi0" id="l5" role="1tU5fm" />
      <node concept="3cmrfG" id="l6" role="33vP2m">
        <property role="3cmrfH" value="17" />
      </node>
    </node>
    <node concept="Wx3nA" id="k3" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="PutOperationObject" />
      <node concept="3Tm1VV" id="l7" role="1B3o_S" />
      <node concept="10Oyi0" id="l8" role="1tU5fm" />
      <node concept="3cmrfG" id="l9" role="33vP2m">
        <property role="3cmrfH" value="18" />
      </node>
    </node>
    <node concept="Wx3nA" id="k4" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="Response" />
      <node concept="3Tm1VV" id="la" role="1B3o_S" />
      <node concept="10Oyi0" id="lb" role="1tU5fm" />
      <node concept="3cmrfG" id="lc" role="33vP2m">
        <property role="3cmrfH" value="19" />
      </node>
    </node>
    <node concept="Wx3nA" id="k5" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="Schema" />
      <node concept="3Tm1VV" id="ld" role="1B3o_S" />
      <node concept="10Oyi0" id="le" role="1tU5fm" />
      <node concept="3cmrfG" id="lf" role="33vP2m">
        <property role="3cmrfH" value="20" />
      </node>
    </node>
    <node concept="Wx3nA" id="k6" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="SchemaRef" />
      <node concept="3Tm1VV" id="lg" role="1B3o_S" />
      <node concept="10Oyi0" id="lh" role="1tU5fm" />
      <node concept="3cmrfG" id="li" role="33vP2m">
        <property role="3cmrfH" value="21" />
      </node>
    </node>
    <node concept="Wx3nA" id="k7" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="StringSchema" />
      <node concept="3Tm1VV" id="lj" role="1B3o_S" />
      <node concept="10Oyi0" id="lk" role="1tU5fm" />
      <node concept="3cmrfG" id="ll" role="33vP2m">
        <property role="3cmrfH" value="22" />
      </node>
    </node>
    <node concept="2tJIrI" id="k8" role="jymVt" />
    <node concept="3clFbW" id="k9" role="jymVt">
      <node concept="3cqZAl" id="lm" role="3clF45" />
      <node concept="3Tm1VV" id="ln" role="1B3o_S" />
      <node concept="3clFbS" id="lo" role="3clF47">
        <node concept="3cpWs8" id="lp" role="3cqZAp">
          <node concept="3cpWsn" id="lM" role="3cpWs9">
            <property role="TrG5h" value="builder" />
            <node concept="3uibUv" id="lN" role="1tU5fm">
              <ref role="3uigEE" to="ksn4:~LanguageConceptIndexBuilder" resolve="LanguageConceptIndexBuilder" />
            </node>
            <node concept="2ShNRf" id="lO" role="33vP2m">
              <node concept="1pGfFk" id="lP" role="2ShVmc">
                <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.&lt;init&gt;(long,long)" resolve="LanguageConceptIndexBuilder" />
                <node concept="1adDum" id="lQ" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="lR" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lq" role="3cqZAp">
          <node concept="2OqwBi" id="lS" role="3clFbG">
            <node concept="37vLTw" id="lT" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="lU" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="lV" role="37wK5m">
                <property role="1adDun" value="0x3bdca882929c266cL" />
              </node>
              <node concept="37vLTw" id="lW" role="37wK5m">
                <ref role="3cqZAo" node="jL" resolve="AllOfSchema" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lr" role="3cqZAp">
          <node concept="2OqwBi" id="lX" role="3clFbG">
            <node concept="37vLTw" id="lY" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="lZ" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="m0" role="37wK5m">
                <property role="1adDun" value="0x3bdca882929c2684L" />
              </node>
              <node concept="37vLTw" id="m1" role="37wK5m">
                <ref role="3cqZAo" node="jM" resolve="AnyOfSchema" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ls" role="3cqZAp">
          <node concept="2OqwBi" id="m2" role="3clFbG">
            <node concept="37vLTw" id="m3" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="m4" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="m5" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529f3e36L" />
              </node>
              <node concept="37vLTw" id="m6" role="37wK5m">
                <ref role="3cqZAo" node="jN" resolve="ArraySchema" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lt" role="3cqZAp">
          <node concept="2OqwBi" id="m7" role="3clFbG">
            <node concept="37vLTw" id="m8" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="m9" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="ma" role="37wK5m">
                <property role="1adDun" value="0x3bdca882929aa6b5L" />
              </node>
              <node concept="37vLTw" id="mb" role="37wK5m">
                <ref role="3cqZAo" node="jO" resolve="BooleanSchema" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lu" role="3cqZAp">
          <node concept="2OqwBi" id="mc" role="3clFbG">
            <node concept="37vLTw" id="md" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="me" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="mf" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529e41f0L" />
              </node>
              <node concept="37vLTw" id="mg" role="37wK5m">
                <ref role="3cqZAo" node="jP" resolve="Content" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lv" role="3cqZAp">
          <node concept="2OqwBi" id="mh" role="3clFbG">
            <node concept="37vLTw" id="mi" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="mj" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="mk" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529d5050L" />
              </node>
              <node concept="37vLTw" id="ml" role="37wK5m">
                <ref role="3cqZAo" node="jQ" resolve="DeleteOperationObject" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lw" role="3cqZAp">
          <node concept="2OqwBi" id="mm" role="3clFbG">
            <node concept="37vLTw" id="mn" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="mo" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="mp" role="37wK5m">
                <property role="1adDun" value="0x3bfc2252a1cc3d60L" />
              </node>
              <node concept="37vLTw" id="mq" role="37wK5m">
                <ref role="3cqZAo" node="jR" resolve="GetOperationObject" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lx" role="3cqZAp">
          <node concept="2OqwBi" id="mr" role="3clFbG">
            <node concept="37vLTw" id="ms" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="mt" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="mu" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529f3e0cL" />
              </node>
              <node concept="37vLTw" id="mv" role="37wK5m">
                <ref role="3cqZAo" node="jS" resolve="IntegerSchema" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ly" role="3cqZAp">
          <node concept="2OqwBi" id="mw" role="3clFbG">
            <node concept="37vLTw" id="mx" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="my" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="mz" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529f3e28L" />
              </node>
              <node concept="37vLTw" id="m$" role="37wK5m">
                <ref role="3cqZAo" node="jT" resolve="ObjectSchema" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lz" role="3cqZAp">
          <node concept="2OqwBi" id="m_" role="3clFbG">
            <node concept="37vLTw" id="mA" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="mB" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="mC" role="37wK5m">
                <property role="1adDun" value="0x3bdca882925f8943L" />
              </node>
              <node concept="37vLTw" id="mD" role="37wK5m">
                <ref role="3cqZAo" node="jU" resolve="ObjectSchemaAdditionalProperty" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="l$" role="3cqZAp">
          <node concept="2OqwBi" id="mE" role="3clFbG">
            <node concept="37vLTw" id="mF" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="mG" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="mH" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529f3e31L" />
              </node>
              <node concept="37vLTw" id="mI" role="37wK5m">
                <ref role="3cqZAo" node="jV" resolve="ObjectSchemaProperty" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="l_" role="3cqZAp">
          <node concept="2OqwBi" id="mJ" role="3clFbG">
            <node concept="37vLTw" id="mK" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="mL" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="mM" role="37wK5m">
                <property role="1adDun" value="0x3bdca882927a4101L" />
              </node>
              <node concept="37vLTw" id="mN" role="37wK5m">
                <ref role="3cqZAo" node="jW" resolve="ObjectSchemaRequiredProperty" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lA" role="3cqZAp">
          <node concept="2OqwBi" id="mO" role="3clFbG">
            <node concept="37vLTw" id="mP" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="mQ" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="mR" role="37wK5m">
                <property role="1adDun" value="0x3bdca882929c263eL" />
              </node>
              <node concept="37vLTw" id="mS" role="37wK5m">
                <ref role="3cqZAo" node="jX" resolve="OneOfSchema" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lB" role="3cqZAp">
          <node concept="2OqwBi" id="mT" role="3clFbG">
            <node concept="37vLTw" id="mU" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="mV" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="mW" role="37wK5m">
                <property role="1adDun" value="0x3bfc2252a1cbc1eeL" />
              </node>
              <node concept="37vLTw" id="mX" role="37wK5m">
                <ref role="3cqZAo" node="jY" resolve="OpenApi" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lC" role="3cqZAp">
          <node concept="2OqwBi" id="mY" role="3clFbG">
            <node concept="37vLTw" id="mZ" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="n0" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="n1" role="37wK5m">
                <property role="1adDun" value="0x3bfc2252a1cab6b8L" />
              </node>
              <node concept="37vLTw" id="n2" role="37wK5m">
                <ref role="3cqZAo" node="jZ" resolve="OpenApiInfo" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lD" role="3cqZAp">
          <node concept="2OqwBi" id="n3" role="3clFbG">
            <node concept="37vLTw" id="n4" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="n5" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="n6" role="37wK5m">
                <property role="1adDun" value="0x3bfc2252a1cc33d0L" />
              </node>
              <node concept="37vLTw" id="n7" role="37wK5m">
                <ref role="3cqZAo" node="k0" resolve="OperationObject" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lE" role="3cqZAp">
          <node concept="2OqwBi" id="n8" role="3clFbG">
            <node concept="37vLTw" id="n9" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="na" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="nb" role="37wK5m">
                <property role="1adDun" value="0x3bfc2252a1cc33c4L" />
              </node>
              <node concept="37vLTw" id="nc" role="37wK5m">
                <ref role="3cqZAo" node="k1" resolve="PathObject" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lF" role="3cqZAp">
          <node concept="2OqwBi" id="nd" role="3clFbG">
            <node concept="37vLTw" id="ne" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="nf" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="ng" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529d504fL" />
              </node>
              <node concept="37vLTw" id="nh" role="37wK5m">
                <ref role="3cqZAo" node="k2" resolve="PostOperationObject" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lG" role="3cqZAp">
          <node concept="2OqwBi" id="ni" role="3clFbG">
            <node concept="37vLTw" id="nj" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="nk" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="nl" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529d504eL" />
              </node>
              <node concept="37vLTw" id="nm" role="37wK5m">
                <ref role="3cqZAo" node="k3" resolve="PutOperationObject" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lH" role="3cqZAp">
          <node concept="2OqwBi" id="nn" role="3clFbG">
            <node concept="37vLTw" id="no" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="np" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="nq" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529a23cbL" />
              </node>
              <node concept="37vLTw" id="nr" role="37wK5m">
                <ref role="3cqZAo" node="k4" resolve="Response" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lI" role="3cqZAp">
          <node concept="2OqwBi" id="ns" role="3clFbG">
            <node concept="37vLTw" id="nt" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="nu" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="nv" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529f3c94L" />
              </node>
              <node concept="37vLTw" id="nw" role="37wK5m">
                <ref role="3cqZAo" node="k5" resolve="Schema" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lJ" role="3cqZAp">
          <node concept="2OqwBi" id="nx" role="3clFbG">
            <node concept="37vLTw" id="ny" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="nz" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="n$" role="37wK5m">
                <property role="1adDun" value="0x3bdca882929f537fL" />
              </node>
              <node concept="37vLTw" id="n_" role="37wK5m">
                <ref role="3cqZAo" node="k6" resolve="SchemaRef" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lK" role="3cqZAp">
          <node concept="2OqwBi" id="nA" role="3clFbG">
            <node concept="37vLTw" id="nB" role="2Oq$k0">
              <ref role="3cqZAo" node="lM" resolve="builder" />
            </node>
            <node concept="liA8E" id="nC" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int)" resolve="put" />
              <node concept="1adDum" id="nD" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529f3cb2L" />
              </node>
              <node concept="37vLTw" id="nE" role="37wK5m">
                <ref role="3cqZAo" node="k7" resolve="StringSchema" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lL" role="3cqZAp">
          <node concept="37vLTI" id="nF" role="3clFbG">
            <node concept="2OqwBi" id="nG" role="37vLTx">
              <node concept="37vLTw" id="nI" role="2Oq$k0">
                <ref role="3cqZAo" node="lM" resolve="builder" />
              </node>
              <node concept="liA8E" id="nJ" role="2OqNvi">
                <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.seal()" resolve="seal" />
              </node>
            </node>
            <node concept="37vLTw" id="nH" role="37vLTJ">
              <ref role="3cqZAo" node="jK" resolve="myIndex" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="ka" role="jymVt" />
    <node concept="3clFb_" id="kb" role="jymVt">
      <property role="TrG5h" value="index" />
      <node concept="10Oyi0" id="nK" role="3clF45" />
      <node concept="3clFbS" id="nL" role="3clF47">
        <node concept="3cpWs6" id="nN" role="3cqZAp">
          <node concept="2OqwBi" id="nO" role="3cqZAk">
            <node concept="37vLTw" id="nP" role="2Oq$k0">
              <ref role="3cqZAo" node="jK" resolve="myIndex" />
            </node>
            <node concept="liA8E" id="nQ" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndex.index(jetbrains.mps.smodel.adapter.ids.SConceptId)" resolve="index" />
              <node concept="37vLTw" id="nR" role="37wK5m">
                <ref role="3cqZAo" node="nM" resolve="cid" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="nM" role="3clF46">
        <property role="TrG5h" value="cid" />
        <node concept="3uibUv" id="nS" role="1tU5fm">
          <ref role="3uigEE" to="e8bb:~SConceptId" resolve="SConceptId" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="kc" role="jymVt" />
    <node concept="3clFb_" id="kd" role="jymVt">
      <property role="TrG5h" value="index" />
      <node concept="10Oyi0" id="nT" role="3clF45" />
      <node concept="3Tm1VV" id="nU" role="1B3o_S" />
      <node concept="3clFbS" id="nV" role="3clF47">
        <node concept="3cpWs6" id="nX" role="3cqZAp">
          <node concept="2OqwBi" id="nY" role="3cqZAk">
            <node concept="37vLTw" id="nZ" role="2Oq$k0">
              <ref role="3cqZAo" node="jK" resolve="myIndex" />
            </node>
            <node concept="liA8E" id="o0" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~ConceptIndex.index(org.jetbrains.mps.openapi.language.SAbstractConcept)" resolve="index" />
              <node concept="37vLTw" id="o1" role="37wK5m">
                <ref role="3cqZAo" node="nW" resolve="concept" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="nW" role="3clF46">
        <property role="TrG5h" value="concept" />
        <node concept="3uibUv" id="o2" role="1tU5fm">
          <ref role="3uigEE" to="c17a:~SAbstractConcept" resolve="SAbstractConcept" />
        </node>
      </node>
    </node>
    <node concept="3Tm1VV" id="ke" role="1B3o_S" />
  </node>
  <node concept="312cEu" id="o3">
    <property role="TrG5h" value="StructureAspectDescriptor" />
    <node concept="3uibUv" id="o4" role="1zkMxy">
      <ref role="3uigEE" to="ze1i:~BaseStructureAspectDescriptor" resolve="BaseStructureAspectDescriptor" />
    </node>
    <node concept="312cEg" id="o5" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptAllOfSchema" />
      <node concept="3uibUv" id="p8" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="p9" role="33vP2m">
        <ref role="37wK5l" node="oL" resolve="createDescriptorForAllOfSchema" />
      </node>
    </node>
    <node concept="312cEg" id="o6" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptAnyOfSchema" />
      <node concept="3uibUv" id="pa" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pb" role="33vP2m">
        <ref role="37wK5l" node="oM" resolve="createDescriptorForAnyOfSchema" />
      </node>
    </node>
    <node concept="312cEg" id="o7" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptArraySchema" />
      <node concept="3uibUv" id="pc" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pd" role="33vP2m">
        <ref role="37wK5l" node="oN" resolve="createDescriptorForArraySchema" />
      </node>
    </node>
    <node concept="312cEg" id="o8" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptBooleanSchema" />
      <node concept="3uibUv" id="pe" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pf" role="33vP2m">
        <ref role="37wK5l" node="oO" resolve="createDescriptorForBooleanSchema" />
      </node>
    </node>
    <node concept="312cEg" id="o9" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptContent" />
      <node concept="3uibUv" id="pg" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="ph" role="33vP2m">
        <ref role="37wK5l" node="oP" resolve="createDescriptorForContent" />
      </node>
    </node>
    <node concept="312cEg" id="oa" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptDeleteOperationObject" />
      <node concept="3uibUv" id="pi" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pj" role="33vP2m">
        <ref role="37wK5l" node="oQ" resolve="createDescriptorForDeleteOperationObject" />
      </node>
    </node>
    <node concept="312cEg" id="ob" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptGetOperationObject" />
      <node concept="3uibUv" id="pk" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pl" role="33vP2m">
        <ref role="37wK5l" node="oR" resolve="createDescriptorForGetOperationObject" />
      </node>
    </node>
    <node concept="312cEg" id="oc" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptIntegerSchema" />
      <node concept="3uibUv" id="pm" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pn" role="33vP2m">
        <ref role="37wK5l" node="oS" resolve="createDescriptorForIntegerSchema" />
      </node>
    </node>
    <node concept="312cEg" id="od" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptObjectSchema" />
      <node concept="3uibUv" id="po" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pp" role="33vP2m">
        <ref role="37wK5l" node="oT" resolve="createDescriptorForObjectSchema" />
      </node>
    </node>
    <node concept="312cEg" id="oe" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptObjectSchemaAdditionalProperty" />
      <node concept="3uibUv" id="pq" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pr" role="33vP2m">
        <ref role="37wK5l" node="oU" resolve="createDescriptorForObjectSchemaAdditionalProperty" />
      </node>
    </node>
    <node concept="312cEg" id="of" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptObjectSchemaProperty" />
      <node concept="3uibUv" id="ps" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pt" role="33vP2m">
        <ref role="37wK5l" node="oV" resolve="createDescriptorForObjectSchemaProperty" />
      </node>
    </node>
    <node concept="312cEg" id="og" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptObjectSchemaRequiredProperty" />
      <node concept="3uibUv" id="pu" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pv" role="33vP2m">
        <ref role="37wK5l" node="oW" resolve="createDescriptorForObjectSchemaRequiredProperty" />
      </node>
    </node>
    <node concept="312cEg" id="oh" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptOneOfSchema" />
      <node concept="3uibUv" id="pw" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="px" role="33vP2m">
        <ref role="37wK5l" node="oX" resolve="createDescriptorForOneOfSchema" />
      </node>
    </node>
    <node concept="312cEg" id="oi" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptOpenApi" />
      <node concept="3uibUv" id="py" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pz" role="33vP2m">
        <ref role="37wK5l" node="oY" resolve="createDescriptorForOpenApi" />
      </node>
    </node>
    <node concept="312cEg" id="oj" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptOpenApiInfo" />
      <node concept="3uibUv" id="p$" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="p_" role="33vP2m">
        <ref role="37wK5l" node="oZ" resolve="createDescriptorForOpenApiInfo" />
      </node>
    </node>
    <node concept="312cEg" id="ok" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptOperationObject" />
      <node concept="3uibUv" id="pA" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pB" role="33vP2m">
        <ref role="37wK5l" node="p0" resolve="createDescriptorForOperationObject" />
      </node>
    </node>
    <node concept="312cEg" id="ol" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptPathObject" />
      <node concept="3uibUv" id="pC" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pD" role="33vP2m">
        <ref role="37wK5l" node="p1" resolve="createDescriptorForPathObject" />
      </node>
    </node>
    <node concept="312cEg" id="om" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptPostOperationObject" />
      <node concept="3uibUv" id="pE" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pF" role="33vP2m">
        <ref role="37wK5l" node="p2" resolve="createDescriptorForPostOperationObject" />
      </node>
    </node>
    <node concept="312cEg" id="on" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptPutOperationObject" />
      <node concept="3uibUv" id="pG" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pH" role="33vP2m">
        <ref role="37wK5l" node="p3" resolve="createDescriptorForPutOperationObject" />
      </node>
    </node>
    <node concept="312cEg" id="oo" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptResponse" />
      <node concept="3uibUv" id="pI" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pJ" role="33vP2m">
        <ref role="37wK5l" node="p4" resolve="createDescriptorForResponse" />
      </node>
    </node>
    <node concept="312cEg" id="op" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptSchema" />
      <node concept="3uibUv" id="pK" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pL" role="33vP2m">
        <ref role="37wK5l" node="p5" resolve="createDescriptorForSchema" />
      </node>
    </node>
    <node concept="312cEg" id="oq" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptSchemaRef" />
      <node concept="3uibUv" id="pM" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pN" role="33vP2m">
        <ref role="37wK5l" node="p6" resolve="createDescriptorForSchemaRef" />
      </node>
    </node>
    <node concept="312cEg" id="or" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptStringSchema" />
      <node concept="3uibUv" id="pO" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="pP" role="33vP2m">
        <ref role="37wK5l" node="p7" resolve="createDescriptorForStringSchema" />
      </node>
    </node>
    <node concept="312cEg" id="os" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myEnumerationDataType" />
      <node concept="3uibUv" id="pQ" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor" resolve="EnumerationDescriptor" />
      </node>
      <node concept="2ShNRf" id="pR" role="33vP2m">
        <node concept="1pGfFk" id="pS" role="2ShVmc">
          <ref role="37wK5l" node="b6" resolve="EnumerationDescriptor_DataType" />
        </node>
      </node>
    </node>
    <node concept="312cEg" id="ot" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myEnumerationMediaType" />
      <node concept="3uibUv" id="pT" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor" resolve="EnumerationDescriptor" />
      </node>
      <node concept="2ShNRf" id="pU" role="33vP2m">
        <node concept="1pGfFk" id="pV" role="2ShVmc">
          <ref role="37wK5l" node="dC" resolve="EnumerationDescriptor_MediaType" />
        </node>
      </node>
    </node>
    <node concept="312cEg" id="ou" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myEnumerationOperation" />
      <node concept="3uibUv" id="pW" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~EnumerationDescriptor" resolve="EnumerationDescriptor" />
      </node>
      <node concept="2ShNRf" id="pX" role="33vP2m">
        <node concept="1pGfFk" id="pY" role="2ShVmc">
          <ref role="37wK5l" node="fU" resolve="EnumerationDescriptor_Operation" />
        </node>
      </node>
    </node>
    <node concept="312cEg" id="ov" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myCSDatatypePath" />
      <node concept="3uibUv" id="pZ" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConstrainedStringDatatypeDescriptor" resolve="ConstrainedStringDatatypeDescriptor" />
      </node>
      <node concept="2ShNRf" id="q0" role="33vP2m">
        <node concept="1pGfFk" id="q1" role="2ShVmc">
          <ref role="37wK5l" to="ze1i:~ConstrainedStringDatatypeDescriptorImpl.&lt;init&gt;(long,long,long,java.lang.String,java.lang.String,java.lang.String)" resolve="ConstrainedStringDatatypeDescriptorImpl" />
          <node concept="1adDum" id="q2" role="37wK5m">
            <property role="1adDun" value="0x29d429a205314919L" />
          </node>
          <node concept="1adDum" id="q3" role="37wK5m">
            <property role="1adDun" value="0x8e806706b89fb3b7L" />
          </node>
          <node concept="1adDum" id="q4" role="37wK5m">
            <property role="1adDun" value="0x3bfc2252a1cc33cdL" />
          </node>
          <node concept="Xl_RD" id="q5" role="37wK5m">
            <property role="Xl_RC" value="Path" />
          </node>
          <node concept="Xl_RD" id="q6" role="37wK5m">
            <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4322367480666010573" />
          </node>
          <node concept="Xl_RD" id="q7" role="37wK5m">
            <property role="Xl_RC" value="/[a-z0-9]+" />
          </node>
        </node>
      </node>
    </node>
    <node concept="312cEg" id="ow" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myCSDatatypeVersion" />
      <node concept="3uibUv" id="q8" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConstrainedStringDatatypeDescriptor" resolve="ConstrainedStringDatatypeDescriptor" />
      </node>
      <node concept="2ShNRf" id="q9" role="33vP2m">
        <node concept="1pGfFk" id="qa" role="2ShVmc">
          <ref role="37wK5l" to="ze1i:~ConstrainedStringDatatypeDescriptorImpl.&lt;init&gt;(long,long,long,java.lang.String,java.lang.String,java.lang.String)" resolve="ConstrainedStringDatatypeDescriptorImpl" />
          <node concept="1adDum" id="qb" role="37wK5m">
            <property role="1adDun" value="0x29d429a205314919L" />
          </node>
          <node concept="1adDum" id="qc" role="37wK5m">
            <property role="1adDun" value="0x8e806706b89fb3b7L" />
          </node>
          <node concept="1adDum" id="qd" role="37wK5m">
            <property role="1adDun" value="0x3bfc2252a1cbe9c5L" />
          </node>
          <node concept="Xl_RD" id="qe" role="37wK5m">
            <property role="Xl_RC" value="Version" />
          </node>
          <node concept="Xl_RD" id="qf" role="37wK5m">
            <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4322367480665991621" />
          </node>
          <node concept="Xl_RD" id="qg" role="37wK5m">
            <property role="Xl_RC" value="[0-9]+\\.[0-9]+\\.[0-9]+" />
          </node>
        </node>
      </node>
    </node>
    <node concept="312cEg" id="ox" role="jymVt">
      <property role="TrG5h" value="myIndexSwitch" />
      <property role="3TUv4t" value="true" />
      <node concept="3Tm6S6" id="qh" role="1B3o_S" />
      <node concept="3uibUv" id="qi" role="1tU5fm">
        <ref role="3uigEE" node="jJ" resolve="LanguageConceptSwitch" />
      </node>
    </node>
    <node concept="3Tm1VV" id="oy" role="1B3o_S" />
    <node concept="2tJIrI" id="oz" role="jymVt" />
    <node concept="3clFbW" id="o$" role="jymVt">
      <node concept="3cqZAl" id="qj" role="3clF45" />
      <node concept="3Tm1VV" id="qk" role="1B3o_S" />
      <node concept="3clFbS" id="ql" role="3clF47">
        <node concept="3clFbF" id="qm" role="3cqZAp">
          <node concept="37vLTI" id="qn" role="3clFbG">
            <node concept="2ShNRf" id="qo" role="37vLTx">
              <node concept="1pGfFk" id="qq" role="2ShVmc">
                <ref role="37wK5l" node="k9" resolve="LanguageConceptSwitch" />
              </node>
            </node>
            <node concept="37vLTw" id="qp" role="37vLTJ">
              <ref role="3cqZAo" node="ox" resolve="myIndexSwitch" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="o_" role="jymVt" />
    <node concept="2tJIrI" id="oA" role="jymVt" />
    <node concept="3clFb_" id="oB" role="jymVt">
      <property role="TrG5h" value="reportDependencies" />
      <node concept="3Tm1VV" id="qr" role="1B3o_S" />
      <node concept="3cqZAl" id="qs" role="3clF45" />
      <node concept="37vLTG" id="qt" role="3clF46">
        <property role="TrG5h" value="deps" />
        <node concept="3uibUv" id="qw" role="1tU5fm">
          <ref role="3uigEE" to="ze1i:~StructureAspectDescriptor$Dependencies" resolve="StructureAspectDescriptor.Dependencies" />
        </node>
      </node>
      <node concept="3clFbS" id="qu" role="3clF47">
        <node concept="3clFbF" id="qx" role="3cqZAp">
          <node concept="2OqwBi" id="qy" role="3clFbG">
            <node concept="37vLTw" id="qz" role="2Oq$k0">
              <ref role="3cqZAo" node="qt" resolve="deps" />
            </node>
            <node concept="liA8E" id="q$" role="2OqNvi">
              <ref role="37wK5l" to="ze1i:~StructureAspectDescriptor$Dependencies.extendedLanguage(long,long,java.lang.String)" resolve="extendedLanguage" />
              <node concept="1adDum" id="q_" role="37wK5m">
                <property role="1adDun" value="0xceab519525ea4f22L" />
              </node>
              <node concept="1adDum" id="qA" role="37wK5m">
                <property role="1adDun" value="0x9b92103b95ca8c0cL" />
              </node>
              <node concept="Xl_RD" id="qB" role="37wK5m">
                <property role="Xl_RC" value="jetbrains.mps.lang.core" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="qv" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="oC" role="jymVt" />
    <node concept="3clFb_" id="oD" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="od$2w" value="false" />
      <property role="TrG5h" value="getDescriptors" />
      <property role="DiZV1" value="false" />
      <node concept="3clFbS" id="qC" role="3clF47">
        <node concept="3cpWs6" id="qG" role="3cqZAp">
          <node concept="2YIFZM" id="qH" role="3cqZAk">
            <ref role="1Pybhc" to="33ny:~Arrays" resolve="Arrays" />
            <ref role="37wK5l" to="33ny:~Arrays.asList(java.lang.Object...)" resolve="asList" />
            <node concept="37vLTw" id="qI" role="37wK5m">
              <ref role="3cqZAo" node="o5" resolve="myConceptAllOfSchema" />
            </node>
            <node concept="37vLTw" id="qJ" role="37wK5m">
              <ref role="3cqZAo" node="o6" resolve="myConceptAnyOfSchema" />
            </node>
            <node concept="37vLTw" id="qK" role="37wK5m">
              <ref role="3cqZAo" node="o7" resolve="myConceptArraySchema" />
            </node>
            <node concept="37vLTw" id="qL" role="37wK5m">
              <ref role="3cqZAo" node="o8" resolve="myConceptBooleanSchema" />
            </node>
            <node concept="37vLTw" id="qM" role="37wK5m">
              <ref role="3cqZAo" node="o9" resolve="myConceptContent" />
            </node>
            <node concept="37vLTw" id="qN" role="37wK5m">
              <ref role="3cqZAo" node="oa" resolve="myConceptDeleteOperationObject" />
            </node>
            <node concept="37vLTw" id="qO" role="37wK5m">
              <ref role="3cqZAo" node="ob" resolve="myConceptGetOperationObject" />
            </node>
            <node concept="37vLTw" id="qP" role="37wK5m">
              <ref role="3cqZAo" node="oc" resolve="myConceptIntegerSchema" />
            </node>
            <node concept="37vLTw" id="qQ" role="37wK5m">
              <ref role="3cqZAo" node="od" resolve="myConceptObjectSchema" />
            </node>
            <node concept="37vLTw" id="qR" role="37wK5m">
              <ref role="3cqZAo" node="oe" resolve="myConceptObjectSchemaAdditionalProperty" />
            </node>
            <node concept="37vLTw" id="qS" role="37wK5m">
              <ref role="3cqZAo" node="of" resolve="myConceptObjectSchemaProperty" />
            </node>
            <node concept="37vLTw" id="qT" role="37wK5m">
              <ref role="3cqZAo" node="og" resolve="myConceptObjectSchemaRequiredProperty" />
            </node>
            <node concept="37vLTw" id="qU" role="37wK5m">
              <ref role="3cqZAo" node="oh" resolve="myConceptOneOfSchema" />
            </node>
            <node concept="37vLTw" id="qV" role="37wK5m">
              <ref role="3cqZAo" node="oi" resolve="myConceptOpenApi" />
            </node>
            <node concept="37vLTw" id="qW" role="37wK5m">
              <ref role="3cqZAo" node="oj" resolve="myConceptOpenApiInfo" />
            </node>
            <node concept="37vLTw" id="qX" role="37wK5m">
              <ref role="3cqZAo" node="ok" resolve="myConceptOperationObject" />
            </node>
            <node concept="37vLTw" id="qY" role="37wK5m">
              <ref role="3cqZAo" node="ol" resolve="myConceptPathObject" />
            </node>
            <node concept="37vLTw" id="qZ" role="37wK5m">
              <ref role="3cqZAo" node="om" resolve="myConceptPostOperationObject" />
            </node>
            <node concept="37vLTw" id="r0" role="37wK5m">
              <ref role="3cqZAo" node="on" resolve="myConceptPutOperationObject" />
            </node>
            <node concept="37vLTw" id="r1" role="37wK5m">
              <ref role="3cqZAo" node="oo" resolve="myConceptResponse" />
            </node>
            <node concept="37vLTw" id="r2" role="37wK5m">
              <ref role="3cqZAo" node="op" resolve="myConceptSchema" />
            </node>
            <node concept="37vLTw" id="r3" role="37wK5m">
              <ref role="3cqZAo" node="oq" resolve="myConceptSchemaRef" />
            </node>
            <node concept="37vLTw" id="r4" role="37wK5m">
              <ref role="3cqZAo" node="or" resolve="myConceptStringSchema" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="qD" role="1B3o_S" />
      <node concept="3uibUv" id="qE" role="3clF45">
        <ref role="3uigEE" to="33ny:~Collection" resolve="Collection" />
        <node concept="3uibUv" id="r5" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
        </node>
      </node>
      <node concept="2AHcQZ" id="qF" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="oE" role="jymVt" />
    <node concept="3clFb_" id="oF" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getDescriptor" />
      <property role="DiZV1" value="false" />
      <node concept="3Tm1VV" id="r6" role="1B3o_S" />
      <node concept="37vLTG" id="r7" role="3clF46">
        <property role="TrG5h" value="id" />
        <node concept="3uibUv" id="rc" role="1tU5fm">
          <ref role="3uigEE" to="e8bb:~SConceptId" resolve="SConceptId" />
        </node>
      </node>
      <node concept="3clFbS" id="r8" role="3clF47">
        <node concept="3KaCP$" id="rd" role="3cqZAp">
          <node concept="3KbdKl" id="re" role="3KbHQx">
            <node concept="3clFbS" id="rB" role="3Kbo56">
              <node concept="3cpWs6" id="rD" role="3cqZAp">
                <node concept="37vLTw" id="rE" role="3cqZAk">
                  <ref role="3cqZAo" node="o5" resolve="myConceptAllOfSchema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="rC" role="3Kbmr1">
              <ref role="3cqZAo" node="jL" resolve="AllOfSchema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="rf" role="3KbHQx">
            <node concept="3clFbS" id="rF" role="3Kbo56">
              <node concept="3cpWs6" id="rH" role="3cqZAp">
                <node concept="37vLTw" id="rI" role="3cqZAk">
                  <ref role="3cqZAo" node="o6" resolve="myConceptAnyOfSchema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="rG" role="3Kbmr1">
              <ref role="3cqZAo" node="jM" resolve="AnyOfSchema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="rg" role="3KbHQx">
            <node concept="3clFbS" id="rJ" role="3Kbo56">
              <node concept="3cpWs6" id="rL" role="3cqZAp">
                <node concept="37vLTw" id="rM" role="3cqZAk">
                  <ref role="3cqZAo" node="o7" resolve="myConceptArraySchema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="rK" role="3Kbmr1">
              <ref role="3cqZAo" node="jN" resolve="ArraySchema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="rh" role="3KbHQx">
            <node concept="3clFbS" id="rN" role="3Kbo56">
              <node concept="3cpWs6" id="rP" role="3cqZAp">
                <node concept="37vLTw" id="rQ" role="3cqZAk">
                  <ref role="3cqZAo" node="o8" resolve="myConceptBooleanSchema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="rO" role="3Kbmr1">
              <ref role="3cqZAo" node="jO" resolve="BooleanSchema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="ri" role="3KbHQx">
            <node concept="3clFbS" id="rR" role="3Kbo56">
              <node concept="3cpWs6" id="rT" role="3cqZAp">
                <node concept="37vLTw" id="rU" role="3cqZAk">
                  <ref role="3cqZAo" node="o9" resolve="myConceptContent" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="rS" role="3Kbmr1">
              <ref role="3cqZAo" node="jP" resolve="Content" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="rj" role="3KbHQx">
            <node concept="3clFbS" id="rV" role="3Kbo56">
              <node concept="3cpWs6" id="rX" role="3cqZAp">
                <node concept="37vLTw" id="rY" role="3cqZAk">
                  <ref role="3cqZAo" node="oa" resolve="myConceptDeleteOperationObject" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="rW" role="3Kbmr1">
              <ref role="3cqZAo" node="jQ" resolve="DeleteOperationObject" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="rk" role="3KbHQx">
            <node concept="3clFbS" id="rZ" role="3Kbo56">
              <node concept="3cpWs6" id="s1" role="3cqZAp">
                <node concept="37vLTw" id="s2" role="3cqZAk">
                  <ref role="3cqZAo" node="ob" resolve="myConceptGetOperationObject" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="s0" role="3Kbmr1">
              <ref role="3cqZAo" node="jR" resolve="GetOperationObject" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="rl" role="3KbHQx">
            <node concept="3clFbS" id="s3" role="3Kbo56">
              <node concept="3cpWs6" id="s5" role="3cqZAp">
                <node concept="37vLTw" id="s6" role="3cqZAk">
                  <ref role="3cqZAo" node="oc" resolve="myConceptIntegerSchema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="s4" role="3Kbmr1">
              <ref role="3cqZAo" node="jS" resolve="IntegerSchema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="rm" role="3KbHQx">
            <node concept="3clFbS" id="s7" role="3Kbo56">
              <node concept="3cpWs6" id="s9" role="3cqZAp">
                <node concept="37vLTw" id="sa" role="3cqZAk">
                  <ref role="3cqZAo" node="od" resolve="myConceptObjectSchema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="s8" role="3Kbmr1">
              <ref role="3cqZAo" node="jT" resolve="ObjectSchema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="rn" role="3KbHQx">
            <node concept="3clFbS" id="sb" role="3Kbo56">
              <node concept="3cpWs6" id="sd" role="3cqZAp">
                <node concept="37vLTw" id="se" role="3cqZAk">
                  <ref role="3cqZAo" node="oe" resolve="myConceptObjectSchemaAdditionalProperty" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="sc" role="3Kbmr1">
              <ref role="3cqZAo" node="jU" resolve="ObjectSchemaAdditionalProperty" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="ro" role="3KbHQx">
            <node concept="3clFbS" id="sf" role="3Kbo56">
              <node concept="3cpWs6" id="sh" role="3cqZAp">
                <node concept="37vLTw" id="si" role="3cqZAk">
                  <ref role="3cqZAo" node="of" resolve="myConceptObjectSchemaProperty" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="sg" role="3Kbmr1">
              <ref role="3cqZAo" node="jV" resolve="ObjectSchemaProperty" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="rp" role="3KbHQx">
            <node concept="3clFbS" id="sj" role="3Kbo56">
              <node concept="3cpWs6" id="sl" role="3cqZAp">
                <node concept="37vLTw" id="sm" role="3cqZAk">
                  <ref role="3cqZAo" node="og" resolve="myConceptObjectSchemaRequiredProperty" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="sk" role="3Kbmr1">
              <ref role="3cqZAo" node="jW" resolve="ObjectSchemaRequiredProperty" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="rq" role="3KbHQx">
            <node concept="3clFbS" id="sn" role="3Kbo56">
              <node concept="3cpWs6" id="sp" role="3cqZAp">
                <node concept="37vLTw" id="sq" role="3cqZAk">
                  <ref role="3cqZAo" node="oh" resolve="myConceptOneOfSchema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="so" role="3Kbmr1">
              <ref role="3cqZAo" node="jX" resolve="OneOfSchema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="rr" role="3KbHQx">
            <node concept="3clFbS" id="sr" role="3Kbo56">
              <node concept="3cpWs6" id="st" role="3cqZAp">
                <node concept="37vLTw" id="su" role="3cqZAk">
                  <ref role="3cqZAo" node="oi" resolve="myConceptOpenApi" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="ss" role="3Kbmr1">
              <ref role="3cqZAo" node="jY" resolve="OpenApi" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="rs" role="3KbHQx">
            <node concept="3clFbS" id="sv" role="3Kbo56">
              <node concept="3cpWs6" id="sx" role="3cqZAp">
                <node concept="37vLTw" id="sy" role="3cqZAk">
                  <ref role="3cqZAo" node="oj" resolve="myConceptOpenApiInfo" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="sw" role="3Kbmr1">
              <ref role="3cqZAo" node="jZ" resolve="OpenApiInfo" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="rt" role="3KbHQx">
            <node concept="3clFbS" id="sz" role="3Kbo56">
              <node concept="3cpWs6" id="s_" role="3cqZAp">
                <node concept="37vLTw" id="sA" role="3cqZAk">
                  <ref role="3cqZAo" node="ok" resolve="myConceptOperationObject" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="s$" role="3Kbmr1">
              <ref role="3cqZAo" node="k0" resolve="OperationObject" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="ru" role="3KbHQx">
            <node concept="3clFbS" id="sB" role="3Kbo56">
              <node concept="3cpWs6" id="sD" role="3cqZAp">
                <node concept="37vLTw" id="sE" role="3cqZAk">
                  <ref role="3cqZAo" node="ol" resolve="myConceptPathObject" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="sC" role="3Kbmr1">
              <ref role="3cqZAo" node="k1" resolve="PathObject" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="rv" role="3KbHQx">
            <node concept="3clFbS" id="sF" role="3Kbo56">
              <node concept="3cpWs6" id="sH" role="3cqZAp">
                <node concept="37vLTw" id="sI" role="3cqZAk">
                  <ref role="3cqZAo" node="om" resolve="myConceptPostOperationObject" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="sG" role="3Kbmr1">
              <ref role="3cqZAo" node="k2" resolve="PostOperationObject" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="rw" role="3KbHQx">
            <node concept="3clFbS" id="sJ" role="3Kbo56">
              <node concept="3cpWs6" id="sL" role="3cqZAp">
                <node concept="37vLTw" id="sM" role="3cqZAk">
                  <ref role="3cqZAo" node="on" resolve="myConceptPutOperationObject" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="sK" role="3Kbmr1">
              <ref role="3cqZAo" node="k3" resolve="PutOperationObject" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="rx" role="3KbHQx">
            <node concept="3clFbS" id="sN" role="3Kbo56">
              <node concept="3cpWs6" id="sP" role="3cqZAp">
                <node concept="37vLTw" id="sQ" role="3cqZAk">
                  <ref role="3cqZAo" node="oo" resolve="myConceptResponse" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="sO" role="3Kbmr1">
              <ref role="3cqZAo" node="k4" resolve="Response" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="ry" role="3KbHQx">
            <node concept="3clFbS" id="sR" role="3Kbo56">
              <node concept="3cpWs6" id="sT" role="3cqZAp">
                <node concept="37vLTw" id="sU" role="3cqZAk">
                  <ref role="3cqZAo" node="op" resolve="myConceptSchema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="sS" role="3Kbmr1">
              <ref role="3cqZAo" node="k5" resolve="Schema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="rz" role="3KbHQx">
            <node concept="3clFbS" id="sV" role="3Kbo56">
              <node concept="3cpWs6" id="sX" role="3cqZAp">
                <node concept="37vLTw" id="sY" role="3cqZAk">
                  <ref role="3cqZAo" node="oq" resolve="myConceptSchemaRef" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="sW" role="3Kbmr1">
              <ref role="3cqZAo" node="k6" resolve="SchemaRef" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="3KbdKl" id="r$" role="3KbHQx">
            <node concept="3clFbS" id="sZ" role="3Kbo56">
              <node concept="3cpWs6" id="t1" role="3cqZAp">
                <node concept="37vLTw" id="t2" role="3cqZAk">
                  <ref role="3cqZAo" node="or" resolve="myConceptStringSchema" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="t0" role="3Kbmr1">
              <ref role="3cqZAo" node="k7" resolve="StringSchema" />
              <ref role="1PxDUh" node="jJ" resolve="LanguageConceptSwitch" />
            </node>
          </node>
          <node concept="2OqwBi" id="r_" role="3KbGdf">
            <node concept="37vLTw" id="t3" role="2Oq$k0">
              <ref role="3cqZAo" node="ox" resolve="myIndexSwitch" />
            </node>
            <node concept="liA8E" id="t4" role="2OqNvi">
              <ref role="37wK5l" node="kb" resolve="index" />
              <node concept="37vLTw" id="t5" role="37wK5m">
                <ref role="3cqZAo" node="r7" resolve="id" />
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="rA" role="3Kb1Dw">
            <node concept="3cpWs6" id="t6" role="3cqZAp">
              <node concept="10Nm6u" id="t7" role="3cqZAk" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="r9" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="2AHcQZ" id="ra" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="2AHcQZ" id="rb" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
      </node>
    </node>
    <node concept="2tJIrI" id="oG" role="jymVt" />
    <node concept="3clFb_" id="oH" role="jymVt">
      <property role="TrG5h" value="getDataTypeDescriptors" />
      <node concept="3Tm1VV" id="t8" role="1B3o_S" />
      <node concept="3uibUv" id="t9" role="3clF45">
        <ref role="3uigEE" to="33ny:~Collection" resolve="Collection" />
        <node concept="3uibUv" id="tc" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~DataTypeDescriptor" resolve="DataTypeDescriptor" />
        </node>
      </node>
      <node concept="3clFbS" id="ta" role="3clF47">
        <node concept="3cpWs6" id="td" role="3cqZAp">
          <node concept="2YIFZM" id="te" role="3cqZAk">
            <ref role="37wK5l" to="33ny:~Arrays.asList(java.lang.Object...)" resolve="asList" />
            <ref role="1Pybhc" to="33ny:~Arrays" resolve="Arrays" />
            <node concept="37vLTw" id="tf" role="37wK5m">
              <ref role="3cqZAo" node="os" resolve="myEnumerationDataType" />
            </node>
            <node concept="37vLTw" id="tg" role="37wK5m">
              <ref role="3cqZAo" node="ot" resolve="myEnumerationMediaType" />
            </node>
            <node concept="37vLTw" id="th" role="37wK5m">
              <ref role="3cqZAo" node="ou" resolve="myEnumerationOperation" />
            </node>
            <node concept="37vLTw" id="ti" role="37wK5m">
              <ref role="3cqZAo" node="ov" resolve="myCSDatatypePath" />
            </node>
            <node concept="37vLTw" id="tj" role="37wK5m">
              <ref role="3cqZAo" node="ow" resolve="myCSDatatypeVersion" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="tb" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="oI" role="jymVt" />
    <node concept="3clFb_" id="oJ" role="jymVt">
      <property role="TrG5h" value="internalIndex" />
      <node concept="10Oyi0" id="tk" role="3clF45" />
      <node concept="3clFbS" id="tl" role="3clF47">
        <node concept="3cpWs6" id="tn" role="3cqZAp">
          <node concept="2OqwBi" id="to" role="3cqZAk">
            <node concept="37vLTw" id="tp" role="2Oq$k0">
              <ref role="3cqZAo" node="ox" resolve="myIndexSwitch" />
            </node>
            <node concept="liA8E" id="tq" role="2OqNvi">
              <ref role="37wK5l" node="kd" resolve="index" />
              <node concept="37vLTw" id="tr" role="37wK5m">
                <ref role="3cqZAo" node="tm" resolve="c" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="tm" role="3clF46">
        <property role="TrG5h" value="c" />
        <node concept="3uibUv" id="ts" role="1tU5fm">
          <ref role="3uigEE" to="c17a:~SAbstractConcept" resolve="SAbstractConcept" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="oK" role="jymVt" />
    <node concept="2YIFZL" id="oL" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForAllOfSchema" />
      <node concept="3clFbS" id="tt" role="3clF47">
        <node concept="3cpWs8" id="tw" role="3cqZAp">
          <node concept="3cpWsn" id="tC" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="tD" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="tE" role="33vP2m">
              <node concept="1pGfFk" id="tF" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="tG" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="tH" role="37wK5m">
                  <property role="Xl_RC" value="AllOfSchema" />
                </node>
                <node concept="1adDum" id="tI" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="tJ" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="tK" role="37wK5m">
                  <property role="1adDun" value="0x3bdca882929c266cL" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="tx" role="3cqZAp">
          <node concept="2OqwBi" id="tL" role="3clFbG">
            <node concept="37vLTw" id="tM" role="2Oq$k0">
              <ref role="3cqZAo" node="tC" resolve="b" />
            </node>
            <node concept="liA8E" id="tN" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="tO" role="37wK5m" />
              <node concept="3clFbT" id="tP" role="37wK5m" />
              <node concept="3clFbT" id="tQ" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ty" role="3cqZAp">
          <node concept="2OqwBi" id="tR" role="3clFbG">
            <node concept="37vLTw" id="tS" role="2Oq$k0">
              <ref role="3cqZAo" node="tC" resolve="b" />
            </node>
            <node concept="liA8E" id="tT" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.super_(java.lang.String,long,long,long)" resolve="super_" />
              <node concept="Xl_RD" id="tU" role="37wK5m">
                <property role="Xl_RC" value="de.thb.openapidsl.copy.structure.Schema" />
              </node>
              <node concept="1adDum" id="tV" role="37wK5m">
                <property role="1adDun" value="0x29d429a205314919L" />
              </node>
              <node concept="1adDum" id="tW" role="37wK5m">
                <property role="1adDun" value="0x8e806706b89fb3b7L" />
              </node>
              <node concept="1adDum" id="tX" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529f3c94L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="tz" role="3cqZAp">
          <node concept="2OqwBi" id="tY" role="3clFbG">
            <node concept="37vLTw" id="tZ" role="2Oq$k0">
              <ref role="3cqZAo" node="tC" resolve="b" />
            </node>
            <node concept="liA8E" id="u0" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="u1" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4313507821873014380" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="t$" role="3cqZAp">
          <node concept="2OqwBi" id="u2" role="3clFbG">
            <node concept="37vLTw" id="u3" role="2Oq$k0">
              <ref role="3cqZAo" node="tC" resolve="b" />
            </node>
            <node concept="liA8E" id="u4" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="u5" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="t_" role="3cqZAp">
          <node concept="2OqwBi" id="u6" role="3clFbG">
            <node concept="2OqwBi" id="u7" role="2Oq$k0">
              <node concept="2OqwBi" id="u9" role="2Oq$k0">
                <node concept="2OqwBi" id="ub" role="2Oq$k0">
                  <node concept="2OqwBi" id="ud" role="2Oq$k0">
                    <node concept="2OqwBi" id="uf" role="2Oq$k0">
                      <node concept="2OqwBi" id="uh" role="2Oq$k0">
                        <node concept="37vLTw" id="uj" role="2Oq$k0">
                          <ref role="3cqZAo" node="tC" resolve="b" />
                        </node>
                        <node concept="liA8E" id="uk" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long)" resolve="aggregate" />
                          <node concept="Xl_RD" id="ul" role="37wK5m">
                            <property role="Xl_RC" value="allOf" />
                          </node>
                          <node concept="1adDum" id="um" role="37wK5m">
                            <property role="1adDun" value="0x3bdca882929c266dL" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="ui" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long)" resolve="target" />
                        <node concept="1adDum" id="un" role="37wK5m">
                          <property role="1adDun" value="0x29d429a205314919L" />
                        </node>
                        <node concept="1adDum" id="uo" role="37wK5m">
                          <property role="1adDun" value="0x8e806706b89fb3b7L" />
                        </node>
                        <node concept="1adDum" id="up" role="37wK5m">
                          <property role="1adDun" value="0x3bdca882929f537fL" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="ug" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean)" resolve="optional" />
                      <node concept="3clFbT" id="uq" role="37wK5m" />
                    </node>
                  </node>
                  <node concept="liA8E" id="ue" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean)" resolve="ordered" />
                    <node concept="3clFbT" id="ur" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="uc" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean)" resolve="multiple" />
                  <node concept="3clFbT" id="us" role="37wK5m">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="ua" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="ut" role="37wK5m">
                  <property role="Xl_RC" value="4313507821873014381" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="u8" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="tA" role="3cqZAp">
          <node concept="2OqwBi" id="uu" role="3clFbG">
            <node concept="37vLTw" id="uv" role="2Oq$k0">
              <ref role="3cqZAo" node="tC" resolve="b" />
            </node>
            <node concept="liA8E" id="uw" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.alias(java.lang.String)" resolve="alias" />
              <node concept="Xl_RD" id="ux" role="37wK5m">
                <property role="Xl_RC" value="allOf" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="tB" role="3cqZAp">
          <node concept="2OqwBi" id="uy" role="3cqZAk">
            <node concept="37vLTw" id="uz" role="2Oq$k0">
              <ref role="3cqZAo" node="tC" resolve="b" />
            </node>
            <node concept="liA8E" id="u$" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="tu" role="1B3o_S" />
      <node concept="3uibUv" id="tv" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="oM" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForAnyOfSchema" />
      <node concept="3clFbS" id="u_" role="3clF47">
        <node concept="3cpWs8" id="uC" role="3cqZAp">
          <node concept="3cpWsn" id="uK" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="uL" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="uM" role="33vP2m">
              <node concept="1pGfFk" id="uN" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="uO" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="uP" role="37wK5m">
                  <property role="Xl_RC" value="AnyOfSchema" />
                </node>
                <node concept="1adDum" id="uQ" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="uR" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="uS" role="37wK5m">
                  <property role="1adDun" value="0x3bdca882929c2684L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="uD" role="3cqZAp">
          <node concept="2OqwBi" id="uT" role="3clFbG">
            <node concept="37vLTw" id="uU" role="2Oq$k0">
              <ref role="3cqZAo" node="uK" resolve="b" />
            </node>
            <node concept="liA8E" id="uV" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="uW" role="37wK5m" />
              <node concept="3clFbT" id="uX" role="37wK5m" />
              <node concept="3clFbT" id="uY" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="uE" role="3cqZAp">
          <node concept="2OqwBi" id="uZ" role="3clFbG">
            <node concept="37vLTw" id="v0" role="2Oq$k0">
              <ref role="3cqZAo" node="uK" resolve="b" />
            </node>
            <node concept="liA8E" id="v1" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.super_(java.lang.String,long,long,long)" resolve="super_" />
              <node concept="Xl_RD" id="v2" role="37wK5m">
                <property role="Xl_RC" value="de.thb.openapidsl.copy.structure.Schema" />
              </node>
              <node concept="1adDum" id="v3" role="37wK5m">
                <property role="1adDun" value="0x29d429a205314919L" />
              </node>
              <node concept="1adDum" id="v4" role="37wK5m">
                <property role="1adDun" value="0x8e806706b89fb3b7L" />
              </node>
              <node concept="1adDum" id="v5" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529f3c94L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="uF" role="3cqZAp">
          <node concept="2OqwBi" id="v6" role="3clFbG">
            <node concept="37vLTw" id="v7" role="2Oq$k0">
              <ref role="3cqZAo" node="uK" resolve="b" />
            </node>
            <node concept="liA8E" id="v8" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="v9" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4313507821873014404" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="uG" role="3cqZAp">
          <node concept="2OqwBi" id="va" role="3clFbG">
            <node concept="37vLTw" id="vb" role="2Oq$k0">
              <ref role="3cqZAo" node="uK" resolve="b" />
            </node>
            <node concept="liA8E" id="vc" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="vd" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="uH" role="3cqZAp">
          <node concept="2OqwBi" id="ve" role="3clFbG">
            <node concept="2OqwBi" id="vf" role="2Oq$k0">
              <node concept="2OqwBi" id="vh" role="2Oq$k0">
                <node concept="2OqwBi" id="vj" role="2Oq$k0">
                  <node concept="2OqwBi" id="vl" role="2Oq$k0">
                    <node concept="2OqwBi" id="vn" role="2Oq$k0">
                      <node concept="2OqwBi" id="vp" role="2Oq$k0">
                        <node concept="37vLTw" id="vr" role="2Oq$k0">
                          <ref role="3cqZAo" node="uK" resolve="b" />
                        </node>
                        <node concept="liA8E" id="vs" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long)" resolve="aggregate" />
                          <node concept="Xl_RD" id="vt" role="37wK5m">
                            <property role="Xl_RC" value="anyOf" />
                          </node>
                          <node concept="1adDum" id="vu" role="37wK5m">
                            <property role="1adDun" value="0x3bdca882929c2685L" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="vq" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long)" resolve="target" />
                        <node concept="1adDum" id="vv" role="37wK5m">
                          <property role="1adDun" value="0x29d429a205314919L" />
                        </node>
                        <node concept="1adDum" id="vw" role="37wK5m">
                          <property role="1adDun" value="0x8e806706b89fb3b7L" />
                        </node>
                        <node concept="1adDum" id="vx" role="37wK5m">
                          <property role="1adDun" value="0x3bdca882929f537fL" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="vo" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean)" resolve="optional" />
                      <node concept="3clFbT" id="vy" role="37wK5m" />
                    </node>
                  </node>
                  <node concept="liA8E" id="vm" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean)" resolve="ordered" />
                    <node concept="3clFbT" id="vz" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="vk" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean)" resolve="multiple" />
                  <node concept="3clFbT" id="v$" role="37wK5m">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="vi" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="v_" role="37wK5m">
                  <property role="Xl_RC" value="4313507821873014405" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="vg" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="uI" role="3cqZAp">
          <node concept="2OqwBi" id="vA" role="3clFbG">
            <node concept="37vLTw" id="vB" role="2Oq$k0">
              <ref role="3cqZAo" node="uK" resolve="b" />
            </node>
            <node concept="liA8E" id="vC" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.alias(java.lang.String)" resolve="alias" />
              <node concept="Xl_RD" id="vD" role="37wK5m">
                <property role="Xl_RC" value="anyOf" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="uJ" role="3cqZAp">
          <node concept="2OqwBi" id="vE" role="3cqZAk">
            <node concept="37vLTw" id="vF" role="2Oq$k0">
              <ref role="3cqZAo" node="uK" resolve="b" />
            </node>
            <node concept="liA8E" id="vG" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="uA" role="1B3o_S" />
      <node concept="3uibUv" id="uB" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="oN" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForArraySchema" />
      <node concept="3clFbS" id="vH" role="3clF47">
        <node concept="3cpWs8" id="vK" role="3cqZAp">
          <node concept="3cpWsn" id="vU" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="vV" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="vW" role="33vP2m">
              <node concept="1pGfFk" id="vX" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="vY" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="vZ" role="37wK5m">
                  <property role="Xl_RC" value="ArraySchema" />
                </node>
                <node concept="1adDum" id="w0" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="w1" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="w2" role="37wK5m">
                  <property role="1adDun" value="0x43ca7e69529f3e36L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="vL" role="3cqZAp">
          <node concept="2OqwBi" id="w3" role="3clFbG">
            <node concept="37vLTw" id="w4" role="2Oq$k0">
              <ref role="3cqZAo" node="vU" resolve="b" />
            </node>
            <node concept="liA8E" id="w5" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="w6" role="37wK5m" />
              <node concept="3clFbT" id="w7" role="37wK5m" />
              <node concept="3clFbT" id="w8" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="vM" role="3cqZAp">
          <node concept="2OqwBi" id="w9" role="3clFbG">
            <node concept="37vLTw" id="wa" role="2Oq$k0">
              <ref role="3cqZAo" node="vU" resolve="b" />
            </node>
            <node concept="liA8E" id="wb" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.super_(java.lang.String,long,long,long)" resolve="super_" />
              <node concept="Xl_RD" id="wc" role="37wK5m">
                <property role="Xl_RC" value="de.thb.openapidsl.copy.structure.Schema" />
              </node>
              <node concept="1adDum" id="wd" role="37wK5m">
                <property role="1adDun" value="0x29d429a205314919L" />
              </node>
              <node concept="1adDum" id="we" role="37wK5m">
                <property role="1adDun" value="0x8e806706b89fb3b7L" />
              </node>
              <node concept="1adDum" id="wf" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529f3c94L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="vN" role="3cqZAp">
          <node concept="2OqwBi" id="wg" role="3clFbG">
            <node concept="37vLTw" id="wh" role="2Oq$k0">
              <ref role="3cqZAo" node="vU" resolve="b" />
            </node>
            <node concept="liA8E" id="wi" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="wj" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4884855736659557942" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="vO" role="3cqZAp">
          <node concept="2OqwBi" id="wk" role="3clFbG">
            <node concept="37vLTw" id="wl" role="2Oq$k0">
              <ref role="3cqZAo" node="vU" resolve="b" />
            </node>
            <node concept="liA8E" id="wm" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="wn" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="vP" role="3cqZAp">
          <node concept="2OqwBi" id="wo" role="3clFbG">
            <node concept="2OqwBi" id="wp" role="2Oq$k0">
              <node concept="2OqwBi" id="wr" role="2Oq$k0">
                <node concept="2OqwBi" id="wt" role="2Oq$k0">
                  <node concept="37vLTw" id="wv" role="2Oq$k0">
                    <ref role="3cqZAo" node="vU" resolve="b" />
                  </node>
                  <node concept="liA8E" id="ww" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="wx" role="37wK5m">
                      <property role="Xl_RC" value="minItems" />
                    </node>
                    <node concept="1adDum" id="wy" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529f3e38L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="wu" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="wz" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.INTEGER" resolve="INTEGER" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="ws" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="w$" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659557944" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="wq" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="vQ" role="3cqZAp">
          <node concept="2OqwBi" id="w_" role="3clFbG">
            <node concept="2OqwBi" id="wA" role="2Oq$k0">
              <node concept="2OqwBi" id="wC" role="2Oq$k0">
                <node concept="2OqwBi" id="wE" role="2Oq$k0">
                  <node concept="37vLTw" id="wG" role="2Oq$k0">
                    <ref role="3cqZAo" node="vU" resolve="b" />
                  </node>
                  <node concept="liA8E" id="wH" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="wI" role="37wK5m">
                      <property role="Xl_RC" value="maxItems" />
                    </node>
                    <node concept="1adDum" id="wJ" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529f3e3aL" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="wF" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="wK" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.INTEGER" resolve="INTEGER" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="wD" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="wL" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659557946" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="wB" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="vR" role="3cqZAp">
          <node concept="2OqwBi" id="wM" role="3clFbG">
            <node concept="2OqwBi" id="wN" role="2Oq$k0">
              <node concept="2OqwBi" id="wP" role="2Oq$k0">
                <node concept="2OqwBi" id="wR" role="2Oq$k0">
                  <node concept="2OqwBi" id="wT" role="2Oq$k0">
                    <node concept="37vLTw" id="wV" role="2Oq$k0">
                      <ref role="3cqZAo" node="vU" resolve="b" />
                    </node>
                    <node concept="liA8E" id="wW" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.associate(java.lang.String,long)" resolve="associate" />
                      <node concept="Xl_RD" id="wX" role="37wK5m">
                        <property role="Xl_RC" value="items" />
                      </node>
                      <node concept="1adDum" id="wY" role="37wK5m">
                        <property role="1adDun" value="0x43ca7e69529f3e43L" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="wU" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.target(long,long,long)" resolve="target" />
                    <node concept="1adDum" id="wZ" role="37wK5m">
                      <property role="1adDun" value="0x29d429a205314919L" />
                    </node>
                    <node concept="1adDum" id="x0" role="37wK5m">
                      <property role="1adDun" value="0x8e806706b89fb3b7L" />
                    </node>
                    <node concept="1adDum" id="x1" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529f3c94L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="wS" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.optional(boolean)" resolve="optional" />
                  <node concept="3clFbT" id="x2" role="37wK5m" />
                </node>
              </node>
              <node concept="liA8E" id="wQ" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="x3" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659557955" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="wO" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="vS" role="3cqZAp">
          <node concept="2OqwBi" id="x4" role="3clFbG">
            <node concept="37vLTw" id="x5" role="2Oq$k0">
              <ref role="3cqZAo" node="vU" resolve="b" />
            </node>
            <node concept="liA8E" id="x6" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.alias(java.lang.String)" resolve="alias" />
              <node concept="Xl_RD" id="x7" role="37wK5m">
                <property role="Xl_RC" value="array" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="vT" role="3cqZAp">
          <node concept="2OqwBi" id="x8" role="3cqZAk">
            <node concept="37vLTw" id="x9" role="2Oq$k0">
              <ref role="3cqZAo" node="vU" resolve="b" />
            </node>
            <node concept="liA8E" id="xa" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="vI" role="1B3o_S" />
      <node concept="3uibUv" id="vJ" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="oO" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForBooleanSchema" />
      <node concept="3clFbS" id="xb" role="3clF47">
        <node concept="3cpWs8" id="xe" role="3cqZAp">
          <node concept="3cpWsn" id="xl" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="xm" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="xn" role="33vP2m">
              <node concept="1pGfFk" id="xo" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="xp" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="xq" role="37wK5m">
                  <property role="Xl_RC" value="BooleanSchema" />
                </node>
                <node concept="1adDum" id="xr" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="xs" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="xt" role="37wK5m">
                  <property role="1adDun" value="0x3bdca882929aa6b5L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="xf" role="3cqZAp">
          <node concept="2OqwBi" id="xu" role="3clFbG">
            <node concept="37vLTw" id="xv" role="2Oq$k0">
              <ref role="3cqZAo" node="xl" resolve="b" />
            </node>
            <node concept="liA8E" id="xw" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="xx" role="37wK5m" />
              <node concept="3clFbT" id="xy" role="37wK5m" />
              <node concept="3clFbT" id="xz" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="xg" role="3cqZAp">
          <node concept="2OqwBi" id="x$" role="3clFbG">
            <node concept="37vLTw" id="x_" role="2Oq$k0">
              <ref role="3cqZAo" node="xl" resolve="b" />
            </node>
            <node concept="liA8E" id="xA" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.super_(java.lang.String,long,long,long)" resolve="super_" />
              <node concept="Xl_RD" id="xB" role="37wK5m">
                <property role="Xl_RC" value="de.thb.openapidsl.copy.structure.Schema" />
              </node>
              <node concept="1adDum" id="xC" role="37wK5m">
                <property role="1adDun" value="0x29d429a205314919L" />
              </node>
              <node concept="1adDum" id="xD" role="37wK5m">
                <property role="1adDun" value="0x8e806706b89fb3b7L" />
              </node>
              <node concept="1adDum" id="xE" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529f3c94L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="xh" role="3cqZAp">
          <node concept="2OqwBi" id="xF" role="3clFbG">
            <node concept="37vLTw" id="xG" role="2Oq$k0">
              <ref role="3cqZAo" node="xl" resolve="b" />
            </node>
            <node concept="liA8E" id="xH" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="xI" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4313507821872916149" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="xi" role="3cqZAp">
          <node concept="2OqwBi" id="xJ" role="3clFbG">
            <node concept="37vLTw" id="xK" role="2Oq$k0">
              <ref role="3cqZAo" node="xl" resolve="b" />
            </node>
            <node concept="liA8E" id="xL" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="xM" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="xj" role="3cqZAp">
          <node concept="2OqwBi" id="xN" role="3clFbG">
            <node concept="37vLTw" id="xO" role="2Oq$k0">
              <ref role="3cqZAo" node="xl" resolve="b" />
            </node>
            <node concept="liA8E" id="xP" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.alias(java.lang.String)" resolve="alias" />
              <node concept="Xl_RD" id="xQ" role="37wK5m">
                <property role="Xl_RC" value="boolean" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="xk" role="3cqZAp">
          <node concept="2OqwBi" id="xR" role="3cqZAk">
            <node concept="37vLTw" id="xS" role="2Oq$k0">
              <ref role="3cqZAo" node="xl" resolve="b" />
            </node>
            <node concept="liA8E" id="xT" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="xc" role="1B3o_S" />
      <node concept="3uibUv" id="xd" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="oP" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForContent" />
      <node concept="3clFbS" id="xU" role="3clF47">
        <node concept="3cpWs8" id="xX" role="3cqZAp">
          <node concept="3cpWsn" id="y5" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="y6" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="y7" role="33vP2m">
              <node concept="1pGfFk" id="y8" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="y9" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="ya" role="37wK5m">
                  <property role="Xl_RC" value="Content" />
                </node>
                <node concept="1adDum" id="yb" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="yc" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="yd" role="37wK5m">
                  <property role="1adDun" value="0x43ca7e69529e41f0L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="xY" role="3cqZAp">
          <node concept="2OqwBi" id="ye" role="3clFbG">
            <node concept="37vLTw" id="yf" role="2Oq$k0">
              <ref role="3cqZAo" node="y5" resolve="b" />
            </node>
            <node concept="liA8E" id="yg" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="yh" role="37wK5m" />
              <node concept="3clFbT" id="yi" role="37wK5m" />
              <node concept="3clFbT" id="yj" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="xZ" role="3cqZAp">
          <node concept="2OqwBi" id="yk" role="3clFbG">
            <node concept="37vLTw" id="yl" role="2Oq$k0">
              <ref role="3cqZAo" node="y5" resolve="b" />
            </node>
            <node concept="liA8E" id="ym" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="yn" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4884855736659493360" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="y0" role="3cqZAp">
          <node concept="2OqwBi" id="yo" role="3clFbG">
            <node concept="37vLTw" id="yp" role="2Oq$k0">
              <ref role="3cqZAo" node="y5" resolve="b" />
            </node>
            <node concept="liA8E" id="yq" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="yr" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="y1" role="3cqZAp">
          <node concept="2OqwBi" id="ys" role="3clFbG">
            <node concept="2OqwBi" id="yt" role="2Oq$k0">
              <node concept="2OqwBi" id="yv" role="2Oq$k0">
                <node concept="2OqwBi" id="yx" role="2Oq$k0">
                  <node concept="37vLTw" id="yz" role="2Oq$k0">
                    <ref role="3cqZAo" node="y5" resolve="b" />
                  </node>
                  <node concept="liA8E" id="y$" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="y_" role="37wK5m">
                      <property role="Xl_RC" value="mediaType" />
                    </node>
                    <node concept="1adDum" id="yA" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529e41f1L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="yy" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="2YIFZM" id="yB" role="37wK5m">
                    <ref role="37wK5l" to="e8bb:~MetaIdFactory.dataTypeId(long,long,long)" resolve="dataTypeId" />
                    <ref role="1Pybhc" to="e8bb:~MetaIdFactory" resolve="MetaIdFactory" />
                    <uo k="s:originTrace" v="n:4884855736659493363" />
                    <node concept="1adDum" id="yC" role="37wK5m">
                      <property role="1adDun" value="0x29d429a205314919L" />
                      <uo k="s:originTrace" v="n:4884855736659493363" />
                    </node>
                    <node concept="1adDum" id="yD" role="37wK5m">
                      <property role="1adDun" value="0x8e806706b89fb3b7L" />
                      <uo k="s:originTrace" v="n:4884855736659493363" />
                    </node>
                    <node concept="1adDum" id="yE" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529e41f3L" />
                      <uo k="s:originTrace" v="n:4884855736659493363" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="yw" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="yF" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659493361" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="yu" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="y2" role="3cqZAp">
          <node concept="2OqwBi" id="yG" role="3clFbG">
            <node concept="2OqwBi" id="yH" role="2Oq$k0">
              <node concept="2OqwBi" id="yJ" role="2Oq$k0">
                <node concept="2OqwBi" id="yL" role="2Oq$k0">
                  <node concept="2OqwBi" id="yN" role="2Oq$k0">
                    <node concept="37vLTw" id="yP" role="2Oq$k0">
                      <ref role="3cqZAo" node="y5" resolve="b" />
                    </node>
                    <node concept="liA8E" id="yQ" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.associate(java.lang.String,long)" resolve="associate" />
                      <node concept="Xl_RD" id="yR" role="37wK5m">
                        <property role="Xl_RC" value="schema" />
                      </node>
                      <node concept="1adDum" id="yS" role="37wK5m">
                        <property role="1adDun" value="0x43ca7e69529f3e26L" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="yO" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.target(long,long,long)" resolve="target" />
                    <node concept="1adDum" id="yT" role="37wK5m">
                      <property role="1adDun" value="0x29d429a205314919L" />
                    </node>
                    <node concept="1adDum" id="yU" role="37wK5m">
                      <property role="1adDun" value="0x8e806706b89fb3b7L" />
                    </node>
                    <node concept="1adDum" id="yV" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529f3c94L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="yM" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.optional(boolean)" resolve="optional" />
                  <node concept="3clFbT" id="yW" role="37wK5m" />
                </node>
              </node>
              <node concept="liA8E" id="yK" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="yX" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659557926" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="yI" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="y3" role="3cqZAp">
          <node concept="2OqwBi" id="yY" role="3clFbG">
            <node concept="37vLTw" id="yZ" role="2Oq$k0">
              <ref role="3cqZAo" node="y5" resolve="b" />
            </node>
            <node concept="liA8E" id="z0" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.alias(java.lang.String)" resolve="alias" />
              <node concept="Xl_RD" id="z1" role="37wK5m">
                <property role="Xl_RC" value="content" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="y4" role="3cqZAp">
          <node concept="2OqwBi" id="z2" role="3cqZAk">
            <node concept="37vLTw" id="z3" role="2Oq$k0">
              <ref role="3cqZAo" node="y5" resolve="b" />
            </node>
            <node concept="liA8E" id="z4" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="xV" role="1B3o_S" />
      <node concept="3uibUv" id="xW" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="oQ" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForDeleteOperationObject" />
      <node concept="3clFbS" id="z5" role="3clF47">
        <node concept="3cpWs8" id="z8" role="3cqZAp">
          <node concept="3cpWsn" id="zf" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="zg" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="zh" role="33vP2m">
              <node concept="1pGfFk" id="zi" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="zj" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="zk" role="37wK5m">
                  <property role="Xl_RC" value="DeleteOperationObject" />
                </node>
                <node concept="1adDum" id="zl" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="zm" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="zn" role="37wK5m">
                  <property role="1adDun" value="0x43ca7e69529d5050L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="z9" role="3cqZAp">
          <node concept="2OqwBi" id="zo" role="3clFbG">
            <node concept="37vLTw" id="zp" role="2Oq$k0">
              <ref role="3cqZAo" node="zf" resolve="b" />
            </node>
            <node concept="liA8E" id="zq" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="zr" role="37wK5m" />
              <node concept="3clFbT" id="zs" role="37wK5m" />
              <node concept="3clFbT" id="zt" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="za" role="3cqZAp">
          <node concept="2OqwBi" id="zu" role="3clFbG">
            <node concept="37vLTw" id="zv" role="2Oq$k0">
              <ref role="3cqZAo" node="zf" resolve="b" />
            </node>
            <node concept="liA8E" id="zw" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.super_(java.lang.String,long,long,long)" resolve="super_" />
              <node concept="Xl_RD" id="zx" role="37wK5m">
                <property role="Xl_RC" value="de.thb.openapidsl.copy.structure.OperationObject" />
              </node>
              <node concept="1adDum" id="zy" role="37wK5m">
                <property role="1adDun" value="0x29d429a205314919L" />
              </node>
              <node concept="1adDum" id="zz" role="37wK5m">
                <property role="1adDun" value="0x8e806706b89fb3b7L" />
              </node>
              <node concept="1adDum" id="z$" role="37wK5m">
                <property role="1adDun" value="0x3bfc2252a1cc33d0L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="zb" role="3cqZAp">
          <node concept="2OqwBi" id="z_" role="3clFbG">
            <node concept="37vLTw" id="zA" role="2Oq$k0">
              <ref role="3cqZAo" node="zf" resolve="b" />
            </node>
            <node concept="liA8E" id="zB" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="zC" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4884855736659431504" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="zc" role="3cqZAp">
          <node concept="2OqwBi" id="zD" role="3clFbG">
            <node concept="37vLTw" id="zE" role="2Oq$k0">
              <ref role="3cqZAo" node="zf" resolve="b" />
            </node>
            <node concept="liA8E" id="zF" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="zG" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="zd" role="3cqZAp">
          <node concept="2OqwBi" id="zH" role="3clFbG">
            <node concept="37vLTw" id="zI" role="2Oq$k0">
              <ref role="3cqZAo" node="zf" resolve="b" />
            </node>
            <node concept="liA8E" id="zJ" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.alias(java.lang.String)" resolve="alias" />
              <node concept="Xl_RD" id="zK" role="37wK5m">
                <property role="Xl_RC" value="delete" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="ze" role="3cqZAp">
          <node concept="2OqwBi" id="zL" role="3cqZAk">
            <node concept="37vLTw" id="zM" role="2Oq$k0">
              <ref role="3cqZAo" node="zf" resolve="b" />
            </node>
            <node concept="liA8E" id="zN" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="z6" role="1B3o_S" />
      <node concept="3uibUv" id="z7" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="oR" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForGetOperationObject" />
      <node concept="3clFbS" id="zO" role="3clF47">
        <node concept="3cpWs8" id="zR" role="3cqZAp">
          <node concept="3cpWsn" id="zY" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="zZ" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="$0" role="33vP2m">
              <node concept="1pGfFk" id="$1" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="$2" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="$3" role="37wK5m">
                  <property role="Xl_RC" value="GetOperationObject" />
                </node>
                <node concept="1adDum" id="$4" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="$5" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="$6" role="37wK5m">
                  <property role="1adDun" value="0x3bfc2252a1cc3d60L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="zS" role="3cqZAp">
          <node concept="2OqwBi" id="$7" role="3clFbG">
            <node concept="37vLTw" id="$8" role="2Oq$k0">
              <ref role="3cqZAo" node="zY" resolve="b" />
            </node>
            <node concept="liA8E" id="$9" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="$a" role="37wK5m" />
              <node concept="3clFbT" id="$b" role="37wK5m" />
              <node concept="3clFbT" id="$c" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="zT" role="3cqZAp">
          <node concept="2OqwBi" id="$d" role="3clFbG">
            <node concept="37vLTw" id="$e" role="2Oq$k0">
              <ref role="3cqZAo" node="zY" resolve="b" />
            </node>
            <node concept="liA8E" id="$f" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.super_(java.lang.String,long,long,long)" resolve="super_" />
              <node concept="Xl_RD" id="$g" role="37wK5m">
                <property role="Xl_RC" value="de.thb.openapidsl.copy.structure.OperationObject" />
              </node>
              <node concept="1adDum" id="$h" role="37wK5m">
                <property role="1adDun" value="0x29d429a205314919L" />
              </node>
              <node concept="1adDum" id="$i" role="37wK5m">
                <property role="1adDun" value="0x8e806706b89fb3b7L" />
              </node>
              <node concept="1adDum" id="$j" role="37wK5m">
                <property role="1adDun" value="0x3bfc2252a1cc33d0L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="zU" role="3cqZAp">
          <node concept="2OqwBi" id="$k" role="3clFbG">
            <node concept="37vLTw" id="$l" role="2Oq$k0">
              <ref role="3cqZAo" node="zY" resolve="b" />
            </node>
            <node concept="liA8E" id="$m" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="$n" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4322367480666013024" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="zV" role="3cqZAp">
          <node concept="2OqwBi" id="$o" role="3clFbG">
            <node concept="37vLTw" id="$p" role="2Oq$k0">
              <ref role="3cqZAo" node="zY" resolve="b" />
            </node>
            <node concept="liA8E" id="$q" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="$r" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="zW" role="3cqZAp">
          <node concept="2OqwBi" id="$s" role="3clFbG">
            <node concept="37vLTw" id="$t" role="2Oq$k0">
              <ref role="3cqZAo" node="zY" resolve="b" />
            </node>
            <node concept="liA8E" id="$u" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.alias(java.lang.String)" resolve="alias" />
              <node concept="Xl_RD" id="$v" role="37wK5m">
                <property role="Xl_RC" value="get" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="zX" role="3cqZAp">
          <node concept="2OqwBi" id="$w" role="3cqZAk">
            <node concept="37vLTw" id="$x" role="2Oq$k0">
              <ref role="3cqZAo" node="zY" resolve="b" />
            </node>
            <node concept="liA8E" id="$y" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="zP" role="1B3o_S" />
      <node concept="3uibUv" id="zQ" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="oS" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForIntegerSchema" />
      <node concept="3clFbS" id="$z" role="3clF47">
        <node concept="3cpWs8" id="$A" role="3cqZAp">
          <node concept="3cpWsn" id="$K" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="$L" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="$M" role="33vP2m">
              <node concept="1pGfFk" id="$N" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="$O" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="$P" role="37wK5m">
                  <property role="Xl_RC" value="IntegerSchema" />
                </node>
                <node concept="1adDum" id="$Q" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="$R" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="$S" role="37wK5m">
                  <property role="1adDun" value="0x43ca7e69529f3e0cL" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$B" role="3cqZAp">
          <node concept="2OqwBi" id="$T" role="3clFbG">
            <node concept="37vLTw" id="$U" role="2Oq$k0">
              <ref role="3cqZAo" node="$K" resolve="b" />
            </node>
            <node concept="liA8E" id="$V" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="$W" role="37wK5m" />
              <node concept="3clFbT" id="$X" role="37wK5m" />
              <node concept="3clFbT" id="$Y" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$C" role="3cqZAp">
          <node concept="2OqwBi" id="$Z" role="3clFbG">
            <node concept="37vLTw" id="_0" role="2Oq$k0">
              <ref role="3cqZAo" node="$K" resolve="b" />
            </node>
            <node concept="liA8E" id="_1" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.super_(java.lang.String,long,long,long)" resolve="super_" />
              <node concept="Xl_RD" id="_2" role="37wK5m">
                <property role="Xl_RC" value="de.thb.openapidsl.copy.structure.Schema" />
              </node>
              <node concept="1adDum" id="_3" role="37wK5m">
                <property role="1adDun" value="0x29d429a205314919L" />
              </node>
              <node concept="1adDum" id="_4" role="37wK5m">
                <property role="1adDun" value="0x8e806706b89fb3b7L" />
              </node>
              <node concept="1adDum" id="_5" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529f3c94L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$D" role="3cqZAp">
          <node concept="2OqwBi" id="_6" role="3clFbG">
            <node concept="37vLTw" id="_7" role="2Oq$k0">
              <ref role="3cqZAo" node="$K" resolve="b" />
            </node>
            <node concept="liA8E" id="_8" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="_9" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4884855736659557900" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$E" role="3cqZAp">
          <node concept="2OqwBi" id="_a" role="3clFbG">
            <node concept="37vLTw" id="_b" role="2Oq$k0">
              <ref role="3cqZAo" node="$K" resolve="b" />
            </node>
            <node concept="liA8E" id="_c" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="_d" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$F" role="3cqZAp">
          <node concept="2OqwBi" id="_e" role="3clFbG">
            <node concept="2OqwBi" id="_f" role="2Oq$k0">
              <node concept="2OqwBi" id="_h" role="2Oq$k0">
                <node concept="2OqwBi" id="_j" role="2Oq$k0">
                  <node concept="37vLTw" id="_l" role="2Oq$k0">
                    <ref role="3cqZAo" node="$K" resolve="b" />
                  </node>
                  <node concept="liA8E" id="_m" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="_n" role="37wK5m">
                      <property role="Xl_RC" value="minimum" />
                    </node>
                    <node concept="1adDum" id="_o" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529f3e0dL" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="_k" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="_p" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.INTEGER" resolve="INTEGER" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="_i" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="_q" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659557901" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="_g" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$G" role="3cqZAp">
          <node concept="2OqwBi" id="_r" role="3clFbG">
            <node concept="2OqwBi" id="_s" role="2Oq$k0">
              <node concept="2OqwBi" id="_u" role="2Oq$k0">
                <node concept="2OqwBi" id="_w" role="2Oq$k0">
                  <node concept="37vLTw" id="_y" role="2Oq$k0">
                    <ref role="3cqZAo" node="$K" resolve="b" />
                  </node>
                  <node concept="liA8E" id="_z" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="_$" role="37wK5m">
                      <property role="Xl_RC" value="maximum" />
                    </node>
                    <node concept="1adDum" id="__" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529f3e0eL" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="_x" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="_A" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.INTEGER" resolve="INTEGER" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="_v" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="_B" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659557902" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="_t" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$H" role="3cqZAp">
          <node concept="2OqwBi" id="_C" role="3clFbG">
            <node concept="2OqwBi" id="_D" role="2Oq$k0">
              <node concept="2OqwBi" id="_F" role="2Oq$k0">
                <node concept="2OqwBi" id="_H" role="2Oq$k0">
                  <node concept="37vLTw" id="_J" role="2Oq$k0">
                    <ref role="3cqZAo" node="$K" resolve="b" />
                  </node>
                  <node concept="liA8E" id="_K" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="_L" role="37wK5m">
                      <property role="Xl_RC" value="pattern" />
                    </node>
                    <node concept="1adDum" id="_M" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529f3e0fL" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="_I" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="_N" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.STRING" resolve="STRING" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="_G" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="_O" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659557903" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="_E" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$I" role="3cqZAp">
          <node concept="2OqwBi" id="_P" role="3clFbG">
            <node concept="37vLTw" id="_Q" role="2Oq$k0">
              <ref role="3cqZAo" node="$K" resolve="b" />
            </node>
            <node concept="liA8E" id="_R" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.alias(java.lang.String)" resolve="alias" />
              <node concept="Xl_RD" id="_S" role="37wK5m">
                <property role="Xl_RC" value="integer" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="$J" role="3cqZAp">
          <node concept="2OqwBi" id="_T" role="3cqZAk">
            <node concept="37vLTw" id="_U" role="2Oq$k0">
              <ref role="3cqZAo" node="$K" resolve="b" />
            </node>
            <node concept="liA8E" id="_V" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="$$" role="1B3o_S" />
      <node concept="3uibUv" id="$_" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="oT" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForObjectSchema" />
      <node concept="3clFbS" id="_W" role="3clF47">
        <node concept="3cpWs8" id="_Z" role="3cqZAp">
          <node concept="3cpWsn" id="A9" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="Aa" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="Ab" role="33vP2m">
              <node concept="1pGfFk" id="Ac" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="Ad" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="Ae" role="37wK5m">
                  <property role="Xl_RC" value="ObjectSchema" />
                </node>
                <node concept="1adDum" id="Af" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="Ag" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="Ah" role="37wK5m">
                  <property role="1adDun" value="0x43ca7e69529f3e28L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="A0" role="3cqZAp">
          <node concept="2OqwBi" id="Ai" role="3clFbG">
            <node concept="37vLTw" id="Aj" role="2Oq$k0">
              <ref role="3cqZAo" node="A9" resolve="b" />
            </node>
            <node concept="liA8E" id="Ak" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="Al" role="37wK5m" />
              <node concept="3clFbT" id="Am" role="37wK5m" />
              <node concept="3clFbT" id="An" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="A1" role="3cqZAp">
          <node concept="2OqwBi" id="Ao" role="3clFbG">
            <node concept="37vLTw" id="Ap" role="2Oq$k0">
              <ref role="3cqZAo" node="A9" resolve="b" />
            </node>
            <node concept="liA8E" id="Aq" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.super_(java.lang.String,long,long,long)" resolve="super_" />
              <node concept="Xl_RD" id="Ar" role="37wK5m">
                <property role="Xl_RC" value="de.thb.openapidsl.copy.structure.Schema" />
              </node>
              <node concept="1adDum" id="As" role="37wK5m">
                <property role="1adDun" value="0x29d429a205314919L" />
              </node>
              <node concept="1adDum" id="At" role="37wK5m">
                <property role="1adDun" value="0x8e806706b89fb3b7L" />
              </node>
              <node concept="1adDum" id="Au" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529f3c94L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="A2" role="3cqZAp">
          <node concept="2OqwBi" id="Av" role="3clFbG">
            <node concept="37vLTw" id="Aw" role="2Oq$k0">
              <ref role="3cqZAo" node="A9" resolve="b" />
            </node>
            <node concept="liA8E" id="Ax" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="Ay" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4884855736659557928" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="A3" role="3cqZAp">
          <node concept="2OqwBi" id="Az" role="3clFbG">
            <node concept="37vLTw" id="A$" role="2Oq$k0">
              <ref role="3cqZAo" node="A9" resolve="b" />
            </node>
            <node concept="liA8E" id="A_" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="AA" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="A4" role="3cqZAp">
          <node concept="2OqwBi" id="AB" role="3clFbG">
            <node concept="2OqwBi" id="AC" role="2Oq$k0">
              <node concept="2OqwBi" id="AE" role="2Oq$k0">
                <node concept="2OqwBi" id="AG" role="2Oq$k0">
                  <node concept="2OqwBi" id="AI" role="2Oq$k0">
                    <node concept="2OqwBi" id="AK" role="2Oq$k0">
                      <node concept="2OqwBi" id="AM" role="2Oq$k0">
                        <node concept="37vLTw" id="AO" role="2Oq$k0">
                          <ref role="3cqZAo" node="A9" resolve="b" />
                        </node>
                        <node concept="liA8E" id="AP" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long)" resolve="aggregate" />
                          <node concept="Xl_RD" id="AQ" role="37wK5m">
                            <property role="Xl_RC" value="required" />
                          </node>
                          <node concept="1adDum" id="AR" role="37wK5m">
                            <property role="1adDun" value="0x3bdca88292770e83L" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="AN" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long)" resolve="target" />
                        <node concept="1adDum" id="AS" role="37wK5m">
                          <property role="1adDun" value="0x29d429a205314919L" />
                        </node>
                        <node concept="1adDum" id="AT" role="37wK5m">
                          <property role="1adDun" value="0x8e806706b89fb3b7L" />
                        </node>
                        <node concept="1adDum" id="AU" role="37wK5m">
                          <property role="1adDun" value="0x3bdca882927a4101L" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="AL" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean)" resolve="optional" />
                      <node concept="3clFbT" id="AV" role="37wK5m">
                        <property role="3clFbU" value="true" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="AJ" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean)" resolve="ordered" />
                    <node concept="3clFbT" id="AW" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="AH" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean)" resolve="multiple" />
                  <node concept="3clFbT" id="AX" role="37wK5m">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="AF" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="AY" role="37wK5m">
                  <property role="Xl_RC" value="4313507821870583427" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="AD" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="A5" role="3cqZAp">
          <node concept="2OqwBi" id="AZ" role="3clFbG">
            <node concept="2OqwBi" id="B0" role="2Oq$k0">
              <node concept="2OqwBi" id="B2" role="2Oq$k0">
                <node concept="2OqwBi" id="B4" role="2Oq$k0">
                  <node concept="2OqwBi" id="B6" role="2Oq$k0">
                    <node concept="2OqwBi" id="B8" role="2Oq$k0">
                      <node concept="2OqwBi" id="Ba" role="2Oq$k0">
                        <node concept="37vLTw" id="Bc" role="2Oq$k0">
                          <ref role="3cqZAo" node="A9" resolve="b" />
                        </node>
                        <node concept="liA8E" id="Bd" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long)" resolve="aggregate" />
                          <node concept="Xl_RD" id="Be" role="37wK5m">
                            <property role="Xl_RC" value="properties" />
                          </node>
                          <node concept="1adDum" id="Bf" role="37wK5m">
                            <property role="1adDun" value="0x43ca7e69529f3e2fL" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="Bb" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long)" resolve="target" />
                        <node concept="1adDum" id="Bg" role="37wK5m">
                          <property role="1adDun" value="0x29d429a205314919L" />
                        </node>
                        <node concept="1adDum" id="Bh" role="37wK5m">
                          <property role="1adDun" value="0x8e806706b89fb3b7L" />
                        </node>
                        <node concept="1adDum" id="Bi" role="37wK5m">
                          <property role="1adDun" value="0x43ca7e69529f3e31L" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="B9" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean)" resolve="optional" />
                      <node concept="3clFbT" id="Bj" role="37wK5m" />
                    </node>
                  </node>
                  <node concept="liA8E" id="B7" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean)" resolve="ordered" />
                    <node concept="3clFbT" id="Bk" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="B5" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean)" resolve="multiple" />
                  <node concept="3clFbT" id="Bl" role="37wK5m">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="B3" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="Bm" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659557935" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="B1" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="A6" role="3cqZAp">
          <node concept="2OqwBi" id="Bn" role="3clFbG">
            <node concept="2OqwBi" id="Bo" role="2Oq$k0">
              <node concept="2OqwBi" id="Bq" role="2Oq$k0">
                <node concept="2OqwBi" id="Bs" role="2Oq$k0">
                  <node concept="2OqwBi" id="Bu" role="2Oq$k0">
                    <node concept="2OqwBi" id="Bw" role="2Oq$k0">
                      <node concept="2OqwBi" id="By" role="2Oq$k0">
                        <node concept="37vLTw" id="B$" role="2Oq$k0">
                          <ref role="3cqZAo" node="A9" resolve="b" />
                        </node>
                        <node concept="liA8E" id="B_" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long)" resolve="aggregate" />
                          <node concept="Xl_RD" id="BA" role="37wK5m">
                            <property role="Xl_RC" value="additionalProperties" />
                          </node>
                          <node concept="1adDum" id="BB" role="37wK5m">
                            <property role="1adDun" value="0x3bdca882925f8940L" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="Bz" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long)" resolve="target" />
                        <node concept="1adDum" id="BC" role="37wK5m">
                          <property role="1adDun" value="0x29d429a205314919L" />
                        </node>
                        <node concept="1adDum" id="BD" role="37wK5m">
                          <property role="1adDun" value="0x8e806706b89fb3b7L" />
                        </node>
                        <node concept="1adDum" id="BE" role="37wK5m">
                          <property role="1adDun" value="0x3bdca882925f8943L" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="Bx" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean)" resolve="optional" />
                      <node concept="3clFbT" id="BF" role="37wK5m">
                        <property role="3clFbU" value="true" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="Bv" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean)" resolve="ordered" />
                    <node concept="3clFbT" id="BG" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="Bt" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean)" resolve="multiple" />
                  <node concept="3clFbT" id="BH" role="37wK5m" />
                </node>
              </node>
              <node concept="liA8E" id="Br" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="BI" role="37wK5m">
                  <property role="Xl_RC" value="4313507821869041984" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="Bp" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="A7" role="3cqZAp">
          <node concept="2OqwBi" id="BJ" role="3clFbG">
            <node concept="37vLTw" id="BK" role="2Oq$k0">
              <ref role="3cqZAo" node="A9" resolve="b" />
            </node>
            <node concept="liA8E" id="BL" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.alias(java.lang.String)" resolve="alias" />
              <node concept="Xl_RD" id="BM" role="37wK5m">
                <property role="Xl_RC" value="object" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="A8" role="3cqZAp">
          <node concept="2OqwBi" id="BN" role="3cqZAk">
            <node concept="37vLTw" id="BO" role="2Oq$k0">
              <ref role="3cqZAo" node="A9" resolve="b" />
            </node>
            <node concept="liA8E" id="BP" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="_X" role="1B3o_S" />
      <node concept="3uibUv" id="_Y" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="oU" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForObjectSchemaAdditionalProperty" />
      <node concept="3clFbS" id="BQ" role="3clF47">
        <node concept="3cpWs8" id="BT" role="3cqZAp">
          <node concept="3cpWsn" id="C0" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="C1" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="C2" role="33vP2m">
              <node concept="1pGfFk" id="C3" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="C4" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="C5" role="37wK5m">
                  <property role="Xl_RC" value="ObjectSchemaAdditionalProperty" />
                </node>
                <node concept="1adDum" id="C6" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="C7" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="C8" role="37wK5m">
                  <property role="1adDun" value="0x3bdca882925f8943L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="BU" role="3cqZAp">
          <node concept="2OqwBi" id="C9" role="3clFbG">
            <node concept="37vLTw" id="Ca" role="2Oq$k0">
              <ref role="3cqZAo" node="C0" resolve="b" />
            </node>
            <node concept="liA8E" id="Cb" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="Cc" role="37wK5m" />
              <node concept="3clFbT" id="Cd" role="37wK5m" />
              <node concept="3clFbT" id="Ce" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="BV" role="3cqZAp">
          <node concept="2OqwBi" id="Cf" role="3clFbG">
            <node concept="37vLTw" id="Cg" role="2Oq$k0">
              <ref role="3cqZAo" node="C0" resolve="b" />
            </node>
            <node concept="liA8E" id="Ch" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="Ci" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4313507821869041987" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="BW" role="3cqZAp">
          <node concept="2OqwBi" id="Cj" role="3clFbG">
            <node concept="37vLTw" id="Ck" role="2Oq$k0">
              <ref role="3cqZAo" node="C0" resolve="b" />
            </node>
            <node concept="liA8E" id="Cl" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="Cm" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="BX" role="3cqZAp">
          <node concept="2OqwBi" id="Cn" role="3clFbG">
            <node concept="2OqwBi" id="Co" role="2Oq$k0">
              <node concept="2OqwBi" id="Cq" role="2Oq$k0">
                <node concept="2OqwBi" id="Cs" role="2Oq$k0">
                  <node concept="37vLTw" id="Cu" role="2Oq$k0">
                    <ref role="3cqZAo" node="C0" resolve="b" />
                  </node>
                  <node concept="liA8E" id="Cv" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="Cw" role="37wK5m">
                      <property role="Xl_RC" value="type" />
                    </node>
                    <node concept="1adDum" id="Cx" role="37wK5m">
                      <property role="1adDun" value="0x3bdca882925f8944L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="Ct" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="2YIFZM" id="Cy" role="37wK5m">
                    <ref role="37wK5l" to="e8bb:~MetaIdFactory.dataTypeId(long,long,long)" resolve="dataTypeId" />
                    <ref role="1Pybhc" to="e8bb:~MetaIdFactory" resolve="MetaIdFactory" />
                    <uo k="s:originTrace" v="n:4313507821869042030" />
                    <node concept="1adDum" id="Cz" role="37wK5m">
                      <property role="1adDun" value="0x29d429a205314919L" />
                      <uo k="s:originTrace" v="n:4313507821869042030" />
                    </node>
                    <node concept="1adDum" id="C$" role="37wK5m">
                      <property role="1adDun" value="0x8e806706b89fb3b7L" />
                      <uo k="s:originTrace" v="n:4313507821869042030" />
                    </node>
                    <node concept="1adDum" id="C_" role="37wK5m">
                      <property role="1adDun" value="0x3bdca882925f896eL" />
                      <uo k="s:originTrace" v="n:4313507821869042030" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="Cr" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="CA" role="37wK5m">
                  <property role="Xl_RC" value="4313507821869041988" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="Cp" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="BY" role="3cqZAp">
          <node concept="2OqwBi" id="CB" role="3clFbG">
            <node concept="2OqwBi" id="CC" role="2Oq$k0">
              <node concept="2OqwBi" id="CE" role="2Oq$k0">
                <node concept="2OqwBi" id="CG" role="2Oq$k0">
                  <node concept="2OqwBi" id="CI" role="2Oq$k0">
                    <node concept="37vLTw" id="CK" role="2Oq$k0">
                      <ref role="3cqZAo" node="C0" resolve="b" />
                    </node>
                    <node concept="liA8E" id="CL" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.associate(java.lang.String,long)" resolve="associate" />
                      <node concept="Xl_RD" id="CM" role="37wK5m">
                        <property role="Xl_RC" value="schema" />
                      </node>
                      <node concept="1adDum" id="CN" role="37wK5m">
                        <property role="1adDun" value="0x3bdca882925f8945L" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="CJ" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.target(long,long,long)" resolve="target" />
                    <node concept="1adDum" id="CO" role="37wK5m">
                      <property role="1adDun" value="0x29d429a205314919L" />
                    </node>
                    <node concept="1adDum" id="CP" role="37wK5m">
                      <property role="1adDun" value="0x8e806706b89fb3b7L" />
                    </node>
                    <node concept="1adDum" id="CQ" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529f3c94L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="CH" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.optional(boolean)" resolve="optional" />
                  <node concept="3clFbT" id="CR" role="37wK5m" />
                </node>
              </node>
              <node concept="liA8E" id="CF" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="CS" role="37wK5m">
                  <property role="Xl_RC" value="4313507821869041989" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="CD" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="BZ" role="3cqZAp">
          <node concept="2OqwBi" id="CT" role="3cqZAk">
            <node concept="37vLTw" id="CU" role="2Oq$k0">
              <ref role="3cqZAo" node="C0" resolve="b" />
            </node>
            <node concept="liA8E" id="CV" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="BR" role="1B3o_S" />
      <node concept="3uibUv" id="BS" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="oV" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForObjectSchemaProperty" />
      <node concept="3clFbS" id="CW" role="3clF47">
        <node concept="3cpWs8" id="CZ" role="3cqZAp">
          <node concept="3cpWsn" id="D6" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="D7" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="D8" role="33vP2m">
              <node concept="1pGfFk" id="D9" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="Da" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="Db" role="37wK5m">
                  <property role="Xl_RC" value="ObjectSchemaProperty" />
                </node>
                <node concept="1adDum" id="Dc" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="Dd" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="De" role="37wK5m">
                  <property role="1adDun" value="0x43ca7e69529f3e31L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="D0" role="3cqZAp">
          <node concept="2OqwBi" id="Df" role="3clFbG">
            <node concept="37vLTw" id="Dg" role="2Oq$k0">
              <ref role="3cqZAo" node="D6" resolve="b" />
            </node>
            <node concept="liA8E" id="Dh" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="Di" role="37wK5m" />
              <node concept="3clFbT" id="Dj" role="37wK5m" />
              <node concept="3clFbT" id="Dk" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="D1" role="3cqZAp">
          <node concept="2OqwBi" id="Dl" role="3clFbG">
            <node concept="37vLTw" id="Dm" role="2Oq$k0">
              <ref role="3cqZAo" node="D6" resolve="b" />
            </node>
            <node concept="liA8E" id="Dn" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="Do" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4884855736659557937" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="D2" role="3cqZAp">
          <node concept="2OqwBi" id="Dp" role="3clFbG">
            <node concept="37vLTw" id="Dq" role="2Oq$k0">
              <ref role="3cqZAo" node="D6" resolve="b" />
            </node>
            <node concept="liA8E" id="Dr" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="Ds" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="D3" role="3cqZAp">
          <node concept="2OqwBi" id="Dt" role="3clFbG">
            <node concept="2OqwBi" id="Du" role="2Oq$k0">
              <node concept="2OqwBi" id="Dw" role="2Oq$k0">
                <node concept="2OqwBi" id="Dy" role="2Oq$k0">
                  <node concept="37vLTw" id="D$" role="2Oq$k0">
                    <ref role="3cqZAo" node="D6" resolve="b" />
                  </node>
                  <node concept="liA8E" id="D_" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="DA" role="37wK5m">
                      <property role="Xl_RC" value="name" />
                    </node>
                    <node concept="1adDum" id="DB" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529f3e32L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="Dz" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="DC" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.STRING" resolve="STRING" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="Dx" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="DD" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659557938" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="Dv" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="D4" role="3cqZAp">
          <node concept="2OqwBi" id="DE" role="3clFbG">
            <node concept="2OqwBi" id="DF" role="2Oq$k0">
              <node concept="2OqwBi" id="DH" role="2Oq$k0">
                <node concept="2OqwBi" id="DJ" role="2Oq$k0">
                  <node concept="2OqwBi" id="DL" role="2Oq$k0">
                    <node concept="37vLTw" id="DN" role="2Oq$k0">
                      <ref role="3cqZAo" node="D6" resolve="b" />
                    </node>
                    <node concept="liA8E" id="DO" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.associate(java.lang.String,long)" resolve="associate" />
                      <node concept="Xl_RD" id="DP" role="37wK5m">
                        <property role="Xl_RC" value="schema" />
                      </node>
                      <node concept="1adDum" id="DQ" role="37wK5m">
                        <property role="1adDun" value="0x43ca7e69529f3e34L" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="DM" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.target(long,long,long)" resolve="target" />
                    <node concept="1adDum" id="DR" role="37wK5m">
                      <property role="1adDun" value="0x29d429a205314919L" />
                    </node>
                    <node concept="1adDum" id="DS" role="37wK5m">
                      <property role="1adDun" value="0x8e806706b89fb3b7L" />
                    </node>
                    <node concept="1adDum" id="DT" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529f3c94L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="DK" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.optional(boolean)" resolve="optional" />
                  <node concept="3clFbT" id="DU" role="37wK5m" />
                </node>
              </node>
              <node concept="liA8E" id="DI" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="DV" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659557940" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="DG" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="D5" role="3cqZAp">
          <node concept="2OqwBi" id="DW" role="3cqZAk">
            <node concept="37vLTw" id="DX" role="2Oq$k0">
              <ref role="3cqZAo" node="D6" resolve="b" />
            </node>
            <node concept="liA8E" id="DY" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="CX" role="1B3o_S" />
      <node concept="3uibUv" id="CY" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="oW" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForObjectSchemaRequiredProperty" />
      <node concept="3clFbS" id="DZ" role="3clF47">
        <node concept="3cpWs8" id="E2" role="3cqZAp">
          <node concept="3cpWsn" id="E8" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="E9" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="Ea" role="33vP2m">
              <node concept="1pGfFk" id="Eb" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="Ec" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="Ed" role="37wK5m">
                  <property role="Xl_RC" value="ObjectSchemaRequiredProperty" />
                </node>
                <node concept="1adDum" id="Ee" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="Ef" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="Eg" role="37wK5m">
                  <property role="1adDun" value="0x3bdca882927a4101L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="E3" role="3cqZAp">
          <node concept="2OqwBi" id="Eh" role="3clFbG">
            <node concept="37vLTw" id="Ei" role="2Oq$k0">
              <ref role="3cqZAo" node="E8" resolve="b" />
            </node>
            <node concept="liA8E" id="Ej" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="Ek" role="37wK5m" />
              <node concept="3clFbT" id="El" role="37wK5m" />
              <node concept="3clFbT" id="Em" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="E4" role="3cqZAp">
          <node concept="2OqwBi" id="En" role="3clFbG">
            <node concept="37vLTw" id="Eo" role="2Oq$k0">
              <ref role="3cqZAo" node="E8" resolve="b" />
            </node>
            <node concept="liA8E" id="Ep" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="Eq" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4313507821870792961" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="E5" role="3cqZAp">
          <node concept="2OqwBi" id="Er" role="3clFbG">
            <node concept="37vLTw" id="Es" role="2Oq$k0">
              <ref role="3cqZAo" node="E8" resolve="b" />
            </node>
            <node concept="liA8E" id="Et" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="Eu" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="E6" role="3cqZAp">
          <node concept="2OqwBi" id="Ev" role="3clFbG">
            <node concept="2OqwBi" id="Ew" role="2Oq$k0">
              <node concept="2OqwBi" id="Ey" role="2Oq$k0">
                <node concept="2OqwBi" id="E$" role="2Oq$k0">
                  <node concept="2OqwBi" id="EA" role="2Oq$k0">
                    <node concept="37vLTw" id="EC" role="2Oq$k0">
                      <ref role="3cqZAo" node="E8" resolve="b" />
                    </node>
                    <node concept="liA8E" id="ED" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.associate(java.lang.String,long)" resolve="associate" />
                      <node concept="Xl_RD" id="EE" role="37wK5m">
                        <property role="Xl_RC" value="property" />
                      </node>
                      <node concept="1adDum" id="EF" role="37wK5m">
                        <property role="1adDun" value="0x3bdca882927a4103L" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="EB" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.target(long,long,long)" resolve="target" />
                    <node concept="1adDum" id="EG" role="37wK5m">
                      <property role="1adDun" value="0x29d429a205314919L" />
                    </node>
                    <node concept="1adDum" id="EH" role="37wK5m">
                      <property role="1adDun" value="0x8e806706b89fb3b7L" />
                    </node>
                    <node concept="1adDum" id="EI" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529f3e31L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="E_" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.optional(boolean)" resolve="optional" />
                  <node concept="3clFbT" id="EJ" role="37wK5m" />
                </node>
              </node>
              <node concept="liA8E" id="Ez" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="EK" role="37wK5m">
                  <property role="Xl_RC" value="4313507821870792963" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="Ex" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="E7" role="3cqZAp">
          <node concept="2OqwBi" id="EL" role="3cqZAk">
            <node concept="37vLTw" id="EM" role="2Oq$k0">
              <ref role="3cqZAo" node="E8" resolve="b" />
            </node>
            <node concept="liA8E" id="EN" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="E0" role="1B3o_S" />
      <node concept="3uibUv" id="E1" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="oX" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForOneOfSchema" />
      <node concept="3clFbS" id="EO" role="3clF47">
        <node concept="3cpWs8" id="ER" role="3cqZAp">
          <node concept="3cpWsn" id="EZ" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="F0" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="F1" role="33vP2m">
              <node concept="1pGfFk" id="F2" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="F3" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="F4" role="37wK5m">
                  <property role="Xl_RC" value="OneOfSchema" />
                </node>
                <node concept="1adDum" id="F5" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="F6" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="F7" role="37wK5m">
                  <property role="1adDun" value="0x3bdca882929c263eL" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ES" role="3cqZAp">
          <node concept="2OqwBi" id="F8" role="3clFbG">
            <node concept="37vLTw" id="F9" role="2Oq$k0">
              <ref role="3cqZAo" node="EZ" resolve="b" />
            </node>
            <node concept="liA8E" id="Fa" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="Fb" role="37wK5m" />
              <node concept="3clFbT" id="Fc" role="37wK5m" />
              <node concept="3clFbT" id="Fd" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ET" role="3cqZAp">
          <node concept="2OqwBi" id="Fe" role="3clFbG">
            <node concept="37vLTw" id="Ff" role="2Oq$k0">
              <ref role="3cqZAo" node="EZ" resolve="b" />
            </node>
            <node concept="liA8E" id="Fg" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.super_(java.lang.String,long,long,long)" resolve="super_" />
              <node concept="Xl_RD" id="Fh" role="37wK5m">
                <property role="Xl_RC" value="de.thb.openapidsl.copy.structure.Schema" />
              </node>
              <node concept="1adDum" id="Fi" role="37wK5m">
                <property role="1adDun" value="0x29d429a205314919L" />
              </node>
              <node concept="1adDum" id="Fj" role="37wK5m">
                <property role="1adDun" value="0x8e806706b89fb3b7L" />
              </node>
              <node concept="1adDum" id="Fk" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529f3c94L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="EU" role="3cqZAp">
          <node concept="2OqwBi" id="Fl" role="3clFbG">
            <node concept="37vLTw" id="Fm" role="2Oq$k0">
              <ref role="3cqZAo" node="EZ" resolve="b" />
            </node>
            <node concept="liA8E" id="Fn" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="Fo" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4313507821873014334" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="EV" role="3cqZAp">
          <node concept="2OqwBi" id="Fp" role="3clFbG">
            <node concept="37vLTw" id="Fq" role="2Oq$k0">
              <ref role="3cqZAo" node="EZ" resolve="b" />
            </node>
            <node concept="liA8E" id="Fr" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="Fs" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="EW" role="3cqZAp">
          <node concept="2OqwBi" id="Ft" role="3clFbG">
            <node concept="2OqwBi" id="Fu" role="2Oq$k0">
              <node concept="2OqwBi" id="Fw" role="2Oq$k0">
                <node concept="2OqwBi" id="Fy" role="2Oq$k0">
                  <node concept="2OqwBi" id="F$" role="2Oq$k0">
                    <node concept="2OqwBi" id="FA" role="2Oq$k0">
                      <node concept="2OqwBi" id="FC" role="2Oq$k0">
                        <node concept="37vLTw" id="FE" role="2Oq$k0">
                          <ref role="3cqZAo" node="EZ" resolve="b" />
                        </node>
                        <node concept="liA8E" id="FF" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long)" resolve="aggregate" />
                          <node concept="Xl_RD" id="FG" role="37wK5m">
                            <property role="Xl_RC" value="oneOf" />
                          </node>
                          <node concept="1adDum" id="FH" role="37wK5m">
                            <property role="1adDun" value="0x3bdca882929c263fL" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="FD" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long)" resolve="target" />
                        <node concept="1adDum" id="FI" role="37wK5m">
                          <property role="1adDun" value="0x29d429a205314919L" />
                        </node>
                        <node concept="1adDum" id="FJ" role="37wK5m">
                          <property role="1adDun" value="0x8e806706b89fb3b7L" />
                        </node>
                        <node concept="1adDum" id="FK" role="37wK5m">
                          <property role="1adDun" value="0x3bdca882929f537fL" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="FB" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean)" resolve="optional" />
                      <node concept="3clFbT" id="FL" role="37wK5m" />
                    </node>
                  </node>
                  <node concept="liA8E" id="F_" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean)" resolve="ordered" />
                    <node concept="3clFbT" id="FM" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="Fz" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean)" resolve="multiple" />
                  <node concept="3clFbT" id="FN" role="37wK5m">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="Fx" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="FO" role="37wK5m">
                  <property role="Xl_RC" value="4313507821873014335" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="Fv" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="EX" role="3cqZAp">
          <node concept="2OqwBi" id="FP" role="3clFbG">
            <node concept="37vLTw" id="FQ" role="2Oq$k0">
              <ref role="3cqZAo" node="EZ" resolve="b" />
            </node>
            <node concept="liA8E" id="FR" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.alias(java.lang.String)" resolve="alias" />
              <node concept="Xl_RD" id="FS" role="37wK5m">
                <property role="Xl_RC" value="oneOf" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="EY" role="3cqZAp">
          <node concept="2OqwBi" id="FT" role="3cqZAk">
            <node concept="37vLTw" id="FU" role="2Oq$k0">
              <ref role="3cqZAo" node="EZ" resolve="b" />
            </node>
            <node concept="liA8E" id="FV" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="EP" role="1B3o_S" />
      <node concept="3uibUv" id="EQ" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="oY" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForOpenApi" />
      <node concept="3clFbS" id="FW" role="3clF47">
        <node concept="3cpWs8" id="FZ" role="3cqZAp">
          <node concept="3cpWsn" id="G8" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="G9" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="Ga" role="33vP2m">
              <node concept="1pGfFk" id="Gb" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="Gc" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="Gd" role="37wK5m">
                  <property role="Xl_RC" value="OpenApi" />
                </node>
                <node concept="1adDum" id="Ge" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="Gf" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="Gg" role="37wK5m">
                  <property role="1adDun" value="0x3bfc2252a1cbc1eeL" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="G0" role="3cqZAp">
          <node concept="2OqwBi" id="Gh" role="3clFbG">
            <node concept="37vLTw" id="Gi" role="2Oq$k0">
              <ref role="3cqZAo" node="G8" resolve="b" />
            </node>
            <node concept="liA8E" id="Gj" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="Gk" role="37wK5m" />
              <node concept="3clFbT" id="Gl" role="37wK5m" />
              <node concept="3clFbT" id="Gm" role="37wK5m">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="G1" role="3cqZAp">
          <node concept="2OqwBi" id="Gn" role="3clFbG">
            <node concept="37vLTw" id="Go" role="2Oq$k0">
              <ref role="3cqZAo" node="G8" resolve="b" />
            </node>
            <node concept="liA8E" id="Gp" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="Gq" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4322367480665981422" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="G2" role="3cqZAp">
          <node concept="2OqwBi" id="Gr" role="3clFbG">
            <node concept="37vLTw" id="Gs" role="2Oq$k0">
              <ref role="3cqZAo" node="G8" resolve="b" />
            </node>
            <node concept="liA8E" id="Gt" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="Gu" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="G3" role="3cqZAp">
          <node concept="2OqwBi" id="Gv" role="3clFbG">
            <node concept="2OqwBi" id="Gw" role="2Oq$k0">
              <node concept="2OqwBi" id="Gy" role="2Oq$k0">
                <node concept="2OqwBi" id="G$" role="2Oq$k0">
                  <node concept="37vLTw" id="GA" role="2Oq$k0">
                    <ref role="3cqZAo" node="G8" resolve="b" />
                  </node>
                  <node concept="liA8E" id="GB" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="GC" role="37wK5m">
                      <property role="Xl_RC" value="openapi" />
                    </node>
                    <node concept="1adDum" id="GD" role="37wK5m">
                      <property role="1adDun" value="0x3bfc2252a1cbe9a8L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="G_" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="2YIFZM" id="GE" role="37wK5m">
                    <ref role="37wK5l" to="e8bb:~MetaIdFactory.dataTypeId(long,long,long)" resolve="dataTypeId" />
                    <ref role="1Pybhc" to="e8bb:~MetaIdFactory" resolve="MetaIdFactory" />
                    <uo k="s:originTrace" v="n:4322367480665991621" />
                    <node concept="1adDum" id="GF" role="37wK5m">
                      <property role="1adDun" value="0x29d429a205314919L" />
                      <uo k="s:originTrace" v="n:4322367480665991621" />
                    </node>
                    <node concept="1adDum" id="GG" role="37wK5m">
                      <property role="1adDun" value="0x8e806706b89fb3b7L" />
                      <uo k="s:originTrace" v="n:4322367480665991621" />
                    </node>
                    <node concept="1adDum" id="GH" role="37wK5m">
                      <property role="1adDun" value="0x3bfc2252a1cbe9c5L" />
                      <uo k="s:originTrace" v="n:4322367480665991621" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="Gz" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="GI" role="37wK5m">
                  <property role="Xl_RC" value="4322367480665991592" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="Gx" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="G4" role="3cqZAp">
          <node concept="2OqwBi" id="GJ" role="3clFbG">
            <node concept="2OqwBi" id="GK" role="2Oq$k0">
              <node concept="2OqwBi" id="GM" role="2Oq$k0">
                <node concept="2OqwBi" id="GO" role="2Oq$k0">
                  <node concept="2OqwBi" id="GQ" role="2Oq$k0">
                    <node concept="2OqwBi" id="GS" role="2Oq$k0">
                      <node concept="2OqwBi" id="GU" role="2Oq$k0">
                        <node concept="37vLTw" id="GW" role="2Oq$k0">
                          <ref role="3cqZAo" node="G8" resolve="b" />
                        </node>
                        <node concept="liA8E" id="GX" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long)" resolve="aggregate" />
                          <node concept="Xl_RD" id="GY" role="37wK5m">
                            <property role="Xl_RC" value="info" />
                          </node>
                          <node concept="1adDum" id="GZ" role="37wK5m">
                            <property role="1adDun" value="0x3bfc2252a1cbc1efL" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="GV" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long)" resolve="target" />
                        <node concept="1adDum" id="H0" role="37wK5m">
                          <property role="1adDun" value="0x29d429a205314919L" />
                        </node>
                        <node concept="1adDum" id="H1" role="37wK5m">
                          <property role="1adDun" value="0x8e806706b89fb3b7L" />
                        </node>
                        <node concept="1adDum" id="H2" role="37wK5m">
                          <property role="1adDun" value="0x3bfc2252a1cab6b8L" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="GT" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean)" resolve="optional" />
                      <node concept="3clFbT" id="H3" role="37wK5m" />
                    </node>
                  </node>
                  <node concept="liA8E" id="GR" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean)" resolve="ordered" />
                    <node concept="3clFbT" id="H4" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="GP" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean)" resolve="multiple" />
                  <node concept="3clFbT" id="H5" role="37wK5m" />
                </node>
              </node>
              <node concept="liA8E" id="GN" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="H6" role="37wK5m">
                  <property role="Xl_RC" value="4322367480665981423" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="GL" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="G5" role="3cqZAp">
          <node concept="2OqwBi" id="H7" role="3clFbG">
            <node concept="2OqwBi" id="H8" role="2Oq$k0">
              <node concept="2OqwBi" id="Ha" role="2Oq$k0">
                <node concept="2OqwBi" id="Hc" role="2Oq$k0">
                  <node concept="2OqwBi" id="He" role="2Oq$k0">
                    <node concept="2OqwBi" id="Hg" role="2Oq$k0">
                      <node concept="2OqwBi" id="Hi" role="2Oq$k0">
                        <node concept="37vLTw" id="Hk" role="2Oq$k0">
                          <ref role="3cqZAo" node="G8" resolve="b" />
                        </node>
                        <node concept="liA8E" id="Hl" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long)" resolve="aggregate" />
                          <node concept="Xl_RD" id="Hm" role="37wK5m">
                            <property role="Xl_RC" value="paths" />
                          </node>
                          <node concept="1adDum" id="Hn" role="37wK5m">
                            <property role="1adDun" value="0x3bfc2252a1cc387fL" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="Hj" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long)" resolve="target" />
                        <node concept="1adDum" id="Ho" role="37wK5m">
                          <property role="1adDun" value="0x29d429a205314919L" />
                        </node>
                        <node concept="1adDum" id="Hp" role="37wK5m">
                          <property role="1adDun" value="0x8e806706b89fb3b7L" />
                        </node>
                        <node concept="1adDum" id="Hq" role="37wK5m">
                          <property role="1adDun" value="0x3bfc2252a1cc33c4L" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="Hh" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean)" resolve="optional" />
                      <node concept="3clFbT" id="Hr" role="37wK5m">
                        <property role="3clFbU" value="true" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="Hf" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean)" resolve="ordered" />
                    <node concept="3clFbT" id="Hs" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="Hd" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean)" resolve="multiple" />
                  <node concept="3clFbT" id="Ht" role="37wK5m">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="Hb" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="Hu" role="37wK5m">
                  <property role="Xl_RC" value="4322367480666011775" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="H9" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="G6" role="3cqZAp">
          <node concept="2OqwBi" id="Hv" role="3clFbG">
            <node concept="2OqwBi" id="Hw" role="2Oq$k0">
              <node concept="2OqwBi" id="Hy" role="2Oq$k0">
                <node concept="2OqwBi" id="H$" role="2Oq$k0">
                  <node concept="2OqwBi" id="HA" role="2Oq$k0">
                    <node concept="2OqwBi" id="HC" role="2Oq$k0">
                      <node concept="2OqwBi" id="HE" role="2Oq$k0">
                        <node concept="37vLTw" id="HG" role="2Oq$k0">
                          <ref role="3cqZAo" node="G8" resolve="b" />
                        </node>
                        <node concept="liA8E" id="HH" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long)" resolve="aggregate" />
                          <node concept="Xl_RD" id="HI" role="37wK5m">
                            <property role="Xl_RC" value="schema" />
                          </node>
                          <node concept="1adDum" id="HJ" role="37wK5m">
                            <property role="1adDun" value="0x43ca7e6952a36e2fL" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="HF" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long)" resolve="target" />
                        <node concept="1adDum" id="HK" role="37wK5m">
                          <property role="1adDun" value="0x29d429a205314919L" />
                        </node>
                        <node concept="1adDum" id="HL" role="37wK5m">
                          <property role="1adDun" value="0x8e806706b89fb3b7L" />
                        </node>
                        <node concept="1adDum" id="HM" role="37wK5m">
                          <property role="1adDun" value="0x43ca7e69529f3c94L" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="HD" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean)" resolve="optional" />
                      <node concept="3clFbT" id="HN" role="37wK5m">
                        <property role="3clFbU" value="true" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="HB" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean)" resolve="ordered" />
                    <node concept="3clFbT" id="HO" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="H_" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean)" resolve="multiple" />
                  <node concept="3clFbT" id="HP" role="37wK5m">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="Hz" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="HQ" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659832367" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="Hx" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="G7" role="3cqZAp">
          <node concept="2OqwBi" id="HR" role="3cqZAk">
            <node concept="37vLTw" id="HS" role="2Oq$k0">
              <ref role="3cqZAo" node="G8" resolve="b" />
            </node>
            <node concept="liA8E" id="HT" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="FX" role="1B3o_S" />
      <node concept="3uibUv" id="FY" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="oZ" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForOpenApiInfo" />
      <node concept="3clFbS" id="HU" role="3clF47">
        <node concept="3cpWs8" id="HX" role="3cqZAp">
          <node concept="3cpWsn" id="I6" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="I7" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="I8" role="33vP2m">
              <node concept="1pGfFk" id="I9" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="Ia" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="Ib" role="37wK5m">
                  <property role="Xl_RC" value="OpenApiInfo" />
                </node>
                <node concept="1adDum" id="Ic" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="Id" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="Ie" role="37wK5m">
                  <property role="1adDun" value="0x3bfc2252a1cab6b8L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="HY" role="3cqZAp">
          <node concept="2OqwBi" id="If" role="3clFbG">
            <node concept="37vLTw" id="Ig" role="2Oq$k0">
              <ref role="3cqZAo" node="I6" resolve="b" />
            </node>
            <node concept="liA8E" id="Ih" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="Ii" role="37wK5m" />
              <node concept="3clFbT" id="Ij" role="37wK5m" />
              <node concept="3clFbT" id="Ik" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="HZ" role="3cqZAp">
          <node concept="2OqwBi" id="Il" role="3clFbG">
            <node concept="37vLTw" id="Im" role="2Oq$k0">
              <ref role="3cqZAo" node="I6" resolve="b" />
            </node>
            <node concept="liA8E" id="In" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.parent(long,long,long)" resolve="parent" />
              <node concept="1adDum" id="Io" role="37wK5m">
                <property role="1adDun" value="0xceab519525ea4f22L" />
              </node>
              <node concept="1adDum" id="Ip" role="37wK5m">
                <property role="1adDun" value="0x9b92103b95ca8c0cL" />
              </node>
              <node concept="1adDum" id="Iq" role="37wK5m">
                <property role="1adDun" value="0x110396eaaa4L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="I0" role="3cqZAp">
          <node concept="2OqwBi" id="Ir" role="3clFbG">
            <node concept="37vLTw" id="Is" role="2Oq$k0">
              <ref role="3cqZAo" node="I6" resolve="b" />
            </node>
            <node concept="liA8E" id="It" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="Iu" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4322367480665913016" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="I1" role="3cqZAp">
          <node concept="2OqwBi" id="Iv" role="3clFbG">
            <node concept="37vLTw" id="Iw" role="2Oq$k0">
              <ref role="3cqZAo" node="I6" resolve="b" />
            </node>
            <node concept="liA8E" id="Ix" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="Iy" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="I2" role="3cqZAp">
          <node concept="2OqwBi" id="Iz" role="3clFbG">
            <node concept="2OqwBi" id="I$" role="2Oq$k0">
              <node concept="2OqwBi" id="IA" role="2Oq$k0">
                <node concept="2OqwBi" id="IC" role="2Oq$k0">
                  <node concept="37vLTw" id="IE" role="2Oq$k0">
                    <ref role="3cqZAo" node="I6" resolve="b" />
                  </node>
                  <node concept="liA8E" id="IF" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="IG" role="37wK5m">
                      <property role="Xl_RC" value="title" />
                    </node>
                    <node concept="1adDum" id="IH" role="37wK5m">
                      <property role="1adDun" value="0x3bfc2252a1cab6bbL" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="ID" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="II" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.STRING" resolve="STRING" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="IB" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="IJ" role="37wK5m">
                  <property role="Xl_RC" value="4322367480665913019" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="I_" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="I3" role="3cqZAp">
          <node concept="2OqwBi" id="IK" role="3clFbG">
            <node concept="2OqwBi" id="IL" role="2Oq$k0">
              <node concept="2OqwBi" id="IN" role="2Oq$k0">
                <node concept="2OqwBi" id="IP" role="2Oq$k0">
                  <node concept="37vLTw" id="IR" role="2Oq$k0">
                    <ref role="3cqZAo" node="I6" resolve="b" />
                  </node>
                  <node concept="liA8E" id="IS" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="IT" role="37wK5m">
                      <property role="Xl_RC" value="description" />
                    </node>
                    <node concept="1adDum" id="IU" role="37wK5m">
                      <property role="1adDun" value="0x3bfc2252a1cab6bdL" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="IQ" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="IV" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.STRING" resolve="STRING" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="IO" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="IW" role="37wK5m">
                  <property role="Xl_RC" value="4322367480665913021" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="IM" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="I4" role="3cqZAp">
          <node concept="2OqwBi" id="IX" role="3clFbG">
            <node concept="2OqwBi" id="IY" role="2Oq$k0">
              <node concept="2OqwBi" id="J0" role="2Oq$k0">
                <node concept="2OqwBi" id="J2" role="2Oq$k0">
                  <node concept="37vLTw" id="J4" role="2Oq$k0">
                    <ref role="3cqZAo" node="I6" resolve="b" />
                  </node>
                  <node concept="liA8E" id="J5" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="J6" role="37wK5m">
                      <property role="Xl_RC" value="version" />
                    </node>
                    <node concept="1adDum" id="J7" role="37wK5m">
                      <property role="1adDun" value="0x3bfc2252a1cab6c0L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="J3" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="2YIFZM" id="J8" role="37wK5m">
                    <ref role="37wK5l" to="e8bb:~MetaIdFactory.dataTypeId(long,long,long)" resolve="dataTypeId" />
                    <ref role="1Pybhc" to="e8bb:~MetaIdFactory" resolve="MetaIdFactory" />
                    <uo k="s:originTrace" v="n:4322367480665991621" />
                    <node concept="1adDum" id="J9" role="37wK5m">
                      <property role="1adDun" value="0x29d429a205314919L" />
                      <uo k="s:originTrace" v="n:4322367480665991621" />
                    </node>
                    <node concept="1adDum" id="Ja" role="37wK5m">
                      <property role="1adDun" value="0x8e806706b89fb3b7L" />
                      <uo k="s:originTrace" v="n:4322367480665991621" />
                    </node>
                    <node concept="1adDum" id="Jb" role="37wK5m">
                      <property role="1adDun" value="0x3bfc2252a1cbe9c5L" />
                      <uo k="s:originTrace" v="n:4322367480665991621" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="J1" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="Jc" role="37wK5m">
                  <property role="Xl_RC" value="4322367480665913024" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="IZ" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="I5" role="3cqZAp">
          <node concept="2OqwBi" id="Jd" role="3cqZAk">
            <node concept="37vLTw" id="Je" role="2Oq$k0">
              <ref role="3cqZAo" node="I6" resolve="b" />
            </node>
            <node concept="liA8E" id="Jf" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="HV" role="1B3o_S" />
      <node concept="3uibUv" id="HW" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="p0" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForOperationObject" />
      <node concept="3clFbS" id="Jg" role="3clF47">
        <node concept="3cpWs8" id="Jj" role="3cqZAp">
          <node concept="3cpWsn" id="Jr" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="Js" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="Jt" role="33vP2m">
              <node concept="1pGfFk" id="Ju" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="Jv" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="Jw" role="37wK5m">
                  <property role="Xl_RC" value="OperationObject" />
                </node>
                <node concept="1adDum" id="Jx" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="Jy" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="Jz" role="37wK5m">
                  <property role="1adDun" value="0x3bfc2252a1cc33d0L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="Jk" role="3cqZAp">
          <node concept="2OqwBi" id="J$" role="3clFbG">
            <node concept="37vLTw" id="J_" role="2Oq$k0">
              <ref role="3cqZAo" node="Jr" resolve="b" />
            </node>
            <node concept="liA8E" id="JA" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="JB" role="37wK5m" />
              <node concept="3clFbT" id="JC" role="37wK5m">
                <property role="3clFbU" value="true" />
              </node>
              <node concept="3clFbT" id="JD" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="Jl" role="3cqZAp">
          <node concept="2OqwBi" id="JE" role="3clFbG">
            <node concept="37vLTw" id="JF" role="2Oq$k0">
              <ref role="3cqZAo" node="Jr" resolve="b" />
            </node>
            <node concept="liA8E" id="JG" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="JH" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4322367480666010576" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="Jm" role="3cqZAp">
          <node concept="2OqwBi" id="JI" role="3clFbG">
            <node concept="37vLTw" id="JJ" role="2Oq$k0">
              <ref role="3cqZAo" node="Jr" resolve="b" />
            </node>
            <node concept="liA8E" id="JK" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="JL" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="Jn" role="3cqZAp">
          <node concept="2OqwBi" id="JM" role="3clFbG">
            <node concept="2OqwBi" id="JN" role="2Oq$k0">
              <node concept="2OqwBi" id="JP" role="2Oq$k0">
                <node concept="2OqwBi" id="JR" role="2Oq$k0">
                  <node concept="37vLTw" id="JT" role="2Oq$k0">
                    <ref role="3cqZAo" node="Jr" resolve="b" />
                  </node>
                  <node concept="liA8E" id="JU" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="JV" role="37wK5m">
                      <property role="Xl_RC" value="summary" />
                    </node>
                    <node concept="1adDum" id="JW" role="37wK5m">
                      <property role="1adDun" value="0x3bfc2252a1cc33e7L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="JS" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="JX" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.STRING" resolve="STRING" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="JQ" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="JY" role="37wK5m">
                  <property role="Xl_RC" value="4322367480666010599" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="JO" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="Jo" role="3cqZAp">
          <node concept="2OqwBi" id="JZ" role="3clFbG">
            <node concept="2OqwBi" id="K0" role="2Oq$k0">
              <node concept="2OqwBi" id="K2" role="2Oq$k0">
                <node concept="2OqwBi" id="K4" role="2Oq$k0">
                  <node concept="37vLTw" id="K6" role="2Oq$k0">
                    <ref role="3cqZAo" node="Jr" resolve="b" />
                  </node>
                  <node concept="liA8E" id="K7" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="K8" role="37wK5m">
                      <property role="Xl_RC" value="description" />
                    </node>
                    <node concept="1adDum" id="K9" role="37wK5m">
                      <property role="1adDun" value="0x3bfc2252a1cc33eeL" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="K5" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="Ka" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.STRING" resolve="STRING" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="K3" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="Kb" role="37wK5m">
                  <property role="Xl_RC" value="4322367480666010606" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="K1" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="Jp" role="3cqZAp">
          <node concept="2OqwBi" id="Kc" role="3clFbG">
            <node concept="2OqwBi" id="Kd" role="2Oq$k0">
              <node concept="2OqwBi" id="Kf" role="2Oq$k0">
                <node concept="2OqwBi" id="Kh" role="2Oq$k0">
                  <node concept="2OqwBi" id="Kj" role="2Oq$k0">
                    <node concept="2OqwBi" id="Kl" role="2Oq$k0">
                      <node concept="2OqwBi" id="Kn" role="2Oq$k0">
                        <node concept="37vLTw" id="Kp" role="2Oq$k0">
                          <ref role="3cqZAo" node="Jr" resolve="b" />
                        </node>
                        <node concept="liA8E" id="Kq" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long)" resolve="aggregate" />
                          <node concept="Xl_RD" id="Kr" role="37wK5m">
                            <property role="Xl_RC" value="responses" />
                          </node>
                          <node concept="1adDum" id="Ks" role="37wK5m">
                            <property role="1adDun" value="0x43ca7e69529a23c8L" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="Ko" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long)" resolve="target" />
                        <node concept="1adDum" id="Kt" role="37wK5m">
                          <property role="1adDun" value="0x29d429a205314919L" />
                        </node>
                        <node concept="1adDum" id="Ku" role="37wK5m">
                          <property role="1adDun" value="0x8e806706b89fb3b7L" />
                        </node>
                        <node concept="1adDum" id="Kv" role="37wK5m">
                          <property role="1adDun" value="0x43ca7e69529a23cbL" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="Km" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean)" resolve="optional" />
                      <node concept="3clFbT" id="Kw" role="37wK5m" />
                    </node>
                  </node>
                  <node concept="liA8E" id="Kk" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean)" resolve="ordered" />
                    <node concept="3clFbT" id="Kx" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="Ki" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean)" resolve="multiple" />
                  <node concept="3clFbT" id="Ky" role="37wK5m">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="Kg" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="Kz" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659223496" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="Ke" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="Jq" role="3cqZAp">
          <node concept="2OqwBi" id="K$" role="3cqZAk">
            <node concept="37vLTw" id="K_" role="2Oq$k0">
              <ref role="3cqZAo" node="Jr" resolve="b" />
            </node>
            <node concept="liA8E" id="KA" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="Jh" role="1B3o_S" />
      <node concept="3uibUv" id="Ji" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="p1" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForPathObject" />
      <node concept="3clFbS" id="KB" role="3clF47">
        <node concept="3cpWs8" id="KE" role="3cqZAp">
          <node concept="3cpWsn" id="KL" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="KM" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="KN" role="33vP2m">
              <node concept="1pGfFk" id="KO" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="KP" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="KQ" role="37wK5m">
                  <property role="Xl_RC" value="PathObject" />
                </node>
                <node concept="1adDum" id="KR" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="KS" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="KT" role="37wK5m">
                  <property role="1adDun" value="0x3bfc2252a1cc33c4L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="KF" role="3cqZAp">
          <node concept="2OqwBi" id="KU" role="3clFbG">
            <node concept="37vLTw" id="KV" role="2Oq$k0">
              <ref role="3cqZAo" node="KL" resolve="b" />
            </node>
            <node concept="liA8E" id="KW" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="KX" role="37wK5m" />
              <node concept="3clFbT" id="KY" role="37wK5m" />
              <node concept="3clFbT" id="KZ" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="KG" role="3cqZAp">
          <node concept="2OqwBi" id="L0" role="3clFbG">
            <node concept="37vLTw" id="L1" role="2Oq$k0">
              <ref role="3cqZAo" node="KL" resolve="b" />
            </node>
            <node concept="liA8E" id="L2" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="L3" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4322367480666010564" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="KH" role="3cqZAp">
          <node concept="2OqwBi" id="L4" role="3clFbG">
            <node concept="37vLTw" id="L5" role="2Oq$k0">
              <ref role="3cqZAo" node="KL" resolve="b" />
            </node>
            <node concept="liA8E" id="L6" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="L7" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="KI" role="3cqZAp">
          <node concept="2OqwBi" id="L8" role="3clFbG">
            <node concept="2OqwBi" id="L9" role="2Oq$k0">
              <node concept="2OqwBi" id="Lb" role="2Oq$k0">
                <node concept="2OqwBi" id="Ld" role="2Oq$k0">
                  <node concept="37vLTw" id="Lf" role="2Oq$k0">
                    <ref role="3cqZAo" node="KL" resolve="b" />
                  </node>
                  <node concept="liA8E" id="Lg" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="Lh" role="37wK5m">
                      <property role="Xl_RC" value="path" />
                    </node>
                    <node concept="1adDum" id="Li" role="37wK5m">
                      <property role="1adDun" value="0x3bfc2252a1cc33cbL" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="Le" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="2YIFZM" id="Lj" role="37wK5m">
                    <ref role="37wK5l" to="e8bb:~MetaIdFactory.dataTypeId(long,long,long)" resolve="dataTypeId" />
                    <ref role="1Pybhc" to="e8bb:~MetaIdFactory" resolve="MetaIdFactory" />
                    <uo k="s:originTrace" v="n:4322367480666010573" />
                    <node concept="1adDum" id="Lk" role="37wK5m">
                      <property role="1adDun" value="0x29d429a205314919L" />
                      <uo k="s:originTrace" v="n:4322367480666010573" />
                    </node>
                    <node concept="1adDum" id="Ll" role="37wK5m">
                      <property role="1adDun" value="0x8e806706b89fb3b7L" />
                      <uo k="s:originTrace" v="n:4322367480666010573" />
                    </node>
                    <node concept="1adDum" id="Lm" role="37wK5m">
                      <property role="1adDun" value="0x3bfc2252a1cc33cdL" />
                      <uo k="s:originTrace" v="n:4322367480666010573" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="Lc" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="Ln" role="37wK5m">
                  <property role="Xl_RC" value="4322367480666010571" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="La" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="KJ" role="3cqZAp">
          <node concept="2OqwBi" id="Lo" role="3clFbG">
            <node concept="2OqwBi" id="Lp" role="2Oq$k0">
              <node concept="2OqwBi" id="Lr" role="2Oq$k0">
                <node concept="2OqwBi" id="Lt" role="2Oq$k0">
                  <node concept="2OqwBi" id="Lv" role="2Oq$k0">
                    <node concept="2OqwBi" id="Lx" role="2Oq$k0">
                      <node concept="2OqwBi" id="Lz" role="2Oq$k0">
                        <node concept="37vLTw" id="L_" role="2Oq$k0">
                          <ref role="3cqZAo" node="KL" resolve="b" />
                        </node>
                        <node concept="liA8E" id="LA" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long)" resolve="aggregate" />
                          <node concept="Xl_RD" id="LB" role="37wK5m">
                            <property role="Xl_RC" value="operation" />
                          </node>
                          <node concept="1adDum" id="LC" role="37wK5m">
                            <property role="1adDun" value="0x3bfc2252a1cc33ceL" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="L$" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long)" resolve="target" />
                        <node concept="1adDum" id="LD" role="37wK5m">
                          <property role="1adDun" value="0x29d429a205314919L" />
                        </node>
                        <node concept="1adDum" id="LE" role="37wK5m">
                          <property role="1adDun" value="0x8e806706b89fb3b7L" />
                        </node>
                        <node concept="1adDum" id="LF" role="37wK5m">
                          <property role="1adDun" value="0x3bfc2252a1cc33d0L" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="Ly" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean)" resolve="optional" />
                      <node concept="3clFbT" id="LG" role="37wK5m" />
                    </node>
                  </node>
                  <node concept="liA8E" id="Lw" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean)" resolve="ordered" />
                    <node concept="3clFbT" id="LH" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="Lu" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean)" resolve="multiple" />
                  <node concept="3clFbT" id="LI" role="37wK5m">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="Ls" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="LJ" role="37wK5m">
                  <property role="Xl_RC" value="4322367480666010574" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="Lq" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="KK" role="3cqZAp">
          <node concept="2OqwBi" id="LK" role="3cqZAk">
            <node concept="37vLTw" id="LL" role="2Oq$k0">
              <ref role="3cqZAo" node="KL" resolve="b" />
            </node>
            <node concept="liA8E" id="LM" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="KC" role="1B3o_S" />
      <node concept="3uibUv" id="KD" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="p2" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForPostOperationObject" />
      <node concept="3clFbS" id="LN" role="3clF47">
        <node concept="3cpWs8" id="LQ" role="3cqZAp">
          <node concept="3cpWsn" id="LX" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="LY" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="LZ" role="33vP2m">
              <node concept="1pGfFk" id="M0" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="M1" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="M2" role="37wK5m">
                  <property role="Xl_RC" value="PostOperationObject" />
                </node>
                <node concept="1adDum" id="M3" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="M4" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="M5" role="37wK5m">
                  <property role="1adDun" value="0x43ca7e69529d504fL" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="LR" role="3cqZAp">
          <node concept="2OqwBi" id="M6" role="3clFbG">
            <node concept="37vLTw" id="M7" role="2Oq$k0">
              <ref role="3cqZAo" node="LX" resolve="b" />
            </node>
            <node concept="liA8E" id="M8" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="M9" role="37wK5m" />
              <node concept="3clFbT" id="Ma" role="37wK5m" />
              <node concept="3clFbT" id="Mb" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="LS" role="3cqZAp">
          <node concept="2OqwBi" id="Mc" role="3clFbG">
            <node concept="37vLTw" id="Md" role="2Oq$k0">
              <ref role="3cqZAo" node="LX" resolve="b" />
            </node>
            <node concept="liA8E" id="Me" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.super_(java.lang.String,long,long,long)" resolve="super_" />
              <node concept="Xl_RD" id="Mf" role="37wK5m">
                <property role="Xl_RC" value="de.thb.openapidsl.copy.structure.OperationObject" />
              </node>
              <node concept="1adDum" id="Mg" role="37wK5m">
                <property role="1adDun" value="0x29d429a205314919L" />
              </node>
              <node concept="1adDum" id="Mh" role="37wK5m">
                <property role="1adDun" value="0x8e806706b89fb3b7L" />
              </node>
              <node concept="1adDum" id="Mi" role="37wK5m">
                <property role="1adDun" value="0x3bfc2252a1cc33d0L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="LT" role="3cqZAp">
          <node concept="2OqwBi" id="Mj" role="3clFbG">
            <node concept="37vLTw" id="Mk" role="2Oq$k0">
              <ref role="3cqZAo" node="LX" resolve="b" />
            </node>
            <node concept="liA8E" id="Ml" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="Mm" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4884855736659431503" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="LU" role="3cqZAp">
          <node concept="2OqwBi" id="Mn" role="3clFbG">
            <node concept="37vLTw" id="Mo" role="2Oq$k0">
              <ref role="3cqZAo" node="LX" resolve="b" />
            </node>
            <node concept="liA8E" id="Mp" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="Mq" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="LV" role="3cqZAp">
          <node concept="2OqwBi" id="Mr" role="3clFbG">
            <node concept="37vLTw" id="Ms" role="2Oq$k0">
              <ref role="3cqZAo" node="LX" resolve="b" />
            </node>
            <node concept="liA8E" id="Mt" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.alias(java.lang.String)" resolve="alias" />
              <node concept="Xl_RD" id="Mu" role="37wK5m">
                <property role="Xl_RC" value="post" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="LW" role="3cqZAp">
          <node concept="2OqwBi" id="Mv" role="3cqZAk">
            <node concept="37vLTw" id="Mw" role="2Oq$k0">
              <ref role="3cqZAo" node="LX" resolve="b" />
            </node>
            <node concept="liA8E" id="Mx" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="LO" role="1B3o_S" />
      <node concept="3uibUv" id="LP" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="p3" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForPutOperationObject" />
      <node concept="3clFbS" id="My" role="3clF47">
        <node concept="3cpWs8" id="M_" role="3cqZAp">
          <node concept="3cpWsn" id="MG" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="MH" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="MI" role="33vP2m">
              <node concept="1pGfFk" id="MJ" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="MK" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="ML" role="37wK5m">
                  <property role="Xl_RC" value="PutOperationObject" />
                </node>
                <node concept="1adDum" id="MM" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="MN" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="MO" role="37wK5m">
                  <property role="1adDun" value="0x43ca7e69529d504eL" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="MA" role="3cqZAp">
          <node concept="2OqwBi" id="MP" role="3clFbG">
            <node concept="37vLTw" id="MQ" role="2Oq$k0">
              <ref role="3cqZAo" node="MG" resolve="b" />
            </node>
            <node concept="liA8E" id="MR" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="MS" role="37wK5m" />
              <node concept="3clFbT" id="MT" role="37wK5m" />
              <node concept="3clFbT" id="MU" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="MB" role="3cqZAp">
          <node concept="2OqwBi" id="MV" role="3clFbG">
            <node concept="37vLTw" id="MW" role="2Oq$k0">
              <ref role="3cqZAo" node="MG" resolve="b" />
            </node>
            <node concept="liA8E" id="MX" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.super_(java.lang.String,long,long,long)" resolve="super_" />
              <node concept="Xl_RD" id="MY" role="37wK5m">
                <property role="Xl_RC" value="de.thb.openapidsl.copy.structure.OperationObject" />
              </node>
              <node concept="1adDum" id="MZ" role="37wK5m">
                <property role="1adDun" value="0x29d429a205314919L" />
              </node>
              <node concept="1adDum" id="N0" role="37wK5m">
                <property role="1adDun" value="0x8e806706b89fb3b7L" />
              </node>
              <node concept="1adDum" id="N1" role="37wK5m">
                <property role="1adDun" value="0x3bfc2252a1cc33d0L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="MC" role="3cqZAp">
          <node concept="2OqwBi" id="N2" role="3clFbG">
            <node concept="37vLTw" id="N3" role="2Oq$k0">
              <ref role="3cqZAo" node="MG" resolve="b" />
            </node>
            <node concept="liA8E" id="N4" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="N5" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4884855736659431502" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="MD" role="3cqZAp">
          <node concept="2OqwBi" id="N6" role="3clFbG">
            <node concept="37vLTw" id="N7" role="2Oq$k0">
              <ref role="3cqZAo" node="MG" resolve="b" />
            </node>
            <node concept="liA8E" id="N8" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="N9" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ME" role="3cqZAp">
          <node concept="2OqwBi" id="Na" role="3clFbG">
            <node concept="37vLTw" id="Nb" role="2Oq$k0">
              <ref role="3cqZAo" node="MG" resolve="b" />
            </node>
            <node concept="liA8E" id="Nc" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.alias(java.lang.String)" resolve="alias" />
              <node concept="Xl_RD" id="Nd" role="37wK5m">
                <property role="Xl_RC" value="put" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="MF" role="3cqZAp">
          <node concept="2OqwBi" id="Ne" role="3cqZAk">
            <node concept="37vLTw" id="Nf" role="2Oq$k0">
              <ref role="3cqZAo" node="MG" resolve="b" />
            </node>
            <node concept="liA8E" id="Ng" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="Mz" role="1B3o_S" />
      <node concept="3uibUv" id="M$" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="p4" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForResponse" />
      <node concept="3clFbS" id="Nh" role="3clF47">
        <node concept="3cpWs8" id="Nk" role="3cqZAp">
          <node concept="3cpWsn" id="Ns" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="Nt" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="Nu" role="33vP2m">
              <node concept="1pGfFk" id="Nv" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="Nw" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="Nx" role="37wK5m">
                  <property role="Xl_RC" value="Response" />
                </node>
                <node concept="1adDum" id="Ny" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="Nz" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="N$" role="37wK5m">
                  <property role="1adDun" value="0x43ca7e69529a23cbL" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="Nl" role="3cqZAp">
          <node concept="2OqwBi" id="N_" role="3clFbG">
            <node concept="37vLTw" id="NA" role="2Oq$k0">
              <ref role="3cqZAo" node="Ns" resolve="b" />
            </node>
            <node concept="liA8E" id="NB" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="NC" role="37wK5m" />
              <node concept="3clFbT" id="ND" role="37wK5m" />
              <node concept="3clFbT" id="NE" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="Nm" role="3cqZAp">
          <node concept="2OqwBi" id="NF" role="3clFbG">
            <node concept="37vLTw" id="NG" role="2Oq$k0">
              <ref role="3cqZAo" node="Ns" resolve="b" />
            </node>
            <node concept="liA8E" id="NH" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="NI" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4884855736659223499" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="Nn" role="3cqZAp">
          <node concept="2OqwBi" id="NJ" role="3clFbG">
            <node concept="37vLTw" id="NK" role="2Oq$k0">
              <ref role="3cqZAo" node="Ns" resolve="b" />
            </node>
            <node concept="liA8E" id="NL" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="NM" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="No" role="3cqZAp">
          <node concept="2OqwBi" id="NN" role="3clFbG">
            <node concept="2OqwBi" id="NO" role="2Oq$k0">
              <node concept="2OqwBi" id="NQ" role="2Oq$k0">
                <node concept="2OqwBi" id="NS" role="2Oq$k0">
                  <node concept="37vLTw" id="NU" role="2Oq$k0">
                    <ref role="3cqZAo" node="Ns" resolve="b" />
                  </node>
                  <node concept="liA8E" id="NV" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="NW" role="37wK5m">
                      <property role="Xl_RC" value="httpCode" />
                    </node>
                    <node concept="1adDum" id="NX" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529a23ccL" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="NT" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="NY" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.INTEGER" resolve="INTEGER" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="NR" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="NZ" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659223500" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="NP" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="Np" role="3cqZAp">
          <node concept="2OqwBi" id="O0" role="3clFbG">
            <node concept="2OqwBi" id="O1" role="2Oq$k0">
              <node concept="2OqwBi" id="O3" role="2Oq$k0">
                <node concept="2OqwBi" id="O5" role="2Oq$k0">
                  <node concept="37vLTw" id="O7" role="2Oq$k0">
                    <ref role="3cqZAo" node="Ns" resolve="b" />
                  </node>
                  <node concept="liA8E" id="O8" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="O9" role="37wK5m">
                      <property role="Xl_RC" value="description" />
                    </node>
                    <node concept="1adDum" id="Oa" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529a23ceL" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="O6" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="Ob" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.STRING" resolve="STRING" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="O4" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="Oc" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659223502" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="O2" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="Nq" role="3cqZAp">
          <node concept="2OqwBi" id="Od" role="3clFbG">
            <node concept="2OqwBi" id="Oe" role="2Oq$k0">
              <node concept="2OqwBi" id="Og" role="2Oq$k0">
                <node concept="2OqwBi" id="Oi" role="2Oq$k0">
                  <node concept="2OqwBi" id="Ok" role="2Oq$k0">
                    <node concept="2OqwBi" id="Om" role="2Oq$k0">
                      <node concept="2OqwBi" id="Oo" role="2Oq$k0">
                        <node concept="37vLTw" id="Oq" role="2Oq$k0">
                          <ref role="3cqZAo" node="Ns" resolve="b" />
                        </node>
                        <node concept="liA8E" id="Or" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long)" resolve="aggregate" />
                          <node concept="Xl_RD" id="Os" role="37wK5m">
                            <property role="Xl_RC" value="content" />
                          </node>
                          <node concept="1adDum" id="Ot" role="37wK5m">
                            <property role="1adDun" value="0x43ca7e69529e5174L" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="Op" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long)" resolve="target" />
                        <node concept="1adDum" id="Ou" role="37wK5m">
                          <property role="1adDun" value="0x29d429a205314919L" />
                        </node>
                        <node concept="1adDum" id="Ov" role="37wK5m">
                          <property role="1adDun" value="0x8e806706b89fb3b7L" />
                        </node>
                        <node concept="1adDum" id="Ow" role="37wK5m">
                          <property role="1adDun" value="0x43ca7e69529e41f0L" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="On" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean)" resolve="optional" />
                      <node concept="3clFbT" id="Ox" role="37wK5m">
                        <property role="3clFbU" value="true" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="Ol" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean)" resolve="ordered" />
                    <node concept="3clFbT" id="Oy" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="Oj" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean)" resolve="multiple" />
                  <node concept="3clFbT" id="Oz" role="37wK5m">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="Oh" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="O$" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659497332" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="Of" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="Nr" role="3cqZAp">
          <node concept="2OqwBi" id="O_" role="3cqZAk">
            <node concept="37vLTw" id="OA" role="2Oq$k0">
              <ref role="3cqZAo" node="Ns" resolve="b" />
            </node>
            <node concept="liA8E" id="OB" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="Ni" role="1B3o_S" />
      <node concept="3uibUv" id="Nj" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="p5" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForSchema" />
      <node concept="3clFbS" id="OC" role="3clF47">
        <node concept="3cpWs8" id="OF" role="3cqZAp">
          <node concept="3cpWsn" id="OO" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="OP" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="OQ" role="33vP2m">
              <node concept="1pGfFk" id="OR" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="OS" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="OT" role="37wK5m">
                  <property role="Xl_RC" value="Schema" />
                </node>
                <node concept="1adDum" id="OU" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="OV" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="OW" role="37wK5m">
                  <property role="1adDun" value="0x43ca7e69529f3c94L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="OG" role="3cqZAp">
          <node concept="2OqwBi" id="OX" role="3clFbG">
            <node concept="37vLTw" id="OY" role="2Oq$k0">
              <ref role="3cqZAo" node="OO" resolve="b" />
            </node>
            <node concept="liA8E" id="OZ" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="P0" role="37wK5m" />
              <node concept="3clFbT" id="P1" role="37wK5m">
                <property role="3clFbU" value="true" />
              </node>
              <node concept="3clFbT" id="P2" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="OH" role="3cqZAp">
          <node concept="2OqwBi" id="P3" role="3clFbG">
            <node concept="37vLTw" id="P4" role="2Oq$k0">
              <ref role="3cqZAo" node="OO" resolve="b" />
            </node>
            <node concept="liA8E" id="P5" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="P6" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4884855736659557524" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="OI" role="3cqZAp">
          <node concept="2OqwBi" id="P7" role="3clFbG">
            <node concept="37vLTw" id="P8" role="2Oq$k0">
              <ref role="3cqZAo" node="OO" resolve="b" />
            </node>
            <node concept="liA8E" id="P9" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="Pa" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="OJ" role="3cqZAp">
          <node concept="2OqwBi" id="Pb" role="3clFbG">
            <node concept="2OqwBi" id="Pc" role="2Oq$k0">
              <node concept="2OqwBi" id="Pe" role="2Oq$k0">
                <node concept="2OqwBi" id="Pg" role="2Oq$k0">
                  <node concept="37vLTw" id="Pi" role="2Oq$k0">
                    <ref role="3cqZAo" node="OO" resolve="b" />
                  </node>
                  <node concept="liA8E" id="Pj" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="Pk" role="37wK5m">
                      <property role="Xl_RC" value="name" />
                    </node>
                    <node concept="1adDum" id="Pl" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529f3cb3L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="Ph" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="Pm" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.STRING" resolve="STRING" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="Pf" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="Pn" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659557555" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="Pd" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="OK" role="3cqZAp">
          <node concept="2OqwBi" id="Po" role="3clFbG">
            <node concept="2OqwBi" id="Pp" role="2Oq$k0">
              <node concept="2OqwBi" id="Pr" role="2Oq$k0">
                <node concept="2OqwBi" id="Pt" role="2Oq$k0">
                  <node concept="37vLTw" id="Pv" role="2Oq$k0">
                    <ref role="3cqZAo" node="OO" resolve="b" />
                  </node>
                  <node concept="liA8E" id="Pw" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="Px" role="37wK5m">
                      <property role="Xl_RC" value="description" />
                    </node>
                    <node concept="1adDum" id="Py" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529f3cadL" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="Pu" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="Pz" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.STRING" resolve="STRING" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="Ps" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="P$" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659557549" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="Pq" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="OL" role="3cqZAp">
          <node concept="2OqwBi" id="P_" role="3clFbG">
            <node concept="2OqwBi" id="PA" role="2Oq$k0">
              <node concept="2OqwBi" id="PC" role="2Oq$k0">
                <node concept="2OqwBi" id="PE" role="2Oq$k0">
                  <node concept="37vLTw" id="PG" role="2Oq$k0">
                    <ref role="3cqZAo" node="OO" resolve="b" />
                  </node>
                  <node concept="liA8E" id="PH" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="PI" role="37wK5m">
                      <property role="Xl_RC" value="example" />
                    </node>
                    <node concept="1adDum" id="PJ" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529f3cafL" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="PF" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="PK" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.STRING" resolve="STRING" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="PD" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="PL" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659557551" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="PB" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="OM" role="3cqZAp">
          <node concept="2OqwBi" id="PM" role="3clFbG">
            <node concept="37vLTw" id="PN" role="2Oq$k0">
              <ref role="3cqZAo" node="OO" resolve="b" />
            </node>
            <node concept="liA8E" id="PO" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.alias(java.lang.String)" resolve="alias" />
              <node concept="Xl_RD" id="PP" role="37wK5m">
                <property role="Xl_RC" value="schema" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="ON" role="3cqZAp">
          <node concept="2OqwBi" id="PQ" role="3cqZAk">
            <node concept="37vLTw" id="PR" role="2Oq$k0">
              <ref role="3cqZAo" node="OO" resolve="b" />
            </node>
            <node concept="liA8E" id="PS" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="OD" role="1B3o_S" />
      <node concept="3uibUv" id="OE" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="p6" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForSchemaRef" />
      <node concept="3clFbS" id="PT" role="3clF47">
        <node concept="3cpWs8" id="PW" role="3cqZAp">
          <node concept="3cpWsn" id="Q2" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="Q3" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="Q4" role="33vP2m">
              <node concept="1pGfFk" id="Q5" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="Q6" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="Q7" role="37wK5m">
                  <property role="Xl_RC" value="SchemaRef" />
                </node>
                <node concept="1adDum" id="Q8" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="Q9" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="Qa" role="37wK5m">
                  <property role="1adDun" value="0x3bdca882929f537fL" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="PX" role="3cqZAp">
          <node concept="2OqwBi" id="Qb" role="3clFbG">
            <node concept="37vLTw" id="Qc" role="2Oq$k0">
              <ref role="3cqZAo" node="Q2" resolve="b" />
            </node>
            <node concept="liA8E" id="Qd" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="Qe" role="37wK5m" />
              <node concept="3clFbT" id="Qf" role="37wK5m" />
              <node concept="3clFbT" id="Qg" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="PY" role="3cqZAp">
          <node concept="2OqwBi" id="Qh" role="3clFbG">
            <node concept="37vLTw" id="Qi" role="2Oq$k0">
              <ref role="3cqZAo" node="Q2" resolve="b" />
            </node>
            <node concept="liA8E" id="Qj" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="Qk" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4313507821873222527" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="PZ" role="3cqZAp">
          <node concept="2OqwBi" id="Ql" role="3clFbG">
            <node concept="37vLTw" id="Qm" role="2Oq$k0">
              <ref role="3cqZAo" node="Q2" resolve="b" />
            </node>
            <node concept="liA8E" id="Qn" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="Qo" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="Q0" role="3cqZAp">
          <node concept="2OqwBi" id="Qp" role="3clFbG">
            <node concept="2OqwBi" id="Qq" role="2Oq$k0">
              <node concept="2OqwBi" id="Qs" role="2Oq$k0">
                <node concept="2OqwBi" id="Qu" role="2Oq$k0">
                  <node concept="2OqwBi" id="Qw" role="2Oq$k0">
                    <node concept="37vLTw" id="Qy" role="2Oq$k0">
                      <ref role="3cqZAo" node="Q2" resolve="b" />
                    </node>
                    <node concept="liA8E" id="Qz" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.associate(java.lang.String,long)" resolve="associate" />
                      <node concept="Xl_RD" id="Q$" role="37wK5m">
                        <property role="Xl_RC" value="schema" />
                      </node>
                      <node concept="1adDum" id="Q_" role="37wK5m">
                        <property role="1adDun" value="0x3bdca882929f5380L" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="Qx" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.target(long,long,long)" resolve="target" />
                    <node concept="1adDum" id="QA" role="37wK5m">
                      <property role="1adDun" value="0x29d429a205314919L" />
                    </node>
                    <node concept="1adDum" id="QB" role="37wK5m">
                      <property role="1adDun" value="0x8e806706b89fb3b7L" />
                    </node>
                    <node concept="1adDum" id="QC" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529f3c94L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="Qv" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.optional(boolean)" resolve="optional" />
                  <node concept="3clFbT" id="QD" role="37wK5m" />
                </node>
              </node>
              <node concept="liA8E" id="Qt" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="QE" role="37wK5m">
                  <property role="Xl_RC" value="4313507821873222528" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="Qr" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="Q1" role="3cqZAp">
          <node concept="2OqwBi" id="QF" role="3cqZAk">
            <node concept="37vLTw" id="QG" role="2Oq$k0">
              <ref role="3cqZAo" node="Q2" resolve="b" />
            </node>
            <node concept="liA8E" id="QH" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="PU" role="1B3o_S" />
      <node concept="3uibUv" id="PV" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="p7" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForStringSchema" />
      <node concept="3clFbS" id="QI" role="3clF47">
        <node concept="3cpWs8" id="QL" role="3cqZAp">
          <node concept="3cpWsn" id="QW" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="QX" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="QY" role="33vP2m">
              <node concept="1pGfFk" id="QZ" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="R0" role="37wK5m">
                  <property role="Xl_RC" value="de.thb.openapidsl.copy" />
                </node>
                <node concept="Xl_RD" id="R1" role="37wK5m">
                  <property role="Xl_RC" value="StringSchema" />
                </node>
                <node concept="1adDum" id="R2" role="37wK5m">
                  <property role="1adDun" value="0x29d429a205314919L" />
                </node>
                <node concept="1adDum" id="R3" role="37wK5m">
                  <property role="1adDun" value="0x8e806706b89fb3b7L" />
                </node>
                <node concept="1adDum" id="R4" role="37wK5m">
                  <property role="1adDun" value="0x43ca7e69529f3cb2L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="QM" role="3cqZAp">
          <node concept="2OqwBi" id="R5" role="3clFbG">
            <node concept="37vLTw" id="R6" role="2Oq$k0">
              <ref role="3cqZAo" node="QW" resolve="b" />
            </node>
            <node concept="liA8E" id="R7" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean)" resolve="class_" />
              <node concept="3clFbT" id="R8" role="37wK5m" />
              <node concept="3clFbT" id="R9" role="37wK5m" />
              <node concept="3clFbT" id="Ra" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="QN" role="3cqZAp">
          <node concept="2OqwBi" id="Rb" role="3clFbG">
            <node concept="37vLTw" id="Rc" role="2Oq$k0">
              <ref role="3cqZAo" node="QW" resolve="b" />
            </node>
            <node concept="liA8E" id="Rd" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.super_(java.lang.String,long,long,long)" resolve="super_" />
              <node concept="Xl_RD" id="Re" role="37wK5m">
                <property role="Xl_RC" value="de.thb.openapidsl.copy.structure.Schema" />
              </node>
              <node concept="1adDum" id="Rf" role="37wK5m">
                <property role="1adDun" value="0x29d429a205314919L" />
              </node>
              <node concept="1adDum" id="Rg" role="37wK5m">
                <property role="1adDun" value="0x8e806706b89fb3b7L" />
              </node>
              <node concept="1adDum" id="Rh" role="37wK5m">
                <property role="1adDun" value="0x43ca7e69529f3c94L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="QO" role="3cqZAp">
          <node concept="2OqwBi" id="Ri" role="3clFbG">
            <node concept="37vLTw" id="Rj" role="2Oq$k0">
              <ref role="3cqZAo" node="QW" resolve="b" />
            </node>
            <node concept="liA8E" id="Rk" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String)" resolve="origin" />
              <node concept="Xl_RD" id="Rl" role="37wK5m">
                <property role="Xl_RC" value="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)/4884855736659557554" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="QP" role="3cqZAp">
          <node concept="2OqwBi" id="Rm" role="3clFbG">
            <node concept="37vLTw" id="Rn" role="2Oq$k0">
              <ref role="3cqZAo" node="QW" resolve="b" />
            </node>
            <node concept="liA8E" id="Ro" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.version(int)" resolve="version" />
              <node concept="3cmrfG" id="Rp" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="QQ" role="3cqZAp">
          <node concept="2OqwBi" id="Rq" role="3clFbG">
            <node concept="2OqwBi" id="Rr" role="2Oq$k0">
              <node concept="2OqwBi" id="Rt" role="2Oq$k0">
                <node concept="2OqwBi" id="Rv" role="2Oq$k0">
                  <node concept="37vLTw" id="Rx" role="2Oq$k0">
                    <ref role="3cqZAo" node="QW" resolve="b" />
                  </node>
                  <node concept="liA8E" id="Ry" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="Rz" role="37wK5m">
                      <property role="Xl_RC" value="minLength" />
                    </node>
                    <node concept="1adDum" id="R$" role="37wK5m">
                      <property role="1adDun" value="0x3bdca88292b8c2efL" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="Rw" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="R_" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.INTEGER" resolve="INTEGER" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="Ru" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="RA" role="37wK5m">
                  <property role="Xl_RC" value="4313507821874889455" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="Rs" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="QR" role="3cqZAp">
          <node concept="2OqwBi" id="RB" role="3clFbG">
            <node concept="2OqwBi" id="RC" role="2Oq$k0">
              <node concept="2OqwBi" id="RE" role="2Oq$k0">
                <node concept="2OqwBi" id="RG" role="2Oq$k0">
                  <node concept="37vLTw" id="RI" role="2Oq$k0">
                    <ref role="3cqZAo" node="QW" resolve="b" />
                  </node>
                  <node concept="liA8E" id="RJ" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="RK" role="37wK5m">
                      <property role="Xl_RC" value="maxLength" />
                    </node>
                    <node concept="1adDum" id="RL" role="37wK5m">
                      <property role="1adDun" value="0x3bdca88292b8c2f3L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="RH" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="RM" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.INTEGER" resolve="INTEGER" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="RF" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="RN" role="37wK5m">
                  <property role="Xl_RC" value="4313507821874889459" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="RD" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="QS" role="3cqZAp">
          <node concept="2OqwBi" id="RO" role="3clFbG">
            <node concept="2OqwBi" id="RP" role="2Oq$k0">
              <node concept="2OqwBi" id="RR" role="2Oq$k0">
                <node concept="2OqwBi" id="RT" role="2Oq$k0">
                  <node concept="37vLTw" id="RV" role="2Oq$k0">
                    <ref role="3cqZAo" node="QW" resolve="b" />
                  </node>
                  <node concept="liA8E" id="RW" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="RX" role="37wK5m">
                      <property role="Xl_RC" value="pattern" />
                    </node>
                    <node concept="1adDum" id="RY" role="37wK5m">
                      <property role="1adDun" value="0x43ca7e69529f3e08L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="RU" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="RZ" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.STRING" resolve="STRING" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="RS" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="S0" role="37wK5m">
                  <property role="Xl_RC" value="4884855736659557896" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="RQ" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="QT" role="3cqZAp">
          <node concept="2OqwBi" id="S1" role="3clFbG">
            <node concept="2OqwBi" id="S2" role="2Oq$k0">
              <node concept="2OqwBi" id="S4" role="2Oq$k0">
                <node concept="2OqwBi" id="S6" role="2Oq$k0">
                  <node concept="37vLTw" id="S8" role="2Oq$k0">
                    <ref role="3cqZAo" node="QW" resolve="b" />
                  </node>
                  <node concept="liA8E" id="S9" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.property(java.lang.String,long)" resolve="property" />
                    <node concept="Xl_RD" id="Sa" role="37wK5m">
                      <property role="Xl_RC" value="format" />
                    </node>
                    <node concept="1adDum" id="Sb" role="37wK5m">
                      <property role="1adDun" value="0x3bdca882925f87d8L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="S7" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.type(jetbrains.mps.smodel.adapter.ids.STypeId)" resolve="type" />
                  <node concept="Rm8GO" id="Sc" role="37wK5m">
                    <ref role="Rm8GQ" to="e8bb:~PrimitiveTypeId.STRING" resolve="STRING" />
                    <ref role="1Px2BO" to="e8bb:~PrimitiveTypeId" resolve="PrimitiveTypeId" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="S5" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.origin(java.lang.String)" resolve="origin" />
                <node concept="Xl_RD" id="Sd" role="37wK5m">
                  <property role="Xl_RC" value="4313507821869041624" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="S3" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$PropertyBuilder.done()" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="QU" role="3cqZAp">
          <node concept="2OqwBi" id="Se" role="3clFbG">
            <node concept="37vLTw" id="Sf" role="2Oq$k0">
              <ref role="3cqZAo" node="QW" resolve="b" />
            </node>
            <node concept="liA8E" id="Sg" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.alias(java.lang.String)" resolve="alias" />
              <node concept="Xl_RD" id="Sh" role="37wK5m">
                <property role="Xl_RC" value="string" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="QV" role="3cqZAp">
          <node concept="2OqwBi" id="Si" role="3cqZAk">
            <node concept="37vLTw" id="Sj" role="2Oq$k0">
              <ref role="3cqZAo" node="QW" resolve="b" />
            </node>
            <node concept="liA8E" id="Sk" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create()" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="QJ" role="1B3o_S" />
      <node concept="3uibUv" id="QK" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
  </node>
</model>

