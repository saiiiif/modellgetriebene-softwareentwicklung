package de.thb.openapidsl.copy.structure;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.ConceptPresentationAspectBase;
import jetbrains.mps.smodel.runtime.ConceptPresentation;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.smodel.runtime.ConceptPresentationBuilder;

public class ConceptPresentationAspectImpl extends ConceptPresentationAspectBase {
  private ConceptPresentation props_AllOfSchema;
  private ConceptPresentation props_AnyOfSchema;
  private ConceptPresentation props_ArraySchema;
  private ConceptPresentation props_BooleanSchema;
  private ConceptPresentation props_Content;
  private ConceptPresentation props_DeleteOperationObject;
  private ConceptPresentation props_GetOperationObject;
  private ConceptPresentation props_IntegerSchema;
  private ConceptPresentation props_ObjectSchema;
  private ConceptPresentation props_ObjectSchemaAdditionalProperty;
  private ConceptPresentation props_ObjectSchemaProperty;
  private ConceptPresentation props_ObjectSchemaRequiredProperty;
  private ConceptPresentation props_OneOfSchema;
  private ConceptPresentation props_OpenApi;
  private ConceptPresentation props_OpenApiInfo;
  private ConceptPresentation props_OperationObject;
  private ConceptPresentation props_PathObject;
  private ConceptPresentation props_PostOperationObject;
  private ConceptPresentation props_PutOperationObject;
  private ConceptPresentation props_Response;
  private ConceptPresentation props_Schema;
  private ConceptPresentation props_SchemaRef;
  private ConceptPresentation props_StringSchema;

  @Override
  @Nullable
  public ConceptPresentation getDescriptor(SAbstractConcept c) {
    StructureAspectDescriptor structureDescriptor = (StructureAspectDescriptor) myLanguageRuntime.getAspect(jetbrains.mps.smodel.runtime.StructureAspectDescriptor.class);
    switch (structureDescriptor.internalIndex(c)) {
      case LanguageConceptSwitch.AllOfSchema:
        if (props_AllOfSchema == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("allOf");
          props_AllOfSchema = cpb.create();
        }
        return props_AllOfSchema;
      case LanguageConceptSwitch.AnyOfSchema:
        if (props_AnyOfSchema == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("anyOf");
          props_AnyOfSchema = cpb.create();
        }
        return props_AnyOfSchema;
      case LanguageConceptSwitch.ArraySchema:
        if (props_ArraySchema == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("array");
          props_ArraySchema = cpb.create();
        }
        return props_ArraySchema;
      case LanguageConceptSwitch.BooleanSchema:
        if (props_BooleanSchema == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("boolean");
          props_BooleanSchema = cpb.create();
        }
        return props_BooleanSchema;
      case LanguageConceptSwitch.Content:
        if (props_Content == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("content");
          props_Content = cpb.create();
        }
        return props_Content;
      case LanguageConceptSwitch.DeleteOperationObject:
        if (props_DeleteOperationObject == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("delete");
          props_DeleteOperationObject = cpb.create();
        }
        return props_DeleteOperationObject;
      case LanguageConceptSwitch.GetOperationObject:
        if (props_GetOperationObject == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("get");
          props_GetOperationObject = cpb.create();
        }
        return props_GetOperationObject;
      case LanguageConceptSwitch.IntegerSchema:
        if (props_IntegerSchema == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("integer");
          props_IntegerSchema = cpb.create();
        }
        return props_IntegerSchema;
      case LanguageConceptSwitch.ObjectSchema:
        if (props_ObjectSchema == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("object");
          props_ObjectSchema = cpb.create();
        }
        return props_ObjectSchema;
      case LanguageConceptSwitch.ObjectSchemaAdditionalProperty:
        if (props_ObjectSchemaAdditionalProperty == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.presentationByReference(0x29d429a205314919L, 0x8e806706b89fb3b7L, 0x3bdca882925f8943L, 0x3bdca882925f8945L, "schema", "", "");
          props_ObjectSchemaAdditionalProperty = cpb.create();
        }
        return props_ObjectSchemaAdditionalProperty;
      case LanguageConceptSwitch.ObjectSchemaProperty:
        if (props_ObjectSchemaProperty == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.presentationByReference(0x29d429a205314919L, 0x8e806706b89fb3b7L, 0x43ca7e69529f3e31L, 0x43ca7e69529f3e34L, "schema", "", "");
          props_ObjectSchemaProperty = cpb.create();
        }
        return props_ObjectSchemaProperty;
      case LanguageConceptSwitch.ObjectSchemaRequiredProperty:
        if (props_ObjectSchemaRequiredProperty == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.presentationByReference(0x29d429a205314919L, 0x8e806706b89fb3b7L, 0x3bdca882927a4101L, 0x3bdca882927a4103L, "property", "", "");
          props_ObjectSchemaRequiredProperty = cpb.create();
        }
        return props_ObjectSchemaRequiredProperty;
      case LanguageConceptSwitch.OneOfSchema:
        if (props_OneOfSchema == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("oneOf");
          props_OneOfSchema = cpb.create();
        }
        return props_OneOfSchema;
      case LanguageConceptSwitch.OpenApi:
        if (props_OpenApi == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("OpenApi");
          props_OpenApi = cpb.create();
        }
        return props_OpenApi;
      case LanguageConceptSwitch.OpenApiInfo:
        if (props_OpenApiInfo == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.presentationByName();
          props_OpenApiInfo = cpb.create();
        }
        return props_OpenApiInfo;
      case LanguageConceptSwitch.OperationObject:
        if (props_OperationObject == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          props_OperationObject = cpb.create();
        }
        return props_OperationObject;
      case LanguageConceptSwitch.PathObject:
        if (props_PathObject == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("PathObject");
          props_PathObject = cpb.create();
        }
        return props_PathObject;
      case LanguageConceptSwitch.PostOperationObject:
        if (props_PostOperationObject == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("post");
          props_PostOperationObject = cpb.create();
        }
        return props_PostOperationObject;
      case LanguageConceptSwitch.PutOperationObject:
        if (props_PutOperationObject == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("put");
          props_PutOperationObject = cpb.create();
        }
        return props_PutOperationObject;
      case LanguageConceptSwitch.Response:
        if (props_Response == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("Response");
          props_Response = cpb.create();
        }
        return props_Response;
      case LanguageConceptSwitch.Schema:
        if (props_Schema == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          props_Schema = cpb.create();
        }
        return props_Schema;
      case LanguageConceptSwitch.SchemaRef:
        if (props_SchemaRef == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.presentationByReference(0x29d429a205314919L, 0x8e806706b89fb3b7L, 0x3bdca882929f537fL, 0x3bdca882929f5380L, "schema", "", "");
          props_SchemaRef = cpb.create();
        }
        return props_SchemaRef;
      case LanguageConceptSwitch.StringSchema:
        if (props_StringSchema == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("string");
          props_StringSchema = cpb.create();
        }
        return props_StringSchema;
    }
    return null;
  }
}
