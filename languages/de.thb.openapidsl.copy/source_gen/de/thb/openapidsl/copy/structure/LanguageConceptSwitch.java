package de.thb.openapidsl.copy.structure;

/*Generated by MPS */

import jetbrains.mps.lang.smodel.LanguageConceptIndex;
import jetbrains.mps.lang.smodel.LanguageConceptIndexBuilder;
import jetbrains.mps.smodel.adapter.ids.SConceptId;
import org.jetbrains.mps.openapi.language.SAbstractConcept;

public final class LanguageConceptSwitch {
  private final LanguageConceptIndex myIndex;
  public static final int AllOfSchema = 0;
  public static final int AnyOfSchema = 1;
  public static final int ArraySchema = 2;
  public static final int BooleanSchema = 3;
  public static final int Content = 4;
  public static final int DeleteOperationObject = 5;
  public static final int GetOperationObject = 6;
  public static final int IntegerSchema = 7;
  public static final int ObjectSchema = 8;
  public static final int ObjectSchemaAdditionalProperty = 9;
  public static final int ObjectSchemaProperty = 10;
  public static final int ObjectSchemaRequiredProperty = 11;
  public static final int OneOfSchema = 12;
  public static final int OpenApi = 13;
  public static final int OpenApiInfo = 14;
  public static final int OperationObject = 15;
  public static final int PathObject = 16;
  public static final int PostOperationObject = 17;
  public static final int PutOperationObject = 18;
  public static final int Response = 19;
  public static final int Schema = 20;
  public static final int SchemaRef = 21;
  public static final int StringSchema = 22;

  public LanguageConceptSwitch() {
    LanguageConceptIndexBuilder builder = new LanguageConceptIndexBuilder(0x29d429a205314919L, 0x8e806706b89fb3b7L);
    builder.put(0x3bdca882929c266cL, AllOfSchema);
    builder.put(0x3bdca882929c2684L, AnyOfSchema);
    builder.put(0x43ca7e69529f3e36L, ArraySchema);
    builder.put(0x3bdca882929aa6b5L, BooleanSchema);
    builder.put(0x43ca7e69529e41f0L, Content);
    builder.put(0x43ca7e69529d5050L, DeleteOperationObject);
    builder.put(0x3bfc2252a1cc3d60L, GetOperationObject);
    builder.put(0x43ca7e69529f3e0cL, IntegerSchema);
    builder.put(0x43ca7e69529f3e28L, ObjectSchema);
    builder.put(0x3bdca882925f8943L, ObjectSchemaAdditionalProperty);
    builder.put(0x43ca7e69529f3e31L, ObjectSchemaProperty);
    builder.put(0x3bdca882927a4101L, ObjectSchemaRequiredProperty);
    builder.put(0x3bdca882929c263eL, OneOfSchema);
    builder.put(0x3bfc2252a1cbc1eeL, OpenApi);
    builder.put(0x3bfc2252a1cab6b8L, OpenApiInfo);
    builder.put(0x3bfc2252a1cc33d0L, OperationObject);
    builder.put(0x3bfc2252a1cc33c4L, PathObject);
    builder.put(0x43ca7e69529d504fL, PostOperationObject);
    builder.put(0x43ca7e69529d504eL, PutOperationObject);
    builder.put(0x43ca7e69529a23cbL, Response);
    builder.put(0x43ca7e69529f3c94L, Schema);
    builder.put(0x3bdca882929f537fL, SchemaRef);
    builder.put(0x43ca7e69529f3cb2L, StringSchema);
    myIndex = builder.seal();
  }

  /*package*/ int index(SConceptId cid) {
    return myIndex.index(cid);
  }

  public int index(SAbstractConcept concept) {
    return myIndex.index(concept);
  }
}
