package de.thb.openapidsl.copy.textGen;

/*Generated by MPS */

import jetbrains.mps.text.rt.TextGenDescriptorBase;
import jetbrains.mps.text.rt.TextGenContext;
import jetbrains.mps.text.impl.TextGenSupport;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SPropertyOperations;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.internal.collections.runtime.ListSequence;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SLinkOperations;
import org.jetbrains.mps.openapi.language.SProperty;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import org.jetbrains.mps.openapi.language.SReferenceLink;
import org.jetbrains.mps.openapi.language.SContainmentLink;

public class OneOfSchema_TextGen extends TextGenDescriptorBase {
  @Override
  public void generateText(final TextGenContext ctx) {
    final TextGenSupport tgs = new TextGenSupport(ctx);
    tgs.indent();
    tgs.append(SPropertyOperations.getString(ctx.getPrimaryInput(), PROPS.name$4rU1));
    tgs.append(":");
    tgs.newLine();
    ctx.getBuffer().area().increaseIndent();
    if (isNotEmptyString(SPropertyOperations.getString(ctx.getPrimaryInput(), PROPS.description$4l$A))) {
      tgs.indent();
      tgs.append("description: ");
      tgs.append(SPropertyOperations.getString(ctx.getPrimaryInput(), PROPS.description$4l$A));
      tgs.newLine();
    }
    tgs.indent();
    tgs.append("oneOf:");
    tgs.newLine();
    for (SNode elem : ListSequence.fromList(SLinkOperations.getChildren(ctx.getPrimaryInput(), LINKS.oneOf$qBP4))) {
      ctx.getBuffer().area().increaseIndent();
      tgs.indent();
      tgs.append("- $ref: ");
      tgs.append("'#/components/schemas/");
      tgs.append(SPropertyOperations.getString(SLinkOperations.getTarget(elem, LINKS.schema$aML$), PROPS.name$4rU1));
      tgs.append("'");
      tgs.newLine();
      ctx.getBuffer().area().decreaseIndent();
    }
    if (ListSequence.fromList(SLinkOperations.getChildren(ctx.getPrimaryInput(), LINKS.oneOf$qBP4)).isEmpty()) {
      tgs.reportError("oneOf may not be empty");
    }

    ctx.getBuffer().area().decreaseIndent();
  }
  private static boolean isNotEmptyString(String str) {
    return str != null && str.length() > 0;
  }

  private static final class PROPS {
    /*package*/ static final SProperty name$4rU1 = MetaAdapterFactory.getProperty(0x29d429a205314919L, 0x8e806706b89fb3b7L, 0x43ca7e69529f3c94L, 0x43ca7e69529f3cb3L, "name");
    /*package*/ static final SProperty description$4l$A = MetaAdapterFactory.getProperty(0x29d429a205314919L, 0x8e806706b89fb3b7L, 0x43ca7e69529f3c94L, 0x43ca7e69529f3cadL, "description");
  }

  private static final class LINKS {
    /*package*/ static final SReferenceLink schema$aML$ = MetaAdapterFactory.getReferenceLink(0x29d429a205314919L, 0x8e806706b89fb3b7L, 0x3bdca882929f537fL, 0x3bdca882929f5380L, "schema");
    /*package*/ static final SContainmentLink oneOf$qBP4 = MetaAdapterFactory.getContainmentLink(0x29d429a205314919L, 0x8e806706b89fb3b7L, 0x3bdca882929c263eL, 0x3bdca882929c263fL, "oneOf");
  }
}
