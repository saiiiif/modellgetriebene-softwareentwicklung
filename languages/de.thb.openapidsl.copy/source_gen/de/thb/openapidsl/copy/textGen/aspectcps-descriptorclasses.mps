<?xml version="1.0" encoding="UTF-8"?>
<model ref="00000000-0000-4000-5f02-5beb5f025beb/i:f93dbc2(checkpoints/de.thb.openapidsl.copy.textGen@descriptorclasses)">
  <persistence version="9" />
  <attribute name="checkpoint" value="DescriptorClasses" />
  <attribute name="generation-plan" value="AspectCPS" />
  <attribute name="user-objects" value="true" />
  <languages />
  <imports>
    <import index="th05" ref="r:0fb7bff9-59f0-44cd-a233-8a32cc7ffb53(de.thb.openapidsl.copy.textGen)" />
    <import index="kpbf" ref="7124e466-fc92-4803-a656-d7a6b7eb3910/java:jetbrains.mps.text.impl(MPS.TextGen/)" />
    <import index="yfwt" ref="7124e466-fc92-4803-a656-d7a6b7eb3910/java:jetbrains.mps.text.rt(MPS.TextGen/)" />
    <import index="tpcf" ref="r:00000000-0000-4000-0000-011c89590293(jetbrains.mps.lang.structure.generator_new.baseLanguage@generator)" />
    <import index="ao3" ref="7124e466-fc92-4803-a656-d7a6b7eb3910/java:jetbrains.mps.text(MPS.TextGen/)" />
    <import index="mhfm" ref="3f233e7f-b8a6-46d2-a57f-795d56775243/java:org.jetbrains.annotations(Annotations/)" />
    <import index="mhbf" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.model(MPS.OpenAPI/)" />
    <import index="7thy" ref="r:92d51709-e744-4d16-805f-811ce196e715(de.thb.openapidsl.copy.structure)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="nn" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg" />
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <property id="1075300953594" name="abstractClass" index="1sVAO0" />
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271369338" name="jetbrains.mps.baseLanguage.structure.IsEmptyOperation" flags="nn" index="17RlXB" />
      <concept id="1225271408483" name="jetbrains.mps.baseLanguage.structure.IsNotEmptyOperation" flags="nn" index="17RvpY" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1144226303539" name="jetbrains.mps.baseLanguage.structure.ForeachStatement" flags="nn" index="1DcWWT">
        <child id="1144226360166" name="iterable" index="1DdaDG" />
      </concept>
      <concept id="1144230876926" name="jetbrains.mps.baseLanguage.structure.AbstractForStatement" flags="nn" index="1DupvO">
        <child id="1144230900587" name="variable" index="1Duv9x" />
      </concept>
      <concept id="1163668896201" name="jetbrains.mps.baseLanguage.structure.TernaryOperatorExpression" flags="nn" index="3K4zz7">
        <child id="1163668914799" name="condition" index="3K4Cdx" />
        <child id="1163668922816" name="ifTrue" index="3K4E3e" />
        <child id="1163668934364" name="ifFalse" index="3K4GZi" />
      </concept>
      <concept id="1163670490218" name="jetbrains.mps.baseLanguage.structure.SwitchStatement" flags="nn" index="3KaCP$">
        <child id="1163670766145" name="expression" index="3KbGdf" />
        <child id="1163670772911" name="case" index="3KbHQx" />
      </concept>
      <concept id="1163670641947" name="jetbrains.mps.baseLanguage.structure.SwitchCase" flags="ng" index="3KbdKl">
        <child id="1163670677455" name="expression" index="3Kbmr1" />
        <child id="1163670683720" name="body" index="3Kbo56" />
      </concept>
      <concept id="1082113931046" name="jetbrains.mps.baseLanguage.structure.ContinueStatement" flags="nn" index="3N13vt" />
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
      <concept id="1200397529627" name="jetbrains.mps.baseLanguage.structure.CharConstant" flags="nn" index="1Xhbcc">
        <property id="1200397540847" name="charConstant" index="1XhdNS" />
      </concept>
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="5808518347809715508" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_InputNode" flags="nn" index="385nmt">
        <property id="5808518347809748738" name="presentation" index="385vuF" />
        <child id="5808518347809747118" name="node" index="385v07" />
      </concept>
      <concept id="3864140621129707969" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_Mappings" flags="nn" index="39dXUE">
        <child id="3864140621129713349" name="labels" index="39e2AI" />
      </concept>
      <concept id="3864140621129713351" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_NodeMapEntry" flags="nn" index="39e2AG">
        <property id="5843998055530255671" name="isNewRoot" index="2mV_xN" />
        <reference id="3864140621129713371" name="inputOrigin" index="39e2AK" />
        <child id="5808518347809748862" name="inputNode" index="385vvn" />
        <child id="3864140621129713365" name="outputNode" index="39e2AY" />
      </concept>
      <concept id="3864140621129713348" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_LabelEntry" flags="nn" index="39e2AJ">
        <property id="3864140621129715945" name="label" index="39e3Y2" />
        <child id="3864140621129715947" name="entries" index="39e3Y0" />
      </concept>
      <concept id="3864140621129713362" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_NodeRef" flags="nn" index="39e2AT">
        <reference id="3864140621129713363" name="node" index="39e2AS" />
      </concept>
      <concept id="3637169702552512264" name="jetbrains.mps.lang.generator.structure.ElementaryNodeId" flags="ng" index="3u3nmq">
        <property id="3637169702552512269" name="nodeId" index="3u3nmv" />
      </concept>
    </language>
    <language id="df345b11-b8c7-4213-ac66-48d2a9b75d88" name="jetbrains.mps.baseLanguageInternal">
      <concept id="1174914042989" name="jetbrains.mps.baseLanguageInternal.structure.InternalClassifierType" flags="in" index="2eloPW">
        <property id="1174914081067" name="fqClassName" index="2ely0U" />
      </concept>
      <concept id="1100832983841311024" name="jetbrains.mps.baseLanguageInternal.structure.InternalClassCreator" flags="nn" index="xCZzO">
        <property id="1100832983841311026" name="fqClassName" index="xCZzQ" />
        <child id="1100832983841311029" name="type" index="xCZzL" />
      </concept>
      <concept id="1173995204289" name="jetbrains.mps.baseLanguageInternal.structure.InternalStaticFieldReference" flags="nn" index="1n$iZg">
        <property id="1173995448817" name="fqClassName" index="1n_ezw" />
        <property id="1173995466678" name="fieldName" index="1n_iUB" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="6677504323281689838" name="jetbrains.mps.lang.smodel.structure.SConceptType" flags="in" index="3bZ5Sz" />
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
      <concept id="1165530316231" name="jetbrains.mps.baseLanguage.collections.structure.IsEmptyOperation" flags="nn" index="1v1jN8" />
      <concept id="1176501494711" name="jetbrains.mps.baseLanguage.collections.structure.IsNotEmptyOperation" flags="nn" index="3GX2aA" />
    </language>
  </registry>
  <node concept="312cEu" id="0">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="AllOfSchema_TextGen" />
    <uo k="s:originTrace" v="n:8442470881495857392" />
    <node concept="3Tm1VV" id="1" role="1B3o_S">
      <uo k="s:originTrace" v="n:8442470881495857392" />
    </node>
    <node concept="3uibUv" id="2" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:8442470881495857392" />
    </node>
    <node concept="3clFb_" id="3" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:8442470881495857392" />
      <node concept="3cqZAl" id="4" role="3clF45">
        <uo k="s:originTrace" v="n:8442470881495857392" />
      </node>
      <node concept="3Tm1VV" id="5" role="1B3o_S">
        <uo k="s:originTrace" v="n:8442470881495857392" />
      </node>
      <node concept="3clFbS" id="6" role="3clF47">
        <uo k="s:originTrace" v="n:8442470881495857392" />
        <node concept="3cpWs8" id="9" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495857392" />
          <node concept="3cpWsn" id="m" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:8442470881495857392" />
            <node concept="3uibUv" id="n" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:8442470881495857392" />
            </node>
            <node concept="2ShNRf" id="o" role="33vP2m">
              <uo k="s:originTrace" v="n:8442470881495857392" />
              <node concept="1pGfFk" id="p" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:8442470881495857392" />
                <node concept="37vLTw" id="q" role="37wK5m">
                  <ref role="3cqZAo" node="7" resolve="ctx" />
                  <uo k="s:originTrace" v="n:8442470881495857392" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="a" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495857395" />
          <node concept="2OqwBi" id="r" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881495857395" />
            <node concept="37vLTw" id="s" role="2Oq$k0">
              <ref role="3cqZAo" node="m" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881495857395" />
            </node>
            <node concept="liA8E" id="t" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:8442470881495857395" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="b" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495919750" />
          <node concept="2OqwBi" id="u" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881495919750" />
            <node concept="37vLTw" id="v" role="2Oq$k0">
              <ref role="3cqZAo" node="m" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881495919750" />
            </node>
            <node concept="liA8E" id="w" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:8442470881495919750" />
              <node concept="2OqwBi" id="x" role="37wK5m">
                <uo k="s:originTrace" v="n:8442470881495920230" />
                <node concept="2OqwBi" id="y" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:8442470881495919804" />
                  <node concept="37vLTw" id="$" role="2Oq$k0">
                    <ref role="3cqZAo" node="7" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="_" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="z" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                  <uo k="s:originTrace" v="n:8442470881495921363" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="c" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495921577" />
          <node concept="2OqwBi" id="A" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881495921577" />
            <node concept="37vLTw" id="B" role="2Oq$k0">
              <ref role="3cqZAo" node="m" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881495921577" />
            </node>
            <node concept="liA8E" id="C" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:8442470881495921577" />
              <node concept="Xl_RD" id="D" role="37wK5m">
                <property role="Xl_RC" value=":" />
                <uo k="s:originTrace" v="n:8442470881495921577" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="d" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495921686" />
          <node concept="2OqwBi" id="E" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881495921686" />
            <node concept="37vLTw" id="F" role="2Oq$k0">
              <ref role="3cqZAo" node="m" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881495921686" />
            </node>
            <node concept="liA8E" id="G" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:8442470881495921686" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="e" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495932746" />
          <node concept="2OqwBi" id="H" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881495932746" />
            <node concept="2OqwBi" id="I" role="2Oq$k0">
              <uo k="s:originTrace" v="n:8442470881495932746" />
              <node concept="2OqwBi" id="K" role="2Oq$k0">
                <uo k="s:originTrace" v="n:8442470881495932746" />
                <node concept="37vLTw" id="M" role="2Oq$k0">
                  <ref role="3cqZAo" node="7" resolve="ctx" />
                  <uo k="s:originTrace" v="n:8442470881495932746" />
                </node>
                <node concept="liA8E" id="N" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:8442470881495932746" />
                </node>
              </node>
              <node concept="liA8E" id="L" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:8442470881495932746" />
              </node>
            </node>
            <node concept="liA8E" id="J" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:8442470881495932746" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="f" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495926504" />
          <node concept="3clFbS" id="O" role="3clFbx">
            <uo k="s:originTrace" v="n:8442470881495926506" />
            <node concept="3clFbF" id="Q" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881495922957" />
              <node concept="2OqwBi" id="U" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881495922957" />
                <node concept="37vLTw" id="V" role="2Oq$k0">
                  <ref role="3cqZAo" node="m" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881495922957" />
                </node>
                <node concept="liA8E" id="W" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:8442470881495922957" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="R" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881495932079" />
              <node concept="2OqwBi" id="X" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881495932079" />
                <node concept="37vLTw" id="Y" role="2Oq$k0">
                  <ref role="3cqZAo" node="m" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881495932079" />
                </node>
                <node concept="liA8E" id="Z" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881495932079" />
                  <node concept="Xl_RD" id="10" role="37wK5m">
                    <property role="Xl_RC" value="description: " />
                    <uo k="s:originTrace" v="n:8442470881495932079" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="S" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881495923354" />
              <node concept="2OqwBi" id="11" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881495923354" />
                <node concept="37vLTw" id="12" role="2Oq$k0">
                  <ref role="3cqZAo" node="m" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881495923354" />
                </node>
                <node concept="liA8E" id="13" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881495923354" />
                  <node concept="2OqwBi" id="14" role="37wK5m">
                    <uo k="s:originTrace" v="n:8442470881495923834" />
                    <node concept="2OqwBi" id="15" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:8442470881495923408" />
                      <node concept="37vLTw" id="17" role="2Oq$k0">
                        <ref role="3cqZAo" node="7" resolve="ctx" />
                      </node>
                      <node concept="liA8E" id="18" role="2OqNvi">
                        <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="16" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                      <uo k="s:originTrace" v="n:8442470881495925943" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="T" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881497015917" />
              <node concept="2OqwBi" id="19" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881497015917" />
                <node concept="37vLTw" id="1a" role="2Oq$k0">
                  <ref role="3cqZAo" node="m" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881497015917" />
                </node>
                <node concept="liA8E" id="1b" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:8442470881497015917" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="P" role="3clFbw">
            <uo k="s:originTrace" v="n:8442470881495929019" />
            <node concept="2OqwBi" id="1c" role="2Oq$k0">
              <uo k="s:originTrace" v="n:8442470881495927190" />
              <node concept="2OqwBi" id="1e" role="2Oq$k0">
                <uo k="s:originTrace" v="n:8442470881495926675" />
                <node concept="37vLTw" id="1g" role="2Oq$k0">
                  <ref role="3cqZAo" node="7" resolve="ctx" />
                </node>
                <node concept="liA8E" id="1h" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="1f" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                <uo k="s:originTrace" v="n:8442470881495927802" />
              </node>
            </node>
            <node concept="17RvpY" id="1d" role="2OqNvi">
              <uo k="s:originTrace" v="n:8442470881495931054" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="g" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495967129" />
          <node concept="2OqwBi" id="1i" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881495967129" />
            <node concept="37vLTw" id="1j" role="2Oq$k0">
              <ref role="3cqZAo" node="m" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881495967129" />
            </node>
            <node concept="liA8E" id="1k" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:8442470881495967129" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="h" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495857397" />
          <node concept="2OqwBi" id="1l" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881495857397" />
            <node concept="37vLTw" id="1m" role="2Oq$k0">
              <ref role="3cqZAo" node="m" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881495857397" />
            </node>
            <node concept="liA8E" id="1n" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:8442470881495857397" />
              <node concept="Xl_RD" id="1o" role="37wK5m">
                <property role="Xl_RC" value="allOf:" />
                <uo k="s:originTrace" v="n:8442470881495857397" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="i" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495857398" />
          <node concept="2OqwBi" id="1p" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881495857398" />
            <node concept="37vLTw" id="1q" role="2Oq$k0">
              <ref role="3cqZAo" node="m" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881495857398" />
            </node>
            <node concept="liA8E" id="1r" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:8442470881495857398" />
            </node>
          </node>
        </node>
        <node concept="2Gpval" id="j" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495857399" />
          <node concept="2GrKxI" id="1s" role="2Gsz3X">
            <property role="TrG5h" value="elem" />
            <uo k="s:originTrace" v="n:8442470881495857400" />
          </node>
          <node concept="2OqwBi" id="1t" role="2GsD0m">
            <uo k="s:originTrace" v="n:8442470881495857401" />
            <node concept="2OqwBi" id="1v" role="2Oq$k0">
              <uo k="s:originTrace" v="n:8442470881495857402" />
              <node concept="37vLTw" id="1x" role="2Oq$k0">
                <ref role="3cqZAo" node="7" resolve="ctx" />
              </node>
              <node concept="liA8E" id="1y" role="2OqNvi">
                <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
              </node>
            </node>
            <node concept="3Tsc0h" id="1w" role="2OqNvi">
              <ref role="3TtcxE" to="7thy:3JsE8aiB2pH" resolve="allOf" />
              <uo k="s:originTrace" v="n:8442470881495857403" />
            </node>
          </node>
          <node concept="3clFbS" id="1u" role="2LFqv$">
            <uo k="s:originTrace" v="n:8442470881495857404" />
            <node concept="3clFbF" id="1z" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881495857405" />
              <node concept="2OqwBi" id="1F" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881495857405" />
                <node concept="2OqwBi" id="1G" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:8442470881495857405" />
                  <node concept="2OqwBi" id="1I" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:8442470881495857405" />
                    <node concept="37vLTw" id="1K" role="2Oq$k0">
                      <ref role="3cqZAo" node="7" resolve="ctx" />
                      <uo k="s:originTrace" v="n:8442470881495857405" />
                    </node>
                    <node concept="liA8E" id="1L" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                      <uo k="s:originTrace" v="n:8442470881495857405" />
                    </node>
                  </node>
                  <node concept="liA8E" id="1J" role="2OqNvi">
                    <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                    <uo k="s:originTrace" v="n:8442470881495857405" />
                  </node>
                </node>
                <node concept="liA8E" id="1H" role="2OqNvi">
                  <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
                  <uo k="s:originTrace" v="n:8442470881495857405" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1$" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881495857407" />
              <node concept="2OqwBi" id="1M" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881495857407" />
                <node concept="37vLTw" id="1N" role="2Oq$k0">
                  <ref role="3cqZAo" node="m" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881495857407" />
                </node>
                <node concept="liA8E" id="1O" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:8442470881495857407" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1_" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881495857409" />
              <node concept="2OqwBi" id="1P" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881495857409" />
                <node concept="37vLTw" id="1Q" role="2Oq$k0">
                  <ref role="3cqZAo" node="m" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881495857409" />
                </node>
                <node concept="liA8E" id="1R" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881495857409" />
                  <node concept="Xl_RD" id="1S" role="37wK5m">
                    <property role="Xl_RC" value="- $ref: " />
                    <uo k="s:originTrace" v="n:8442470881495857409" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1A" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881495857410" />
              <node concept="2OqwBi" id="1T" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881495857410" />
                <node concept="37vLTw" id="1U" role="2Oq$k0">
                  <ref role="3cqZAo" node="m" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881495857410" />
                </node>
                <node concept="liA8E" id="1V" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881495857410" />
                  <node concept="Xl_RD" id="1W" role="37wK5m">
                    <property role="Xl_RC" value="'#/components/schemas/" />
                    <uo k="s:originTrace" v="n:8442470881495857410" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1B" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881495857411" />
              <node concept="2OqwBi" id="1X" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881495857411" />
                <node concept="37vLTw" id="1Y" role="2Oq$k0">
                  <ref role="3cqZAo" node="m" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881495857411" />
                </node>
                <node concept="liA8E" id="1Z" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881495857411" />
                  <node concept="2OqwBi" id="20" role="37wK5m">
                    <uo k="s:originTrace" v="n:8442470881495857412" />
                    <node concept="2OqwBi" id="21" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:8442470881495857413" />
                      <node concept="2GrUjf" id="23" role="2Oq$k0">
                        <ref role="2Gs0qQ" node="1s" resolve="elem" />
                        <uo k="s:originTrace" v="n:8442470881495857414" />
                      </node>
                      <node concept="3TrEf2" id="24" role="2OqNvi">
                        <ref role="3Tt5mk" to="7thy:3JsE8aiBPe0" resolve="schema" />
                        <uo k="s:originTrace" v="n:8442470881495857415" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="22" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                      <uo k="s:originTrace" v="n:8442470881495857416" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1C" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496235363" />
              <node concept="2OqwBi" id="25" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496235363" />
                <node concept="37vLTw" id="26" role="2Oq$k0">
                  <ref role="3cqZAo" node="m" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881496235363" />
                </node>
                <node concept="liA8E" id="27" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881496235363" />
                  <node concept="Xl_RD" id="28" role="37wK5m">
                    <property role="Xl_RC" value="'" />
                    <uo k="s:originTrace" v="n:8442470881496235363" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1D" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881497015992" />
              <node concept="2OqwBi" id="29" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881497015992" />
                <node concept="37vLTw" id="2a" role="2Oq$k0">
                  <ref role="3cqZAo" node="m" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881497015992" />
                </node>
                <node concept="liA8E" id="2b" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:8442470881497015992" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1E" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881495857405" />
              <node concept="2OqwBi" id="2c" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881495857405" />
                <node concept="2OqwBi" id="2d" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:8442470881495857405" />
                  <node concept="2OqwBi" id="2f" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:8442470881495857405" />
                    <node concept="37vLTw" id="2h" role="2Oq$k0">
                      <ref role="3cqZAo" node="7" resolve="ctx" />
                      <uo k="s:originTrace" v="n:8442470881495857405" />
                    </node>
                    <node concept="liA8E" id="2i" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                      <uo k="s:originTrace" v="n:8442470881495857405" />
                    </node>
                  </node>
                  <node concept="liA8E" id="2g" role="2OqNvi">
                    <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                    <uo k="s:originTrace" v="n:8442470881495857405" />
                  </node>
                </node>
                <node concept="liA8E" id="2e" role="2OqNvi">
                  <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
                  <uo k="s:originTrace" v="n:8442470881495857405" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="k" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600232987" />
          <node concept="3clFbS" id="2j" role="3clFbx">
            <uo k="s:originTrace" v="n:227067257600232989" />
            <node concept="3clFbF" id="2l" role="3cqZAp">
              <uo k="s:originTrace" v="n:227067257600253106" />
              <node concept="2OqwBi" id="2m" role="3clFbG">
                <node concept="37vLTw" id="2n" role="2Oq$k0">
                  <ref role="3cqZAo" node="m" resolve="tgs" />
                </node>
                <node concept="liA8E" id="2o" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.reportError(java.lang.String)" resolve="reportError" />
                  <node concept="Xl_RD" id="2p" role="37wK5m">
                    <property role="Xl_RC" value="allOf may not be empty" />
                    <uo k="s:originTrace" v="n:227067257600253123" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="2k" role="3clFbw">
            <uo k="s:originTrace" v="n:227067257600240304" />
            <node concept="2OqwBi" id="2q" role="2Oq$k0">
              <uo k="s:originTrace" v="n:227067257600234030" />
              <node concept="2OqwBi" id="2s" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600233516" />
                <node concept="37vLTw" id="2u" role="2Oq$k0">
                  <ref role="3cqZAo" node="7" resolve="ctx" />
                </node>
                <node concept="liA8E" id="2v" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3Tsc0h" id="2t" role="2OqNvi">
                <ref role="3TtcxE" to="7thy:3JsE8aiB2pH" resolve="allOf" />
                <uo k="s:originTrace" v="n:227067257600234712" />
              </node>
            </node>
            <node concept="1v1jN8" id="2r" role="2OqNvi">
              <uo k="s:originTrace" v="n:227067257600253043" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="l" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495932746" />
          <node concept="2OqwBi" id="2w" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881495932746" />
            <node concept="2OqwBi" id="2x" role="2Oq$k0">
              <uo k="s:originTrace" v="n:8442470881495932746" />
              <node concept="2OqwBi" id="2z" role="2Oq$k0">
                <uo k="s:originTrace" v="n:8442470881495932746" />
                <node concept="37vLTw" id="2_" role="2Oq$k0">
                  <ref role="3cqZAo" node="7" resolve="ctx" />
                  <uo k="s:originTrace" v="n:8442470881495932746" />
                </node>
                <node concept="liA8E" id="2A" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:8442470881495932746" />
                </node>
              </node>
              <node concept="liA8E" id="2$" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:8442470881495932746" />
              </node>
            </node>
            <node concept="liA8E" id="2y" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:8442470881495932746" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="7" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:8442470881495857392" />
        <node concept="3uibUv" id="2B" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:8442470881495857392" />
        </node>
      </node>
      <node concept="2AHcQZ" id="8" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:8442470881495857392" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="2C">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="AnyOfSchema_TextGen" />
    <uo k="s:originTrace" v="n:4313507821875263548" />
    <node concept="3Tm1VV" id="2D" role="1B3o_S">
      <uo k="s:originTrace" v="n:4313507821875263548" />
    </node>
    <node concept="3uibUv" id="2E" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:4313507821875263548" />
    </node>
    <node concept="3clFb_" id="2F" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:4313507821875263548" />
      <node concept="3cqZAl" id="2G" role="3clF45">
        <uo k="s:originTrace" v="n:4313507821875263548" />
      </node>
      <node concept="3Tm1VV" id="2H" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821875263548" />
      </node>
      <node concept="3clFbS" id="2I" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821875263548" />
        <node concept="3cpWs8" id="2L" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875263548" />
          <node concept="3cpWsn" id="2Y" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:4313507821875263548" />
            <node concept="3uibUv" id="2Z" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:4313507821875263548" />
            </node>
            <node concept="2ShNRf" id="30" role="33vP2m">
              <uo k="s:originTrace" v="n:4313507821875263548" />
              <node concept="1pGfFk" id="31" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:4313507821875263548" />
                <node concept="37vLTw" id="32" role="37wK5m">
                  <ref role="3cqZAo" node="2J" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821875263548" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2M" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496000429" />
          <node concept="2OqwBi" id="33" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496000429" />
            <node concept="37vLTw" id="34" role="2Oq$k0">
              <ref role="3cqZAo" node="2Y" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496000429" />
            </node>
            <node concept="liA8E" id="35" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:8442470881496000429" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2N" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496000431" />
          <node concept="2OqwBi" id="36" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496000431" />
            <node concept="37vLTw" id="37" role="2Oq$k0">
              <ref role="3cqZAo" node="2Y" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496000431" />
            </node>
            <node concept="liA8E" id="38" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:8442470881496000431" />
              <node concept="2OqwBi" id="39" role="37wK5m">
                <uo k="s:originTrace" v="n:8442470881496000432" />
                <node concept="2OqwBi" id="3a" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:8442470881496000433" />
                  <node concept="37vLTw" id="3c" role="2Oq$k0">
                    <ref role="3cqZAo" node="2J" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="3d" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="3b" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                  <uo k="s:originTrace" v="n:8442470881496000434" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2O" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496000435" />
          <node concept="2OqwBi" id="3e" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496000435" />
            <node concept="37vLTw" id="3f" role="2Oq$k0">
              <ref role="3cqZAo" node="2Y" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496000435" />
            </node>
            <node concept="liA8E" id="3g" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:8442470881496000435" />
              <node concept="Xl_RD" id="3h" role="37wK5m">
                <property role="Xl_RC" value=":" />
                <uo k="s:originTrace" v="n:8442470881496000435" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2P" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496000436" />
          <node concept="2OqwBi" id="3i" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496000436" />
            <node concept="37vLTw" id="3j" role="2Oq$k0">
              <ref role="3cqZAo" node="2Y" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496000436" />
            </node>
            <node concept="liA8E" id="3k" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:8442470881496000436" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2Q" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496000437" />
          <node concept="2OqwBi" id="3l" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496000437" />
            <node concept="2OqwBi" id="3m" role="2Oq$k0">
              <uo k="s:originTrace" v="n:8442470881496000437" />
              <node concept="2OqwBi" id="3o" role="2Oq$k0">
                <uo k="s:originTrace" v="n:8442470881496000437" />
                <node concept="37vLTw" id="3q" role="2Oq$k0">
                  <ref role="3cqZAo" node="2J" resolve="ctx" />
                  <uo k="s:originTrace" v="n:8442470881496000437" />
                </node>
                <node concept="liA8E" id="3r" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:8442470881496000437" />
                </node>
              </node>
              <node concept="liA8E" id="3p" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:8442470881496000437" />
              </node>
            </node>
            <node concept="liA8E" id="3n" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:8442470881496000437" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="2R" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496000439" />
          <node concept="3clFbS" id="3s" role="3clFbx">
            <uo k="s:originTrace" v="n:8442470881496000440" />
            <node concept="3clFbF" id="3u" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496000441" />
              <node concept="2OqwBi" id="3y" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496000441" />
                <node concept="37vLTw" id="3z" role="2Oq$k0">
                  <ref role="3cqZAo" node="2Y" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881496000441" />
                </node>
                <node concept="liA8E" id="3$" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:8442470881496000441" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="3v" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496000443" />
              <node concept="2OqwBi" id="3_" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496000443" />
                <node concept="37vLTw" id="3A" role="2Oq$k0">
                  <ref role="3cqZAo" node="2Y" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881496000443" />
                </node>
                <node concept="liA8E" id="3B" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881496000443" />
                  <node concept="Xl_RD" id="3C" role="37wK5m">
                    <property role="Xl_RC" value="description: " />
                    <uo k="s:originTrace" v="n:8442470881496000443" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="3w" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496000444" />
              <node concept="2OqwBi" id="3D" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496000444" />
                <node concept="37vLTw" id="3E" role="2Oq$k0">
                  <ref role="3cqZAo" node="2Y" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881496000444" />
                </node>
                <node concept="liA8E" id="3F" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881496000444" />
                  <node concept="2OqwBi" id="3G" role="37wK5m">
                    <uo k="s:originTrace" v="n:8442470881496000445" />
                    <node concept="2OqwBi" id="3H" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:8442470881496000446" />
                      <node concept="37vLTw" id="3J" role="2Oq$k0">
                        <ref role="3cqZAo" node="2J" resolve="ctx" />
                      </node>
                      <node concept="liA8E" id="3K" role="2OqNvi">
                        <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="3I" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                      <uo k="s:originTrace" v="n:8442470881496000447" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="3x" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881497015333" />
              <node concept="2OqwBi" id="3L" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881497015333" />
                <node concept="37vLTw" id="3M" role="2Oq$k0">
                  <ref role="3cqZAo" node="2Y" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881497015333" />
                </node>
                <node concept="liA8E" id="3N" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:8442470881497015333" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="3t" role="3clFbw">
            <uo k="s:originTrace" v="n:8442470881496000448" />
            <node concept="2OqwBi" id="3O" role="2Oq$k0">
              <uo k="s:originTrace" v="n:8442470881496000449" />
              <node concept="2OqwBi" id="3Q" role="2Oq$k0">
                <uo k="s:originTrace" v="n:8442470881496000450" />
                <node concept="37vLTw" id="3S" role="2Oq$k0">
                  <ref role="3cqZAo" node="2J" resolve="ctx" />
                </node>
                <node concept="liA8E" id="3T" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="3R" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                <uo k="s:originTrace" v="n:8442470881496000451" />
              </node>
            </node>
            <node concept="17RvpY" id="3P" role="2OqNvi">
              <uo k="s:originTrace" v="n:8442470881496000452" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2S" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496000455" />
          <node concept="2OqwBi" id="3U" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496000455" />
            <node concept="37vLTw" id="3V" role="2Oq$k0">
              <ref role="3cqZAo" node="2Y" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496000455" />
            </node>
            <node concept="liA8E" id="3W" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:8442470881496000455" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2T" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496000457" />
          <node concept="2OqwBi" id="3X" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496000457" />
            <node concept="37vLTw" id="3Y" role="2Oq$k0">
              <ref role="3cqZAo" node="2Y" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496000457" />
            </node>
            <node concept="liA8E" id="3Z" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:8442470881496000457" />
              <node concept="Xl_RD" id="40" role="37wK5m">
                <property role="Xl_RC" value="anyOf:" />
                <uo k="s:originTrace" v="n:8442470881496000457" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2U" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496000458" />
          <node concept="2OqwBi" id="41" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496000458" />
            <node concept="37vLTw" id="42" role="2Oq$k0">
              <ref role="3cqZAo" node="2Y" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496000458" />
            </node>
            <node concept="liA8E" id="43" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:8442470881496000458" />
            </node>
          </node>
        </node>
        <node concept="2Gpval" id="2V" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496000459" />
          <node concept="2GrKxI" id="44" role="2Gsz3X">
            <property role="TrG5h" value="elem" />
            <uo k="s:originTrace" v="n:8442470881496000460" />
          </node>
          <node concept="2OqwBi" id="45" role="2GsD0m">
            <uo k="s:originTrace" v="n:8442470881496000461" />
            <node concept="2OqwBi" id="47" role="2Oq$k0">
              <uo k="s:originTrace" v="n:8442470881496000462" />
              <node concept="37vLTw" id="49" role="2Oq$k0">
                <ref role="3cqZAo" node="2J" resolve="ctx" />
              </node>
              <node concept="liA8E" id="4a" role="2OqNvi">
                <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
              </node>
            </node>
            <node concept="3Tsc0h" id="48" role="2OqNvi">
              <ref role="3TtcxE" to="7thy:3JsE8aiB2q5" resolve="anyOf" />
              <uo k="s:originTrace" v="n:8442470881496000463" />
            </node>
          </node>
          <node concept="3clFbS" id="46" role="2LFqv$">
            <uo k="s:originTrace" v="n:8442470881496000464" />
            <node concept="3clFbF" id="4b" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496000465" />
              <node concept="2OqwBi" id="4j" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496000465" />
                <node concept="2OqwBi" id="4k" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:8442470881496000465" />
                  <node concept="2OqwBi" id="4m" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:8442470881496000465" />
                    <node concept="37vLTw" id="4o" role="2Oq$k0">
                      <ref role="3cqZAo" node="2J" resolve="ctx" />
                      <uo k="s:originTrace" v="n:8442470881496000465" />
                    </node>
                    <node concept="liA8E" id="4p" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                      <uo k="s:originTrace" v="n:8442470881496000465" />
                    </node>
                  </node>
                  <node concept="liA8E" id="4n" role="2OqNvi">
                    <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                    <uo k="s:originTrace" v="n:8442470881496000465" />
                  </node>
                </node>
                <node concept="liA8E" id="4l" role="2OqNvi">
                  <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
                  <uo k="s:originTrace" v="n:8442470881496000465" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4c" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496000467" />
              <node concept="2OqwBi" id="4q" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496000467" />
                <node concept="37vLTw" id="4r" role="2Oq$k0">
                  <ref role="3cqZAo" node="2Y" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881496000467" />
                </node>
                <node concept="liA8E" id="4s" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:8442470881496000467" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4d" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496000469" />
              <node concept="2OqwBi" id="4t" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496000469" />
                <node concept="37vLTw" id="4u" role="2Oq$k0">
                  <ref role="3cqZAo" node="2Y" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881496000469" />
                </node>
                <node concept="liA8E" id="4v" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881496000469" />
                  <node concept="Xl_RD" id="4w" role="37wK5m">
                    <property role="Xl_RC" value="- $ref: " />
                    <uo k="s:originTrace" v="n:8442470881496000469" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4e" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496000470" />
              <node concept="2OqwBi" id="4x" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496000470" />
                <node concept="37vLTw" id="4y" role="2Oq$k0">
                  <ref role="3cqZAo" node="2Y" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881496000470" />
                </node>
                <node concept="liA8E" id="4z" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881496000470" />
                  <node concept="Xl_RD" id="4$" role="37wK5m">
                    <property role="Xl_RC" value="'#/components/schemas/" />
                    <uo k="s:originTrace" v="n:8442470881496000470" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4f" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496000471" />
              <node concept="2OqwBi" id="4_" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496000471" />
                <node concept="37vLTw" id="4A" role="2Oq$k0">
                  <ref role="3cqZAo" node="2Y" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881496000471" />
                </node>
                <node concept="liA8E" id="4B" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881496000471" />
                  <node concept="2OqwBi" id="4C" role="37wK5m">
                    <uo k="s:originTrace" v="n:8442470881496000472" />
                    <node concept="2OqwBi" id="4D" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:8442470881496000473" />
                      <node concept="2GrUjf" id="4F" role="2Oq$k0">
                        <ref role="2Gs0qQ" node="44" resolve="elem" />
                        <uo k="s:originTrace" v="n:8442470881496000474" />
                      </node>
                      <node concept="3TrEf2" id="4G" role="2OqNvi">
                        <ref role="3Tt5mk" to="7thy:3JsE8aiBPe0" resolve="schema" />
                        <uo k="s:originTrace" v="n:8442470881496000475" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="4E" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                      <uo k="s:originTrace" v="n:8442470881496000476" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4g" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496235751" />
              <node concept="2OqwBi" id="4H" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496235751" />
                <node concept="37vLTw" id="4I" role="2Oq$k0">
                  <ref role="3cqZAo" node="2Y" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881496235751" />
                </node>
                <node concept="liA8E" id="4J" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881496235751" />
                  <node concept="Xl_RD" id="4K" role="37wK5m">
                    <property role="Xl_RC" value="'" />
                    <uo k="s:originTrace" v="n:8442470881496235751" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4h" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881497015408" />
              <node concept="2OqwBi" id="4L" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881497015408" />
                <node concept="37vLTw" id="4M" role="2Oq$k0">
                  <ref role="3cqZAo" node="2Y" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881497015408" />
                </node>
                <node concept="liA8E" id="4N" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:8442470881497015408" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4i" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496000465" />
              <node concept="2OqwBi" id="4O" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496000465" />
                <node concept="2OqwBi" id="4P" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:8442470881496000465" />
                  <node concept="2OqwBi" id="4R" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:8442470881496000465" />
                    <node concept="37vLTw" id="4T" role="2Oq$k0">
                      <ref role="3cqZAo" node="2J" resolve="ctx" />
                      <uo k="s:originTrace" v="n:8442470881496000465" />
                    </node>
                    <node concept="liA8E" id="4U" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                      <uo k="s:originTrace" v="n:8442470881496000465" />
                    </node>
                  </node>
                  <node concept="liA8E" id="4S" role="2OqNvi">
                    <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                    <uo k="s:originTrace" v="n:8442470881496000465" />
                  </node>
                </node>
                <node concept="liA8E" id="4Q" role="2OqNvi">
                  <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
                  <uo k="s:originTrace" v="n:8442470881496000465" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="2W" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600253956" />
          <node concept="3clFbS" id="4V" role="3clFbx">
            <uo k="s:originTrace" v="n:227067257600253957" />
            <node concept="3clFbF" id="4X" role="3cqZAp">
              <uo k="s:originTrace" v="n:227067257600253958" />
              <node concept="2OqwBi" id="4Y" role="3clFbG">
                <node concept="37vLTw" id="4Z" role="2Oq$k0">
                  <ref role="3cqZAo" node="2Y" resolve="tgs" />
                </node>
                <node concept="liA8E" id="50" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.reportError(java.lang.String)" resolve="reportError" />
                  <node concept="Xl_RD" id="51" role="37wK5m">
                    <property role="Xl_RC" value="anyOf may not be empty" />
                    <uo k="s:originTrace" v="n:227067257600253959" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="4W" role="3clFbw">
            <uo k="s:originTrace" v="n:227067257600253960" />
            <node concept="2OqwBi" id="52" role="2Oq$k0">
              <uo k="s:originTrace" v="n:227067257600253961" />
              <node concept="2OqwBi" id="54" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600253962" />
                <node concept="37vLTw" id="56" role="2Oq$k0">
                  <ref role="3cqZAo" node="2J" resolve="ctx" />
                </node>
                <node concept="liA8E" id="57" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3Tsc0h" id="55" role="2OqNvi">
                <ref role="3TtcxE" to="7thy:3JsE8aiB2q5" resolve="anyOf" />
                <uo k="s:originTrace" v="n:227067257600253963" />
              </node>
            </node>
            <node concept="1v1jN8" id="53" role="2OqNvi">
              <uo k="s:originTrace" v="n:227067257600253964" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2X" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496000437" />
          <node concept="2OqwBi" id="58" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496000437" />
            <node concept="2OqwBi" id="59" role="2Oq$k0">
              <uo k="s:originTrace" v="n:8442470881496000437" />
              <node concept="2OqwBi" id="5b" role="2Oq$k0">
                <uo k="s:originTrace" v="n:8442470881496000437" />
                <node concept="37vLTw" id="5d" role="2Oq$k0">
                  <ref role="3cqZAo" node="2J" resolve="ctx" />
                  <uo k="s:originTrace" v="n:8442470881496000437" />
                </node>
                <node concept="liA8E" id="5e" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:8442470881496000437" />
                </node>
              </node>
              <node concept="liA8E" id="5c" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:8442470881496000437" />
              </node>
            </node>
            <node concept="liA8E" id="5a" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:8442470881496000437" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="2J" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:4313507821875263548" />
        <node concept="3uibUv" id="5f" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:4313507821875263548" />
        </node>
      </node>
      <node concept="2AHcQZ" id="2K" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821875263548" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="5g">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="ArraySchema_TextGen" />
    <uo k="s:originTrace" v="n:4313507821874921463" />
    <node concept="3Tm1VV" id="5h" role="1B3o_S">
      <uo k="s:originTrace" v="n:4313507821874921463" />
    </node>
    <node concept="3uibUv" id="5i" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:4313507821874921463" />
    </node>
    <node concept="3clFb_" id="5j" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:4313507821874921463" />
      <node concept="3cqZAl" id="5k" role="3clF45">
        <uo k="s:originTrace" v="n:4313507821874921463" />
      </node>
      <node concept="3Tm1VV" id="5l" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821874921463" />
      </node>
      <node concept="3clFbS" id="5m" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821874921463" />
        <node concept="3cpWs8" id="5p" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874921463" />
          <node concept="3cpWsn" id="5N" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:4313507821874921463" />
            <node concept="3uibUv" id="5O" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:4313507821874921463" />
            </node>
            <node concept="2ShNRf" id="5P" role="33vP2m">
              <uo k="s:originTrace" v="n:4313507821874921463" />
              <node concept="1pGfFk" id="5Q" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:4313507821874921463" />
                <node concept="37vLTw" id="5R" role="37wK5m">
                  <ref role="3cqZAo" node="5n" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874921463" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5q" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874922038" />
          <node concept="2OqwBi" id="5S" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874922038" />
            <node concept="37vLTw" id="5T" role="2Oq$k0">
              <ref role="3cqZAo" node="5N" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874922038" />
            </node>
            <node concept="liA8E" id="5U" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874922038" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5r" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874922040" />
          <node concept="2OqwBi" id="5V" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874922040" />
            <node concept="37vLTw" id="5W" role="2Oq$k0">
              <ref role="3cqZAo" node="5N" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874922040" />
            </node>
            <node concept="liA8E" id="5X" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874922040" />
              <node concept="2OqwBi" id="5Y" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821874922041" />
                <node concept="2OqwBi" id="5Z" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874922042" />
                  <node concept="37vLTw" id="61" role="2Oq$k0">
                    <ref role="3cqZAo" node="5n" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="62" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="60" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                  <uo k="s:originTrace" v="n:4313507821874922043" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5s" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874922044" />
          <node concept="2OqwBi" id="63" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874922044" />
            <node concept="37vLTw" id="64" role="2Oq$k0">
              <ref role="3cqZAo" node="5N" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874922044" />
            </node>
            <node concept="liA8E" id="65" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874922044" />
              <node concept="Xl_RD" id="66" role="37wK5m">
                <property role="Xl_RC" value=":\n" />
                <uo k="s:originTrace" v="n:4313507821874922044" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5t" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874922045" />
          <node concept="2OqwBi" id="67" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874922045" />
            <node concept="2OqwBi" id="68" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874922045" />
              <node concept="2OqwBi" id="6a" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874922045" />
                <node concept="37vLTw" id="6c" role="2Oq$k0">
                  <ref role="3cqZAo" node="5n" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874922045" />
                </node>
                <node concept="liA8E" id="6d" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874922045" />
                </node>
              </node>
              <node concept="liA8E" id="6b" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874922045" />
              </node>
            </node>
            <node concept="liA8E" id="69" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874922045" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5u" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874922047" />
          <node concept="2OqwBi" id="6e" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874922047" />
            <node concept="37vLTw" id="6f" role="2Oq$k0">
              <ref role="3cqZAo" node="5N" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874922047" />
            </node>
            <node concept="liA8E" id="6g" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874922047" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5v" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874922049" />
          <node concept="2OqwBi" id="6h" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874922049" />
            <node concept="37vLTw" id="6i" role="2Oq$k0">
              <ref role="3cqZAo" node="5N" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874922049" />
            </node>
            <node concept="liA8E" id="6j" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874922049" />
              <node concept="Xl_RD" id="6k" role="37wK5m">
                <property role="Xl_RC" value="type: array" />
                <uo k="s:originTrace" v="n:4313507821874922049" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5w" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874922050" />
          <node concept="2OqwBi" id="6l" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874922050" />
            <node concept="37vLTw" id="6m" role="2Oq$k0">
              <ref role="3cqZAo" node="5N" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874922050" />
            </node>
            <node concept="liA8E" id="6n" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821874922050" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5x" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874922051" />
          <node concept="2OqwBi" id="6o" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874922051" />
            <node concept="37vLTw" id="6p" role="2Oq$k0">
              <ref role="3cqZAo" node="5N" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874922051" />
            </node>
            <node concept="liA8E" id="6q" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874922051" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5y" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874922053" />
          <node concept="2OqwBi" id="6r" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874922053" />
            <node concept="37vLTw" id="6s" role="2Oq$k0">
              <ref role="3cqZAo" node="5N" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874922053" />
            </node>
            <node concept="liA8E" id="6t" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874922053" />
              <node concept="Xl_RD" id="6u" role="37wK5m">
                <property role="Xl_RC" value="description: " />
                <uo k="s:originTrace" v="n:4313507821874922053" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5z" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874922054" />
          <node concept="2OqwBi" id="6v" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874922054" />
            <node concept="37vLTw" id="6w" role="2Oq$k0">
              <ref role="3cqZAo" node="5N" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874922054" />
            </node>
            <node concept="liA8E" id="6x" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874922054" />
              <node concept="2OqwBi" id="6y" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821874922055" />
                <node concept="2OqwBi" id="6z" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874922056" />
                  <node concept="37vLTw" id="6_" role="2Oq$k0">
                    <ref role="3cqZAo" node="5n" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="6A" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="6$" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                  <uo k="s:originTrace" v="n:4313507821874922057" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5$" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874922058" />
          <node concept="2OqwBi" id="6B" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874922058" />
            <node concept="37vLTw" id="6C" role="2Oq$k0">
              <ref role="3cqZAo" node="5N" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874922058" />
            </node>
            <node concept="liA8E" id="6D" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821874922058" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="5_" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600362493" />
          <node concept="3clFbS" id="6E" role="3clFbx">
            <uo k="s:originTrace" v="n:227067257600362494" />
            <node concept="3clFbF" id="6G" role="3cqZAp">
              <uo k="s:originTrace" v="n:227067257600362495" />
              <node concept="2OqwBi" id="6H" role="3clFbG">
                <node concept="37vLTw" id="6I" role="2Oq$k0">
                  <ref role="3cqZAo" node="5N" resolve="tgs" />
                </node>
                <node concept="liA8E" id="6J" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.reportError(java.lang.String)" resolve="reportError" />
                  <node concept="Xl_RD" id="6K" role="37wK5m">
                    <property role="Xl_RC" value="description is required" />
                    <uo k="s:originTrace" v="n:227067257600362496" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="6F" role="3clFbw">
            <uo k="s:originTrace" v="n:227067257600362497" />
            <node concept="2OqwBi" id="6L" role="2Oq$k0">
              <uo k="s:originTrace" v="n:227067257600362498" />
              <node concept="2OqwBi" id="6N" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600362499" />
                <node concept="37vLTw" id="6P" role="2Oq$k0">
                  <ref role="3cqZAo" node="5n" resolve="ctx" />
                </node>
                <node concept="liA8E" id="6Q" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="6O" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                <uo k="s:originTrace" v="n:227067257600362500" />
              </node>
            </node>
            <node concept="17RlXB" id="6M" role="2OqNvi">
              <uo k="s:originTrace" v="n:227067257600362501" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5A" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600362185" />
        </node>
        <node concept="3clFbJ" id="5B" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874922059" />
          <node concept="3clFbS" id="6R" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874922060" />
            <node concept="3clFbF" id="6T" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874922061" />
              <node concept="2OqwBi" id="6X" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874922061" />
                <node concept="37vLTw" id="6Y" role="2Oq$k0">
                  <ref role="3cqZAo" node="5N" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874922061" />
                </node>
                <node concept="liA8E" id="6Z" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874922061" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="6U" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874922063" />
              <node concept="2OqwBi" id="70" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874922063" />
                <node concept="37vLTw" id="71" role="2Oq$k0">
                  <ref role="3cqZAo" node="5N" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874922063" />
                </node>
                <node concept="liA8E" id="72" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874922063" />
                  <node concept="Xl_RD" id="73" role="37wK5m">
                    <property role="Xl_RC" value="example: " />
                    <uo k="s:originTrace" v="n:4313507821874922063" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="6V" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874922064" />
              <node concept="2OqwBi" id="74" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874922064" />
                <node concept="37vLTw" id="75" role="2Oq$k0">
                  <ref role="3cqZAo" node="5N" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874922064" />
                </node>
                <node concept="liA8E" id="76" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874922064" />
                  <node concept="2OqwBi" id="77" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874922065" />
                    <node concept="2OqwBi" id="78" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:4313507821874922066" />
                      <node concept="37vLTw" id="7a" role="2Oq$k0">
                        <ref role="3cqZAo" node="5n" resolve="ctx" />
                      </node>
                      <node concept="liA8E" id="7b" role="2OqNvi">
                        <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="79" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:4favA_iBNMJ" resolve="example" />
                      <uo k="s:originTrace" v="n:4313507821874922067" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="6W" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874922068" />
              <node concept="2OqwBi" id="7c" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874922068" />
                <node concept="37vLTw" id="7d" role="2Oq$k0">
                  <ref role="3cqZAo" node="5N" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874922068" />
                </node>
                <node concept="liA8E" id="7e" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874922068" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="6S" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874922069" />
            <node concept="2OqwBi" id="7f" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874922070" />
              <node concept="2OqwBi" id="7h" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874922071" />
                <node concept="37vLTw" id="7j" role="2Oq$k0">
                  <ref role="3cqZAo" node="5n" resolve="ctx" />
                </node>
                <node concept="liA8E" id="7k" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="7i" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMJ" resolve="example" />
                <uo k="s:originTrace" v="n:4313507821874922072" />
              </node>
            </node>
            <node concept="17RvpY" id="7g" role="2OqNvi">
              <uo k="s:originTrace" v="n:4313507821874922073" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="5C" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874922089" />
          <node concept="3clFbS" id="7l" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874922090" />
            <node concept="3clFbF" id="7n" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874922091" />
              <node concept="2OqwBi" id="7r" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874922091" />
                <node concept="37vLTw" id="7s" role="2Oq$k0">
                  <ref role="3cqZAo" node="5N" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874922091" />
                </node>
                <node concept="liA8E" id="7t" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874922091" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7o" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874922093" />
              <node concept="2OqwBi" id="7u" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874922093" />
                <node concept="37vLTw" id="7v" role="2Oq$k0">
                  <ref role="3cqZAo" node="5N" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874922093" />
                </node>
                <node concept="liA8E" id="7w" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874922093" />
                  <node concept="Xl_RD" id="7x" role="37wK5m">
                    <property role="Xl_RC" value="minItems: " />
                    <uo k="s:originTrace" v="n:4313507821874922093" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7p" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874922094" />
              <node concept="2OqwBi" id="7y" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874922094" />
                <node concept="37vLTw" id="7z" role="2Oq$k0">
                  <ref role="3cqZAo" node="5N" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874922094" />
                </node>
                <node concept="liA8E" id="7$" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874922094" />
                  <node concept="3cpWs3" id="7_" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874922095" />
                    <node concept="Xl_RD" id="7A" role="3uHU7B">
                      <uo k="s:originTrace" v="n:4313507821874922096" />
                    </node>
                    <node concept="2OqwBi" id="7B" role="3uHU7w">
                      <uo k="s:originTrace" v="n:4313507821874922097" />
                      <node concept="3TrcHB" id="7C" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNSS" resolve="minItems" />
                        <uo k="s:originTrace" v="n:4313507821874922098" />
                      </node>
                      <node concept="2OqwBi" id="7D" role="2Oq$k0">
                        <uo k="s:originTrace" v="n:4313507821874922099" />
                        <node concept="37vLTw" id="7E" role="2Oq$k0">
                          <ref role="3cqZAo" node="5n" resolve="ctx" />
                        </node>
                        <node concept="liA8E" id="7F" role="2OqNvi">
                          <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7q" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874922100" />
              <node concept="2OqwBi" id="7G" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874922100" />
                <node concept="37vLTw" id="7H" role="2Oq$k0">
                  <ref role="3cqZAo" node="5N" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874922100" />
                </node>
                <node concept="liA8E" id="7I" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874922100" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3eOSWO" id="7m" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874922101" />
            <node concept="3cmrfG" id="7J" role="3uHU7w">
              <property role="3cmrfH" value="0" />
              <uo k="s:originTrace" v="n:4313507821874922102" />
            </node>
            <node concept="2OqwBi" id="7K" role="3uHU7B">
              <uo k="s:originTrace" v="n:4313507821874922103" />
              <node concept="2OqwBi" id="7L" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874922104" />
                <node concept="37vLTw" id="7N" role="2Oq$k0">
                  <ref role="3cqZAo" node="5n" resolve="ctx" />
                </node>
                <node concept="liA8E" id="7O" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="7M" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNSS" resolve="minItems" />
                <uo k="s:originTrace" v="n:4313507821874922105" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="5D" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874922106" />
          <node concept="3clFbS" id="7P" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874922107" />
            <node concept="3clFbF" id="7R" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874922108" />
              <node concept="2OqwBi" id="7V" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874922108" />
                <node concept="37vLTw" id="7W" role="2Oq$k0">
                  <ref role="3cqZAo" node="5N" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874922108" />
                </node>
                <node concept="liA8E" id="7X" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874922108" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7S" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874922110" />
              <node concept="2OqwBi" id="7Y" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874922110" />
                <node concept="37vLTw" id="7Z" role="2Oq$k0">
                  <ref role="3cqZAo" node="5N" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874922110" />
                </node>
                <node concept="liA8E" id="80" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874922110" />
                  <node concept="Xl_RD" id="81" role="37wK5m">
                    <property role="Xl_RC" value="maxItems: " />
                    <uo k="s:originTrace" v="n:4313507821874922110" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7T" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874922111" />
              <node concept="2OqwBi" id="82" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874922111" />
                <node concept="37vLTw" id="83" role="2Oq$k0">
                  <ref role="3cqZAo" node="5N" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874922111" />
                </node>
                <node concept="liA8E" id="84" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874922111" />
                  <node concept="3cpWs3" id="85" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874922112" />
                    <node concept="Xl_RD" id="86" role="3uHU7B">
                      <uo k="s:originTrace" v="n:4313507821874922113" />
                    </node>
                    <node concept="2OqwBi" id="87" role="3uHU7w">
                      <uo k="s:originTrace" v="n:4313507821874922114" />
                      <node concept="3TrcHB" id="88" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNSU" resolve="maxItems" />
                        <uo k="s:originTrace" v="n:4313507821874922115" />
                      </node>
                      <node concept="2OqwBi" id="89" role="2Oq$k0">
                        <uo k="s:originTrace" v="n:4313507821874922116" />
                        <node concept="37vLTw" id="8a" role="2Oq$k0">
                          <ref role="3cqZAo" node="5n" resolve="ctx" />
                        </node>
                        <node concept="liA8E" id="8b" role="2OqNvi">
                          <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7U" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874922117" />
              <node concept="2OqwBi" id="8c" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874922117" />
                <node concept="37vLTw" id="8d" role="2Oq$k0">
                  <ref role="3cqZAo" node="5N" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874922117" />
                </node>
                <node concept="liA8E" id="8e" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874922117" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3eOSWO" id="7Q" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874922118" />
            <node concept="3cmrfG" id="8f" role="3uHU7w">
              <property role="3cmrfH" value="0" />
              <uo k="s:originTrace" v="n:4313507821874922119" />
            </node>
            <node concept="2OqwBi" id="8g" role="3uHU7B">
              <uo k="s:originTrace" v="n:4313507821874922120" />
              <node concept="2OqwBi" id="8h" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874922121" />
                <node concept="37vLTw" id="8j" role="2Oq$k0">
                  <ref role="3cqZAo" node="5n" resolve="ctx" />
                </node>
                <node concept="liA8E" id="8k" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="8i" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNSU" resolve="maxItems" />
                <uo k="s:originTrace" v="n:4313507821874922122" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5E" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875069591" />
          <node concept="2OqwBi" id="8l" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875069591" />
            <node concept="37vLTw" id="8m" role="2Oq$k0">
              <ref role="3cqZAo" node="5N" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875069591" />
            </node>
            <node concept="liA8E" id="8n" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821875069591" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5F" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875070207" />
          <node concept="2OqwBi" id="8o" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875070207" />
            <node concept="37vLTw" id="8p" role="2Oq$k0">
              <ref role="3cqZAo" node="5N" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875070207" />
            </node>
            <node concept="liA8E" id="8q" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821875070207" />
              <node concept="Xl_RD" id="8r" role="37wK5m">
                <property role="Xl_RC" value="items:\n" />
                <uo k="s:originTrace" v="n:4313507821875070207" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5G" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875070472" />
          <node concept="2OqwBi" id="8s" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875070472" />
            <node concept="2OqwBi" id="8t" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821875070472" />
              <node concept="2OqwBi" id="8v" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821875070472" />
                <node concept="37vLTw" id="8x" role="2Oq$k0">
                  <ref role="3cqZAo" node="5n" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821875070472" />
                </node>
                <node concept="liA8E" id="8y" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821875070472" />
                </node>
              </node>
              <node concept="liA8E" id="8w" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821875070472" />
              </node>
            </node>
            <node concept="liA8E" id="8u" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821875070472" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5H" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875071405" />
          <node concept="2OqwBi" id="8z" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875071405" />
            <node concept="37vLTw" id="8$" role="2Oq$k0">
              <ref role="3cqZAo" node="5N" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875071405" />
            </node>
            <node concept="liA8E" id="8_" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821875071405" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5I" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875071456" />
          <node concept="2OqwBi" id="8A" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875071456" />
            <node concept="37vLTw" id="8B" role="2Oq$k0">
              <ref role="3cqZAo" node="5N" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875071456" />
            </node>
            <node concept="liA8E" id="8C" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821875071456" />
              <node concept="Xl_RD" id="8D" role="37wK5m">
                <property role="Xl_RC" value="$ref: " />
                <uo k="s:originTrace" v="n:4313507821875071456" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5J" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875071551" />
          <node concept="2OqwBi" id="8E" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875071551" />
            <node concept="37vLTw" id="8F" role="2Oq$k0">
              <ref role="3cqZAo" node="5N" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875071551" />
            </node>
            <node concept="liA8E" id="8G" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821875071551" />
              <node concept="Xl_RD" id="8H" role="37wK5m">
                <property role="Xl_RC" value="#/components/schemas/" />
                <uo k="s:originTrace" v="n:4313507821875071551" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5K" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875071741" />
          <node concept="2OqwBi" id="8I" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875071741" />
            <node concept="37vLTw" id="8J" role="2Oq$k0">
              <ref role="3cqZAo" node="5N" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875071741" />
            </node>
            <node concept="liA8E" id="8K" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821875071741" />
              <node concept="2OqwBi" id="8L" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821875074318" />
                <node concept="2OqwBi" id="8M" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821875072263" />
                  <node concept="2OqwBi" id="8O" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:4313507821875071793" />
                    <node concept="37vLTw" id="8Q" role="2Oq$k0">
                      <ref role="3cqZAo" node="5n" resolve="ctx" />
                    </node>
                    <node concept="liA8E" id="8R" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                    </node>
                  </node>
                  <node concept="3TrEf2" id="8P" role="2OqNvi">
                    <ref role="3Tt5mk" to="7thy:4favA_iBNT3" resolve="items" />
                    <uo k="s:originTrace" v="n:4313507821875073426" />
                  </node>
                </node>
                <node concept="3TrcHB" id="8N" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                  <uo k="s:originTrace" v="n:4313507821875075604" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5L" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875070472" />
          <node concept="2OqwBi" id="8S" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875070472" />
            <node concept="2OqwBi" id="8T" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821875070472" />
              <node concept="2OqwBi" id="8V" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821875070472" />
                <node concept="37vLTw" id="8X" role="2Oq$k0">
                  <ref role="3cqZAo" node="5n" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821875070472" />
                </node>
                <node concept="liA8E" id="8Y" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821875070472" />
                </node>
              </node>
              <node concept="liA8E" id="8W" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821875070472" />
              </node>
            </node>
            <node concept="liA8E" id="8U" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821875070472" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5M" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874922045" />
          <node concept="2OqwBi" id="8Z" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874922045" />
            <node concept="2OqwBi" id="90" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874922045" />
              <node concept="2OqwBi" id="92" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874922045" />
                <node concept="37vLTw" id="94" role="2Oq$k0">
                  <ref role="3cqZAo" node="5n" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874922045" />
                </node>
                <node concept="liA8E" id="95" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874922045" />
                </node>
              </node>
              <node concept="liA8E" id="93" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874922045" />
              </node>
            </node>
            <node concept="liA8E" id="91" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874922045" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="5n" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:4313507821874921463" />
        <node concept="3uibUv" id="96" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:4313507821874921463" />
        </node>
      </node>
      <node concept="2AHcQZ" id="5o" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821874921463" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="97">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="BooleanSchema_TextGen" />
    <uo k="s:originTrace" v="n:8442470881495869491" />
    <node concept="3Tm1VV" id="98" role="1B3o_S">
      <uo k="s:originTrace" v="n:8442470881495869491" />
    </node>
    <node concept="3uibUv" id="99" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:8442470881495869491" />
    </node>
    <node concept="3clFb_" id="9a" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:8442470881495869491" />
      <node concept="3cqZAl" id="9b" role="3clF45">
        <uo k="s:originTrace" v="n:8442470881495869491" />
      </node>
      <node concept="3Tm1VV" id="9c" role="1B3o_S">
        <uo k="s:originTrace" v="n:8442470881495869491" />
      </node>
      <node concept="3clFbS" id="9d" role="3clF47">
        <uo k="s:originTrace" v="n:8442470881495869491" />
        <node concept="3cpWs8" id="9g" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495869491" />
          <node concept="3cpWsn" id="9p" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:8442470881495869491" />
            <node concept="3uibUv" id="9q" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:8442470881495869491" />
            </node>
            <node concept="2ShNRf" id="9r" role="33vP2m">
              <uo k="s:originTrace" v="n:8442470881495869491" />
              <node concept="1pGfFk" id="9s" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:8442470881495869491" />
                <node concept="37vLTw" id="9t" role="37wK5m">
                  <ref role="3cqZAo" node="9e" resolve="ctx" />
                  <uo k="s:originTrace" v="n:8442470881495869491" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="9h" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495869510" />
          <node concept="2OqwBi" id="9u" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881495869510" />
            <node concept="37vLTw" id="9v" role="2Oq$k0">
              <ref role="3cqZAo" node="9p" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881495869510" />
            </node>
            <node concept="liA8E" id="9w" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:8442470881495869510" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="9i" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495869599" />
          <node concept="2OqwBi" id="9x" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881495869599" />
            <node concept="37vLTw" id="9y" role="2Oq$k0">
              <ref role="3cqZAo" node="9p" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881495869599" />
            </node>
            <node concept="liA8E" id="9z" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:8442470881495869599" />
              <node concept="2OqwBi" id="9$" role="37wK5m">
                <uo k="s:originTrace" v="n:8442470881495870078" />
                <node concept="2OqwBi" id="9_" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:8442470881495869651" />
                  <node concept="37vLTw" id="9B" role="2Oq$k0">
                    <ref role="3cqZAo" node="9e" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="9C" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="9A" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                  <uo k="s:originTrace" v="n:8442470881495871246" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="9j" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495871460" />
          <node concept="2OqwBi" id="9D" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881495871460" />
            <node concept="37vLTw" id="9E" role="2Oq$k0">
              <ref role="3cqZAo" node="9p" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881495871460" />
            </node>
            <node concept="liA8E" id="9F" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:8442470881495871460" />
              <node concept="Xl_RD" id="9G" role="37wK5m">
                <property role="Xl_RC" value=":" />
                <uo k="s:originTrace" v="n:8442470881495871460" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="9k" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495871569" />
          <node concept="2OqwBi" id="9H" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881495871569" />
            <node concept="37vLTw" id="9I" role="2Oq$k0">
              <ref role="3cqZAo" node="9p" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881495871569" />
            </node>
            <node concept="liA8E" id="9J" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:8442470881495871569" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="9l" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495871755" />
          <node concept="2OqwBi" id="9K" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881495871755" />
            <node concept="2OqwBi" id="9L" role="2Oq$k0">
              <uo k="s:originTrace" v="n:8442470881495871755" />
              <node concept="2OqwBi" id="9N" role="2Oq$k0">
                <uo k="s:originTrace" v="n:8442470881495871755" />
                <node concept="37vLTw" id="9P" role="2Oq$k0">
                  <ref role="3cqZAo" node="9e" resolve="ctx" />
                  <uo k="s:originTrace" v="n:8442470881495871755" />
                </node>
                <node concept="liA8E" id="9Q" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:8442470881495871755" />
                </node>
              </node>
              <node concept="liA8E" id="9O" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:8442470881495871755" />
              </node>
            </node>
            <node concept="liA8E" id="9M" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:8442470881495871755" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="9m" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495871806" />
          <node concept="2OqwBi" id="9R" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881495871806" />
            <node concept="37vLTw" id="9S" role="2Oq$k0">
              <ref role="3cqZAo" node="9p" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881495871806" />
            </node>
            <node concept="liA8E" id="9T" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:8442470881495871806" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="9n" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495871863" />
          <node concept="2OqwBi" id="9U" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881495871863" />
            <node concept="37vLTw" id="9V" role="2Oq$k0">
              <ref role="3cqZAo" node="9p" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881495871863" />
            </node>
            <node concept="liA8E" id="9W" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:8442470881495871863" />
              <node concept="Xl_RD" id="9X" role="37wK5m">
                <property role="Xl_RC" value="type: boolean" />
                <uo k="s:originTrace" v="n:8442470881495871863" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="9o" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495871755" />
          <node concept="2OqwBi" id="9Y" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881495871755" />
            <node concept="2OqwBi" id="9Z" role="2Oq$k0">
              <uo k="s:originTrace" v="n:8442470881495871755" />
              <node concept="2OqwBi" id="a1" role="2Oq$k0">
                <uo k="s:originTrace" v="n:8442470881495871755" />
                <node concept="37vLTw" id="a3" role="2Oq$k0">
                  <ref role="3cqZAo" node="9e" resolve="ctx" />
                  <uo k="s:originTrace" v="n:8442470881495871755" />
                </node>
                <node concept="liA8E" id="a4" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:8442470881495871755" />
                </node>
              </node>
              <node concept="liA8E" id="a2" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:8442470881495871755" />
              </node>
            </node>
            <node concept="liA8E" id="a0" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:8442470881495871755" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="9e" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:8442470881495869491" />
        <node concept="3uibUv" id="a5" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:8442470881495869491" />
        </node>
      </node>
      <node concept="2AHcQZ" id="9f" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:8442470881495869491" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="a6">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="Content_TextGen" />
    <uo k="s:originTrace" v="n:4313507821874251631" />
    <node concept="3Tm1VV" id="a7" role="1B3o_S">
      <uo k="s:originTrace" v="n:4313507821874251631" />
    </node>
    <node concept="3uibUv" id="a8" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:4313507821874251631" />
    </node>
    <node concept="3clFb_" id="a9" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:4313507821874251631" />
      <node concept="3cqZAl" id="aa" role="3clF45">
        <uo k="s:originTrace" v="n:4313507821874251631" />
      </node>
      <node concept="3Tm1VV" id="ab" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821874251631" />
      </node>
      <node concept="3clFbS" id="ac" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821874251631" />
        <node concept="3cpWs8" id="af" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874251631" />
          <node concept="3cpWsn" id="ax" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:4313507821874251631" />
            <node concept="3uibUv" id="ay" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:4313507821874251631" />
            </node>
            <node concept="2ShNRf" id="az" role="33vP2m">
              <uo k="s:originTrace" v="n:4313507821874251631" />
              <node concept="1pGfFk" id="a$" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:4313507821874251631" />
                <node concept="37vLTw" id="a_" role="37wK5m">
                  <ref role="3cqZAo" node="ad" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874251631" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ag" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874626760" />
          <node concept="2OqwBi" id="aA" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874626760" />
            <node concept="37vLTw" id="aB" role="2Oq$k0">
              <ref role="3cqZAo" node="ax" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874626760" />
            </node>
            <node concept="liA8E" id="aC" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874626760" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ah" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874251684" />
          <node concept="2OqwBi" id="aD" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874251684" />
            <node concept="37vLTw" id="aE" role="2Oq$k0">
              <ref role="3cqZAo" node="ax" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874251684" />
            </node>
            <node concept="liA8E" id="aF" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874251684" />
              <node concept="2OqwBi" id="aG" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821874253952" />
                <node concept="2OqwBi" id="aH" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874252172" />
                  <node concept="2OqwBi" id="aJ" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:4313507821874251736" />
                    <node concept="37vLTw" id="aL" role="2Oq$k0">
                      <ref role="3cqZAo" node="ad" resolve="ctx" />
                    </node>
                    <node concept="liA8E" id="aM" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                    </node>
                  </node>
                  <node concept="3TrcHB" id="aK" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iB$7L" resolve="mediaType" />
                    <uo k="s:originTrace" v="n:4313507821874253113" />
                  </node>
                </node>
                <node concept="liA8E" id="aI" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.toString()" resolve="toString" />
                  <uo k="s:originTrace" v="n:4313507821874254965" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ai" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874255212" />
          <node concept="2OqwBi" id="aN" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874255212" />
            <node concept="37vLTw" id="aO" role="2Oq$k0">
              <ref role="3cqZAo" node="ax" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874255212" />
            </node>
            <node concept="liA8E" id="aP" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874255212" />
              <node concept="Xl_RD" id="aQ" role="37wK5m">
                <property role="Xl_RC" value=":" />
                <uo k="s:originTrace" v="n:4313507821874255212" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="aj" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874255368" />
          <node concept="2OqwBi" id="aR" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874255368" />
            <node concept="37vLTw" id="aS" role="2Oq$k0">
              <ref role="3cqZAo" node="ax" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874255368" />
            </node>
            <node concept="liA8E" id="aT" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821874255368" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ak" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874255506" />
          <node concept="2OqwBi" id="aU" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874255506" />
            <node concept="2OqwBi" id="aV" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874255506" />
              <node concept="2OqwBi" id="aX" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874255506" />
                <node concept="37vLTw" id="aZ" role="2Oq$k0">
                  <ref role="3cqZAo" node="ad" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874255506" />
                </node>
                <node concept="liA8E" id="b0" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874255506" />
                </node>
              </node>
              <node concept="liA8E" id="aY" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874255506" />
              </node>
            </node>
            <node concept="liA8E" id="aW" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874255506" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="al" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874626906" />
          <node concept="2OqwBi" id="b1" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874626906" />
            <node concept="37vLTw" id="b2" role="2Oq$k0">
              <ref role="3cqZAo" node="ax" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874626906" />
            </node>
            <node concept="liA8E" id="b3" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874626906" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="am" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874255594" />
          <node concept="2OqwBi" id="b4" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874255594" />
            <node concept="37vLTw" id="b5" role="2Oq$k0">
              <ref role="3cqZAo" node="ax" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874255594" />
            </node>
            <node concept="liA8E" id="b6" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874255594" />
              <node concept="Xl_RD" id="b7" role="37wK5m">
                <property role="Xl_RC" value="schema:" />
                <uo k="s:originTrace" v="n:4313507821874255594" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="an" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874255659" />
          <node concept="2OqwBi" id="b8" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874255659" />
            <node concept="37vLTw" id="b9" role="2Oq$k0">
              <ref role="3cqZAo" node="ax" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874255659" />
            </node>
            <node concept="liA8E" id="ba" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821874255659" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ao" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874255721" />
          <node concept="2OqwBi" id="bb" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874255721" />
            <node concept="2OqwBi" id="bc" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874255721" />
              <node concept="2OqwBi" id="be" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874255721" />
                <node concept="37vLTw" id="bg" role="2Oq$k0">
                  <ref role="3cqZAo" node="ad" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874255721" />
                </node>
                <node concept="liA8E" id="bh" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874255721" />
                </node>
              </node>
              <node concept="liA8E" id="bf" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874255721" />
              </node>
            </node>
            <node concept="liA8E" id="bd" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874255721" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ap" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874626996" />
          <node concept="2OqwBi" id="bi" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874626996" />
            <node concept="37vLTw" id="bj" role="2Oq$k0">
              <ref role="3cqZAo" node="ax" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874626996" />
            </node>
            <node concept="liA8E" id="bk" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874626996" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="aq" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874259437" />
          <node concept="2OqwBi" id="bl" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874259437" />
            <node concept="37vLTw" id="bm" role="2Oq$k0">
              <ref role="3cqZAo" node="ax" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874259437" />
            </node>
            <node concept="liA8E" id="bn" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874259437" />
              <node concept="Xl_RD" id="bo" role="37wK5m">
                <property role="Xl_RC" value="$ref: " />
                <uo k="s:originTrace" v="n:4313507821874259437" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ar" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874259794" />
          <node concept="2OqwBi" id="bp" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874259794" />
            <node concept="37vLTw" id="bq" role="2Oq$k0">
              <ref role="3cqZAo" node="ax" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874259794" />
            </node>
            <node concept="liA8E" id="br" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874259794" />
              <node concept="Xl_RD" id="bs" role="37wK5m">
                <property role="Xl_RC" value="'#/components/schemas/" />
                <uo k="s:originTrace" v="n:4313507821874259794" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="as" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874255756" />
          <node concept="2OqwBi" id="bt" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874255756" />
            <node concept="37vLTw" id="bu" role="2Oq$k0">
              <ref role="3cqZAo" node="ax" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874255756" />
            </node>
            <node concept="liA8E" id="bv" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874255756" />
              <node concept="2OqwBi" id="bw" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821874258066" />
                <node concept="2OqwBi" id="bx" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874256244" />
                  <node concept="2OqwBi" id="bz" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:4313507821874255808" />
                    <node concept="37vLTw" id="b_" role="2Oq$k0">
                      <ref role="3cqZAo" node="ad" resolve="ctx" />
                    </node>
                    <node concept="liA8E" id="bA" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                    </node>
                  </node>
                  <node concept="3TrEf2" id="b$" role="2OqNvi">
                    <ref role="3Tt5mk" to="7thy:4favA_iBNSA" resolve="schema" />
                    <uo k="s:originTrace" v="n:4313507821874257290" />
                  </node>
                </node>
                <node concept="3TrcHB" id="by" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                  <uo k="s:originTrace" v="n:4313507821874259227" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="at" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496272518" />
          <node concept="2OqwBi" id="bB" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496272518" />
            <node concept="37vLTw" id="bC" role="2Oq$k0">
              <ref role="3cqZAo" node="ax" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496272518" />
            </node>
            <node concept="liA8E" id="bD" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:8442470881496272518" />
              <node concept="Xl_RD" id="bE" role="37wK5m">
                <property role="Xl_RC" value="'" />
                <uo k="s:originTrace" v="n:8442470881496272518" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="au" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874261176" />
          <node concept="2OqwBi" id="bF" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874261176" />
            <node concept="37vLTw" id="bG" role="2Oq$k0">
              <ref role="3cqZAo" node="ax" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874261176" />
            </node>
            <node concept="liA8E" id="bH" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821874261176" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="av" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874255721" />
          <node concept="2OqwBi" id="bI" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874255721" />
            <node concept="2OqwBi" id="bJ" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874255721" />
              <node concept="2OqwBi" id="bL" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874255721" />
                <node concept="37vLTw" id="bN" role="2Oq$k0">
                  <ref role="3cqZAo" node="ad" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874255721" />
                </node>
                <node concept="liA8E" id="bO" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874255721" />
                </node>
              </node>
              <node concept="liA8E" id="bM" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874255721" />
              </node>
            </node>
            <node concept="liA8E" id="bK" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874255721" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="aw" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874255506" />
          <node concept="2OqwBi" id="bP" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874255506" />
            <node concept="2OqwBi" id="bQ" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874255506" />
              <node concept="2OqwBi" id="bS" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874255506" />
                <node concept="37vLTw" id="bU" role="2Oq$k0">
                  <ref role="3cqZAo" node="ad" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874255506" />
                </node>
                <node concept="liA8E" id="bV" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874255506" />
                </node>
              </node>
              <node concept="liA8E" id="bT" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874255506" />
              </node>
            </node>
            <node concept="liA8E" id="bR" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874255506" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="ad" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:4313507821874251631" />
        <node concept="3uibUv" id="bW" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:4313507821874251631" />
        </node>
      </node>
      <node concept="2AHcQZ" id="ae" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821874251631" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="bX">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="DeleteOperationObject_TextGen" />
    <uo k="s:originTrace" v="n:4313507821874235386" />
    <node concept="3Tm1VV" id="bY" role="1B3o_S">
      <uo k="s:originTrace" v="n:4313507821874235386" />
    </node>
    <node concept="3uibUv" id="bZ" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:4313507821874235386" />
    </node>
    <node concept="3clFb_" id="c0" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:4313507821874235386" />
      <node concept="3cqZAl" id="c1" role="3clF45">
        <uo k="s:originTrace" v="n:4313507821874235386" />
      </node>
      <node concept="3Tm1VV" id="c2" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821874235386" />
      </node>
      <node concept="3clFbS" id="c3" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821874235386" />
        <node concept="3cpWs8" id="c6" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874235386" />
          <node concept="3cpWsn" id="cn" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:4313507821874235386" />
            <node concept="3uibUv" id="co" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:4313507821874235386" />
            </node>
            <node concept="2ShNRf" id="cp" role="33vP2m">
              <uo k="s:originTrace" v="n:4313507821874235386" />
              <node concept="1pGfFk" id="cq" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:4313507821874235386" />
                <node concept="37vLTw" id="cr" role="37wK5m">
                  <ref role="3cqZAo" node="c4" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874235386" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="c7" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874644578" />
          <node concept="2OqwBi" id="cs" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874644578" />
            <node concept="37vLTw" id="ct" role="2Oq$k0">
              <ref role="3cqZAo" node="cn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874644578" />
            </node>
            <node concept="liA8E" id="cu" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874644578" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="c8" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874235406" />
          <node concept="2OqwBi" id="cv" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874235406" />
            <node concept="37vLTw" id="cw" role="2Oq$k0">
              <ref role="3cqZAo" node="cn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874235406" />
            </node>
            <node concept="liA8E" id="cx" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874235406" />
              <node concept="Xl_RD" id="cy" role="37wK5m">
                <property role="Xl_RC" value="delete:\n" />
                <uo k="s:originTrace" v="n:4313507821874235406" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="c9" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874235407" />
          <node concept="2OqwBi" id="cz" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874235407" />
            <node concept="2OqwBi" id="c$" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874235407" />
              <node concept="2OqwBi" id="cA" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874235407" />
                <node concept="37vLTw" id="cC" role="2Oq$k0">
                  <ref role="3cqZAo" node="c4" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874235407" />
                </node>
                <node concept="liA8E" id="cD" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874235407" />
                </node>
              </node>
              <node concept="liA8E" id="cB" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874235407" />
              </node>
            </node>
            <node concept="liA8E" id="c_" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874235407" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ca" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874644762" />
          <node concept="2OqwBi" id="cE" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874644762" />
            <node concept="37vLTw" id="cF" role="2Oq$k0">
              <ref role="3cqZAo" node="cn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874644762" />
            </node>
            <node concept="liA8E" id="cG" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874644762" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="cb" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874235410" />
          <node concept="2OqwBi" id="cH" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874235410" />
            <node concept="37vLTw" id="cI" role="2Oq$k0">
              <ref role="3cqZAo" node="cn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874235410" />
            </node>
            <node concept="liA8E" id="cJ" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874235410" />
              <node concept="Xl_RD" id="cK" role="37wK5m">
                <property role="Xl_RC" value="summary: " />
                <uo k="s:originTrace" v="n:4313507821874235410" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="cc" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874235411" />
          <node concept="2OqwBi" id="cL" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874235411" />
            <node concept="37vLTw" id="cM" role="2Oq$k0">
              <ref role="3cqZAo" node="cn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874235411" />
            </node>
            <node concept="liA8E" id="cN" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874235411" />
              <node concept="2OqwBi" id="cO" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821874235412" />
                <node concept="2OqwBi" id="cP" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874235413" />
                  <node concept="37vLTw" id="cR" role="2Oq$k0">
                    <ref role="3cqZAo" node="c4" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="cS" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="cQ" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:3JW8_axN3fB" resolve="summary" />
                  <uo k="s:originTrace" v="n:4313507821874235414" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="cd" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874235415" />
          <node concept="2OqwBi" id="cT" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874235415" />
            <node concept="37vLTw" id="cU" role="2Oq$k0">
              <ref role="3cqZAo" node="cn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874235415" />
            </node>
            <node concept="liA8E" id="cV" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821874235415" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="ce" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600401610" />
          <node concept="3clFbS" id="cW" role="3clFbx">
            <uo k="s:originTrace" v="n:227067257600401611" />
            <node concept="3clFbF" id="cY" role="3cqZAp">
              <uo k="s:originTrace" v="n:227067257600401612" />
              <node concept="2OqwBi" id="cZ" role="3clFbG">
                <node concept="37vLTw" id="d0" role="2Oq$k0">
                  <ref role="3cqZAo" node="cn" resolve="tgs" />
                </node>
                <node concept="liA8E" id="d1" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.reportError(java.lang.String)" resolve="reportError" />
                  <node concept="Xl_RD" id="d2" role="37wK5m">
                    <property role="Xl_RC" value="summary is required" />
                    <uo k="s:originTrace" v="n:227067257600401613" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="cX" role="3clFbw">
            <uo k="s:originTrace" v="n:227067257600401614" />
            <node concept="2OqwBi" id="d3" role="2Oq$k0">
              <uo k="s:originTrace" v="n:227067257600401615" />
              <node concept="2OqwBi" id="d5" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600401616" />
                <node concept="37vLTw" id="d7" role="2Oq$k0">
                  <ref role="3cqZAo" node="c4" resolve="ctx" />
                </node>
                <node concept="liA8E" id="d8" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="d6" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axN3fB" resolve="summary" />
                <uo k="s:originTrace" v="n:227067257600401617" />
              </node>
            </node>
            <node concept="17RlXB" id="d4" role="2OqNvi">
              <uo k="s:originTrace" v="n:227067257600401618" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="cf" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600401486" />
        </node>
        <node concept="3clFbJ" id="cg" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874463692" />
          <node concept="3clFbS" id="d9" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874463693" />
            <node concept="3clFbF" id="db" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874644894" />
              <node concept="2OqwBi" id="df" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874644894" />
                <node concept="37vLTw" id="dg" role="2Oq$k0">
                  <ref role="3cqZAo" node="cn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874644894" />
                </node>
                <node concept="liA8E" id="dh" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874644894" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="dc" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874463695" />
              <node concept="2OqwBi" id="di" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874463695" />
                <node concept="37vLTw" id="dj" role="2Oq$k0">
                  <ref role="3cqZAo" node="cn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874463695" />
                </node>
                <node concept="liA8E" id="dk" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874463695" />
                  <node concept="Xl_RD" id="dl" role="37wK5m">
                    <property role="Xl_RC" value="description: " />
                    <uo k="s:originTrace" v="n:4313507821874463695" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="dd" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874463696" />
              <node concept="2OqwBi" id="dm" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874463696" />
                <node concept="37vLTw" id="dn" role="2Oq$k0">
                  <ref role="3cqZAo" node="cn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874463696" />
                </node>
                <node concept="liA8E" id="do" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874463696" />
                  <node concept="2OqwBi" id="dp" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874463697" />
                    <node concept="2OqwBi" id="dq" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:4313507821874463698" />
                      <node concept="37vLTw" id="ds" role="2Oq$k0">
                        <ref role="3cqZAo" node="c4" resolve="ctx" />
                      </node>
                      <node concept="liA8E" id="dt" role="2OqNvi">
                        <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="dr" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:3JW8_axN3fI" resolve="description" />
                      <uo k="s:originTrace" v="n:4313507821874463699" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="de" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874463700" />
              <node concept="2OqwBi" id="du" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874463700" />
                <node concept="37vLTw" id="dv" role="2Oq$k0">
                  <ref role="3cqZAo" node="cn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874463700" />
                </node>
                <node concept="liA8E" id="dw" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874463700" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="da" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874463701" />
            <node concept="2OqwBi" id="dx" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874463702" />
              <node concept="2OqwBi" id="dz" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874463703" />
                <node concept="37vLTw" id="d_" role="2Oq$k0">
                  <ref role="3cqZAo" node="c4" resolve="ctx" />
                </node>
                <node concept="liA8E" id="dA" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="d$" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axN3fI" resolve="description" />
                <uo k="s:originTrace" v="n:4313507821874463704" />
              </node>
            </node>
            <node concept="17RvpY" id="dy" role="2OqNvi">
              <uo k="s:originTrace" v="n:4313507821874463705" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ch" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874645026" />
          <node concept="2OqwBi" id="dB" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874645026" />
            <node concept="37vLTw" id="dC" role="2Oq$k0">
              <ref role="3cqZAo" node="cn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874645026" />
            </node>
            <node concept="liA8E" id="dD" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874645026" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ci" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874235424" />
          <node concept="2OqwBi" id="dE" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874235424" />
            <node concept="37vLTw" id="dF" role="2Oq$k0">
              <ref role="3cqZAo" node="cn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874235424" />
            </node>
            <node concept="liA8E" id="dG" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874235424" />
              <node concept="Xl_RD" id="dH" role="37wK5m">
                <property role="Xl_RC" value="responses:\n" />
                <uo k="s:originTrace" v="n:4313507821874235424" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="cj" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874235425" />
          <node concept="2OqwBi" id="dI" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874235425" />
            <node concept="2OqwBi" id="dJ" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874235425" />
              <node concept="2OqwBi" id="dL" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874235425" />
                <node concept="37vLTw" id="dN" role="2Oq$k0">
                  <ref role="3cqZAo" node="c4" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874235425" />
                </node>
                <node concept="liA8E" id="dO" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874235425" />
                </node>
              </node>
              <node concept="liA8E" id="dM" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874235425" />
              </node>
            </node>
            <node concept="liA8E" id="dK" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874235425" />
            </node>
          </node>
        </node>
        <node concept="1DcWWT" id="ck" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874462121" />
          <node concept="3clFbS" id="dP" role="2LFqv$">
            <uo k="s:originTrace" v="n:4313507821874462121" />
            <node concept="3clFbF" id="dS" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874462121" />
              <node concept="2OqwBi" id="dT" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874462121" />
                <node concept="37vLTw" id="dU" role="2Oq$k0">
                  <ref role="3cqZAo" node="cn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874462121" />
                </node>
                <node concept="liA8E" id="dV" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.appendNode(org.jetbrains.mps.openapi.model.SNode)" resolve="appendNode" />
                  <uo k="s:originTrace" v="n:4313507821874462121" />
                  <node concept="37vLTw" id="dW" role="37wK5m">
                    <ref role="3cqZAo" node="dQ" resolve="item" />
                    <uo k="s:originTrace" v="n:4313507821874462121" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="dQ" role="1Duv9x">
            <property role="TrG5h" value="item" />
            <uo k="s:originTrace" v="n:4313507821874462121" />
            <node concept="3Tqbb2" id="dX" role="1tU5fm">
              <uo k="s:originTrace" v="n:4313507821874462121" />
            </node>
          </node>
          <node concept="2OqwBi" id="dR" role="1DdaDG">
            <uo k="s:originTrace" v="n:4313507821874462539" />
            <node concept="2OqwBi" id="dY" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874462141" />
              <node concept="37vLTw" id="e0" role="2Oq$k0">
                <ref role="3cqZAo" node="c4" resolve="ctx" />
              </node>
              <node concept="liA8E" id="e1" role="2OqNvi">
                <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
              </node>
            </node>
            <node concept="3Tsc0h" id="dZ" role="2OqNvi">
              <ref role="3TtcxE" to="7thy:4favA_iAyf8" resolve="responses" />
              <uo k="s:originTrace" v="n:4313507821874463666" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="cl" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874235425" />
          <node concept="2OqwBi" id="e2" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874235425" />
            <node concept="2OqwBi" id="e3" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874235425" />
              <node concept="2OqwBi" id="e5" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874235425" />
                <node concept="37vLTw" id="e7" role="2Oq$k0">
                  <ref role="3cqZAo" node="c4" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874235425" />
                </node>
                <node concept="liA8E" id="e8" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874235425" />
                </node>
              </node>
              <node concept="liA8E" id="e6" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874235425" />
              </node>
            </node>
            <node concept="liA8E" id="e4" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874235425" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="cm" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874235407" />
          <node concept="2OqwBi" id="e9" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874235407" />
            <node concept="2OqwBi" id="ea" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874235407" />
              <node concept="2OqwBi" id="ec" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874235407" />
                <node concept="37vLTw" id="ee" role="2Oq$k0">
                  <ref role="3cqZAo" node="c4" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874235407" />
                </node>
                <node concept="liA8E" id="ef" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874235407" />
                </node>
              </node>
              <node concept="liA8E" id="ed" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874235407" />
              </node>
            </node>
            <node concept="liA8E" id="eb" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874235407" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="c4" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:4313507821874235386" />
        <node concept="3uibUv" id="eg" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:4313507821874235386" />
        </node>
      </node>
      <node concept="2AHcQZ" id="c5" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821874235386" />
      </node>
    </node>
  </node>
  <node concept="39dXUE" id="eh">
    <node concept="39e2AJ" id="ei" role="39e2AI">
      <property role="39e3Y2" value="GetExtension" />
      <node concept="39e2AG" id="em" role="39e3Y0">
        <ref role="39e2AK" to="th05:3JsE8aiFqkH" resolve="OpenApi_TextGen" />
        <node concept="385nmt" id="en" role="385vvn">
          <property role="385vuF" value="OpenApi_TextGen" />
          <node concept="3u3nmq" id="ep" role="385v07">
            <property role="3u3nmv" value="4313507821874160941" />
          </node>
        </node>
        <node concept="39e2AT" id="eo" role="39e2AY">
          <ref role="39e2AS" node="JB" resolve="getFileExtension_OpenApi" />
        </node>
      </node>
    </node>
    <node concept="39e2AJ" id="ej" role="39e2AI">
      <property role="39e3Y2" value="GetFilename" />
      <node concept="39e2AG" id="eq" role="39e3Y0">
        <ref role="39e2AK" to="th05:3JsE8aiFqkH" resolve="OpenApi_TextGen" />
        <node concept="385nmt" id="er" role="385vvn">
          <property role="385vuF" value="OpenApi_TextGen" />
          <node concept="3u3nmq" id="et" role="385v07">
            <property role="3u3nmv" value="4313507821874160941" />
          </node>
        </node>
        <node concept="39e2AT" id="es" role="39e2AY">
          <ref role="39e2AS" node="JA" resolve="getFileName_OpenApi" />
        </node>
      </node>
    </node>
    <node concept="39e2AJ" id="ek" role="39e2AI">
      <property role="39e3Y2" value="TextGenClass" />
      <node concept="39e2AG" id="eu" role="39e3Y0">
        <ref role="39e2AK" to="th05:7kDFC_u8VzK" resolve="AllOfSchema_TextGen" />
        <node concept="385nmt" id="eK" role="385vvn">
          <property role="385vuF" value="AllOfSchema_TextGen" />
          <node concept="3u3nmq" id="eM" role="385v07">
            <property role="3u3nmv" value="8442470881495857392" />
          </node>
        </node>
        <node concept="39e2AT" id="eL" role="39e2AY">
          <ref role="39e2AS" node="0" resolve="AllOfSchema_TextGen" />
        </node>
      </node>
      <node concept="39e2AG" id="ev" role="39e3Y0">
        <ref role="39e2AK" to="th05:3JsE8aiJBwW" resolve="AnyOfSchema_TextGen" />
        <node concept="385nmt" id="eN" role="385vvn">
          <property role="385vuF" value="AnyOfSchema_TextGen" />
          <node concept="3u3nmq" id="eP" role="385v07">
            <property role="3u3nmv" value="4313507821875263548" />
          </node>
        </node>
        <node concept="39e2AT" id="eO" role="39e2AY">
          <ref role="39e2AS" node="2C" resolve="AnyOfSchema_TextGen" />
        </node>
      </node>
      <node concept="39e2AG" id="ew" role="39e3Y0">
        <ref role="39e2AK" to="th05:3JsE8aiIjZR" resolve="ArraySchema_TextGen" />
        <node concept="385nmt" id="eQ" role="385vvn">
          <property role="385vuF" value="ArraySchema_TextGen" />
          <node concept="3u3nmq" id="eS" role="385v07">
            <property role="3u3nmv" value="4313507821874921463" />
          </node>
        </node>
        <node concept="39e2AT" id="eR" role="39e2AY">
          <ref role="39e2AS" node="5g" resolve="ArraySchema_TextGen" />
        </node>
      </node>
      <node concept="39e2AG" id="ex" role="39e3Y0">
        <ref role="39e2AK" to="th05:7kDFC_u8YwN" resolve="BooleanSchema_TextGen" />
        <node concept="385nmt" id="eT" role="385vvn">
          <property role="385vuF" value="BooleanSchema_TextGen" />
          <node concept="3u3nmq" id="eV" role="385v07">
            <property role="3u3nmv" value="8442470881495869491" />
          </node>
        </node>
        <node concept="39e2AT" id="eU" role="39e2AY">
          <ref role="39e2AS" node="97" resolve="BooleanSchema_TextGen" />
        </node>
      </node>
      <node concept="39e2AG" id="ey" role="39e3Y0">
        <ref role="39e2AK" to="th05:3JsE8aiFKtJ" resolve="Content_TextGen" />
        <node concept="385nmt" id="eW" role="385vvn">
          <property role="385vuF" value="Content_TextGen" />
          <node concept="3u3nmq" id="eY" role="385v07">
            <property role="3u3nmv" value="4313507821874251631" />
          </node>
        </node>
        <node concept="39e2AT" id="eX" role="39e2AY">
          <ref role="39e2AS" node="a6" resolve="Content_TextGen" />
        </node>
      </node>
      <node concept="39e2AG" id="ez" role="39e3Y0">
        <ref role="39e2AK" to="th05:3JsE8aiFGvU" resolve="DeleteOperationObject_TextGen" />
        <node concept="385nmt" id="eZ" role="385vvn">
          <property role="385vuF" value="DeleteOperationObject_TextGen" />
          <node concept="3u3nmq" id="f1" role="385v07">
            <property role="3u3nmv" value="4313507821874235386" />
          </node>
        </node>
        <node concept="39e2AT" id="f0" role="39e2AY">
          <ref role="39e2AS" node="bX" resolve="DeleteOperationObject_TextGen" />
        </node>
      </node>
      <node concept="39e2AG" id="e$" role="39e3Y0">
        <ref role="39e2AK" to="th05:3JsE8aiF__x" resolve="GetOperationObject_TextGen" />
        <node concept="385nmt" id="f2" role="385vvn">
          <property role="385vuF" value="GetOperationObject_TextGen" />
          <node concept="3u3nmq" id="f4" role="385v07">
            <property role="3u3nmv" value="4313507821874207073" />
          </node>
        </node>
        <node concept="39e2AT" id="f3" role="39e2AY">
          <ref role="39e2AS" node="fC" resolve="GetOperationObject_TextGen" />
        </node>
      </node>
      <node concept="39e2AG" id="e_" role="39e3Y0">
        <ref role="39e2AK" to="th05:3JsE8aiIiBQ" resolve="IntegerSchema_TextGen" />
        <node concept="385nmt" id="f5" role="385vvn">
          <property role="385vuF" value="IntegerSchema_TextGen" />
          <node concept="3u3nmq" id="f7" role="385v07">
            <property role="3u3nmv" value="4313507821874915830" />
          </node>
        </node>
        <node concept="39e2AT" id="f6" role="39e2AY">
          <ref role="39e2AS" node="hW" resolve="IntegerSchema_TextGen" />
        </node>
      </node>
      <node concept="39e2AG" id="eA" role="39e3Y0">
        <ref role="39e2AK" to="th05:3JsE8aiJh1g" resolve="ObjectSchemaProperty_TextGen" />
        <node concept="385nmt" id="f8" role="385vvn">
          <property role="385vuF" value="ObjectSchemaProperty_TextGen" />
          <node concept="3u3nmq" id="fa" role="385v07">
            <property role="3u3nmv" value="4313507821875171408" />
          </node>
        </node>
        <node concept="39e2AT" id="f9" role="39e2AY">
          <ref role="39e2AS" node="lw" resolve="ObjectSchemaProperty_TextGen" />
        </node>
      </node>
      <node concept="39e2AG" id="eB" role="39e3Y0">
        <ref role="39e2AK" to="th05:3JsE8aiIZhU" resolve="ObjectSchema_TextGen" />
        <node concept="385nmt" id="fb" role="385vvn">
          <property role="385vuF" value="ObjectSchema_TextGen" />
          <node concept="3u3nmq" id="fd" role="385v07">
            <property role="3u3nmv" value="4313507821875098746" />
          </node>
        </node>
        <node concept="39e2AT" id="fc" role="39e2AY">
          <ref role="39e2AS" node="mS" resolve="ObjectSchema_TextGen" />
        </node>
      </node>
      <node concept="39e2AG" id="eC" role="39e3Y0">
        <ref role="39e2AK" to="th05:7kDFC_u8Vrl" resolve="OneOfSchema_TextGen" />
        <node concept="385nmt" id="fe" role="385vvn">
          <property role="385vuF" value="OneOfSchema_TextGen" />
          <node concept="3u3nmq" id="fg" role="385v07">
            <property role="3u3nmv" value="8442470881495856853" />
          </node>
        </node>
        <node concept="39e2AT" id="ff" role="39e2AY">
          <ref role="39e2AS" node="rt" resolve="OneOfSchema_TextGen" />
        </node>
      </node>
      <node concept="39e2AG" id="eD" role="39e3Y0">
        <ref role="39e2AK" to="th05:3JsE8aiFy8c" resolve="OpenApiInfo_TextGen" />
        <node concept="385nmt" id="fh" role="385vvn">
          <property role="385vuF" value="OpenApiInfo_TextGen" />
          <node concept="3u3nmq" id="fj" role="385v07">
            <property role="3u3nmv" value="4313507821874192908" />
          </node>
        </node>
        <node concept="39e2AT" id="fi" role="39e2AY">
          <ref role="39e2AS" node="u6" resolve="OpenApiInfo_TextGen" />
        </node>
      </node>
      <node concept="39e2AG" id="eE" role="39e3Y0">
        <ref role="39e2AK" to="th05:3JsE8aiFqkH" resolve="OpenApi_TextGen" />
        <node concept="385nmt" id="fk" role="385vvn">
          <property role="385vuF" value="OpenApi_TextGen" />
          <node concept="3u3nmq" id="fm" role="385v07">
            <property role="3u3nmv" value="4313507821874160941" />
          </node>
        </node>
        <node concept="39e2AT" id="fl" role="39e2AY">
          <ref role="39e2AS" node="vZ" resolve="OpenApi_TextGen" />
        </node>
      </node>
      <node concept="39e2AG" id="eF" role="39e3Y0">
        <ref role="39e2AK" to="th05:3JsE8aiF$la" resolve="PathObject_TextGen" />
        <node concept="385nmt" id="fn" role="385vvn">
          <property role="385vuF" value="PathObject_TextGen" />
          <node concept="3u3nmq" id="fp" role="385v07">
            <property role="3u3nmv" value="4313507821874201930" />
          </node>
        </node>
        <node concept="39e2AT" id="fo" role="39e2AY">
          <ref role="39e2AS" node="yZ" resolve="PathObject_TextGen" />
        </node>
      </node>
      <node concept="39e2AG" id="eG" role="39e3Y0">
        <ref role="39e2AK" to="th05:3JsE8aiFGfw" resolve="PostOperationObject_TextGen" />
        <node concept="385nmt" id="fq" role="385vvn">
          <property role="385vuF" value="PostOperationObject_TextGen" />
          <node concept="3u3nmq" id="fs" role="385v07">
            <property role="3u3nmv" value="4313507821874234336" />
          </node>
        </node>
        <node concept="39e2AT" id="fr" role="39e2AY">
          <ref role="39e2AS" node="$d" resolve="PostOperationObject_TextGen" />
        </node>
      </node>
      <node concept="39e2AG" id="eH" role="39e3Y0">
        <ref role="39e2AK" to="th05:3JsE8aiFGp2" resolve="PutOperationObject_TextGen" />
        <node concept="385nmt" id="ft" role="385vvn">
          <property role="385vuF" value="PutOperationObject_TextGen" />
          <node concept="3u3nmq" id="fv" role="385v07">
            <property role="3u3nmv" value="4313507821874234946" />
          </node>
        </node>
        <node concept="39e2AT" id="fu" role="39e2AY">
          <ref role="39e2AS" node="Ax" resolve="PutOperationObject_TextGen" />
        </node>
      </node>
      <node concept="39e2AG" id="eI" role="39e3Y0">
        <ref role="39e2AK" to="th05:3JsE8aiFGBg" resolve="Response_TextGen" />
        <node concept="385nmt" id="fw" role="385vvn">
          <property role="385vuF" value="Response_TextGen" />
          <node concept="3u3nmq" id="fy" role="385v07">
            <property role="3u3nmv" value="4313507821874235856" />
          </node>
        </node>
        <node concept="39e2AT" id="fx" role="39e2AY">
          <ref role="39e2AS" node="CP" resolve="Response_TextGen" />
        </node>
      </node>
      <node concept="39e2AG" id="eJ" role="39e3Y0">
        <ref role="39e2AK" to="th05:3JsE8aiFcA0" resolve="StringSchema_TextGen" />
        <node concept="385nmt" id="fz" role="385vvn">
          <property role="385vuF" value="StringSchema_TextGen" />
          <node concept="3u3nmq" id="f_" role="385v07">
            <property role="3u3nmv" value="4313507821874104704" />
          </node>
        </node>
        <node concept="39e2AT" id="f$" role="39e2AY">
          <ref role="39e2AS" node="Fp" resolve="StringSchema_TextGen" />
        </node>
      </node>
    </node>
    <node concept="39e2AJ" id="el" role="39e2AI">
      <property role="39e3Y2" value="TextGenAspectDescriptorCons" />
      <node concept="39e2AG" id="fA" role="39e3Y0">
        <property role="2mV_xN" value="true" />
        <node concept="39e2AT" id="fB" role="39e2AY">
          <ref role="39e2AS" node="Jv" resolve="TextGenAspectDescriptor" />
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="fC">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="GetOperationObject_TextGen" />
    <uo k="s:originTrace" v="n:4313507821874207073" />
    <node concept="3Tm1VV" id="fD" role="1B3o_S">
      <uo k="s:originTrace" v="n:4313507821874207073" />
    </node>
    <node concept="3uibUv" id="fE" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:4313507821874207073" />
    </node>
    <node concept="3clFb_" id="fF" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:4313507821874207073" />
      <node concept="3cqZAl" id="fG" role="3clF45">
        <uo k="s:originTrace" v="n:4313507821874207073" />
      </node>
      <node concept="3Tm1VV" id="fH" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821874207073" />
      </node>
      <node concept="3clFbS" id="fI" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821874207073" />
        <node concept="3cpWs8" id="fL" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874207073" />
          <node concept="3cpWsn" id="g2" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:4313507821874207073" />
            <node concept="3uibUv" id="g3" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:4313507821874207073" />
            </node>
            <node concept="2ShNRf" id="g4" role="33vP2m">
              <uo k="s:originTrace" v="n:4313507821874207073" />
              <node concept="1pGfFk" id="g5" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:4313507821874207073" />
                <node concept="37vLTw" id="g6" role="37wK5m">
                  <ref role="3cqZAo" node="fJ" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874207073" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="fM" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874603705" />
          <node concept="2OqwBi" id="g7" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874603705" />
            <node concept="37vLTw" id="g8" role="2Oq$k0">
              <ref role="3cqZAo" node="g2" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874603705" />
            </node>
            <node concept="liA8E" id="g9" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874603705" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="fN" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874225774" />
          <node concept="2OqwBi" id="ga" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874225774" />
            <node concept="37vLTw" id="gb" role="2Oq$k0">
              <ref role="3cqZAo" node="g2" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874225774" />
            </node>
            <node concept="liA8E" id="gc" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874225774" />
              <node concept="Xl_RD" id="gd" role="37wK5m">
                <property role="Xl_RC" value="get:\n" />
                <uo k="s:originTrace" v="n:4313507821874225774" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="fO" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874227971" />
          <node concept="2OqwBi" id="ge" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874227971" />
            <node concept="2OqwBi" id="gf" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874227971" />
              <node concept="2OqwBi" id="gh" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874227971" />
                <node concept="37vLTw" id="gj" role="2Oq$k0">
                  <ref role="3cqZAo" node="fJ" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874227971" />
                </node>
                <node concept="liA8E" id="gk" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874227971" />
                </node>
              </node>
              <node concept="liA8E" id="gi" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874227971" />
              </node>
            </node>
            <node concept="liA8E" id="gg" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874227971" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="fP" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874622431" />
          <node concept="2OqwBi" id="gl" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874622431" />
            <node concept="37vLTw" id="gm" role="2Oq$k0">
              <ref role="3cqZAo" node="g2" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874622431" />
            </node>
            <node concept="liA8E" id="gn" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874622431" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="fQ" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874228256" />
          <node concept="2OqwBi" id="go" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874228256" />
            <node concept="37vLTw" id="gp" role="2Oq$k0">
              <ref role="3cqZAo" node="g2" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874228256" />
            </node>
            <node concept="liA8E" id="gq" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874228256" />
              <node concept="Xl_RD" id="gr" role="37wK5m">
                <property role="Xl_RC" value="summary: " />
                <uo k="s:originTrace" v="n:4313507821874228256" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="fR" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874225871" />
          <node concept="2OqwBi" id="gs" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874225871" />
            <node concept="37vLTw" id="gt" role="2Oq$k0">
              <ref role="3cqZAo" node="g2" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874225871" />
            </node>
            <node concept="liA8E" id="gu" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874225871" />
              <node concept="2OqwBi" id="gv" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821874226393" />
                <node concept="2OqwBi" id="gw" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874225923" />
                  <node concept="37vLTw" id="gy" role="2Oq$k0">
                    <ref role="3cqZAo" node="fJ" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="gz" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="gx" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:3JW8_axN3fB" resolve="summary" />
                  <uo k="s:originTrace" v="n:4313507821874227766" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="fS" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874228369" />
          <node concept="2OqwBi" id="g$" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874228369" />
            <node concept="37vLTw" id="g_" role="2Oq$k0">
              <ref role="3cqZAo" node="g2" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874228369" />
            </node>
            <node concept="liA8E" id="gA" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821874228369" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="fT" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600400106" />
          <node concept="3clFbS" id="gB" role="3clFbx">
            <uo k="s:originTrace" v="n:227067257600400107" />
            <node concept="3clFbF" id="gD" role="3cqZAp">
              <uo k="s:originTrace" v="n:227067257600400108" />
              <node concept="2OqwBi" id="gE" role="3clFbG">
                <node concept="37vLTw" id="gF" role="2Oq$k0">
                  <ref role="3cqZAo" node="g2" resolve="tgs" />
                </node>
                <node concept="liA8E" id="gG" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.reportError(java.lang.String)" resolve="reportError" />
                  <node concept="Xl_RD" id="gH" role="37wK5m">
                    <property role="Xl_RC" value="summary is required" />
                    <uo k="s:originTrace" v="n:227067257600400109" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="gC" role="3clFbw">
            <uo k="s:originTrace" v="n:227067257600400110" />
            <node concept="2OqwBi" id="gI" role="2Oq$k0">
              <uo k="s:originTrace" v="n:227067257600400111" />
              <node concept="2OqwBi" id="gK" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600400112" />
                <node concept="37vLTw" id="gM" role="2Oq$k0">
                  <ref role="3cqZAo" node="fJ" resolve="ctx" />
                </node>
                <node concept="liA8E" id="gN" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="gL" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axN3fB" resolve="summary" />
                <uo k="s:originTrace" v="n:227067257600400113" />
              </node>
            </node>
            <node concept="17RlXB" id="gJ" role="2OqNvi">
              <uo k="s:originTrace" v="n:227067257600400114" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="fU" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600399982" />
        </node>
        <node concept="3clFbJ" id="fV" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874464365" />
          <node concept="3clFbS" id="gO" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874464366" />
            <node concept="3clFbF" id="gQ" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874622563" />
              <node concept="2OqwBi" id="gU" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874622563" />
                <node concept="37vLTw" id="gV" role="2Oq$k0">
                  <ref role="3cqZAo" node="g2" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874622563" />
                </node>
                <node concept="liA8E" id="gW" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874622563" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="gR" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874464368" />
              <node concept="2OqwBi" id="gX" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874464368" />
                <node concept="37vLTw" id="gY" role="2Oq$k0">
                  <ref role="3cqZAo" node="g2" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874464368" />
                </node>
                <node concept="liA8E" id="gZ" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874464368" />
                  <node concept="Xl_RD" id="h0" role="37wK5m">
                    <property role="Xl_RC" value="description: " />
                    <uo k="s:originTrace" v="n:4313507821874464368" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="gS" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874464369" />
              <node concept="2OqwBi" id="h1" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874464369" />
                <node concept="37vLTw" id="h2" role="2Oq$k0">
                  <ref role="3cqZAo" node="g2" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874464369" />
                </node>
                <node concept="liA8E" id="h3" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874464369" />
                  <node concept="2OqwBi" id="h4" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874464370" />
                    <node concept="2OqwBi" id="h5" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:4313507821874464371" />
                      <node concept="37vLTw" id="h7" role="2Oq$k0">
                        <ref role="3cqZAo" node="fJ" resolve="ctx" />
                      </node>
                      <node concept="liA8E" id="h8" role="2OqNvi">
                        <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="h6" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:3JW8_axN3fI" resolve="description" />
                      <uo k="s:originTrace" v="n:4313507821874464372" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="gT" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874464373" />
              <node concept="2OqwBi" id="h9" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874464373" />
                <node concept="37vLTw" id="ha" role="2Oq$k0">
                  <ref role="3cqZAo" node="g2" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874464373" />
                </node>
                <node concept="liA8E" id="hb" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874464373" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="gP" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874464374" />
            <node concept="2OqwBi" id="hc" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874464375" />
              <node concept="2OqwBi" id="he" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874464376" />
                <node concept="37vLTw" id="hg" role="2Oq$k0">
                  <ref role="3cqZAo" node="fJ" resolve="ctx" />
                </node>
                <node concept="liA8E" id="hh" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="hf" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axN3fI" resolve="description" />
                <uo k="s:originTrace" v="n:4313507821874464377" />
              </node>
            </node>
            <node concept="17RvpY" id="hd" role="2OqNvi">
              <uo k="s:originTrace" v="n:4313507821874464378" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="fW" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874622695" />
          <node concept="2OqwBi" id="hi" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874622695" />
            <node concept="37vLTw" id="hj" role="2Oq$k0">
              <ref role="3cqZAo" node="g2" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874622695" />
            </node>
            <node concept="liA8E" id="hk" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874622695" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="fX" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874464380" />
          <node concept="2OqwBi" id="hl" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874464380" />
            <node concept="37vLTw" id="hm" role="2Oq$k0">
              <ref role="3cqZAo" node="g2" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874464380" />
            </node>
            <node concept="liA8E" id="hn" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874464380" />
              <node concept="Xl_RD" id="ho" role="37wK5m">
                <property role="Xl_RC" value="responses:\n" />
                <uo k="s:originTrace" v="n:4313507821874464380" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="fY" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874464381" />
          <node concept="2OqwBi" id="hp" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874464381" />
            <node concept="2OqwBi" id="hq" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874464381" />
              <node concept="2OqwBi" id="hs" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874464381" />
                <node concept="37vLTw" id="hu" role="2Oq$k0">
                  <ref role="3cqZAo" node="fJ" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874464381" />
                </node>
                <node concept="liA8E" id="hv" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874464381" />
                </node>
              </node>
              <node concept="liA8E" id="ht" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874464381" />
              </node>
            </node>
            <node concept="liA8E" id="hr" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874464381" />
            </node>
          </node>
        </node>
        <node concept="1DcWWT" id="fZ" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874464384" />
          <node concept="3clFbS" id="hw" role="2LFqv$">
            <uo k="s:originTrace" v="n:4313507821874464384" />
            <node concept="3clFbF" id="hz" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874464384" />
              <node concept="2OqwBi" id="h$" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874464384" />
                <node concept="37vLTw" id="h_" role="2Oq$k0">
                  <ref role="3cqZAo" node="g2" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874464384" />
                </node>
                <node concept="liA8E" id="hA" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.appendNode(org.jetbrains.mps.openapi.model.SNode)" resolve="appendNode" />
                  <uo k="s:originTrace" v="n:4313507821874464384" />
                  <node concept="37vLTw" id="hB" role="37wK5m">
                    <ref role="3cqZAo" node="hx" resolve="item" />
                    <uo k="s:originTrace" v="n:4313507821874464384" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="hx" role="1Duv9x">
            <property role="TrG5h" value="item" />
            <uo k="s:originTrace" v="n:4313507821874464384" />
            <node concept="3Tqbb2" id="hC" role="1tU5fm">
              <uo k="s:originTrace" v="n:4313507821874464384" />
            </node>
          </node>
          <node concept="2OqwBi" id="hy" role="1DdaDG">
            <uo k="s:originTrace" v="n:4313507821874464385" />
            <node concept="2OqwBi" id="hD" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874464386" />
              <node concept="37vLTw" id="hF" role="2Oq$k0">
                <ref role="3cqZAo" node="fJ" resolve="ctx" />
              </node>
              <node concept="liA8E" id="hG" role="2OqNvi">
                <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
              </node>
            </node>
            <node concept="3Tsc0h" id="hE" role="2OqNvi">
              <ref role="3TtcxE" to="7thy:4favA_iAyf8" resolve="responses" />
              <uo k="s:originTrace" v="n:4313507821874464387" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="g0" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874464381" />
          <node concept="2OqwBi" id="hH" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874464381" />
            <node concept="2OqwBi" id="hI" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874464381" />
              <node concept="2OqwBi" id="hK" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874464381" />
                <node concept="37vLTw" id="hM" role="2Oq$k0">
                  <ref role="3cqZAo" node="fJ" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874464381" />
                </node>
                <node concept="liA8E" id="hN" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874464381" />
                </node>
              </node>
              <node concept="liA8E" id="hL" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874464381" />
              </node>
            </node>
            <node concept="liA8E" id="hJ" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874464381" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="g1" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874227971" />
          <node concept="2OqwBi" id="hO" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874227971" />
            <node concept="2OqwBi" id="hP" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874227971" />
              <node concept="2OqwBi" id="hR" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874227971" />
                <node concept="37vLTw" id="hT" role="2Oq$k0">
                  <ref role="3cqZAo" node="fJ" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874227971" />
                </node>
                <node concept="liA8E" id="hU" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874227971" />
                </node>
              </node>
              <node concept="liA8E" id="hS" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874227971" />
              </node>
            </node>
            <node concept="liA8E" id="hQ" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874227971" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="fJ" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:4313507821874207073" />
        <node concept="3uibUv" id="hV" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:4313507821874207073" />
        </node>
      </node>
      <node concept="2AHcQZ" id="fK" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821874207073" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="hW">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="IntegerSchema_TextGen" />
    <uo k="s:originTrace" v="n:4313507821874915830" />
    <node concept="3Tm1VV" id="hX" role="1B3o_S">
      <uo k="s:originTrace" v="n:4313507821874915830" />
    </node>
    <node concept="3uibUv" id="hY" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:4313507821874915830" />
    </node>
    <node concept="3clFb_" id="hZ" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:4313507821874915830" />
      <node concept="3cqZAl" id="i0" role="3clF45">
        <uo k="s:originTrace" v="n:4313507821874915830" />
      </node>
      <node concept="3Tm1VV" id="i1" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821874915830" />
      </node>
      <node concept="3clFbS" id="i2" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821874915830" />
        <node concept="3cpWs8" id="i5" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874915830" />
          <node concept="3cpWsn" id="io" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:4313507821874915830" />
            <node concept="3uibUv" id="ip" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:4313507821874915830" />
            </node>
            <node concept="2ShNRf" id="iq" role="33vP2m">
              <uo k="s:originTrace" v="n:4313507821874915830" />
              <node concept="1pGfFk" id="ir" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:4313507821874915830" />
                <node concept="37vLTw" id="is" role="37wK5m">
                  <ref role="3cqZAo" node="i3" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874915830" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="i6" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874915849" />
          <node concept="2OqwBi" id="it" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874915849" />
            <node concept="37vLTw" id="iu" role="2Oq$k0">
              <ref role="3cqZAo" node="io" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874915849" />
            </node>
            <node concept="liA8E" id="iv" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874915849" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="i7" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874915851" />
          <node concept="2OqwBi" id="iw" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874915851" />
            <node concept="37vLTw" id="ix" role="2Oq$k0">
              <ref role="3cqZAo" node="io" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874915851" />
            </node>
            <node concept="liA8E" id="iy" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874915851" />
              <node concept="2OqwBi" id="iz" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821874915852" />
                <node concept="2OqwBi" id="i$" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874915853" />
                  <node concept="37vLTw" id="iA" role="2Oq$k0">
                    <ref role="3cqZAo" node="i3" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="iB" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="i_" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                  <uo k="s:originTrace" v="n:4313507821874915854" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="i8" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874915855" />
          <node concept="2OqwBi" id="iC" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874915855" />
            <node concept="37vLTw" id="iD" role="2Oq$k0">
              <ref role="3cqZAo" node="io" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874915855" />
            </node>
            <node concept="liA8E" id="iE" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874915855" />
              <node concept="Xl_RD" id="iF" role="37wK5m">
                <property role="Xl_RC" value=":\n" />
                <uo k="s:originTrace" v="n:4313507821874915855" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="i9" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874915856" />
          <node concept="2OqwBi" id="iG" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874915856" />
            <node concept="2OqwBi" id="iH" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874915856" />
              <node concept="2OqwBi" id="iJ" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874915856" />
                <node concept="37vLTw" id="iL" role="2Oq$k0">
                  <ref role="3cqZAo" node="i3" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874915856" />
                </node>
                <node concept="liA8E" id="iM" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874915856" />
                </node>
              </node>
              <node concept="liA8E" id="iK" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874915856" />
              </node>
            </node>
            <node concept="liA8E" id="iI" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874915856" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ia" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874915858" />
          <node concept="2OqwBi" id="iN" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874915858" />
            <node concept="37vLTw" id="iO" role="2Oq$k0">
              <ref role="3cqZAo" node="io" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874915858" />
            </node>
            <node concept="liA8E" id="iP" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874915858" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ib" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874915860" />
          <node concept="2OqwBi" id="iQ" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874915860" />
            <node concept="37vLTw" id="iR" role="2Oq$k0">
              <ref role="3cqZAo" node="io" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874915860" />
            </node>
            <node concept="liA8E" id="iS" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874915860" />
              <node concept="Xl_RD" id="iT" role="37wK5m">
                <property role="Xl_RC" value="type: integer" />
                <uo k="s:originTrace" v="n:4313507821874915860" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ic" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874915861" />
          <node concept="2OqwBi" id="iU" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874915861" />
            <node concept="37vLTw" id="iV" role="2Oq$k0">
              <ref role="3cqZAo" node="io" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874915861" />
            </node>
            <node concept="liA8E" id="iW" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821874915861" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="id" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874915862" />
          <node concept="2OqwBi" id="iX" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874915862" />
            <node concept="37vLTw" id="iY" role="2Oq$k0">
              <ref role="3cqZAo" node="io" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874915862" />
            </node>
            <node concept="liA8E" id="iZ" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874915862" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ie" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874915864" />
          <node concept="2OqwBi" id="j0" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874915864" />
            <node concept="37vLTw" id="j1" role="2Oq$k0">
              <ref role="3cqZAo" node="io" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874915864" />
            </node>
            <node concept="liA8E" id="j2" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874915864" />
              <node concept="Xl_RD" id="j3" role="37wK5m">
                <property role="Xl_RC" value="description: " />
                <uo k="s:originTrace" v="n:4313507821874915864" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="if" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874915865" />
          <node concept="2OqwBi" id="j4" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874915865" />
            <node concept="37vLTw" id="j5" role="2Oq$k0">
              <ref role="3cqZAo" node="io" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874915865" />
            </node>
            <node concept="liA8E" id="j6" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874915865" />
              <node concept="2OqwBi" id="j7" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821874915866" />
                <node concept="2OqwBi" id="j8" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874915867" />
                  <node concept="37vLTw" id="ja" role="2Oq$k0">
                    <ref role="3cqZAo" node="i3" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="jb" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="j9" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                  <uo k="s:originTrace" v="n:4313507821874915868" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ig" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874915869" />
          <node concept="2OqwBi" id="jc" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874915869" />
            <node concept="37vLTw" id="jd" role="2Oq$k0">
              <ref role="3cqZAo" node="io" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874915869" />
            </node>
            <node concept="liA8E" id="je" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821874915869" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="ih" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600359024" />
          <node concept="3clFbS" id="jf" role="3clFbx">
            <uo k="s:originTrace" v="n:227067257600359025" />
            <node concept="3clFbF" id="jh" role="3cqZAp">
              <uo k="s:originTrace" v="n:227067257600359026" />
              <node concept="2OqwBi" id="ji" role="3clFbG">
                <node concept="37vLTw" id="jj" role="2Oq$k0">
                  <ref role="3cqZAo" node="io" resolve="tgs" />
                </node>
                <node concept="liA8E" id="jk" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.reportError(java.lang.String)" resolve="reportError" />
                  <node concept="Xl_RD" id="jl" role="37wK5m">
                    <property role="Xl_RC" value="description is required" />
                    <uo k="s:originTrace" v="n:227067257600359027" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="jg" role="3clFbw">
            <uo k="s:originTrace" v="n:227067257600359028" />
            <node concept="2OqwBi" id="jm" role="2Oq$k0">
              <uo k="s:originTrace" v="n:227067257600359029" />
              <node concept="2OqwBi" id="jo" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600359030" />
                <node concept="37vLTw" id="jq" role="2Oq$k0">
                  <ref role="3cqZAo" node="i3" resolve="ctx" />
                </node>
                <node concept="liA8E" id="jr" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="jp" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                <uo k="s:originTrace" v="n:227067257600359031" />
              </node>
            </node>
            <node concept="17RlXB" id="jn" role="2OqNvi">
              <uo k="s:originTrace" v="n:227067257600359032" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="ii" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600358701" />
        </node>
        <node concept="3clFbJ" id="ij" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874915870" />
          <node concept="3clFbS" id="js" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874915871" />
            <node concept="3clFbF" id="ju" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874915872" />
              <node concept="2OqwBi" id="jy" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874915872" />
                <node concept="37vLTw" id="jz" role="2Oq$k0">
                  <ref role="3cqZAo" node="io" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874915872" />
                </node>
                <node concept="liA8E" id="j$" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874915872" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="jv" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874915874" />
              <node concept="2OqwBi" id="j_" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874915874" />
                <node concept="37vLTw" id="jA" role="2Oq$k0">
                  <ref role="3cqZAo" node="io" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874915874" />
                </node>
                <node concept="liA8E" id="jB" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874915874" />
                  <node concept="Xl_RD" id="jC" role="37wK5m">
                    <property role="Xl_RC" value="example: " />
                    <uo k="s:originTrace" v="n:4313507821874915874" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="jw" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874915875" />
              <node concept="2OqwBi" id="jD" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874915875" />
                <node concept="37vLTw" id="jE" role="2Oq$k0">
                  <ref role="3cqZAo" node="io" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874915875" />
                </node>
                <node concept="liA8E" id="jF" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874915875" />
                  <node concept="2OqwBi" id="jG" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874915876" />
                    <node concept="2OqwBi" id="jH" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:4313507821874915877" />
                      <node concept="37vLTw" id="jJ" role="2Oq$k0">
                        <ref role="3cqZAo" node="i3" resolve="ctx" />
                      </node>
                      <node concept="liA8E" id="jK" role="2OqNvi">
                        <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="jI" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:4favA_iBNMJ" resolve="example" />
                      <uo k="s:originTrace" v="n:4313507821874915878" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="jx" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874915879" />
              <node concept="2OqwBi" id="jL" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874915879" />
                <node concept="37vLTw" id="jM" role="2Oq$k0">
                  <ref role="3cqZAo" node="io" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874915879" />
                </node>
                <node concept="liA8E" id="jN" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874915879" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="jt" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874915880" />
            <node concept="2OqwBi" id="jO" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874915881" />
              <node concept="2OqwBi" id="jQ" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874915882" />
                <node concept="37vLTw" id="jS" role="2Oq$k0">
                  <ref role="3cqZAo" node="i3" resolve="ctx" />
                </node>
                <node concept="liA8E" id="jT" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="jR" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMJ" resolve="example" />
                <uo k="s:originTrace" v="n:4313507821874915883" />
              </node>
            </node>
            <node concept="17RvpY" id="jP" role="2OqNvi">
              <uo k="s:originTrace" v="n:4313507821874915884" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="ik" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874915900" />
          <node concept="3clFbS" id="jU" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874915901" />
            <node concept="3clFbF" id="jW" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874915902" />
              <node concept="2OqwBi" id="k0" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874915902" />
                <node concept="37vLTw" id="k1" role="2Oq$k0">
                  <ref role="3cqZAo" node="io" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874915902" />
                </node>
                <node concept="liA8E" id="k2" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874915902" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="jX" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874915904" />
              <node concept="2OqwBi" id="k3" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874915904" />
                <node concept="37vLTw" id="k4" role="2Oq$k0">
                  <ref role="3cqZAo" node="io" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874915904" />
                </node>
                <node concept="liA8E" id="k5" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874915904" />
                  <node concept="Xl_RD" id="k6" role="37wK5m">
                    <property role="Xl_RC" value="minimum: " />
                    <uo k="s:originTrace" v="n:4313507821874915904" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="jY" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874915905" />
              <node concept="2OqwBi" id="k7" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874915905" />
                <node concept="37vLTw" id="k8" role="2Oq$k0">
                  <ref role="3cqZAo" node="io" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874915905" />
                </node>
                <node concept="liA8E" id="k9" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874915905" />
                  <node concept="3cpWs3" id="ka" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874915906" />
                    <node concept="Xl_RD" id="kb" role="3uHU7B">
                      <uo k="s:originTrace" v="n:4313507821874915907" />
                    </node>
                    <node concept="2OqwBi" id="kc" role="3uHU7w">
                      <uo k="s:originTrace" v="n:4313507821874915908" />
                      <node concept="3TrcHB" id="kd" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNSd" resolve="minimum" />
                        <uo k="s:originTrace" v="n:4313507821874915909" />
                      </node>
                      <node concept="2OqwBi" id="ke" role="2Oq$k0">
                        <uo k="s:originTrace" v="n:4313507821874915910" />
                        <node concept="37vLTw" id="kf" role="2Oq$k0">
                          <ref role="3cqZAo" node="i3" resolve="ctx" />
                        </node>
                        <node concept="liA8E" id="kg" role="2OqNvi">
                          <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="jZ" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874915911" />
              <node concept="2OqwBi" id="kh" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874915911" />
                <node concept="37vLTw" id="ki" role="2Oq$k0">
                  <ref role="3cqZAo" node="io" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874915911" />
                </node>
                <node concept="liA8E" id="kj" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874915911" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3eOSWO" id="jV" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874915912" />
            <node concept="3cmrfG" id="kk" role="3uHU7w">
              <property role="3cmrfH" value="0" />
              <uo k="s:originTrace" v="n:4313507821874915913" />
            </node>
            <node concept="2OqwBi" id="kl" role="3uHU7B">
              <uo k="s:originTrace" v="n:4313507821874915914" />
              <node concept="2OqwBi" id="km" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874915915" />
                <node concept="37vLTw" id="ko" role="2Oq$k0">
                  <ref role="3cqZAo" node="i3" resolve="ctx" />
                </node>
                <node concept="liA8E" id="kp" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="kn" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNSd" resolve="minimum" />
                <uo k="s:originTrace" v="n:4313507821874915916" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="il" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874915917" />
          <node concept="3clFbS" id="kq" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874915918" />
            <node concept="3clFbF" id="ks" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874915919" />
              <node concept="2OqwBi" id="kw" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874915919" />
                <node concept="37vLTw" id="kx" role="2Oq$k0">
                  <ref role="3cqZAo" node="io" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874915919" />
                </node>
                <node concept="liA8E" id="ky" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874915919" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="kt" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874915921" />
              <node concept="2OqwBi" id="kz" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874915921" />
                <node concept="37vLTw" id="k$" role="2Oq$k0">
                  <ref role="3cqZAo" node="io" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874915921" />
                </node>
                <node concept="liA8E" id="k_" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874915921" />
                  <node concept="Xl_RD" id="kA" role="37wK5m">
                    <property role="Xl_RC" value="maximum: " />
                    <uo k="s:originTrace" v="n:4313507821874915921" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="ku" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874915922" />
              <node concept="2OqwBi" id="kB" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874915922" />
                <node concept="37vLTw" id="kC" role="2Oq$k0">
                  <ref role="3cqZAo" node="io" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874915922" />
                </node>
                <node concept="liA8E" id="kD" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874915922" />
                  <node concept="3cpWs3" id="kE" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874915923" />
                    <node concept="Xl_RD" id="kF" role="3uHU7B">
                      <uo k="s:originTrace" v="n:4313507821874915924" />
                    </node>
                    <node concept="2OqwBi" id="kG" role="3uHU7w">
                      <uo k="s:originTrace" v="n:4313507821874915925" />
                      <node concept="3TrcHB" id="kH" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:4favA_iBNSe" resolve="maximum" />
                        <uo k="s:originTrace" v="n:4313507821874915926" />
                      </node>
                      <node concept="2OqwBi" id="kI" role="2Oq$k0">
                        <uo k="s:originTrace" v="n:4313507821874915927" />
                        <node concept="37vLTw" id="kJ" role="2Oq$k0">
                          <ref role="3cqZAo" node="i3" resolve="ctx" />
                        </node>
                        <node concept="liA8E" id="kK" role="2OqNvi">
                          <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="kv" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874915928" />
              <node concept="2OqwBi" id="kL" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874915928" />
                <node concept="37vLTw" id="kM" role="2Oq$k0">
                  <ref role="3cqZAo" node="io" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874915928" />
                </node>
                <node concept="liA8E" id="kN" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874915928" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3eOSWO" id="kr" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874915929" />
            <node concept="3cmrfG" id="kO" role="3uHU7w">
              <property role="3cmrfH" value="0" />
              <uo k="s:originTrace" v="n:4313507821874915930" />
            </node>
            <node concept="2OqwBi" id="kP" role="3uHU7B">
              <uo k="s:originTrace" v="n:4313507821874915931" />
              <node concept="2OqwBi" id="kQ" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874915932" />
                <node concept="37vLTw" id="kS" role="2Oq$k0">
                  <ref role="3cqZAo" node="i3" resolve="ctx" />
                </node>
                <node concept="liA8E" id="kT" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="kR" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNSe" resolve="maximum" />
                <uo k="s:originTrace" v="n:4313507821874915933" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="im" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874915934" />
          <node concept="3clFbS" id="kU" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874915935" />
            <node concept="3clFbF" id="kW" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874915936" />
              <node concept="2OqwBi" id="l0" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874915936" />
                <node concept="37vLTw" id="l1" role="2Oq$k0">
                  <ref role="3cqZAo" node="io" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874915936" />
                </node>
                <node concept="liA8E" id="l2" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874915936" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="kX" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874915938" />
              <node concept="2OqwBi" id="l3" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874915938" />
                <node concept="37vLTw" id="l4" role="2Oq$k0">
                  <ref role="3cqZAo" node="io" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874915938" />
                </node>
                <node concept="liA8E" id="l5" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874915938" />
                  <node concept="Xl_RD" id="l6" role="37wK5m">
                    <property role="Xl_RC" value="pattern: " />
                    <uo k="s:originTrace" v="n:4313507821874915938" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="kY" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874915939" />
              <node concept="2OqwBi" id="l7" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874915939" />
                <node concept="37vLTw" id="l8" role="2Oq$k0">
                  <ref role="3cqZAo" node="io" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874915939" />
                </node>
                <node concept="liA8E" id="l9" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874915939" />
                  <node concept="2OqwBi" id="la" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874915940" />
                    <node concept="3TrcHB" id="lb" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:4favA_iBNSf" resolve="pattern" />
                      <uo k="s:originTrace" v="n:4313507821874915941" />
                    </node>
                    <node concept="2OqwBi" id="lc" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:4313507821874915942" />
                      <node concept="37vLTw" id="ld" role="2Oq$k0">
                        <ref role="3cqZAo" node="i3" resolve="ctx" />
                      </node>
                      <node concept="liA8E" id="le" role="2OqNvi">
                        <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="kZ" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874915943" />
              <node concept="2OqwBi" id="lf" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874915943" />
                <node concept="37vLTw" id="lg" role="2Oq$k0">
                  <ref role="3cqZAo" node="io" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874915943" />
                </node>
                <node concept="liA8E" id="lh" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874915943" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="kV" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874915944" />
            <node concept="2OqwBi" id="li" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874915945" />
              <node concept="2OqwBi" id="lk" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874915946" />
                <node concept="37vLTw" id="lm" role="2Oq$k0">
                  <ref role="3cqZAo" node="i3" resolve="ctx" />
                </node>
                <node concept="liA8E" id="ln" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="ll" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNSf" resolve="pattern" />
                <uo k="s:originTrace" v="n:4313507821874915947" />
              </node>
            </node>
            <node concept="17RvpY" id="lj" role="2OqNvi">
              <uo k="s:originTrace" v="n:4313507821874915948" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="in" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874915856" />
          <node concept="2OqwBi" id="lo" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874915856" />
            <node concept="2OqwBi" id="lp" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874915856" />
              <node concept="2OqwBi" id="lr" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874915856" />
                <node concept="37vLTw" id="lt" role="2Oq$k0">
                  <ref role="3cqZAo" node="i3" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874915856" />
                </node>
                <node concept="liA8E" id="lu" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874915856" />
                </node>
              </node>
              <node concept="liA8E" id="ls" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874915856" />
              </node>
            </node>
            <node concept="liA8E" id="lq" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874915856" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="i3" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:4313507821874915830" />
        <node concept="3uibUv" id="lv" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:4313507821874915830" />
        </node>
      </node>
      <node concept="2AHcQZ" id="i4" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821874915830" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="lw">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="ObjectSchemaProperty_TextGen" />
    <uo k="s:originTrace" v="n:4313507821875171408" />
    <node concept="3Tm1VV" id="lx" role="1B3o_S">
      <uo k="s:originTrace" v="n:4313507821875171408" />
    </node>
    <node concept="3uibUv" id="ly" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:4313507821875171408" />
    </node>
    <node concept="3clFb_" id="lz" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:4313507821875171408" />
      <node concept="3cqZAl" id="l$" role="3clF45">
        <uo k="s:originTrace" v="n:4313507821875171408" />
      </node>
      <node concept="3Tm1VV" id="l_" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821875171408" />
      </node>
      <node concept="3clFbS" id="lA" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821875171408" />
        <node concept="3cpWs8" id="lD" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875171408" />
          <node concept="3cpWsn" id="lQ" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:4313507821875171408" />
            <node concept="3uibUv" id="lR" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:4313507821875171408" />
            </node>
            <node concept="2ShNRf" id="lS" role="33vP2m">
              <uo k="s:originTrace" v="n:4313507821875171408" />
              <node concept="1pGfFk" id="lT" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:4313507821875171408" />
                <node concept="37vLTw" id="lU" role="37wK5m">
                  <ref role="3cqZAo" node="lB" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821875171408" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lE" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875171427" />
          <node concept="2OqwBi" id="lV" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875171427" />
            <node concept="37vLTw" id="lW" role="2Oq$k0">
              <ref role="3cqZAo" node="lQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875171427" />
            </node>
            <node concept="liA8E" id="lX" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821875171427" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lF" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875171478" />
          <node concept="2OqwBi" id="lY" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875171478" />
            <node concept="37vLTw" id="lZ" role="2Oq$k0">
              <ref role="3cqZAo" node="lQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875171478" />
            </node>
            <node concept="liA8E" id="m0" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821875171478" />
              <node concept="2OqwBi" id="m1" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821875171966" />
                <node concept="2OqwBi" id="m2" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821875171530" />
                  <node concept="37vLTw" id="m4" role="2Oq$k0">
                    <ref role="3cqZAo" node="lB" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="m5" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="m3" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNSM" resolve="name" />
                  <uo k="s:originTrace" v="n:4313507821875172942" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lG" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875173142" />
          <node concept="2OqwBi" id="m6" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875173142" />
            <node concept="37vLTw" id="m7" role="2Oq$k0">
              <ref role="3cqZAo" node="lQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875173142" />
            </node>
            <node concept="liA8E" id="m8" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821875173142" />
              <node concept="Xl_RD" id="m9" role="37wK5m">
                <property role="Xl_RC" value=":" />
                <uo k="s:originTrace" v="n:4313507821875173142" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lH" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875176607" />
          <node concept="2OqwBi" id="ma" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875176607" />
            <node concept="37vLTw" id="mb" role="2Oq$k0">
              <ref role="3cqZAo" node="lQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875176607" />
            </node>
            <node concept="liA8E" id="mc" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821875176607" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lI" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875176787" />
          <node concept="2OqwBi" id="md" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875176787" />
            <node concept="2OqwBi" id="me" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821875176787" />
              <node concept="2OqwBi" id="mg" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821875176787" />
                <node concept="37vLTw" id="mi" role="2Oq$k0">
                  <ref role="3cqZAo" node="lB" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821875176787" />
                </node>
                <node concept="liA8E" id="mj" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821875176787" />
                </node>
              </node>
              <node concept="liA8E" id="mh" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821875176787" />
              </node>
            </node>
            <node concept="liA8E" id="mf" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821875176787" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lJ" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875235319" />
          <node concept="2OqwBi" id="mk" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875235319" />
            <node concept="37vLTw" id="ml" role="2Oq$k0">
              <ref role="3cqZAo" node="lQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875235319" />
            </node>
            <node concept="liA8E" id="mm" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821875235319" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lK" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875177000" />
          <node concept="2OqwBi" id="mn" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875177000" />
            <node concept="37vLTw" id="mo" role="2Oq$k0">
              <ref role="3cqZAo" node="lQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875177000" />
            </node>
            <node concept="liA8E" id="mp" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821875177000" />
              <node concept="Xl_RD" id="mq" role="37wK5m">
                <property role="Xl_RC" value="$ref: " />
                <uo k="s:originTrace" v="n:4313507821875177000" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lL" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875177080" />
          <node concept="2OqwBi" id="mr" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875177080" />
            <node concept="37vLTw" id="ms" role="2Oq$k0">
              <ref role="3cqZAo" node="lQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875177080" />
            </node>
            <node concept="liA8E" id="mt" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821875177080" />
              <node concept="Xl_RD" id="mu" role="37wK5m">
                <property role="Xl_RC" value="'#/components/schemas/" />
                <uo k="s:originTrace" v="n:4313507821875177080" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lM" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875177145" />
          <node concept="2OqwBi" id="mv" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875177145" />
            <node concept="37vLTw" id="mw" role="2Oq$k0">
              <ref role="3cqZAo" node="lQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875177145" />
            </node>
            <node concept="liA8E" id="mx" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821875177145" />
              <node concept="2OqwBi" id="my" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821875179122" />
                <node concept="2OqwBi" id="mz" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821875177633" />
                  <node concept="2OqwBi" id="m_" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:4313507821875177197" />
                    <node concept="37vLTw" id="mB" role="2Oq$k0">
                      <ref role="3cqZAo" node="lB" resolve="ctx" />
                    </node>
                    <node concept="liA8E" id="mC" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                    </node>
                  </node>
                  <node concept="3TrEf2" id="mA" role="2OqNvi">
                    <ref role="3Tt5mk" to="7thy:4favA_iBNSO" resolve="schema" />
                    <uo k="s:originTrace" v="n:4313507821875178644" />
                  </node>
                </node>
                <node concept="3TrcHB" id="m$" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                  <uo k="s:originTrace" v="n:4313507821875180283" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lN" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496236280" />
          <node concept="2OqwBi" id="mD" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496236280" />
            <node concept="37vLTw" id="mE" role="2Oq$k0">
              <ref role="3cqZAo" node="lQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496236280" />
            </node>
            <node concept="liA8E" id="mF" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:8442470881496236280" />
              <node concept="Xl_RD" id="mG" role="37wK5m">
                <property role="Xl_RC" value="'" />
                <uo k="s:originTrace" v="n:8442470881496236280" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lO" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875206840" />
          <node concept="2OqwBi" id="mH" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875206840" />
            <node concept="37vLTw" id="mI" role="2Oq$k0">
              <ref role="3cqZAo" node="lQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875206840" />
            </node>
            <node concept="liA8E" id="mJ" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821875206840" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="lP" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875176787" />
          <node concept="2OqwBi" id="mK" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875176787" />
            <node concept="2OqwBi" id="mL" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821875176787" />
              <node concept="2OqwBi" id="mN" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821875176787" />
                <node concept="37vLTw" id="mP" role="2Oq$k0">
                  <ref role="3cqZAo" node="lB" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821875176787" />
                </node>
                <node concept="liA8E" id="mQ" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821875176787" />
                </node>
              </node>
              <node concept="liA8E" id="mO" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821875176787" />
              </node>
            </node>
            <node concept="liA8E" id="mM" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821875176787" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="lB" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:4313507821875171408" />
        <node concept="3uibUv" id="mR" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:4313507821875171408" />
        </node>
      </node>
      <node concept="2AHcQZ" id="lC" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821875171408" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="mS">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="ObjectSchema_TextGen" />
    <uo k="s:originTrace" v="n:4313507821875098746" />
    <node concept="3Tm1VV" id="mT" role="1B3o_S">
      <uo k="s:originTrace" v="n:4313507821875098746" />
    </node>
    <node concept="3uibUv" id="mU" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:4313507821875098746" />
    </node>
    <node concept="3clFb_" id="mV" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:4313507821875098746" />
      <node concept="3cqZAl" id="mW" role="3clF45">
        <uo k="s:originTrace" v="n:4313507821875098746" />
      </node>
      <node concept="3Tm1VV" id="mX" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821875098746" />
      </node>
      <node concept="3clFbS" id="mY" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821875098746" />
        <node concept="3cpWs8" id="n1" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875098746" />
          <node concept="3cpWsn" id="nn" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:4313507821875098746" />
            <node concept="3uibUv" id="no" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:4313507821875098746" />
            </node>
            <node concept="2ShNRf" id="np" role="33vP2m">
              <uo k="s:originTrace" v="n:4313507821875098746" />
              <node concept="1pGfFk" id="nq" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:4313507821875098746" />
                <node concept="37vLTw" id="nr" role="37wK5m">
                  <ref role="3cqZAo" node="mZ" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821875098746" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="n2" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875099924" />
          <node concept="2OqwBi" id="ns" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875099924" />
            <node concept="37vLTw" id="nt" role="2Oq$k0">
              <ref role="3cqZAo" node="nn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875099924" />
            </node>
            <node concept="liA8E" id="nu" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821875099924" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="n3" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875099926" />
          <node concept="2OqwBi" id="nv" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875099926" />
            <node concept="37vLTw" id="nw" role="2Oq$k0">
              <ref role="3cqZAo" node="nn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875099926" />
            </node>
            <node concept="liA8E" id="nx" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821875099926" />
              <node concept="2OqwBi" id="ny" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821875099927" />
                <node concept="2OqwBi" id="nz" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821875099928" />
                  <node concept="37vLTw" id="n_" role="2Oq$k0">
                    <ref role="3cqZAo" node="mZ" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="nA" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="n$" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                  <uo k="s:originTrace" v="n:4313507821875099929" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="n4" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875099930" />
          <node concept="2OqwBi" id="nB" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875099930" />
            <node concept="37vLTw" id="nC" role="2Oq$k0">
              <ref role="3cqZAo" node="nn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875099930" />
            </node>
            <node concept="liA8E" id="nD" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821875099930" />
              <node concept="Xl_RD" id="nE" role="37wK5m">
                <property role="Xl_RC" value=":\n" />
                <uo k="s:originTrace" v="n:4313507821875099930" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="n5" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875099931" />
          <node concept="2OqwBi" id="nF" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875099931" />
            <node concept="2OqwBi" id="nG" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821875099931" />
              <node concept="2OqwBi" id="nI" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821875099931" />
                <node concept="37vLTw" id="nK" role="2Oq$k0">
                  <ref role="3cqZAo" node="mZ" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821875099931" />
                </node>
                <node concept="liA8E" id="nL" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821875099931" />
                </node>
              </node>
              <node concept="liA8E" id="nJ" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821875099931" />
              </node>
            </node>
            <node concept="liA8E" id="nH" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821875099931" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="n6" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875099933" />
          <node concept="2OqwBi" id="nM" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875099933" />
            <node concept="37vLTw" id="nN" role="2Oq$k0">
              <ref role="3cqZAo" node="nn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875099933" />
            </node>
            <node concept="liA8E" id="nO" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821875099933" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="n7" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875099935" />
          <node concept="2OqwBi" id="nP" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875099935" />
            <node concept="37vLTw" id="nQ" role="2Oq$k0">
              <ref role="3cqZAo" node="nn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875099935" />
            </node>
            <node concept="liA8E" id="nR" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821875099935" />
              <node concept="Xl_RD" id="nS" role="37wK5m">
                <property role="Xl_RC" value="type: object" />
                <uo k="s:originTrace" v="n:4313507821875099935" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="n8" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875099936" />
          <node concept="2OqwBi" id="nT" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875099936" />
            <node concept="37vLTw" id="nU" role="2Oq$k0">
              <ref role="3cqZAo" node="nn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875099936" />
            </node>
            <node concept="liA8E" id="nV" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821875099936" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="n9" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875099937" />
          <node concept="2OqwBi" id="nW" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875099937" />
            <node concept="37vLTw" id="nX" role="2Oq$k0">
              <ref role="3cqZAo" node="nn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875099937" />
            </node>
            <node concept="liA8E" id="nY" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821875099937" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="na" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875099939" />
          <node concept="2OqwBi" id="nZ" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875099939" />
            <node concept="37vLTw" id="o0" role="2Oq$k0">
              <ref role="3cqZAo" node="nn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875099939" />
            </node>
            <node concept="liA8E" id="o1" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821875099939" />
              <node concept="Xl_RD" id="o2" role="37wK5m">
                <property role="Xl_RC" value="description: " />
                <uo k="s:originTrace" v="n:4313507821875099939" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="nb" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875099940" />
          <node concept="2OqwBi" id="o3" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875099940" />
            <node concept="37vLTw" id="o4" role="2Oq$k0">
              <ref role="3cqZAo" node="nn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875099940" />
            </node>
            <node concept="liA8E" id="o5" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821875099940" />
              <node concept="2OqwBi" id="o6" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821875099941" />
                <node concept="2OqwBi" id="o7" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821875099942" />
                  <node concept="37vLTw" id="o9" role="2Oq$k0">
                    <ref role="3cqZAo" node="mZ" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="oa" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="o8" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                  <uo k="s:originTrace" v="n:4313507821875099943" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="nc" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875099944" />
          <node concept="2OqwBi" id="ob" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875099944" />
            <node concept="37vLTw" id="oc" role="2Oq$k0">
              <ref role="3cqZAo" node="nn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875099944" />
            </node>
            <node concept="liA8E" id="od" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821875099944" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="nd" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600347160" />
          <node concept="3clFbS" id="oe" role="3clFbx">
            <uo k="s:originTrace" v="n:227067257600347162" />
            <node concept="3clFbF" id="og" role="3cqZAp">
              <uo k="s:originTrace" v="n:227067257600352593" />
              <node concept="2OqwBi" id="oh" role="3clFbG">
                <node concept="37vLTw" id="oi" role="2Oq$k0">
                  <ref role="3cqZAo" node="nn" resolve="tgs" />
                </node>
                <node concept="liA8E" id="oj" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.reportError(java.lang.String)" resolve="reportError" />
                  <node concept="Xl_RD" id="ok" role="37wK5m">
                    <property role="Xl_RC" value="description is required" />
                    <uo k="s:originTrace" v="n:227067257600352594" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="of" role="3clFbw">
            <uo k="s:originTrace" v="n:227067257600350470" />
            <node concept="2OqwBi" id="ol" role="2Oq$k0">
              <uo k="s:originTrace" v="n:227067257600348050" />
              <node concept="2OqwBi" id="on" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600347536" />
                <node concept="37vLTw" id="op" role="2Oq$k0">
                  <ref role="3cqZAo" node="mZ" resolve="ctx" />
                </node>
                <node concept="liA8E" id="oq" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="oo" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                <uo k="s:originTrace" v="n:227067257600349253" />
              </node>
            </node>
            <node concept="17RlXB" id="om" role="2OqNvi">
              <uo k="s:originTrace" v="n:227067257600352557" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="ne" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875099945" />
          <node concept="3clFbS" id="or" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821875099946" />
            <node concept="3clFbF" id="ot" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875099947" />
              <node concept="2OqwBi" id="ox" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821875099947" />
                <node concept="37vLTw" id="oy" role="2Oq$k0">
                  <ref role="3cqZAo" node="nn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821875099947" />
                </node>
                <node concept="liA8E" id="oz" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821875099947" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="ou" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875099949" />
              <node concept="2OqwBi" id="o$" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821875099949" />
                <node concept="37vLTw" id="o_" role="2Oq$k0">
                  <ref role="3cqZAo" node="nn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821875099949" />
                </node>
                <node concept="liA8E" id="oA" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821875099949" />
                  <node concept="Xl_RD" id="oB" role="37wK5m">
                    <property role="Xl_RC" value="example: " />
                    <uo k="s:originTrace" v="n:4313507821875099949" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="ov" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875099950" />
              <node concept="2OqwBi" id="oC" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821875099950" />
                <node concept="37vLTw" id="oD" role="2Oq$k0">
                  <ref role="3cqZAo" node="nn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821875099950" />
                </node>
                <node concept="liA8E" id="oE" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821875099950" />
                  <node concept="2OqwBi" id="oF" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821875099951" />
                    <node concept="2OqwBi" id="oG" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:4313507821875099952" />
                      <node concept="37vLTw" id="oI" role="2Oq$k0">
                        <ref role="3cqZAo" node="mZ" resolve="ctx" />
                      </node>
                      <node concept="liA8E" id="oJ" role="2OqNvi">
                        <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="oH" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:4favA_iBNMJ" resolve="example" />
                      <uo k="s:originTrace" v="n:4313507821875099953" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="ow" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875099954" />
              <node concept="2OqwBi" id="oK" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821875099954" />
                <node concept="37vLTw" id="oL" role="2Oq$k0">
                  <ref role="3cqZAo" node="nn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821875099954" />
                </node>
                <node concept="liA8E" id="oM" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821875099954" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="os" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821875099955" />
            <node concept="2OqwBi" id="oN" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821875099956" />
              <node concept="2OqwBi" id="oP" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821875099957" />
                <node concept="37vLTw" id="oR" role="2Oq$k0">
                  <ref role="3cqZAo" node="mZ" resolve="ctx" />
                </node>
                <node concept="liA8E" id="oS" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="oQ" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMJ" resolve="example" />
                <uo k="s:originTrace" v="n:4313507821875099958" />
              </node>
            </node>
            <node concept="17RvpY" id="oO" role="2OqNvi">
              <uo k="s:originTrace" v="n:4313507821875099959" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="nf" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875101570" />
          <node concept="2OqwBi" id="oT" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875101570" />
            <node concept="37vLTw" id="oU" role="2Oq$k0">
              <ref role="3cqZAo" node="nn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875101570" />
            </node>
            <node concept="liA8E" id="oV" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821875101570" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ng" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875101826" />
          <node concept="2OqwBi" id="oW" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875101826" />
            <node concept="37vLTw" id="oX" role="2Oq$k0">
              <ref role="3cqZAo" node="nn" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821875101826" />
            </node>
            <node concept="liA8E" id="oY" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821875101826" />
              <node concept="Xl_RD" id="oZ" role="37wK5m">
                <property role="Xl_RC" value="properties:\n" />
                <uo k="s:originTrace" v="n:4313507821875101826" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="nh" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875102016" />
          <node concept="2OqwBi" id="p0" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875102016" />
            <node concept="2OqwBi" id="p1" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821875102016" />
              <node concept="2OqwBi" id="p3" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821875102016" />
                <node concept="37vLTw" id="p5" role="2Oq$k0">
                  <ref role="3cqZAo" node="mZ" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821875102016" />
                </node>
                <node concept="liA8E" id="p6" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821875102016" />
                </node>
              </node>
              <node concept="liA8E" id="p4" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821875102016" />
              </node>
            </node>
            <node concept="liA8E" id="p2" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821875102016" />
            </node>
          </node>
        </node>
        <node concept="1DcWWT" id="ni" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875102119" />
          <node concept="3clFbS" id="p7" role="2LFqv$">
            <uo k="s:originTrace" v="n:4313507821875102119" />
            <node concept="3clFbF" id="pa" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875102119" />
              <node concept="2OqwBi" id="pb" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821875102119" />
                <node concept="37vLTw" id="pc" role="2Oq$k0">
                  <ref role="3cqZAo" node="nn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821875102119" />
                </node>
                <node concept="liA8E" id="pd" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.appendNode(org.jetbrains.mps.openapi.model.SNode)" resolve="appendNode" />
                  <uo k="s:originTrace" v="n:4313507821875102119" />
                  <node concept="37vLTw" id="pe" role="37wK5m">
                    <ref role="3cqZAo" node="p8" resolve="item" />
                    <uo k="s:originTrace" v="n:4313507821875102119" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="p8" role="1Duv9x">
            <property role="TrG5h" value="item" />
            <uo k="s:originTrace" v="n:4313507821875102119" />
            <node concept="3Tqbb2" id="pf" role="1tU5fm">
              <uo k="s:originTrace" v="n:4313507821875102119" />
            </node>
          </node>
          <node concept="2OqwBi" id="p9" role="1DdaDG">
            <uo k="s:originTrace" v="n:4313507821875102537" />
            <node concept="2OqwBi" id="pg" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821875102139" />
              <node concept="37vLTw" id="pi" role="2Oq$k0">
                <ref role="3cqZAo" node="mZ" resolve="ctx" />
              </node>
              <node concept="liA8E" id="pj" role="2OqNvi">
                <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
              </node>
            </node>
            <node concept="3Tsc0h" id="ph" role="2OqNvi">
              <ref role="3TtcxE" to="7thy:4favA_iBNSJ" resolve="properties" />
              <uo k="s:originTrace" v="n:4313507821875103699" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="nj" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875102016" />
          <node concept="2OqwBi" id="pk" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875102016" />
            <node concept="2OqwBi" id="pl" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821875102016" />
              <node concept="2OqwBi" id="pn" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821875102016" />
                <node concept="37vLTw" id="pp" role="2Oq$k0">
                  <ref role="3cqZAo" node="mZ" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821875102016" />
                </node>
                <node concept="liA8E" id="pq" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821875102016" />
                </node>
              </node>
              <node concept="liA8E" id="po" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821875102016" />
              </node>
            </node>
            <node concept="liA8E" id="pm" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821875102016" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="nk" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875104032" />
          <node concept="3clFbS" id="pr" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821875104034" />
            <node concept="3clFbF" id="pt" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875236526" />
              <node concept="2OqwBi" id="py" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821875236526" />
                <node concept="37vLTw" id="pz" role="2Oq$k0">
                  <ref role="3cqZAo" node="nn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821875236526" />
                </node>
                <node concept="liA8E" id="p$" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821875236526" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="pu" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875106454" />
              <node concept="2OqwBi" id="p_" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821875106454" />
                <node concept="37vLTw" id="pA" role="2Oq$k0">
                  <ref role="3cqZAo" node="nn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821875106454" />
                </node>
                <node concept="liA8E" id="pB" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821875106454" />
                  <node concept="Xl_RD" id="pC" role="37wK5m">
                    <property role="Xl_RC" value="required:\n" />
                    <uo k="s:originTrace" v="n:4313507821875106454" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="pv" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875106564" />
              <node concept="2OqwBi" id="pD" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821875106564" />
                <node concept="2OqwBi" id="pE" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821875106564" />
                  <node concept="2OqwBi" id="pG" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:4313507821875106564" />
                    <node concept="37vLTw" id="pI" role="2Oq$k0">
                      <ref role="3cqZAo" node="mZ" resolve="ctx" />
                      <uo k="s:originTrace" v="n:4313507821875106564" />
                    </node>
                    <node concept="liA8E" id="pJ" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                      <uo k="s:originTrace" v="n:4313507821875106564" />
                    </node>
                  </node>
                  <node concept="liA8E" id="pH" role="2OqNvi">
                    <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                    <uo k="s:originTrace" v="n:4313507821875106564" />
                  </node>
                </node>
                <node concept="liA8E" id="pF" role="2OqNvi">
                  <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
                  <uo k="s:originTrace" v="n:4313507821875106564" />
                </node>
              </node>
            </node>
            <node concept="2Gpval" id="pw" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875106581" />
              <node concept="2GrKxI" id="pK" role="2Gsz3X">
                <property role="TrG5h" value="prop" />
                <uo k="s:originTrace" v="n:4313507821875106582" />
              </node>
              <node concept="2OqwBi" id="pL" role="2GsD0m">
                <uo k="s:originTrace" v="n:4313507821875107166" />
                <node concept="2OqwBi" id="pN" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821875106640" />
                  <node concept="37vLTw" id="pP" role="2Oq$k0">
                    <ref role="3cqZAo" node="mZ" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="pQ" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3Tsc0h" id="pO" role="2OqNvi">
                  <ref role="3TtcxE" to="7thy:3JsE8aitKU3" resolve="required" />
                  <uo k="s:originTrace" v="n:4313507821875126942" />
                </node>
              </node>
              <node concept="3clFbS" id="pM" role="2LFqv$">
                <uo k="s:originTrace" v="n:4313507821875106584" />
                <node concept="3clFbF" id="pR" role="3cqZAp">
                  <uo k="s:originTrace" v="n:4313507821875127022" />
                  <node concept="2OqwBi" id="pV" role="3clFbG">
                    <uo k="s:originTrace" v="n:4313507821875127022" />
                    <node concept="37vLTw" id="pW" role="2Oq$k0">
                      <ref role="3cqZAo" node="nn" resolve="tgs" />
                      <uo k="s:originTrace" v="n:4313507821875127022" />
                    </node>
                    <node concept="liA8E" id="pX" role="2OqNvi">
                      <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                      <uo k="s:originTrace" v="n:4313507821875127022" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="pS" role="3cqZAp">
                  <uo k="s:originTrace" v="n:4313507821875127040" />
                  <node concept="2OqwBi" id="pY" role="3clFbG">
                    <uo k="s:originTrace" v="n:4313507821875127040" />
                    <node concept="37vLTw" id="pZ" role="2Oq$k0">
                      <ref role="3cqZAo" node="nn" resolve="tgs" />
                      <uo k="s:originTrace" v="n:4313507821875127040" />
                    </node>
                    <node concept="liA8E" id="q0" role="2OqNvi">
                      <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                      <uo k="s:originTrace" v="n:4313507821875127040" />
                      <node concept="Xl_RD" id="q1" role="37wK5m">
                        <property role="Xl_RC" value="- " />
                        <uo k="s:originTrace" v="n:4313507821875127040" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="pT" role="3cqZAp">
                  <uo k="s:originTrace" v="n:4313507821875127090" />
                  <node concept="2OqwBi" id="q2" role="3clFbG">
                    <uo k="s:originTrace" v="n:4313507821875127090" />
                    <node concept="37vLTw" id="q3" role="2Oq$k0">
                      <ref role="3cqZAo" node="nn" resolve="tgs" />
                      <uo k="s:originTrace" v="n:4313507821875127090" />
                    </node>
                    <node concept="liA8E" id="q4" role="2OqNvi">
                      <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                      <uo k="s:originTrace" v="n:4313507821875127090" />
                      <node concept="2OqwBi" id="q5" role="37wK5m">
                        <uo k="s:originTrace" v="n:4313507821875132088" />
                        <node concept="2OqwBi" id="q6" role="2Oq$k0">
                          <uo k="s:originTrace" v="n:4313507821875129168" />
                          <node concept="2GrUjf" id="q8" role="2Oq$k0">
                            <ref role="2Gs0qQ" node="pK" resolve="prop" />
                            <uo k="s:originTrace" v="n:4313507821875127142" />
                          </node>
                          <node concept="3TrEf2" id="q9" role="2OqNvi">
                            <ref role="3Tt5mk" to="7thy:3JsE8aiu$43" resolve="property" />
                            <uo k="s:originTrace" v="n:4313507821875131215" />
                          </node>
                        </node>
                        <node concept="3TrcHB" id="q7" role="2OqNvi">
                          <ref role="3TsBF5" to="7thy:4favA_iBNSM" resolve="name" />
                          <uo k="s:originTrace" v="n:4313507821875133278" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="pU" role="3cqZAp">
                  <uo k="s:originTrace" v="n:4313507821875207862" />
                  <node concept="2OqwBi" id="qa" role="3clFbG">
                    <uo k="s:originTrace" v="n:4313507821875207862" />
                    <node concept="37vLTw" id="qb" role="2Oq$k0">
                      <ref role="3cqZAo" node="nn" resolve="tgs" />
                      <uo k="s:originTrace" v="n:4313507821875207862" />
                    </node>
                    <node concept="liA8E" id="qc" role="2OqNvi">
                      <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                      <uo k="s:originTrace" v="n:4313507821875207862" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="px" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875106564" />
              <node concept="2OqwBi" id="qd" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821875106564" />
                <node concept="2OqwBi" id="qe" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821875106564" />
                  <node concept="2OqwBi" id="qg" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:4313507821875106564" />
                    <node concept="37vLTw" id="qi" role="2Oq$k0">
                      <ref role="3cqZAo" node="mZ" resolve="ctx" />
                      <uo k="s:originTrace" v="n:4313507821875106564" />
                    </node>
                    <node concept="liA8E" id="qj" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                      <uo k="s:originTrace" v="n:4313507821875106564" />
                    </node>
                  </node>
                  <node concept="liA8E" id="qh" role="2OqNvi">
                    <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                    <uo k="s:originTrace" v="n:4313507821875106564" />
                  </node>
                </node>
                <node concept="liA8E" id="qf" role="2OqNvi">
                  <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
                  <uo k="s:originTrace" v="n:4313507821875106564" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="ps" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821875104880" />
            <node concept="2OqwBi" id="qk" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821875104264" />
              <node concept="2OqwBi" id="qm" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821875104132" />
                <node concept="37vLTw" id="qo" role="2Oq$k0">
                  <ref role="3cqZAo" node="mZ" resolve="ctx" />
                </node>
                <node concept="liA8E" id="qp" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3Tsc0h" id="qn" role="2OqNvi">
                <ref role="3TtcxE" to="7thy:3JsE8aitKU3" resolve="required" />
                <uo k="s:originTrace" v="n:4313507821875109355" />
              </node>
            </node>
            <node concept="3GX2aA" id="ql" role="2OqNvi">
              <uo k="s:originTrace" v="n:4313507821875126619" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="nl" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875133635" />
          <node concept="3clFbS" id="qq" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821875133637" />
            <node concept="3clFbF" id="qs" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875235790" />
              <node concept="2OqwBi" id="q$" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821875235790" />
                <node concept="37vLTw" id="q_" role="2Oq$k0">
                  <ref role="3cqZAo" node="nn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821875235790" />
                </node>
                <node concept="liA8E" id="qA" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821875235790" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="qt" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875138650" />
              <node concept="2OqwBi" id="qB" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821875138650" />
                <node concept="37vLTw" id="qC" role="2Oq$k0">
                  <ref role="3cqZAo" node="nn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821875138650" />
                </node>
                <node concept="liA8E" id="qD" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821875138650" />
                  <node concept="Xl_RD" id="qE" role="37wK5m">
                    <property role="Xl_RC" value="additionalProperties:\n" />
                    <uo k="s:originTrace" v="n:4313507821875138650" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="qu" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875138787" />
              <node concept="2OqwBi" id="qF" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821875138787" />
                <node concept="2OqwBi" id="qG" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821875138787" />
                  <node concept="2OqwBi" id="qI" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:4313507821875138787" />
                    <node concept="37vLTw" id="qK" role="2Oq$k0">
                      <ref role="3cqZAo" node="mZ" resolve="ctx" />
                      <uo k="s:originTrace" v="n:4313507821875138787" />
                    </node>
                    <node concept="liA8E" id="qL" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                      <uo k="s:originTrace" v="n:4313507821875138787" />
                    </node>
                  </node>
                  <node concept="liA8E" id="qJ" role="2OqNvi">
                    <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                    <uo k="s:originTrace" v="n:4313507821875138787" />
                  </node>
                </node>
                <node concept="liA8E" id="qH" role="2OqNvi">
                  <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
                  <uo k="s:originTrace" v="n:4313507821875138787" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="qv" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875144843" />
              <node concept="2OqwBi" id="qM" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821875144843" />
                <node concept="37vLTw" id="qN" role="2Oq$k0">
                  <ref role="3cqZAo" node="nn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821875144843" />
                </node>
                <node concept="liA8E" id="qO" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821875144843" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="qw" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875144650" />
              <node concept="2OqwBi" id="qP" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821875144650" />
                <node concept="37vLTw" id="qQ" role="2Oq$k0">
                  <ref role="3cqZAo" node="nn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821875144650" />
                </node>
                <node concept="liA8E" id="qR" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821875144650" />
                  <node concept="Xl_RD" id="qS" role="37wK5m">
                    <property role="Xl_RC" value="type: " />
                    <uo k="s:originTrace" v="n:4313507821875144650" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="qx" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875138853" />
              <node concept="2OqwBi" id="qT" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821875138853" />
                <node concept="37vLTw" id="qU" role="2Oq$k0">
                  <ref role="3cqZAo" node="nn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821875138853" />
                </node>
                <node concept="liA8E" id="qV" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821875138853" />
                  <node concept="2OqwBi" id="qW" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821875143368" />
                    <node concept="2OqwBi" id="qX" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:4313507821875141355" />
                      <node concept="2OqwBi" id="qZ" role="2Oq$k0">
                        <uo k="s:originTrace" v="n:4313507821875139373" />
                        <node concept="2OqwBi" id="r1" role="2Oq$k0">
                          <uo k="s:originTrace" v="n:4313507821875138903" />
                          <node concept="37vLTw" id="r3" role="2Oq$k0">
                            <ref role="3cqZAo" node="mZ" resolve="ctx" />
                          </node>
                          <node concept="liA8E" id="r4" role="2OqNvi">
                            <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                          </node>
                        </node>
                        <node concept="3TrEf2" id="r2" role="2OqNvi">
                          <ref role="3Tt5mk" to="7thy:3JsE8ainS_0" resolve="additionalProperties" />
                          <uo k="s:originTrace" v="n:4313507821875140571" />
                        </node>
                      </node>
                      <node concept="3TrcHB" id="r0" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:3JsE8ainS_4" resolve="type" />
                        <uo k="s:originTrace" v="n:4313507821875142516" />
                      </node>
                    </node>
                    <node concept="liA8E" id="qY" role="2OqNvi">
                      <ref role="37wK5l" to="wyt6:~Object.toString()" resolve="toString" />
                      <uo k="s:originTrace" v="n:4313507821875144393" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="qy" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875207958" />
              <node concept="2OqwBi" id="r5" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821875207958" />
                <node concept="37vLTw" id="r6" role="2Oq$k0">
                  <ref role="3cqZAo" node="nn" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821875207958" />
                </node>
                <node concept="liA8E" id="r7" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821875207958" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="qz" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821875138787" />
              <node concept="2OqwBi" id="r8" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821875138787" />
                <node concept="2OqwBi" id="r9" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821875138787" />
                  <node concept="2OqwBi" id="rb" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:4313507821875138787" />
                    <node concept="37vLTw" id="rd" role="2Oq$k0">
                      <ref role="3cqZAo" node="mZ" resolve="ctx" />
                      <uo k="s:originTrace" v="n:4313507821875138787" />
                    </node>
                    <node concept="liA8E" id="re" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                      <uo k="s:originTrace" v="n:4313507821875138787" />
                    </node>
                  </node>
                  <node concept="liA8E" id="rc" role="2OqNvi">
                    <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                    <uo k="s:originTrace" v="n:4313507821875138787" />
                  </node>
                </node>
                <node concept="liA8E" id="ra" role="2OqNvi">
                  <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
                  <uo k="s:originTrace" v="n:4313507821875138787" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="qr" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821875137201" />
            <node concept="2OqwBi" id="rf" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821875135349" />
              <node concept="2OqwBi" id="rh" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821875134835" />
                <node concept="37vLTw" id="rj" role="2Oq$k0">
                  <ref role="3cqZAo" node="mZ" resolve="ctx" />
                </node>
                <node concept="liA8E" id="rk" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrEf2" id="ri" role="2OqNvi">
                <ref role="3Tt5mk" to="7thy:3JsE8ainS_0" resolve="additionalProperties" />
                <uo k="s:originTrace" v="n:4313507821875136687" />
              </node>
            </node>
            <node concept="3x8VRR" id="rg" role="2OqNvi">
              <uo k="s:originTrace" v="n:4313507821875138370" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="nm" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821875099931" />
          <node concept="2OqwBi" id="rl" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821875099931" />
            <node concept="2OqwBi" id="rm" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821875099931" />
              <node concept="2OqwBi" id="ro" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821875099931" />
                <node concept="37vLTw" id="rq" role="2Oq$k0">
                  <ref role="3cqZAo" node="mZ" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821875099931" />
                </node>
                <node concept="liA8E" id="rr" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821875099931" />
                </node>
              </node>
              <node concept="liA8E" id="rp" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821875099931" />
              </node>
            </node>
            <node concept="liA8E" id="rn" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821875099931" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="mZ" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:4313507821875098746" />
        <node concept="3uibUv" id="rs" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:4313507821875098746" />
        </node>
      </node>
      <node concept="2AHcQZ" id="n0" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821875098746" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="rt">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="OneOfSchema_TextGen" />
    <uo k="s:originTrace" v="n:8442470881495856853" />
    <node concept="3Tm1VV" id="ru" role="1B3o_S">
      <uo k="s:originTrace" v="n:8442470881495856853" />
    </node>
    <node concept="3uibUv" id="rv" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:8442470881495856853" />
    </node>
    <node concept="3clFb_" id="rw" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:8442470881495856853" />
      <node concept="3cqZAl" id="rx" role="3clF45">
        <uo k="s:originTrace" v="n:8442470881495856853" />
      </node>
      <node concept="3Tm1VV" id="ry" role="1B3o_S">
        <uo k="s:originTrace" v="n:8442470881495856853" />
      </node>
      <node concept="3clFbS" id="rz" role="3clF47">
        <uo k="s:originTrace" v="n:8442470881495856853" />
        <node concept="3cpWs8" id="rA" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881495856853" />
          <node concept="3cpWsn" id="rO" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:8442470881495856853" />
            <node concept="3uibUv" id="rP" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:8442470881495856853" />
            </node>
            <node concept="2ShNRf" id="rQ" role="33vP2m">
              <uo k="s:originTrace" v="n:8442470881495856853" />
              <node concept="1pGfFk" id="rR" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:8442470881495856853" />
                <node concept="37vLTw" id="rS" role="37wK5m">
                  <ref role="3cqZAo" node="r$" resolve="ctx" />
                  <uo k="s:originTrace" v="n:8442470881495856853" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="rB" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496002566" />
          <node concept="2OqwBi" id="rT" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496002566" />
            <node concept="37vLTw" id="rU" role="2Oq$k0">
              <ref role="3cqZAo" node="rO" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496002566" />
            </node>
            <node concept="liA8E" id="rV" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:8442470881496002566" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="rC" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496002568" />
          <node concept="2OqwBi" id="rW" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496002568" />
            <node concept="37vLTw" id="rX" role="2Oq$k0">
              <ref role="3cqZAo" node="rO" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496002568" />
            </node>
            <node concept="liA8E" id="rY" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:8442470881496002568" />
              <node concept="2OqwBi" id="rZ" role="37wK5m">
                <uo k="s:originTrace" v="n:8442470881496002569" />
                <node concept="2OqwBi" id="s0" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:8442470881496002570" />
                  <node concept="37vLTw" id="s2" role="2Oq$k0">
                    <ref role="3cqZAo" node="r$" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="s3" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="s1" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                  <uo k="s:originTrace" v="n:8442470881496002571" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="rD" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496002572" />
          <node concept="2OqwBi" id="s4" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496002572" />
            <node concept="37vLTw" id="s5" role="2Oq$k0">
              <ref role="3cqZAo" node="rO" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496002572" />
            </node>
            <node concept="liA8E" id="s6" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:8442470881496002572" />
              <node concept="Xl_RD" id="s7" role="37wK5m">
                <property role="Xl_RC" value=":" />
                <uo k="s:originTrace" v="n:8442470881496002572" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="rE" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496002573" />
          <node concept="2OqwBi" id="s8" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496002573" />
            <node concept="37vLTw" id="s9" role="2Oq$k0">
              <ref role="3cqZAo" node="rO" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496002573" />
            </node>
            <node concept="liA8E" id="sa" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:8442470881496002573" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="rF" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496002574" />
          <node concept="2OqwBi" id="sb" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496002574" />
            <node concept="2OqwBi" id="sc" role="2Oq$k0">
              <uo k="s:originTrace" v="n:8442470881496002574" />
              <node concept="2OqwBi" id="se" role="2Oq$k0">
                <uo k="s:originTrace" v="n:8442470881496002574" />
                <node concept="37vLTw" id="sg" role="2Oq$k0">
                  <ref role="3cqZAo" node="r$" resolve="ctx" />
                  <uo k="s:originTrace" v="n:8442470881496002574" />
                </node>
                <node concept="liA8E" id="sh" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:8442470881496002574" />
                </node>
              </node>
              <node concept="liA8E" id="sf" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:8442470881496002574" />
              </node>
            </node>
            <node concept="liA8E" id="sd" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:8442470881496002574" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="rG" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496002576" />
          <node concept="3clFbS" id="si" role="3clFbx">
            <uo k="s:originTrace" v="n:8442470881496002577" />
            <node concept="3clFbF" id="sk" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496002578" />
              <node concept="2OqwBi" id="so" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496002578" />
                <node concept="37vLTw" id="sp" role="2Oq$k0">
                  <ref role="3cqZAo" node="rO" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881496002578" />
                </node>
                <node concept="liA8E" id="sq" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:8442470881496002578" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="sl" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496002580" />
              <node concept="2OqwBi" id="sr" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496002580" />
                <node concept="37vLTw" id="ss" role="2Oq$k0">
                  <ref role="3cqZAo" node="rO" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881496002580" />
                </node>
                <node concept="liA8E" id="st" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881496002580" />
                  <node concept="Xl_RD" id="su" role="37wK5m">
                    <property role="Xl_RC" value="description: " />
                    <uo k="s:originTrace" v="n:8442470881496002580" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="sm" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496002581" />
              <node concept="2OqwBi" id="sv" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496002581" />
                <node concept="37vLTw" id="sw" role="2Oq$k0">
                  <ref role="3cqZAo" node="rO" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881496002581" />
                </node>
                <node concept="liA8E" id="sx" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881496002581" />
                  <node concept="2OqwBi" id="sy" role="37wK5m">
                    <uo k="s:originTrace" v="n:8442470881496002582" />
                    <node concept="2OqwBi" id="sz" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:8442470881496002583" />
                      <node concept="37vLTw" id="s_" role="2Oq$k0">
                        <ref role="3cqZAo" node="r$" resolve="ctx" />
                      </node>
                      <node concept="liA8E" id="sA" role="2OqNvi">
                        <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="s$" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                      <uo k="s:originTrace" v="n:8442470881496002584" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="sn" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881497014874" />
              <node concept="2OqwBi" id="sB" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881497014874" />
                <node concept="37vLTw" id="sC" role="2Oq$k0">
                  <ref role="3cqZAo" node="rO" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881497014874" />
                </node>
                <node concept="liA8E" id="sD" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:8442470881497014874" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="sj" role="3clFbw">
            <uo k="s:originTrace" v="n:8442470881496002585" />
            <node concept="2OqwBi" id="sE" role="2Oq$k0">
              <uo k="s:originTrace" v="n:8442470881496002586" />
              <node concept="2OqwBi" id="sG" role="2Oq$k0">
                <uo k="s:originTrace" v="n:8442470881496002587" />
                <node concept="37vLTw" id="sI" role="2Oq$k0">
                  <ref role="3cqZAo" node="r$" resolve="ctx" />
                </node>
                <node concept="liA8E" id="sJ" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="sH" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                <uo k="s:originTrace" v="n:8442470881496002588" />
              </node>
            </node>
            <node concept="17RvpY" id="sF" role="2OqNvi">
              <uo k="s:originTrace" v="n:8442470881496002589" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="rH" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496002592" />
          <node concept="2OqwBi" id="sK" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496002592" />
            <node concept="37vLTw" id="sL" role="2Oq$k0">
              <ref role="3cqZAo" node="rO" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496002592" />
            </node>
            <node concept="liA8E" id="sM" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:8442470881496002592" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="rI" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496002594" />
          <node concept="2OqwBi" id="sN" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496002594" />
            <node concept="37vLTw" id="sO" role="2Oq$k0">
              <ref role="3cqZAo" node="rO" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496002594" />
            </node>
            <node concept="liA8E" id="sP" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:8442470881496002594" />
              <node concept="Xl_RD" id="sQ" role="37wK5m">
                <property role="Xl_RC" value="oneOf:" />
                <uo k="s:originTrace" v="n:8442470881496002594" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="rJ" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496002595" />
          <node concept="2OqwBi" id="sR" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496002595" />
            <node concept="37vLTw" id="sS" role="2Oq$k0">
              <ref role="3cqZAo" node="rO" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496002595" />
            </node>
            <node concept="liA8E" id="sT" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:8442470881496002595" />
            </node>
          </node>
        </node>
        <node concept="2Gpval" id="rK" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496002596" />
          <node concept="2GrKxI" id="sU" role="2Gsz3X">
            <property role="TrG5h" value="elem" />
            <uo k="s:originTrace" v="n:8442470881496002597" />
          </node>
          <node concept="2OqwBi" id="sV" role="2GsD0m">
            <uo k="s:originTrace" v="n:8442470881496002598" />
            <node concept="2OqwBi" id="sX" role="2Oq$k0">
              <uo k="s:originTrace" v="n:8442470881496002599" />
              <node concept="37vLTw" id="sZ" role="2Oq$k0">
                <ref role="3cqZAo" node="r$" resolve="ctx" />
              </node>
              <node concept="liA8E" id="t0" role="2OqNvi">
                <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
              </node>
            </node>
            <node concept="3Tsc0h" id="sY" role="2OqNvi">
              <ref role="3TtcxE" to="7thy:3JsE8aiB2oZ" resolve="oneOf" />
              <uo k="s:originTrace" v="n:8442470881496002600" />
            </node>
          </node>
          <node concept="3clFbS" id="sW" role="2LFqv$">
            <uo k="s:originTrace" v="n:8442470881496002601" />
            <node concept="3clFbF" id="t1" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496002602" />
              <node concept="2OqwBi" id="t9" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496002602" />
                <node concept="2OqwBi" id="ta" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:8442470881496002602" />
                  <node concept="2OqwBi" id="tc" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:8442470881496002602" />
                    <node concept="37vLTw" id="te" role="2Oq$k0">
                      <ref role="3cqZAo" node="r$" resolve="ctx" />
                      <uo k="s:originTrace" v="n:8442470881496002602" />
                    </node>
                    <node concept="liA8E" id="tf" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                      <uo k="s:originTrace" v="n:8442470881496002602" />
                    </node>
                  </node>
                  <node concept="liA8E" id="td" role="2OqNvi">
                    <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                    <uo k="s:originTrace" v="n:8442470881496002602" />
                  </node>
                </node>
                <node concept="liA8E" id="tb" role="2OqNvi">
                  <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
                  <uo k="s:originTrace" v="n:8442470881496002602" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="t2" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496002604" />
              <node concept="2OqwBi" id="tg" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496002604" />
                <node concept="37vLTw" id="th" role="2Oq$k0">
                  <ref role="3cqZAo" node="rO" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881496002604" />
                </node>
                <node concept="liA8E" id="ti" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:8442470881496002604" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="t3" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496002606" />
              <node concept="2OqwBi" id="tj" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496002606" />
                <node concept="37vLTw" id="tk" role="2Oq$k0">
                  <ref role="3cqZAo" node="rO" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881496002606" />
                </node>
                <node concept="liA8E" id="tl" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881496002606" />
                  <node concept="Xl_RD" id="tm" role="37wK5m">
                    <property role="Xl_RC" value="- $ref: " />
                    <uo k="s:originTrace" v="n:8442470881496002606" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="t4" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496002607" />
              <node concept="2OqwBi" id="tn" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496002607" />
                <node concept="37vLTw" id="to" role="2Oq$k0">
                  <ref role="3cqZAo" node="rO" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881496002607" />
                </node>
                <node concept="liA8E" id="tp" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881496002607" />
                  <node concept="Xl_RD" id="tq" role="37wK5m">
                    <property role="Xl_RC" value="'#/components/schemas/" />
                    <uo k="s:originTrace" v="n:8442470881496002607" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="t5" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496002608" />
              <node concept="2OqwBi" id="tr" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496002608" />
                <node concept="37vLTw" id="ts" role="2Oq$k0">
                  <ref role="3cqZAo" node="rO" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881496002608" />
                </node>
                <node concept="liA8E" id="tt" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881496002608" />
                  <node concept="2OqwBi" id="tu" role="37wK5m">
                    <uo k="s:originTrace" v="n:8442470881496002609" />
                    <node concept="2OqwBi" id="tv" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:8442470881496002610" />
                      <node concept="2GrUjf" id="tx" role="2Oq$k0">
                        <ref role="2Gs0qQ" node="sU" resolve="elem" />
                        <uo k="s:originTrace" v="n:8442470881496002611" />
                      </node>
                      <node concept="3TrEf2" id="ty" role="2OqNvi">
                        <ref role="3Tt5mk" to="7thy:3JsE8aiBPe0" resolve="schema" />
                        <uo k="s:originTrace" v="n:8442470881496002612" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="tw" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                      <uo k="s:originTrace" v="n:8442470881496002613" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="t6" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496235834" />
              <node concept="2OqwBi" id="tz" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496235834" />
                <node concept="37vLTw" id="t$" role="2Oq$k0">
                  <ref role="3cqZAo" node="rO" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881496235834" />
                </node>
                <node concept="liA8E" id="t_" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881496235834" />
                  <node concept="Xl_RD" id="tA" role="37wK5m">
                    <property role="Xl_RC" value="'" />
                    <uo k="s:originTrace" v="n:8442470881496235834" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="t7" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881497014949" />
              <node concept="2OqwBi" id="tB" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881497014949" />
                <node concept="37vLTw" id="tC" role="2Oq$k0">
                  <ref role="3cqZAo" node="rO" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881497014949" />
                </node>
                <node concept="liA8E" id="tD" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:8442470881497014949" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="t8" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881496002602" />
              <node concept="2OqwBi" id="tE" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881496002602" />
                <node concept="2OqwBi" id="tF" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:8442470881496002602" />
                  <node concept="2OqwBi" id="tH" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:8442470881496002602" />
                    <node concept="37vLTw" id="tJ" role="2Oq$k0">
                      <ref role="3cqZAo" node="r$" resolve="ctx" />
                      <uo k="s:originTrace" v="n:8442470881496002602" />
                    </node>
                    <node concept="liA8E" id="tK" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                      <uo k="s:originTrace" v="n:8442470881496002602" />
                    </node>
                  </node>
                  <node concept="liA8E" id="tI" role="2OqNvi">
                    <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                    <uo k="s:originTrace" v="n:8442470881496002602" />
                  </node>
                </node>
                <node concept="liA8E" id="tG" role="2OqNvi">
                  <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
                  <uo k="s:originTrace" v="n:8442470881496002602" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="rL" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600256622" />
          <node concept="3clFbS" id="tL" role="3clFbx">
            <uo k="s:originTrace" v="n:227067257600256623" />
            <node concept="3clFbF" id="tN" role="3cqZAp">
              <uo k="s:originTrace" v="n:227067257600256624" />
              <node concept="2OqwBi" id="tO" role="3clFbG">
                <node concept="37vLTw" id="tP" role="2Oq$k0">
                  <ref role="3cqZAo" node="rO" resolve="tgs" />
                </node>
                <node concept="liA8E" id="tQ" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.reportError(java.lang.String)" resolve="reportError" />
                  <node concept="Xl_RD" id="tR" role="37wK5m">
                    <property role="Xl_RC" value="oneOf may not be empty" />
                    <uo k="s:originTrace" v="n:227067257600256625" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="tM" role="3clFbw">
            <uo k="s:originTrace" v="n:227067257600256626" />
            <node concept="2OqwBi" id="tS" role="2Oq$k0">
              <uo k="s:originTrace" v="n:227067257600256627" />
              <node concept="2OqwBi" id="tU" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600256628" />
                <node concept="37vLTw" id="tW" role="2Oq$k0">
                  <ref role="3cqZAo" node="r$" resolve="ctx" />
                </node>
                <node concept="liA8E" id="tX" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3Tsc0h" id="tV" role="2OqNvi">
                <ref role="3TtcxE" to="7thy:3JsE8aiB2oZ" resolve="oneOf" />
                <uo k="s:originTrace" v="n:227067257600256629" />
              </node>
            </node>
            <node concept="1v1jN8" id="tT" role="2OqNvi">
              <uo k="s:originTrace" v="n:227067257600256630" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="rM" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600256466" />
        </node>
        <node concept="3clFbF" id="rN" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496002574" />
          <node concept="2OqwBi" id="tY" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496002574" />
            <node concept="2OqwBi" id="tZ" role="2Oq$k0">
              <uo k="s:originTrace" v="n:8442470881496002574" />
              <node concept="2OqwBi" id="u1" role="2Oq$k0">
                <uo k="s:originTrace" v="n:8442470881496002574" />
                <node concept="37vLTw" id="u3" role="2Oq$k0">
                  <ref role="3cqZAo" node="r$" resolve="ctx" />
                  <uo k="s:originTrace" v="n:8442470881496002574" />
                </node>
                <node concept="liA8E" id="u4" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:8442470881496002574" />
                </node>
              </node>
              <node concept="liA8E" id="u2" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:8442470881496002574" />
              </node>
            </node>
            <node concept="liA8E" id="u0" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:8442470881496002574" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="r$" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:8442470881495856853" />
        <node concept="3uibUv" id="u5" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:8442470881495856853" />
        </node>
      </node>
      <node concept="2AHcQZ" id="r_" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:8442470881495856853" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="u6">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="OpenApiInfo_TextGen" />
    <uo k="s:originTrace" v="n:4313507821874192908" />
    <node concept="3Tm1VV" id="u7" role="1B3o_S">
      <uo k="s:originTrace" v="n:4313507821874192908" />
    </node>
    <node concept="3uibUv" id="u8" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:4313507821874192908" />
    </node>
    <node concept="3clFb_" id="u9" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:4313507821874192908" />
      <node concept="3cqZAl" id="ua" role="3clF45">
        <uo k="s:originTrace" v="n:4313507821874192908" />
      </node>
      <node concept="3Tm1VV" id="ub" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821874192908" />
      </node>
      <node concept="3clFbS" id="uc" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821874192908" />
        <node concept="3cpWs8" id="uf" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874192908" />
          <node concept="3cpWsn" id="ut" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:4313507821874192908" />
            <node concept="3uibUv" id="uu" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:4313507821874192908" />
            </node>
            <node concept="2ShNRf" id="uv" role="33vP2m">
              <uo k="s:originTrace" v="n:4313507821874192908" />
              <node concept="1pGfFk" id="uw" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:4313507821874192908" />
                <node concept="37vLTw" id="ux" role="37wK5m">
                  <ref role="3cqZAo" node="ud" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874192908" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ug" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874523975" />
          <node concept="2OqwBi" id="uy" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874523975" />
            <node concept="37vLTw" id="uz" role="2Oq$k0">
              <ref role="3cqZAo" node="ut" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874523975" />
            </node>
            <node concept="liA8E" id="u$" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874523975" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="uh" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874195646" />
          <node concept="2OqwBi" id="u_" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874195646" />
            <node concept="37vLTw" id="uA" role="2Oq$k0">
              <ref role="3cqZAo" node="ut" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874195646" />
            </node>
            <node concept="liA8E" id="uB" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874195646" />
              <node concept="Xl_RD" id="uC" role="37wK5m">
                <property role="Xl_RC" value="title: " />
                <uo k="s:originTrace" v="n:4313507821874195646" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ui" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874195771" />
          <node concept="2OqwBi" id="uD" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874195771" />
            <node concept="37vLTw" id="uE" role="2Oq$k0">
              <ref role="3cqZAo" node="ut" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874195771" />
            </node>
            <node concept="liA8E" id="uF" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874195771" />
              <node concept="2OqwBi" id="uG" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821874196294" />
                <node concept="2OqwBi" id="uH" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874195823" />
                  <node concept="37vLTw" id="uJ" role="2Oq$k0">
                    <ref role="3cqZAo" node="ud" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="uK" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="uI" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:3JW8_axMFqV" resolve="title" />
                  <uo k="s:originTrace" v="n:4313507821874197422" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="uj" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874197505" />
          <node concept="2OqwBi" id="uL" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874197505" />
            <node concept="37vLTw" id="uM" role="2Oq$k0">
              <ref role="3cqZAo" node="ut" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874197505" />
            </node>
            <node concept="liA8E" id="uN" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821874197505" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="uk" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600404135" />
          <node concept="3clFbS" id="uO" role="3clFbx">
            <uo k="s:originTrace" v="n:227067257600404136" />
            <node concept="3clFbF" id="uQ" role="3cqZAp">
              <uo k="s:originTrace" v="n:227067257600404137" />
              <node concept="2OqwBi" id="uR" role="3clFbG">
                <node concept="37vLTw" id="uS" role="2Oq$k0">
                  <ref role="3cqZAo" node="ut" resolve="tgs" />
                </node>
                <node concept="liA8E" id="uT" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.reportError(java.lang.String)" resolve="reportError" />
                  <node concept="Xl_RD" id="uU" role="37wK5m">
                    <property role="Xl_RC" value="title is required" />
                    <uo k="s:originTrace" v="n:227067257600404138" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="uP" role="3clFbw">
            <uo k="s:originTrace" v="n:227067257600404139" />
            <node concept="2OqwBi" id="uV" role="2Oq$k0">
              <uo k="s:originTrace" v="n:227067257600404140" />
              <node concept="2OqwBi" id="uX" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600404141" />
                <node concept="37vLTw" id="uZ" role="2Oq$k0">
                  <ref role="3cqZAo" node="ud" resolve="ctx" />
                </node>
                <node concept="liA8E" id="v0" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="uY" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axMFqV" resolve="title" />
                <uo k="s:originTrace" v="n:227067257600404142" />
              </node>
            </node>
            <node concept="17RlXB" id="uW" role="2OqNvi">
              <uo k="s:originTrace" v="n:227067257600404143" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="ul" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600403997" />
        </node>
        <node concept="3clFbJ" id="um" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874452888" />
          <node concept="3clFbS" id="v1" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874452890" />
            <node concept="3clFbF" id="v3" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874524123" />
              <node concept="2OqwBi" id="v7" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874524123" />
                <node concept="37vLTw" id="v8" role="2Oq$k0">
                  <ref role="3cqZAo" node="ut" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874524123" />
                </node>
                <node concept="liA8E" id="v9" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874524123" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="v4" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874197629" />
              <node concept="2OqwBi" id="va" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874197629" />
                <node concept="37vLTw" id="vb" role="2Oq$k0">
                  <ref role="3cqZAo" node="ut" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874197629" />
                </node>
                <node concept="liA8E" id="vc" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874197629" />
                  <node concept="Xl_RD" id="vd" role="37wK5m">
                    <property role="Xl_RC" value="description: " />
                    <uo k="s:originTrace" v="n:4313507821874197629" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="v5" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874197764" />
              <node concept="2OqwBi" id="ve" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874197764" />
                <node concept="37vLTw" id="vf" role="2Oq$k0">
                  <ref role="3cqZAo" node="ut" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874197764" />
                </node>
                <node concept="liA8E" id="vg" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874197764" />
                  <node concept="2OqwBi" id="vh" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874198287" />
                    <node concept="2OqwBi" id="vi" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:4313507821874197816" />
                      <node concept="37vLTw" id="vk" role="2Oq$k0">
                        <ref role="3cqZAo" node="ud" resolve="ctx" />
                      </node>
                      <node concept="liA8E" id="vl" role="2OqNvi">
                        <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="vj" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:3JW8_axMFqX" resolve="description" />
                      <uo k="s:originTrace" v="n:4313507821874199450" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="v6" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874199659" />
              <node concept="2OqwBi" id="vm" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874199659" />
                <node concept="37vLTw" id="vn" role="2Oq$k0">
                  <ref role="3cqZAo" node="ut" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874199659" />
                </node>
                <node concept="liA8E" id="vo" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874199659" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="v2" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874455877" />
            <node concept="2OqwBi" id="vp" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874453498" />
              <node concept="2OqwBi" id="vr" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874452984" />
                <node concept="37vLTw" id="vt" role="2Oq$k0">
                  <ref role="3cqZAo" node="ud" resolve="ctx" />
                </node>
                <node concept="liA8E" id="vu" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="vs" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axMFqX" resolve="description" />
                <uo k="s:originTrace" v="n:4313507821874454661" />
              </node>
            </node>
            <node concept="17RvpY" id="vq" role="2OqNvi">
              <uo k="s:originTrace" v="n:4313507821874457903" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="un" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600405427" />
          <node concept="3clFbS" id="vv" role="3clFbx">
            <uo k="s:originTrace" v="n:227067257600405428" />
            <node concept="3clFbF" id="vx" role="3cqZAp">
              <uo k="s:originTrace" v="n:227067257600405429" />
              <node concept="2OqwBi" id="vy" role="3clFbG">
                <node concept="37vLTw" id="vz" role="2Oq$k0">
                  <ref role="3cqZAo" node="ut" resolve="tgs" />
                </node>
                <node concept="liA8E" id="v$" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.reportError(java.lang.String)" resolve="reportError" />
                  <node concept="Xl_RD" id="v_" role="37wK5m">
                    <property role="Xl_RC" value="version is required" />
                    <uo k="s:originTrace" v="n:227067257600405430" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="vw" role="3clFbw">
            <uo k="s:originTrace" v="n:227067257600405431" />
            <node concept="2OqwBi" id="vA" role="2Oq$k0">
              <uo k="s:originTrace" v="n:227067257600405432" />
              <node concept="2OqwBi" id="vC" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600405433" />
                <node concept="37vLTw" id="vE" role="2Oq$k0">
                  <ref role="3cqZAo" node="ud" resolve="ctx" />
                </node>
                <node concept="liA8E" id="vF" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="vD" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axMFr0" resolve="version" />
                <uo k="s:originTrace" v="n:227067257600405434" />
              </node>
            </node>
            <node concept="17RlXB" id="vB" role="2OqNvi">
              <uo k="s:originTrace" v="n:227067257600405435" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="uo" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600405129" />
        </node>
        <node concept="3clFbF" id="up" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874524271" />
          <node concept="2OqwBi" id="vG" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874524271" />
            <node concept="37vLTw" id="vH" role="2Oq$k0">
              <ref role="3cqZAo" node="ut" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874524271" />
            </node>
            <node concept="liA8E" id="vI" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874524271" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="uq" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874199831" />
          <node concept="2OqwBi" id="vJ" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874199831" />
            <node concept="37vLTw" id="vK" role="2Oq$k0">
              <ref role="3cqZAo" node="ut" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874199831" />
            </node>
            <node concept="liA8E" id="vL" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874199831" />
              <node concept="Xl_RD" id="vM" role="37wK5m">
                <property role="Xl_RC" value="version: " />
                <uo k="s:originTrace" v="n:4313507821874199831" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ur" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874199896" />
          <node concept="2OqwBi" id="vN" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874199896" />
            <node concept="37vLTw" id="vO" role="2Oq$k0">
              <ref role="3cqZAo" node="ut" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874199896" />
            </node>
            <node concept="liA8E" id="vP" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874199896" />
              <node concept="2OqwBi" id="vQ" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821874199977" />
                <node concept="2OqwBi" id="vR" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874199948" />
                  <node concept="37vLTw" id="vT" role="2Oq$k0">
                    <ref role="3cqZAo" node="ud" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="vU" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="vS" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:3JW8_axMFr0" resolve="version" />
                  <uo k="s:originTrace" v="n:4313507821874200219" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="us" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874200302" />
          <node concept="2OqwBi" id="vV" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874200302" />
            <node concept="37vLTw" id="vW" role="2Oq$k0">
              <ref role="3cqZAo" node="ut" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874200302" />
            </node>
            <node concept="liA8E" id="vX" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821874200302" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="ud" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:4313507821874192908" />
        <node concept="3uibUv" id="vY" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:4313507821874192908" />
        </node>
      </node>
      <node concept="2AHcQZ" id="ue" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821874192908" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="vZ">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="OpenApi_TextGen" />
    <uo k="s:originTrace" v="n:4313507821874160941" />
    <node concept="3Tm1VV" id="w0" role="1B3o_S">
      <uo k="s:originTrace" v="n:4313507821874160941" />
    </node>
    <node concept="3uibUv" id="w1" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:4313507821874160941" />
    </node>
    <node concept="3clFb_" id="w2" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:4313507821874160941" />
      <node concept="3cqZAl" id="w3" role="3clF45">
        <uo k="s:originTrace" v="n:4313507821874160941" />
      </node>
      <node concept="3Tm1VV" id="w4" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821874160941" />
      </node>
      <node concept="3clFbS" id="w5" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821874160941" />
        <node concept="3cpWs8" id="w8" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874160941" />
          <node concept="3cpWsn" id="wo" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:4313507821874160941" />
            <node concept="3uibUv" id="wp" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:4313507821874160941" />
            </node>
            <node concept="2ShNRf" id="wq" role="33vP2m">
              <uo k="s:originTrace" v="n:4313507821874160941" />
              <node concept="1pGfFk" id="wr" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:4313507821874160941" />
                <node concept="37vLTw" id="ws" role="37wK5m">
                  <ref role="3cqZAo" node="w6" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874160941" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="w9" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874160979" />
          <node concept="2OqwBi" id="wt" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874160979" />
            <node concept="37vLTw" id="wu" role="2Oq$k0">
              <ref role="3cqZAo" node="wo" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874160979" />
            </node>
            <node concept="liA8E" id="wv" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874160979" />
              <node concept="Xl_RD" id="ww" role="37wK5m">
                <property role="Xl_RC" value="openapi: " />
                <uo k="s:originTrace" v="n:4313507821874160979" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="wa" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496074093" />
          <node concept="2OqwBi" id="wx" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496074093" />
            <node concept="37vLTw" id="wy" role="2Oq$k0">
              <ref role="3cqZAo" node="wo" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496074093" />
            </node>
            <node concept="liA8E" id="wz" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:8442470881496074093" />
              <node concept="2OqwBi" id="w$" role="37wK5m">
                <uo k="s:originTrace" v="n:8442470881496074541" />
                <node concept="2OqwBi" id="w_" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:8442470881496074148" />
                  <node concept="37vLTw" id="wB" role="2Oq$k0">
                    <ref role="3cqZAo" node="w6" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="wC" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="wA" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:3JW8_axMYAC" resolve="openapi" />
                  <uo k="s:originTrace" v="n:8442470881496075487" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="wb" role="3cqZAp">
          <uo k="s:originTrace" v="n:8442470881496128094" />
          <node concept="2OqwBi" id="wD" role="3clFbG">
            <uo k="s:originTrace" v="n:8442470881496128094" />
            <node concept="37vLTw" id="wE" role="2Oq$k0">
              <ref role="3cqZAo" node="wo" resolve="tgs" />
              <uo k="s:originTrace" v="n:8442470881496128094" />
            </node>
            <node concept="liA8E" id="wF" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:8442470881496128094" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="wc" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874193337" />
          <node concept="2OqwBi" id="wG" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874193337" />
            <node concept="37vLTw" id="wH" role="2Oq$k0">
              <ref role="3cqZAo" node="wo" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874193337" />
            </node>
            <node concept="liA8E" id="wI" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874193337" />
              <node concept="Xl_RD" id="wJ" role="37wK5m">
                <property role="Xl_RC" value="info:\n" />
                <uo k="s:originTrace" v="n:4313507821874193337" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="wd" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874544366" />
          <node concept="2OqwBi" id="wK" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874544366" />
            <node concept="2OqwBi" id="wL" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874544366" />
              <node concept="2OqwBi" id="wN" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874544366" />
                <node concept="37vLTw" id="wP" role="2Oq$k0">
                  <ref role="3cqZAo" node="w6" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874544366" />
                </node>
                <node concept="liA8E" id="wQ" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874544366" />
                </node>
              </node>
              <node concept="liA8E" id="wO" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874544366" />
              </node>
            </node>
            <node concept="liA8E" id="wM" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874544366" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="we" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874174287" />
          <node concept="2OqwBi" id="wR" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874174287" />
            <node concept="37vLTw" id="wS" role="2Oq$k0">
              <ref role="3cqZAo" node="wo" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874174287" />
            </node>
            <node concept="liA8E" id="wT" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.appendNode(org.jetbrains.mps.openapi.model.SNode)" resolve="appendNode" />
              <uo k="s:originTrace" v="n:4313507821874174287" />
              <node concept="2OqwBi" id="wU" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821874174775" />
                <node concept="2OqwBi" id="wV" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874174339" />
                  <node concept="37vLTw" id="wX" role="2Oq$k0">
                    <ref role="3cqZAo" node="w6" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="wY" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrEf2" id="wW" role="2OqNvi">
                  <ref role="3Tt5mk" to="7thy:3JW8_axMW7J" resolve="info" />
                  <uo k="s:originTrace" v="n:4313507821874175751" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="wf" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874544366" />
          <node concept="2OqwBi" id="wZ" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874544366" />
            <node concept="2OqwBi" id="x0" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874544366" />
              <node concept="2OqwBi" id="x2" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874544366" />
                <node concept="37vLTw" id="x4" role="2Oq$k0">
                  <ref role="3cqZAo" node="w6" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874544366" />
                </node>
                <node concept="liA8E" id="x5" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874544366" />
                </node>
              </node>
              <node concept="liA8E" id="x3" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874544366" />
              </node>
            </node>
            <node concept="liA8E" id="x1" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874544366" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="wg" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874176475" />
        </node>
        <node concept="3clFbF" id="wh" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874176972" />
          <node concept="2OqwBi" id="x6" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874176972" />
            <node concept="37vLTw" id="x7" role="2Oq$k0">
              <ref role="3cqZAo" node="wo" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874176972" />
            </node>
            <node concept="liA8E" id="x8" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874176972" />
              <node concept="Xl_RD" id="x9" role="37wK5m">
                <property role="Xl_RC" value="paths:\n" />
                <uo k="s:originTrace" v="n:4313507821874176972" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="wi" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874564543" />
          <node concept="2OqwBi" id="xa" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874564543" />
            <node concept="2OqwBi" id="xb" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874564543" />
              <node concept="2OqwBi" id="xd" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874564543" />
                <node concept="37vLTw" id="xf" role="2Oq$k0">
                  <ref role="3cqZAo" node="w6" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874564543" />
                </node>
                <node concept="liA8E" id="xg" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874564543" />
                </node>
              </node>
              <node concept="liA8E" id="xe" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874564543" />
              </node>
            </node>
            <node concept="liA8E" id="xc" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874564543" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="wj" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600287725" />
          <node concept="3clFbS" id="xh" role="3clFbx">
            <uo k="s:originTrace" v="n:227067257600287727" />
            <node concept="3clFbF" id="xj" role="3cqZAp">
              <uo k="s:originTrace" v="n:227067257600308094" />
              <node concept="2OqwBi" id="xk" role="3clFbG">
                <node concept="37vLTw" id="xl" role="2Oq$k0">
                  <ref role="3cqZAo" node="wo" resolve="tgs" />
                </node>
                <node concept="liA8E" id="xm" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.reportError(java.lang.String)" resolve="reportError" />
                  <node concept="Xl_RD" id="xn" role="37wK5m">
                    <property role="Xl_RC" value="paths may not be empty" />
                    <uo k="s:originTrace" v="n:227067257600308111" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="xi" role="3clFbw">
            <uo k="s:originTrace" v="n:227067257600295292" />
            <node concept="2OqwBi" id="xo" role="2Oq$k0">
              <uo k="s:originTrace" v="n:227067257600288238" />
              <node concept="2OqwBi" id="xq" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600287766" />
                <node concept="37vLTw" id="xs" role="2Oq$k0">
                  <ref role="3cqZAo" node="w6" resolve="ctx" />
                </node>
                <node concept="liA8E" id="xt" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3Tsc0h" id="xr" role="2OqNvi">
                <ref role="3TtcxE" to="7thy:3JW8_axN3xZ" resolve="paths" />
                <uo k="s:originTrace" v="n:227067257600289149" />
              </node>
            </node>
            <node concept="1v1jN8" id="xp" role="2OqNvi">
              <uo k="s:originTrace" v="n:227067257600308031" />
            </node>
          </node>
        </node>
        <node concept="1DcWWT" id="wk" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874399423" />
          <node concept="3clFbS" id="xu" role="2LFqv$">
            <uo k="s:originTrace" v="n:4313507821874399423" />
            <node concept="3clFbF" id="xx" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874399423" />
              <node concept="2OqwBi" id="xy" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874399423" />
                <node concept="37vLTw" id="xz" role="2Oq$k0">
                  <ref role="3cqZAo" node="wo" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874399423" />
                </node>
                <node concept="liA8E" id="x$" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.appendNode(org.jetbrains.mps.openapi.model.SNode)" resolve="appendNode" />
                  <uo k="s:originTrace" v="n:4313507821874399423" />
                  <node concept="37vLTw" id="x_" role="37wK5m">
                    <ref role="3cqZAo" node="xv" resolve="item" />
                    <uo k="s:originTrace" v="n:4313507821874399423" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="xv" role="1Duv9x">
            <property role="TrG5h" value="item" />
            <uo k="s:originTrace" v="n:4313507821874399423" />
            <node concept="3Tqbb2" id="xA" role="1tU5fm">
              <uo k="s:originTrace" v="n:4313507821874399423" />
            </node>
          </node>
          <node concept="2OqwBi" id="xw" role="1DdaDG">
            <uo k="s:originTrace" v="n:4313507821874399807" />
            <node concept="2OqwBi" id="xB" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874399443" />
              <node concept="37vLTw" id="xD" role="2Oq$k0">
                <ref role="3cqZAo" node="w6" resolve="ctx" />
              </node>
              <node concept="liA8E" id="xE" role="2OqNvi">
                <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
              </node>
            </node>
            <node concept="3Tsc0h" id="xC" role="2OqNvi">
              <ref role="3TtcxE" to="7thy:3JW8_axN3xZ" resolve="paths" />
              <uo k="s:originTrace" v="n:4313507821874400747" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="wl" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874564543" />
          <node concept="2OqwBi" id="xF" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874564543" />
            <node concept="2OqwBi" id="xG" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874564543" />
              <node concept="2OqwBi" id="xI" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874564543" />
                <node concept="37vLTw" id="xK" role="2Oq$k0">
                  <ref role="3cqZAo" node="w6" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874564543" />
                </node>
                <node concept="liA8E" id="xL" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874564543" />
                </node>
              </node>
              <node concept="liA8E" id="xJ" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874564543" />
              </node>
            </node>
            <node concept="liA8E" id="xH" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874564543" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="wm" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874177617" />
        </node>
        <node concept="3clFbJ" id="wn" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600407783" />
          <node concept="3clFbS" id="xM" role="3clFbx">
            <uo k="s:originTrace" v="n:227067257600407785" />
            <node concept="3clFbF" id="xO" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881495873527" />
              <node concept="2OqwBi" id="xY" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881495873527" />
                <node concept="37vLTw" id="xZ" role="2Oq$k0">
                  <ref role="3cqZAo" node="wo" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881495873527" />
                </node>
                <node concept="liA8E" id="y0" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:8442470881495873527" />
                  <node concept="Xl_RD" id="y1" role="37wK5m">
                    <property role="Xl_RC" value="components:" />
                    <uo k="s:originTrace" v="n:8442470881495873527" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="xP" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881495873610" />
              <node concept="2OqwBi" id="y2" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881495873610" />
                <node concept="37vLTw" id="y3" role="2Oq$k0">
                  <ref role="3cqZAo" node="wo" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881495873610" />
                </node>
                <node concept="liA8E" id="y4" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:8442470881495873610" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="xQ" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881495874039" />
              <node concept="2OqwBi" id="y5" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881495874039" />
                <node concept="2OqwBi" id="y6" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:8442470881495874039" />
                  <node concept="2OqwBi" id="y8" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:8442470881495874039" />
                    <node concept="37vLTw" id="ya" role="2Oq$k0">
                      <ref role="3cqZAo" node="w6" resolve="ctx" />
                      <uo k="s:originTrace" v="n:8442470881495874039" />
                    </node>
                    <node concept="liA8E" id="yb" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                      <uo k="s:originTrace" v="n:8442470881495874039" />
                    </node>
                  </node>
                  <node concept="liA8E" id="y9" role="2OqNvi">
                    <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                    <uo k="s:originTrace" v="n:8442470881495874039" />
                  </node>
                </node>
                <node concept="liA8E" id="y7" role="2OqNvi">
                  <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
                  <uo k="s:originTrace" v="n:8442470881495874039" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="xR" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881495874290" />
              <node concept="2OqwBi" id="yc" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881495874290" />
                <node concept="37vLTw" id="yd" role="2Oq$k0">
                  <ref role="3cqZAo" node="wo" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881495874290" />
                </node>
                <node concept="liA8E" id="ye" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:8442470881495874290" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="xS" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874177706" />
              <node concept="2OqwBi" id="yf" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874177706" />
                <node concept="37vLTw" id="yg" role="2Oq$k0">
                  <ref role="3cqZAo" node="wo" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874177706" />
                </node>
                <node concept="liA8E" id="yh" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874177706" />
                  <node concept="Xl_RD" id="yi" role="37wK5m">
                    <property role="Xl_RC" value="schemas:" />
                    <uo k="s:originTrace" v="n:4313507821874177706" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="xT" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881495873664" />
              <node concept="2OqwBi" id="yj" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881495873664" />
                <node concept="37vLTw" id="yk" role="2Oq$k0">
                  <ref role="3cqZAo" node="wo" resolve="tgs" />
                  <uo k="s:originTrace" v="n:8442470881495873664" />
                </node>
                <node concept="liA8E" id="yl" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:8442470881495873664" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="xU" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874177707" />
              <node concept="2OqwBi" id="ym" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874177707" />
                <node concept="2OqwBi" id="yn" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874177707" />
                  <node concept="2OqwBi" id="yp" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:4313507821874177707" />
                    <node concept="37vLTw" id="yr" role="2Oq$k0">
                      <ref role="3cqZAo" node="w6" resolve="ctx" />
                      <uo k="s:originTrace" v="n:4313507821874177707" />
                    </node>
                    <node concept="liA8E" id="ys" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                      <uo k="s:originTrace" v="n:4313507821874177707" />
                    </node>
                  </node>
                  <node concept="liA8E" id="yq" role="2OqNvi">
                    <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                    <uo k="s:originTrace" v="n:4313507821874177707" />
                  </node>
                </node>
                <node concept="liA8E" id="yo" role="2OqNvi">
                  <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
                  <uo k="s:originTrace" v="n:4313507821874177707" />
                </node>
              </node>
            </node>
            <node concept="1DcWWT" id="xV" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874400817" />
              <node concept="3clFbS" id="yt" role="2LFqv$">
                <uo k="s:originTrace" v="n:4313507821874400817" />
                <node concept="3clFbF" id="yw" role="3cqZAp">
                  <uo k="s:originTrace" v="n:4313507821874400817" />
                  <node concept="2OqwBi" id="yx" role="3clFbG">
                    <uo k="s:originTrace" v="n:4313507821874400817" />
                    <node concept="37vLTw" id="yy" role="2Oq$k0">
                      <ref role="3cqZAo" node="wo" resolve="tgs" />
                      <uo k="s:originTrace" v="n:4313507821874400817" />
                    </node>
                    <node concept="liA8E" id="yz" role="2OqNvi">
                      <ref role="37wK5l" to="kpbf:~TextGenSupport.appendNode(org.jetbrains.mps.openapi.model.SNode)" resolve="appendNode" />
                      <uo k="s:originTrace" v="n:4313507821874400817" />
                      <node concept="37vLTw" id="y$" role="37wK5m">
                        <ref role="3cqZAo" node="yu" resolve="item" />
                        <uo k="s:originTrace" v="n:4313507821874400817" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWsn" id="yu" role="1Duv9x">
                <property role="TrG5h" value="item" />
                <uo k="s:originTrace" v="n:4313507821874400817" />
                <node concept="3Tqbb2" id="y_" role="1tU5fm">
                  <uo k="s:originTrace" v="n:4313507821874400817" />
                </node>
              </node>
              <node concept="2OqwBi" id="yv" role="1DdaDG">
                <uo k="s:originTrace" v="n:4313507821874401199" />
                <node concept="2OqwBi" id="yA" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874400835" />
                  <node concept="37vLTw" id="yC" role="2Oq$k0">
                    <ref role="3cqZAo" node="w6" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="yD" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3Tsc0h" id="yB" role="2OqNvi">
                  <ref role="3TtcxE" to="7thy:4favA_iCQSJ" resolve="schema" />
                  <uo k="s:originTrace" v="n:4313507821874402139" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="xW" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874177707" />
              <node concept="2OqwBi" id="yE" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874177707" />
                <node concept="2OqwBi" id="yF" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874177707" />
                  <node concept="2OqwBi" id="yH" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:4313507821874177707" />
                    <node concept="37vLTw" id="yJ" role="2Oq$k0">
                      <ref role="3cqZAo" node="w6" resolve="ctx" />
                      <uo k="s:originTrace" v="n:4313507821874177707" />
                    </node>
                    <node concept="liA8E" id="yK" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                      <uo k="s:originTrace" v="n:4313507821874177707" />
                    </node>
                  </node>
                  <node concept="liA8E" id="yI" role="2OqNvi">
                    <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                    <uo k="s:originTrace" v="n:4313507821874177707" />
                  </node>
                </node>
                <node concept="liA8E" id="yG" role="2OqNvi">
                  <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
                  <uo k="s:originTrace" v="n:4313507821874177707" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="xX" role="3cqZAp">
              <uo k="s:originTrace" v="n:8442470881495874039" />
              <node concept="2OqwBi" id="yL" role="3clFbG">
                <uo k="s:originTrace" v="n:8442470881495874039" />
                <node concept="2OqwBi" id="yM" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:8442470881495874039" />
                  <node concept="2OqwBi" id="yO" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:8442470881495874039" />
                    <node concept="37vLTw" id="yQ" role="2Oq$k0">
                      <ref role="3cqZAo" node="w6" resolve="ctx" />
                      <uo k="s:originTrace" v="n:8442470881495874039" />
                    </node>
                    <node concept="liA8E" id="yR" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                      <uo k="s:originTrace" v="n:8442470881495874039" />
                    </node>
                  </node>
                  <node concept="liA8E" id="yP" role="2OqNvi">
                    <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                    <uo k="s:originTrace" v="n:8442470881495874039" />
                  </node>
                </node>
                <node concept="liA8E" id="yN" role="2OqNvi">
                  <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
                  <uo k="s:originTrace" v="n:8442470881495874039" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="xN" role="3clFbw">
            <uo k="s:originTrace" v="n:227067257600414984" />
            <node concept="2OqwBi" id="yS" role="2Oq$k0">
              <uo k="s:originTrace" v="n:227067257600408446" />
              <node concept="2OqwBi" id="yU" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600407974" />
                <node concept="37vLTw" id="yW" role="2Oq$k0">
                  <ref role="3cqZAo" node="w6" resolve="ctx" />
                </node>
                <node concept="liA8E" id="yX" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3Tsc0h" id="yV" role="2OqNvi">
                <ref role="3TtcxE" to="7thy:4favA_iCQSJ" resolve="schema" />
                <uo k="s:originTrace" v="n:227067257600409392" />
              </node>
            </node>
            <node concept="3GX2aA" id="yT" role="2OqNvi">
              <uo k="s:originTrace" v="n:227067257600427840" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="w6" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:4313507821874160941" />
        <node concept="3uibUv" id="yY" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:4313507821874160941" />
        </node>
      </node>
      <node concept="2AHcQZ" id="w7" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821874160941" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="yZ">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="PathObject_TextGen" />
    <uo k="s:originTrace" v="n:4313507821874201930" />
    <node concept="3Tm1VV" id="z0" role="1B3o_S">
      <uo k="s:originTrace" v="n:4313507821874201930" />
    </node>
    <node concept="3uibUv" id="z1" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:4313507821874201930" />
    </node>
    <node concept="3clFb_" id="z2" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:4313507821874201930" />
      <node concept="3cqZAl" id="z3" role="3clF45">
        <uo k="s:originTrace" v="n:4313507821874201930" />
      </node>
      <node concept="3Tm1VV" id="z4" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821874201930" />
      </node>
      <node concept="3clFbS" id="z5" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821874201930" />
        <node concept="3cpWs8" id="z8" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874201930" />
          <node concept="3cpWsn" id="zg" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:4313507821874201930" />
            <node concept="3uibUv" id="zh" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:4313507821874201930" />
            </node>
            <node concept="2ShNRf" id="zi" role="33vP2m">
              <uo k="s:originTrace" v="n:4313507821874201930" />
              <node concept="1pGfFk" id="zj" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:4313507821874201930" />
                <node concept="37vLTw" id="zk" role="37wK5m">
                  <ref role="3cqZAo" node="z6" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874201930" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="z9" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874583924" />
          <node concept="2OqwBi" id="zl" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874583924" />
            <node concept="37vLTw" id="zm" role="2Oq$k0">
              <ref role="3cqZAo" node="zg" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874583924" />
            </node>
            <node concept="liA8E" id="zn" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874583924" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="za" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874201967" />
          <node concept="2OqwBi" id="zo" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874201967" />
            <node concept="37vLTw" id="zp" role="2Oq$k0">
              <ref role="3cqZAo" node="zg" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874201967" />
            </node>
            <node concept="liA8E" id="zq" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874201967" />
              <node concept="2OqwBi" id="zr" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821874202455" />
                <node concept="2OqwBi" id="zs" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874202019" />
                  <node concept="37vLTw" id="zu" role="2Oq$k0">
                    <ref role="3cqZAo" node="z6" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="zv" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="zt" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:3JW8_axN3fb" resolve="path" />
                  <uo k="s:originTrace" v="n:4313507821874203396" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="zb" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874203596" />
          <node concept="2OqwBi" id="zw" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874203596" />
            <node concept="37vLTw" id="zx" role="2Oq$k0">
              <ref role="3cqZAo" node="zg" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874203596" />
            </node>
            <node concept="liA8E" id="zy" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874203596" />
              <node concept="Xl_RD" id="zz" role="37wK5m">
                <property role="Xl_RC" value=":\n" />
                <uo k="s:originTrace" v="n:4313507821874203596" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="zc" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874203823" />
          <node concept="2OqwBi" id="z$" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874203823" />
            <node concept="2OqwBi" id="z_" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874203823" />
              <node concept="2OqwBi" id="zB" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874203823" />
                <node concept="37vLTw" id="zD" role="2Oq$k0">
                  <ref role="3cqZAo" node="z6" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874203823" />
                </node>
                <node concept="liA8E" id="zE" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874203823" />
                </node>
              </node>
              <node concept="liA8E" id="zC" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874203823" />
              </node>
            </node>
            <node concept="liA8E" id="zA" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874203823" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="zd" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600265188" />
          <node concept="3clFbS" id="zF" role="3clFbx">
            <uo k="s:originTrace" v="n:227067257600265190" />
            <node concept="3clFbF" id="zH" role="3cqZAp">
              <uo k="s:originTrace" v="n:227067257600286288" />
              <node concept="2OqwBi" id="zI" role="3clFbG">
                <node concept="37vLTw" id="zJ" role="2Oq$k0">
                  <ref role="3cqZAo" node="zg" resolve="tgs" />
                </node>
                <node concept="liA8E" id="zK" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.reportError(java.lang.String)" resolve="reportError" />
                  <node concept="Xl_RD" id="zL" role="37wK5m">
                    <property role="Xl_RC" value="operation may not be empty" />
                    <uo k="s:originTrace" v="n:227067257600286305" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="zG" role="3clFbw">
            <uo k="s:originTrace" v="n:227067257600273369" />
            <node concept="2OqwBi" id="zM" role="2Oq$k0">
              <uo k="s:originTrace" v="n:227067257600265701" />
              <node concept="2OqwBi" id="zO" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600265229" />
                <node concept="37vLTw" id="zQ" role="2Oq$k0">
                  <ref role="3cqZAo" node="z6" resolve="ctx" />
                </node>
                <node concept="liA8E" id="zR" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3Tsc0h" id="zP" role="2OqNvi">
                <ref role="3TtcxE" to="7thy:3JW8_axN3fe" resolve="operation" />
                <uo k="s:originTrace" v="n:227067257600266682" />
              </node>
            </node>
            <node concept="1v1jN8" id="zN" role="2OqNvi">
              <uo k="s:originTrace" v="n:227067257600286225" />
            </node>
          </node>
        </node>
        <node concept="1DcWWT" id="ze" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874460208" />
          <node concept="3clFbS" id="zS" role="2LFqv$">
            <uo k="s:originTrace" v="n:4313507821874460208" />
            <node concept="3clFbF" id="zV" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874460208" />
              <node concept="2OqwBi" id="zW" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874460208" />
                <node concept="37vLTw" id="zX" role="2Oq$k0">
                  <ref role="3cqZAo" node="zg" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874460208" />
                </node>
                <node concept="liA8E" id="zY" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.appendNode(org.jetbrains.mps.openapi.model.SNode)" resolve="appendNode" />
                  <uo k="s:originTrace" v="n:4313507821874460208" />
                  <node concept="37vLTw" id="zZ" role="37wK5m">
                    <ref role="3cqZAo" node="zT" resolve="item" />
                    <uo k="s:originTrace" v="n:4313507821874460208" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="zT" role="1Duv9x">
            <property role="TrG5h" value="item" />
            <uo k="s:originTrace" v="n:4313507821874460208" />
            <node concept="3Tqbb2" id="$0" role="1tU5fm">
              <uo k="s:originTrace" v="n:4313507821874460208" />
            </node>
          </node>
          <node concept="2OqwBi" id="zU" role="1DdaDG">
            <uo k="s:originTrace" v="n:4313507821874460592" />
            <node concept="2OqwBi" id="$1" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874460228" />
              <node concept="37vLTw" id="$3" role="2Oq$k0">
                <ref role="3cqZAo" node="z6" resolve="ctx" />
              </node>
              <node concept="liA8E" id="$4" role="2OqNvi">
                <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
              </node>
            </node>
            <node concept="3Tsc0h" id="$2" role="2OqNvi">
              <ref role="3TtcxE" to="7thy:3JW8_axN3fe" resolve="operation" />
              <uo k="s:originTrace" v="n:4313507821874461567" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="zf" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874203823" />
          <node concept="2OqwBi" id="$5" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874203823" />
            <node concept="2OqwBi" id="$6" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874203823" />
              <node concept="2OqwBi" id="$8" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874203823" />
                <node concept="37vLTw" id="$a" role="2Oq$k0">
                  <ref role="3cqZAo" node="z6" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874203823" />
                </node>
                <node concept="liA8E" id="$b" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874203823" />
                </node>
              </node>
              <node concept="liA8E" id="$9" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874203823" />
              </node>
            </node>
            <node concept="liA8E" id="$7" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874203823" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="z6" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:4313507821874201930" />
        <node concept="3uibUv" id="$c" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:4313507821874201930" />
        </node>
      </node>
      <node concept="2AHcQZ" id="z7" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821874201930" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="$d">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="PostOperationObject_TextGen" />
    <uo k="s:originTrace" v="n:4313507821874234336" />
    <node concept="3Tm1VV" id="$e" role="1B3o_S">
      <uo k="s:originTrace" v="n:4313507821874234336" />
    </node>
    <node concept="3uibUv" id="$f" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:4313507821874234336" />
    </node>
    <node concept="3clFb_" id="$g" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:4313507821874234336" />
      <node concept="3cqZAl" id="$h" role="3clF45">
        <uo k="s:originTrace" v="n:4313507821874234336" />
      </node>
      <node concept="3Tm1VV" id="$i" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821874234336" />
      </node>
      <node concept="3clFbS" id="$j" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821874234336" />
        <node concept="3cpWs8" id="$m" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874234336" />
          <node concept="3cpWsn" id="$B" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:4313507821874234336" />
            <node concept="3uibUv" id="$C" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:4313507821874234336" />
            </node>
            <node concept="2ShNRf" id="$D" role="33vP2m">
              <uo k="s:originTrace" v="n:4313507821874234336" />
              <node concept="1pGfFk" id="$E" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:4313507821874234336" />
                <node concept="37vLTw" id="$F" role="37wK5m">
                  <ref role="3cqZAo" node="$k" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874234336" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$n" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874642534" />
          <node concept="2OqwBi" id="$G" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874642534" />
            <node concept="37vLTw" id="$H" role="2Oq$k0">
              <ref role="3cqZAo" node="$B" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874642534" />
            </node>
            <node concept="liA8E" id="$I" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874642534" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$o" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874234358" />
          <node concept="2OqwBi" id="$J" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874234358" />
            <node concept="37vLTw" id="$K" role="2Oq$k0">
              <ref role="3cqZAo" node="$B" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874234358" />
            </node>
            <node concept="liA8E" id="$L" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874234358" />
              <node concept="Xl_RD" id="$M" role="37wK5m">
                <property role="Xl_RC" value="post:\n" />
                <uo k="s:originTrace" v="n:4313507821874234358" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$p" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874234359" />
          <node concept="2OqwBi" id="$N" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874234359" />
            <node concept="2OqwBi" id="$O" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874234359" />
              <node concept="2OqwBi" id="$Q" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874234359" />
                <node concept="37vLTw" id="$S" role="2Oq$k0">
                  <ref role="3cqZAo" node="$k" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874234359" />
                </node>
                <node concept="liA8E" id="$T" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874234359" />
                </node>
              </node>
              <node concept="liA8E" id="$R" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874234359" />
              </node>
            </node>
            <node concept="liA8E" id="$P" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874234359" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$q" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874642718" />
          <node concept="2OqwBi" id="$U" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874642718" />
            <node concept="37vLTw" id="$V" role="2Oq$k0">
              <ref role="3cqZAo" node="$B" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874642718" />
            </node>
            <node concept="liA8E" id="$W" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874642718" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$r" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874234362" />
          <node concept="2OqwBi" id="$X" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874234362" />
            <node concept="37vLTw" id="$Y" role="2Oq$k0">
              <ref role="3cqZAo" node="$B" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874234362" />
            </node>
            <node concept="liA8E" id="$Z" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874234362" />
              <node concept="Xl_RD" id="_0" role="37wK5m">
                <property role="Xl_RC" value="summary: " />
                <uo k="s:originTrace" v="n:4313507821874234362" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$s" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874234363" />
          <node concept="2OqwBi" id="_1" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874234363" />
            <node concept="37vLTw" id="_2" role="2Oq$k0">
              <ref role="3cqZAo" node="$B" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874234363" />
            </node>
            <node concept="liA8E" id="_3" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874234363" />
              <node concept="2OqwBi" id="_4" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821874234364" />
                <node concept="2OqwBi" id="_5" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874234365" />
                  <node concept="37vLTw" id="_7" role="2Oq$k0">
                    <ref role="3cqZAo" node="$k" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="_8" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="_6" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:3JW8_axN3fB" resolve="summary" />
                  <uo k="s:originTrace" v="n:4313507821874234366" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$t" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874234367" />
          <node concept="2OqwBi" id="_9" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874234367" />
            <node concept="37vLTw" id="_a" role="2Oq$k0">
              <ref role="3cqZAo" node="$B" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874234367" />
            </node>
            <node concept="liA8E" id="_b" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821874234367" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="$u" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600402888" />
          <node concept="3clFbS" id="_c" role="3clFbx">
            <uo k="s:originTrace" v="n:227067257600402889" />
            <node concept="3clFbF" id="_e" role="3cqZAp">
              <uo k="s:originTrace" v="n:227067257600402890" />
              <node concept="2OqwBi" id="_f" role="3clFbG">
                <node concept="37vLTw" id="_g" role="2Oq$k0">
                  <ref role="3cqZAo" node="$B" resolve="tgs" />
                </node>
                <node concept="liA8E" id="_h" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.reportError(java.lang.String)" resolve="reportError" />
                  <node concept="Xl_RD" id="_i" role="37wK5m">
                    <property role="Xl_RC" value="summary is required" />
                    <uo k="s:originTrace" v="n:227067257600402891" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="_d" role="3clFbw">
            <uo k="s:originTrace" v="n:227067257600402892" />
            <node concept="2OqwBi" id="_j" role="2Oq$k0">
              <uo k="s:originTrace" v="n:227067257600402893" />
              <node concept="2OqwBi" id="_l" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600402894" />
                <node concept="37vLTw" id="_n" role="2Oq$k0">
                  <ref role="3cqZAo" node="$k" resolve="ctx" />
                </node>
                <node concept="liA8E" id="_o" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="_m" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axN3fB" resolve="summary" />
                <uo k="s:originTrace" v="n:227067257600402895" />
              </node>
            </node>
            <node concept="17RlXB" id="_k" role="2OqNvi">
              <uo k="s:originTrace" v="n:227067257600402896" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="$v" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600402638" />
        </node>
        <node concept="3clFbJ" id="$w" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874464839" />
          <node concept="3clFbS" id="_p" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874464840" />
            <node concept="3clFbF" id="_r" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874642975" />
              <node concept="2OqwBi" id="_v" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874642975" />
                <node concept="37vLTw" id="_w" role="2Oq$k0">
                  <ref role="3cqZAo" node="$B" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874642975" />
                </node>
                <node concept="liA8E" id="_x" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874642975" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="_s" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874464842" />
              <node concept="2OqwBi" id="_y" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874464842" />
                <node concept="37vLTw" id="_z" role="2Oq$k0">
                  <ref role="3cqZAo" node="$B" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874464842" />
                </node>
                <node concept="liA8E" id="_$" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874464842" />
                  <node concept="Xl_RD" id="__" role="37wK5m">
                    <property role="Xl_RC" value="description: " />
                    <uo k="s:originTrace" v="n:4313507821874464842" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="_t" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874464843" />
              <node concept="2OqwBi" id="_A" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874464843" />
                <node concept="37vLTw" id="_B" role="2Oq$k0">
                  <ref role="3cqZAo" node="$B" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874464843" />
                </node>
                <node concept="liA8E" id="_C" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874464843" />
                  <node concept="2OqwBi" id="_D" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874464844" />
                    <node concept="2OqwBi" id="_E" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:4313507821874464845" />
                      <node concept="37vLTw" id="_G" role="2Oq$k0">
                        <ref role="3cqZAo" node="$k" resolve="ctx" />
                      </node>
                      <node concept="liA8E" id="_H" role="2OqNvi">
                        <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="_F" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:3JW8_axN3fI" resolve="description" />
                      <uo k="s:originTrace" v="n:4313507821874464846" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="_u" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874464847" />
              <node concept="2OqwBi" id="_I" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874464847" />
                <node concept="37vLTw" id="_J" role="2Oq$k0">
                  <ref role="3cqZAo" node="$B" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874464847" />
                </node>
                <node concept="liA8E" id="_K" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874464847" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="_q" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874464848" />
            <node concept="2OqwBi" id="_L" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874464849" />
              <node concept="2OqwBi" id="_N" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874464850" />
                <node concept="37vLTw" id="_P" role="2Oq$k0">
                  <ref role="3cqZAo" node="$k" resolve="ctx" />
                </node>
                <node concept="liA8E" id="_Q" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="_O" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axN3fI" resolve="description" />
                <uo k="s:originTrace" v="n:4313507821874464851" />
              </node>
            </node>
            <node concept="17RvpY" id="_M" role="2OqNvi">
              <uo k="s:originTrace" v="n:4313507821874464852" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$x" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874643107" />
          <node concept="2OqwBi" id="_R" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874643107" />
            <node concept="37vLTw" id="_S" role="2Oq$k0">
              <ref role="3cqZAo" node="$B" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874643107" />
            </node>
            <node concept="liA8E" id="_T" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874643107" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$y" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874464854" />
          <node concept="2OqwBi" id="_U" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874464854" />
            <node concept="37vLTw" id="_V" role="2Oq$k0">
              <ref role="3cqZAo" node="$B" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874464854" />
            </node>
            <node concept="liA8E" id="_W" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874464854" />
              <node concept="Xl_RD" id="_X" role="37wK5m">
                <property role="Xl_RC" value="responses:\n" />
                <uo k="s:originTrace" v="n:4313507821874464854" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$z" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874464855" />
          <node concept="2OqwBi" id="_Y" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874464855" />
            <node concept="2OqwBi" id="_Z" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874464855" />
              <node concept="2OqwBi" id="A1" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874464855" />
                <node concept="37vLTw" id="A3" role="2Oq$k0">
                  <ref role="3cqZAo" node="$k" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874464855" />
                </node>
                <node concept="liA8E" id="A4" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874464855" />
                </node>
              </node>
              <node concept="liA8E" id="A2" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874464855" />
              </node>
            </node>
            <node concept="liA8E" id="A0" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874464855" />
            </node>
          </node>
        </node>
        <node concept="1DcWWT" id="$$" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874464858" />
          <node concept="3clFbS" id="A5" role="2LFqv$">
            <uo k="s:originTrace" v="n:4313507821874464858" />
            <node concept="3clFbF" id="A8" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874464858" />
              <node concept="2OqwBi" id="A9" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874464858" />
                <node concept="37vLTw" id="Aa" role="2Oq$k0">
                  <ref role="3cqZAo" node="$B" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874464858" />
                </node>
                <node concept="liA8E" id="Ab" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.appendNode(org.jetbrains.mps.openapi.model.SNode)" resolve="appendNode" />
                  <uo k="s:originTrace" v="n:4313507821874464858" />
                  <node concept="37vLTw" id="Ac" role="37wK5m">
                    <ref role="3cqZAo" node="A6" resolve="item" />
                    <uo k="s:originTrace" v="n:4313507821874464858" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="A6" role="1Duv9x">
            <property role="TrG5h" value="item" />
            <uo k="s:originTrace" v="n:4313507821874464858" />
            <node concept="3Tqbb2" id="Ad" role="1tU5fm">
              <uo k="s:originTrace" v="n:4313507821874464858" />
            </node>
          </node>
          <node concept="2OqwBi" id="A7" role="1DdaDG">
            <uo k="s:originTrace" v="n:4313507821874464859" />
            <node concept="2OqwBi" id="Ae" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874464860" />
              <node concept="37vLTw" id="Ag" role="2Oq$k0">
                <ref role="3cqZAo" node="$k" resolve="ctx" />
              </node>
              <node concept="liA8E" id="Ah" role="2OqNvi">
                <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
              </node>
            </node>
            <node concept="3Tsc0h" id="Af" role="2OqNvi">
              <ref role="3TtcxE" to="7thy:4favA_iAyf8" resolve="responses" />
              <uo k="s:originTrace" v="n:4313507821874464861" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$_" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874464855" />
          <node concept="2OqwBi" id="Ai" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874464855" />
            <node concept="2OqwBi" id="Aj" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874464855" />
              <node concept="2OqwBi" id="Al" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874464855" />
                <node concept="37vLTw" id="An" role="2Oq$k0">
                  <ref role="3cqZAo" node="$k" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874464855" />
                </node>
                <node concept="liA8E" id="Ao" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874464855" />
                </node>
              </node>
              <node concept="liA8E" id="Am" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874464855" />
              </node>
            </node>
            <node concept="liA8E" id="Ak" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874464855" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="$A" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874234359" />
          <node concept="2OqwBi" id="Ap" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874234359" />
            <node concept="2OqwBi" id="Aq" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874234359" />
              <node concept="2OqwBi" id="As" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874234359" />
                <node concept="37vLTw" id="Au" role="2Oq$k0">
                  <ref role="3cqZAo" node="$k" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874234359" />
                </node>
                <node concept="liA8E" id="Av" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874234359" />
                </node>
              </node>
              <node concept="liA8E" id="At" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874234359" />
              </node>
            </node>
            <node concept="liA8E" id="Ar" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874234359" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="$k" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:4313507821874234336" />
        <node concept="3uibUv" id="Aw" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:4313507821874234336" />
        </node>
      </node>
      <node concept="2AHcQZ" id="$l" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821874234336" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="Ax">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="PutOperationObject_TextGen" />
    <uo k="s:originTrace" v="n:4313507821874234946" />
    <node concept="3Tm1VV" id="Ay" role="1B3o_S">
      <uo k="s:originTrace" v="n:4313507821874234946" />
    </node>
    <node concept="3uibUv" id="Az" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:4313507821874234946" />
    </node>
    <node concept="3clFb_" id="A$" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:4313507821874234946" />
      <node concept="3cqZAl" id="A_" role="3clF45">
        <uo k="s:originTrace" v="n:4313507821874234946" />
      </node>
      <node concept="3Tm1VV" id="AA" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821874234946" />
      </node>
      <node concept="3clFbS" id="AB" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821874234946" />
        <node concept="3cpWs8" id="AE" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874234946" />
          <node concept="3cpWsn" id="AV" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:4313507821874234946" />
            <node concept="3uibUv" id="AW" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:4313507821874234946" />
            </node>
            <node concept="2ShNRf" id="AX" role="33vP2m">
              <uo k="s:originTrace" v="n:4313507821874234946" />
              <node concept="1pGfFk" id="AY" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:4313507821874234946" />
                <node concept="37vLTw" id="AZ" role="37wK5m">
                  <ref role="3cqZAo" node="AC" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874234946" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="AF" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874643541" />
          <node concept="2OqwBi" id="B0" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874643541" />
            <node concept="37vLTw" id="B1" role="2Oq$k0">
              <ref role="3cqZAo" node="AV" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874643541" />
            </node>
            <node concept="liA8E" id="B2" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874643541" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="AG" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874234966" />
          <node concept="2OqwBi" id="B3" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874234966" />
            <node concept="37vLTw" id="B4" role="2Oq$k0">
              <ref role="3cqZAo" node="AV" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874234966" />
            </node>
            <node concept="liA8E" id="B5" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874234966" />
              <node concept="Xl_RD" id="B6" role="37wK5m">
                <property role="Xl_RC" value="put:\n" />
                <uo k="s:originTrace" v="n:4313507821874234966" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="AH" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874234967" />
          <node concept="2OqwBi" id="B7" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874234967" />
            <node concept="2OqwBi" id="B8" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874234967" />
              <node concept="2OqwBi" id="Ba" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874234967" />
                <node concept="37vLTw" id="Bc" role="2Oq$k0">
                  <ref role="3cqZAo" node="AC" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874234967" />
                </node>
                <node concept="liA8E" id="Bd" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874234967" />
                </node>
              </node>
              <node concept="liA8E" id="Bb" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874234967" />
              </node>
            </node>
            <node concept="liA8E" id="B9" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874234967" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="AI" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874643725" />
          <node concept="2OqwBi" id="Be" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874643725" />
            <node concept="37vLTw" id="Bf" role="2Oq$k0">
              <ref role="3cqZAo" node="AV" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874643725" />
            </node>
            <node concept="liA8E" id="Bg" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874643725" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="AJ" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874234970" />
          <node concept="2OqwBi" id="Bh" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874234970" />
            <node concept="37vLTw" id="Bi" role="2Oq$k0">
              <ref role="3cqZAo" node="AV" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874234970" />
            </node>
            <node concept="liA8E" id="Bj" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874234970" />
              <node concept="Xl_RD" id="Bk" role="37wK5m">
                <property role="Xl_RC" value="summary: " />
                <uo k="s:originTrace" v="n:4313507821874234970" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="AK" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874234971" />
          <node concept="2OqwBi" id="Bl" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874234971" />
            <node concept="37vLTw" id="Bm" role="2Oq$k0">
              <ref role="3cqZAo" node="AV" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874234971" />
            </node>
            <node concept="liA8E" id="Bn" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874234971" />
              <node concept="2OqwBi" id="Bo" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821874234972" />
                <node concept="2OqwBi" id="Bp" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874234973" />
                  <node concept="37vLTw" id="Br" role="2Oq$k0">
                    <ref role="3cqZAo" node="AC" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="Bs" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="Bq" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:3JW8_axN3fB" resolve="summary" />
                  <uo k="s:originTrace" v="n:4313507821874234974" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="AL" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874234975" />
          <node concept="2OqwBi" id="Bt" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874234975" />
            <node concept="37vLTw" id="Bu" role="2Oq$k0">
              <ref role="3cqZAo" node="AV" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874234975" />
            </node>
            <node concept="liA8E" id="Bv" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821874234975" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="AM" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600402186" />
          <node concept="3clFbS" id="Bw" role="3clFbx">
            <uo k="s:originTrace" v="n:227067257600402187" />
            <node concept="3clFbF" id="By" role="3cqZAp">
              <uo k="s:originTrace" v="n:227067257600402188" />
              <node concept="2OqwBi" id="Bz" role="3clFbG">
                <node concept="37vLTw" id="B$" role="2Oq$k0">
                  <ref role="3cqZAo" node="AV" resolve="tgs" />
                </node>
                <node concept="liA8E" id="B_" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.reportError(java.lang.String)" resolve="reportError" />
                  <node concept="Xl_RD" id="BA" role="37wK5m">
                    <property role="Xl_RC" value="summary is required" />
                    <uo k="s:originTrace" v="n:227067257600402189" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="Bx" role="3clFbw">
            <uo k="s:originTrace" v="n:227067257600402190" />
            <node concept="2OqwBi" id="BB" role="2Oq$k0">
              <uo k="s:originTrace" v="n:227067257600402191" />
              <node concept="2OqwBi" id="BD" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600402192" />
                <node concept="37vLTw" id="BF" role="2Oq$k0">
                  <ref role="3cqZAo" node="AC" resolve="ctx" />
                </node>
                <node concept="liA8E" id="BG" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="BE" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axN3fB" resolve="summary" />
                <uo k="s:originTrace" v="n:227067257600402193" />
              </node>
            </node>
            <node concept="17RlXB" id="BC" role="2OqNvi">
              <uo k="s:originTrace" v="n:227067257600402194" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="AN" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600402062" />
        </node>
        <node concept="3clFbJ" id="AO" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874465313" />
          <node concept="3clFbS" id="BH" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874465314" />
            <node concept="3clFbF" id="BJ" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874643857" />
              <node concept="2OqwBi" id="BN" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874643857" />
                <node concept="37vLTw" id="BO" role="2Oq$k0">
                  <ref role="3cqZAo" node="AV" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874643857" />
                </node>
                <node concept="liA8E" id="BP" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874643857" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="BK" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874465316" />
              <node concept="2OqwBi" id="BQ" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874465316" />
                <node concept="37vLTw" id="BR" role="2Oq$k0">
                  <ref role="3cqZAo" node="AV" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874465316" />
                </node>
                <node concept="liA8E" id="BS" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874465316" />
                  <node concept="Xl_RD" id="BT" role="37wK5m">
                    <property role="Xl_RC" value="description: " />
                    <uo k="s:originTrace" v="n:4313507821874465316" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="BL" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874465317" />
              <node concept="2OqwBi" id="BU" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874465317" />
                <node concept="37vLTw" id="BV" role="2Oq$k0">
                  <ref role="3cqZAo" node="AV" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874465317" />
                </node>
                <node concept="liA8E" id="BW" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874465317" />
                  <node concept="2OqwBi" id="BX" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874465318" />
                    <node concept="2OqwBi" id="BY" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:4313507821874465319" />
                      <node concept="37vLTw" id="C0" role="2Oq$k0">
                        <ref role="3cqZAo" node="AC" resolve="ctx" />
                      </node>
                      <node concept="liA8E" id="C1" role="2OqNvi">
                        <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="BZ" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:3JW8_axN3fI" resolve="description" />
                      <uo k="s:originTrace" v="n:4313507821874465320" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="BM" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874465321" />
              <node concept="2OqwBi" id="C2" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874465321" />
                <node concept="37vLTw" id="C3" role="2Oq$k0">
                  <ref role="3cqZAo" node="AV" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874465321" />
                </node>
                <node concept="liA8E" id="C4" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874465321" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="BI" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874465322" />
            <node concept="2OqwBi" id="C5" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874465323" />
              <node concept="2OqwBi" id="C7" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874465324" />
                <node concept="37vLTw" id="C9" role="2Oq$k0">
                  <ref role="3cqZAo" node="AC" resolve="ctx" />
                </node>
                <node concept="liA8E" id="Ca" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="C8" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axN3fI" resolve="description" />
                <uo k="s:originTrace" v="n:4313507821874465325" />
              </node>
            </node>
            <node concept="17RvpY" id="C6" role="2OqNvi">
              <uo k="s:originTrace" v="n:4313507821874465326" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="AP" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874644019" />
          <node concept="2OqwBi" id="Cb" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874644019" />
            <node concept="37vLTw" id="Cc" role="2Oq$k0">
              <ref role="3cqZAo" node="AV" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874644019" />
            </node>
            <node concept="liA8E" id="Cd" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874644019" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="AQ" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874465328" />
          <node concept="2OqwBi" id="Ce" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874465328" />
            <node concept="37vLTw" id="Cf" role="2Oq$k0">
              <ref role="3cqZAo" node="AV" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874465328" />
            </node>
            <node concept="liA8E" id="Cg" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874465328" />
              <node concept="Xl_RD" id="Ch" role="37wK5m">
                <property role="Xl_RC" value="responses:\n" />
                <uo k="s:originTrace" v="n:4313507821874465328" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="AR" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874465329" />
          <node concept="2OqwBi" id="Ci" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874465329" />
            <node concept="2OqwBi" id="Cj" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874465329" />
              <node concept="2OqwBi" id="Cl" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874465329" />
                <node concept="37vLTw" id="Cn" role="2Oq$k0">
                  <ref role="3cqZAo" node="AC" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874465329" />
                </node>
                <node concept="liA8E" id="Co" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874465329" />
                </node>
              </node>
              <node concept="liA8E" id="Cm" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874465329" />
              </node>
            </node>
            <node concept="liA8E" id="Ck" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874465329" />
            </node>
          </node>
        </node>
        <node concept="1DcWWT" id="AS" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874465332" />
          <node concept="3clFbS" id="Cp" role="2LFqv$">
            <uo k="s:originTrace" v="n:4313507821874465332" />
            <node concept="3clFbF" id="Cs" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874465332" />
              <node concept="2OqwBi" id="Ct" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874465332" />
                <node concept="37vLTw" id="Cu" role="2Oq$k0">
                  <ref role="3cqZAo" node="AV" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874465332" />
                </node>
                <node concept="liA8E" id="Cv" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.appendNode(org.jetbrains.mps.openapi.model.SNode)" resolve="appendNode" />
                  <uo k="s:originTrace" v="n:4313507821874465332" />
                  <node concept="37vLTw" id="Cw" role="37wK5m">
                    <ref role="3cqZAo" node="Cq" resolve="item" />
                    <uo k="s:originTrace" v="n:4313507821874465332" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="Cq" role="1Duv9x">
            <property role="TrG5h" value="item" />
            <uo k="s:originTrace" v="n:4313507821874465332" />
            <node concept="3Tqbb2" id="Cx" role="1tU5fm">
              <uo k="s:originTrace" v="n:4313507821874465332" />
            </node>
          </node>
          <node concept="2OqwBi" id="Cr" role="1DdaDG">
            <uo k="s:originTrace" v="n:4313507821874465333" />
            <node concept="2OqwBi" id="Cy" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874465334" />
              <node concept="37vLTw" id="C$" role="2Oq$k0">
                <ref role="3cqZAo" node="AC" resolve="ctx" />
              </node>
              <node concept="liA8E" id="C_" role="2OqNvi">
                <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
              </node>
            </node>
            <node concept="3Tsc0h" id="Cz" role="2OqNvi">
              <ref role="3TtcxE" to="7thy:4favA_iAyf8" resolve="responses" />
              <uo k="s:originTrace" v="n:4313507821874465335" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="AT" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874465329" />
          <node concept="2OqwBi" id="CA" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874465329" />
            <node concept="2OqwBi" id="CB" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874465329" />
              <node concept="2OqwBi" id="CD" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874465329" />
                <node concept="37vLTw" id="CF" role="2Oq$k0">
                  <ref role="3cqZAo" node="AC" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874465329" />
                </node>
                <node concept="liA8E" id="CG" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874465329" />
                </node>
              </node>
              <node concept="liA8E" id="CE" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874465329" />
              </node>
            </node>
            <node concept="liA8E" id="CC" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874465329" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="AU" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874234967" />
          <node concept="2OqwBi" id="CH" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874234967" />
            <node concept="2OqwBi" id="CI" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874234967" />
              <node concept="2OqwBi" id="CK" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874234967" />
                <node concept="37vLTw" id="CM" role="2Oq$k0">
                  <ref role="3cqZAo" node="AC" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874234967" />
                </node>
                <node concept="liA8E" id="CN" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874234967" />
                </node>
              </node>
              <node concept="liA8E" id="CL" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874234967" />
              </node>
            </node>
            <node concept="liA8E" id="CJ" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874234967" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="AC" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:4313507821874234946" />
        <node concept="3uibUv" id="CO" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:4313507821874234946" />
        </node>
      </node>
      <node concept="2AHcQZ" id="AD" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821874234946" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="CP">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="Response_TextGen" />
    <uo k="s:originTrace" v="n:4313507821874235856" />
    <node concept="3Tm1VV" id="CQ" role="1B3o_S">
      <uo k="s:originTrace" v="n:4313507821874235856" />
    </node>
    <node concept="3uibUv" id="CR" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:4313507821874235856" />
    </node>
    <node concept="3clFb_" id="CS" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:4313507821874235856" />
      <node concept="3cqZAl" id="CT" role="3clF45">
        <uo k="s:originTrace" v="n:4313507821874235856" />
      </node>
      <node concept="3Tm1VV" id="CU" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821874235856" />
      </node>
      <node concept="3clFbS" id="CV" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821874235856" />
        <node concept="3cpWs8" id="CY" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874235856" />
          <node concept="3cpWsn" id="D8" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:4313507821874235856" />
            <node concept="3uibUv" id="D9" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:4313507821874235856" />
            </node>
            <node concept="2ShNRf" id="Da" role="33vP2m">
              <uo k="s:originTrace" v="n:4313507821874235856" />
              <node concept="1pGfFk" id="Db" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:4313507821874235856" />
                <node concept="37vLTw" id="Dc" role="37wK5m">
                  <ref role="3cqZAo" node="CW" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874235856" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="CZ" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874624334" />
          <node concept="2OqwBi" id="Dd" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874624334" />
            <node concept="37vLTw" id="De" role="2Oq$k0">
              <ref role="3cqZAo" node="D8" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874624334" />
            </node>
            <node concept="liA8E" id="Df" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874624334" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="D0" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874235925" />
          <node concept="2OqwBi" id="Dg" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874235925" />
            <node concept="37vLTw" id="Dh" role="2Oq$k0">
              <ref role="3cqZAo" node="D8" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874235925" />
            </node>
            <node concept="liA8E" id="Di" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874235925" />
              <node concept="3cpWs3" id="Dj" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821874240823" />
                <node concept="Xl_RD" id="Dk" role="3uHU7B">
                  <property role="Xl_RC" value="'" />
                  <uo k="s:originTrace" v="n:4313507821874239299" />
                </node>
                <node concept="2OqwBi" id="Dl" role="3uHU7w">
                  <uo k="s:originTrace" v="n:4313507821874236413" />
                  <node concept="3TrcHB" id="Dm" role="2OqNvi">
                    <ref role="3TsBF5" to="7thy:4favA_iAyfc" resolve="httpCode" />
                    <uo k="s:originTrace" v="n:4313507821874237389" />
                  </node>
                  <node concept="2OqwBi" id="Dn" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:4313507821874241424" />
                    <node concept="37vLTw" id="Do" role="2Oq$k0">
                      <ref role="3cqZAo" node="CW" resolve="ctx" />
                    </node>
                    <node concept="liA8E" id="Dp" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="D1" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874241472" />
          <node concept="2OqwBi" id="Dq" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874241472" />
            <node concept="37vLTw" id="Dr" role="2Oq$k0">
              <ref role="3cqZAo" node="D8" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874241472" />
            </node>
            <node concept="liA8E" id="Ds" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874241472" />
              <node concept="Xl_RD" id="Dt" role="37wK5m">
                <property role="Xl_RC" value="':" />
                <uo k="s:originTrace" v="n:4313507821874241472" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="D2" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874241590" />
          <node concept="2OqwBi" id="Du" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874241590" />
            <node concept="37vLTw" id="Dv" role="2Oq$k0">
              <ref role="3cqZAo" node="D8" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874241590" />
            </node>
            <node concept="liA8E" id="Dw" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821874241590" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="D3" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600154690" />
          <node concept="3clFbS" id="Dx" role="3clFbx">
            <uo k="s:originTrace" v="n:227067257600154692" />
            <node concept="3clFbF" id="Dz" role="3cqZAp">
              <uo k="s:originTrace" v="n:227067257600196896" />
              <node concept="2OqwBi" id="D$" role="3clFbG">
                <node concept="37vLTw" id="D_" role="2Oq$k0">
                  <ref role="3cqZAo" node="D8" resolve="tgs" />
                </node>
                <node concept="liA8E" id="DA" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.reportError(java.lang.String)" resolve="reportError" />
                  <node concept="Xl_RD" id="DB" role="37wK5m">
                    <property role="Xl_RC" value="At least a description is required" />
                    <uo k="s:originTrace" v="n:227067257600196912" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1Wc70l" id="Dy" role="3clFbw">
            <uo k="s:originTrace" v="n:227067257600186036" />
            <node concept="2OqwBi" id="DC" role="3uHU7w">
              <uo k="s:originTrace" v="n:227067257600189005" />
              <node concept="2OqwBi" id="DE" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600186736" />
                <node concept="2OqwBi" id="DG" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:227067257600186243" />
                  <node concept="37vLTw" id="DI" role="2Oq$k0">
                    <ref role="3cqZAo" node="CW" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="DJ" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="DH" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iAyfe" resolve="description" />
                  <uo k="s:originTrace" v="n:227067257600187735" />
                </node>
              </node>
              <node concept="17RlXB" id="DF" role="2OqNvi">
                <uo k="s:originTrace" v="n:227067257600191093" />
              </node>
            </node>
            <node concept="2OqwBi" id="DD" role="3uHU7B">
              <uo k="s:originTrace" v="n:227067257600162346" />
              <node concept="2OqwBi" id="DK" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600155759" />
                <node concept="2OqwBi" id="DM" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:227067257600155287" />
                  <node concept="37vLTw" id="DO" role="2Oq$k0">
                    <ref role="3cqZAo" node="CW" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="DP" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3Tsc0h" id="DN" role="2OqNvi">
                  <ref role="3TtcxE" to="7thy:4favA_iB_5O" resolve="content" />
                  <uo k="s:originTrace" v="n:227067257600156740" />
                </node>
              </node>
              <node concept="1v1jN8" id="DL" role="2OqNvi">
                <uo k="s:originTrace" v="n:227067257600175085" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="D4" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874241840" />
          <node concept="2OqwBi" id="DQ" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874241840" />
            <node concept="2OqwBi" id="DR" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874241840" />
              <node concept="2OqwBi" id="DT" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874241840" />
                <node concept="37vLTw" id="DV" role="2Oq$k0">
                  <ref role="3cqZAo" node="CW" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874241840" />
                </node>
                <node concept="liA8E" id="DW" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874241840" />
                </node>
              </node>
              <node concept="liA8E" id="DU" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874241840" />
              </node>
            </node>
            <node concept="liA8E" id="DS" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874241840" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="D5" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874459359" />
          <node concept="3clFbS" id="DX" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874459360" />
            <node concept="3clFbF" id="E0" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874624516" />
              <node concept="2OqwBi" id="E4" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874624516" />
                <node concept="37vLTw" id="E5" role="2Oq$k0">
                  <ref role="3cqZAo" node="D8" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874624516" />
                </node>
                <node concept="liA8E" id="E6" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874624516" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="E1" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874459362" />
              <node concept="2OqwBi" id="E7" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874459362" />
                <node concept="37vLTw" id="E8" role="2Oq$k0">
                  <ref role="3cqZAo" node="D8" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874459362" />
                </node>
                <node concept="liA8E" id="E9" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874459362" />
                  <node concept="Xl_RD" id="Ea" role="37wK5m">
                    <property role="Xl_RC" value="description: " />
                    <uo k="s:originTrace" v="n:4313507821874459362" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="E2" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874459363" />
              <node concept="2OqwBi" id="Eb" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874459363" />
                <node concept="37vLTw" id="Ec" role="2Oq$k0">
                  <ref role="3cqZAo" node="D8" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874459363" />
                </node>
                <node concept="liA8E" id="Ed" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874459363" />
                  <node concept="2OqwBi" id="Ee" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874459364" />
                    <node concept="2OqwBi" id="Ef" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:4313507821874459365" />
                      <node concept="37vLTw" id="Eh" role="2Oq$k0">
                        <ref role="3cqZAo" node="CW" resolve="ctx" />
                      </node>
                      <node concept="liA8E" id="Ei" role="2OqNvi">
                        <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="Eg" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:4favA_iAyfe" resolve="description" />
                      <uo k="s:originTrace" v="n:4313507821874459366" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="E3" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874459367" />
              <node concept="2OqwBi" id="Ej" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874459367" />
                <node concept="37vLTw" id="Ek" role="2Oq$k0">
                  <ref role="3cqZAo" node="D8" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874459367" />
                </node>
                <node concept="liA8E" id="El" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874459367" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="DY" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874459368" />
            <node concept="2OqwBi" id="Em" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874459369" />
              <node concept="2OqwBi" id="Eo" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874459370" />
                <node concept="37vLTw" id="Eq" role="2Oq$k0">
                  <ref role="3cqZAo" node="CW" resolve="ctx" />
                </node>
                <node concept="liA8E" id="Er" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="Ep" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iAyfe" resolve="description" />
                <uo k="s:originTrace" v="n:4313507821874459371" />
              </node>
            </node>
            <node concept="17RvpY" id="En" role="2OqNvi">
              <uo k="s:originTrace" v="n:4313507821874459372" />
            </node>
          </node>
          <node concept="9aQIb" id="DZ" role="9aQIa">
            <uo k="s:originTrace" v="n:227067257600344666" />
            <node concept="3clFbS" id="Es" role="9aQI4">
              <uo k="s:originTrace" v="n:227067257600344667" />
              <node concept="3clFbF" id="Et" role="3cqZAp">
                <uo k="s:originTrace" v="n:227067257600344922" />
                <node concept="2OqwBi" id="Eu" role="3clFbG">
                  <node concept="37vLTw" id="Ev" role="2Oq$k0">
                    <ref role="3cqZAo" node="D8" resolve="tgs" />
                  </node>
                  <node concept="liA8E" id="Ew" role="2OqNvi">
                    <ref role="37wK5l" to="kpbf:~TextGenSupport.reportError(java.lang.String)" resolve="reportError" />
                    <node concept="Xl_RD" id="Ex" role="37wK5m">
                      <property role="Xl_RC" value="description is required" />
                      <uo k="s:originTrace" v="n:227067257600344938" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="D6" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874776031" />
          <node concept="3clFbS" id="Ey" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874776033" />
            <node concept="3clFbF" id="E$" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874624664" />
              <node concept="2OqwBi" id="ED" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874624664" />
                <node concept="37vLTw" id="EE" role="2Oq$k0">
                  <ref role="3cqZAo" node="D8" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874624664" />
                </node>
                <node concept="liA8E" id="EF" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874624664" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="E_" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874244337" />
              <node concept="2OqwBi" id="EG" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874244337" />
                <node concept="37vLTw" id="EH" role="2Oq$k0">
                  <ref role="3cqZAo" node="D8" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874244337" />
                </node>
                <node concept="liA8E" id="EI" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874244337" />
                  <node concept="Xl_RD" id="EJ" role="37wK5m">
                    <property role="Xl_RC" value="content:\n" />
                    <uo k="s:originTrace" v="n:4313507821874244337" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="EA" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874249500" />
              <node concept="2OqwBi" id="EK" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874249500" />
                <node concept="2OqwBi" id="EL" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874249500" />
                  <node concept="2OqwBi" id="EN" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:4313507821874249500" />
                    <node concept="37vLTw" id="EP" role="2Oq$k0">
                      <ref role="3cqZAo" node="CW" resolve="ctx" />
                      <uo k="s:originTrace" v="n:4313507821874249500" />
                    </node>
                    <node concept="liA8E" id="EQ" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                      <uo k="s:originTrace" v="n:4313507821874249500" />
                    </node>
                  </node>
                  <node concept="liA8E" id="EO" role="2OqNvi">
                    <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                    <uo k="s:originTrace" v="n:4313507821874249500" />
                  </node>
                </node>
                <node concept="liA8E" id="EM" role="2OqNvi">
                  <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
                  <uo k="s:originTrace" v="n:4313507821874249500" />
                </node>
              </node>
            </node>
            <node concept="1DcWWT" id="EB" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874624805" />
              <node concept="3clFbS" id="ER" role="2LFqv$">
                <uo k="s:originTrace" v="n:4313507821874624805" />
                <node concept="3clFbF" id="EU" role="3cqZAp">
                  <uo k="s:originTrace" v="n:4313507821874624805" />
                  <node concept="2OqwBi" id="EV" role="3clFbG">
                    <uo k="s:originTrace" v="n:4313507821874624805" />
                    <node concept="37vLTw" id="EW" role="2Oq$k0">
                      <ref role="3cqZAo" node="D8" resolve="tgs" />
                      <uo k="s:originTrace" v="n:4313507821874624805" />
                    </node>
                    <node concept="liA8E" id="EX" role="2OqNvi">
                      <ref role="37wK5l" to="kpbf:~TextGenSupport.appendNode(org.jetbrains.mps.openapi.model.SNode)" resolve="appendNode" />
                      <uo k="s:originTrace" v="n:4313507821874624805" />
                      <node concept="37vLTw" id="EY" role="37wK5m">
                        <ref role="3cqZAo" node="ES" resolve="item" />
                        <uo k="s:originTrace" v="n:4313507821874624805" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWsn" id="ES" role="1Duv9x">
                <property role="TrG5h" value="item" />
                <uo k="s:originTrace" v="n:4313507821874624805" />
                <node concept="3Tqbb2" id="EZ" role="1tU5fm">
                  <uo k="s:originTrace" v="n:4313507821874624805" />
                </node>
              </node>
              <node concept="2OqwBi" id="ET" role="1DdaDG">
                <uo k="s:originTrace" v="n:4313507821874625189" />
                <node concept="2OqwBi" id="F0" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874624825" />
                  <node concept="37vLTw" id="F2" role="2Oq$k0">
                    <ref role="3cqZAo" node="CW" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="F3" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3Tsc0h" id="F1" role="2OqNvi">
                  <ref role="3TtcxE" to="7thy:4favA_iB_5O" resolve="content" />
                  <uo k="s:originTrace" v="n:4313507821874626164" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="EC" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874249500" />
              <node concept="2OqwBi" id="F4" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874249500" />
                <node concept="2OqwBi" id="F5" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874249500" />
                  <node concept="2OqwBi" id="F7" role="2Oq$k0">
                    <uo k="s:originTrace" v="n:4313507821874249500" />
                    <node concept="37vLTw" id="F9" role="2Oq$k0">
                      <ref role="3cqZAo" node="CW" resolve="ctx" />
                      <uo k="s:originTrace" v="n:4313507821874249500" />
                    </node>
                    <node concept="liA8E" id="Fa" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                      <uo k="s:originTrace" v="n:4313507821874249500" />
                    </node>
                  </node>
                  <node concept="liA8E" id="F8" role="2OqNvi">
                    <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                    <uo k="s:originTrace" v="n:4313507821874249500" />
                  </node>
                </node>
                <node concept="liA8E" id="F6" role="2OqNvi">
                  <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
                  <uo k="s:originTrace" v="n:4313507821874249500" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="Ez" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874783205" />
            <node concept="2OqwBi" id="Fb" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874776708" />
              <node concept="2OqwBi" id="Fd" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874776224" />
                <node concept="37vLTw" id="Ff" role="2Oq$k0">
                  <ref role="3cqZAo" node="CW" resolve="ctx" />
                </node>
                <node concept="liA8E" id="Fg" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3Tsc0h" id="Fe" role="2OqNvi">
                <ref role="3TtcxE" to="7thy:4favA_iB_5O" resolve="content" />
                <uo k="s:originTrace" v="n:4313507821874777614" />
              </node>
            </node>
            <node concept="3GX2aA" id="Fc" role="2OqNvi">
              <uo k="s:originTrace" v="n:4313507821874795935" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="D7" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874241840" />
          <node concept="2OqwBi" id="Fh" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874241840" />
            <node concept="2OqwBi" id="Fi" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874241840" />
              <node concept="2OqwBi" id="Fk" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874241840" />
                <node concept="37vLTw" id="Fm" role="2Oq$k0">
                  <ref role="3cqZAo" node="CW" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874241840" />
                </node>
                <node concept="liA8E" id="Fn" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874241840" />
                </node>
              </node>
              <node concept="liA8E" id="Fl" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874241840" />
              </node>
            </node>
            <node concept="liA8E" id="Fj" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874241840" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="CW" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:4313507821874235856" />
        <node concept="3uibUv" id="Fo" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:4313507821874235856" />
        </node>
      </node>
      <node concept="2AHcQZ" id="CX" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821874235856" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="Fp">
    <property role="1sVAO0" value="false" />
    <property role="TrG5h" value="StringSchema_TextGen" />
    <uo k="s:originTrace" v="n:4313507821874104704" />
    <node concept="3Tm1VV" id="Fq" role="1B3o_S">
      <uo k="s:originTrace" v="n:4313507821874104704" />
    </node>
    <node concept="3uibUv" id="Fr" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenDescriptorBase" resolve="TextGenDescriptorBase" />
      <uo k="s:originTrace" v="n:4313507821874104704" />
    </node>
    <node concept="3clFb_" id="Fs" role="jymVt">
      <property role="TrG5h" value="generateText" />
      <uo k="s:originTrace" v="n:4313507821874104704" />
      <node concept="3cqZAl" id="Ft" role="3clF45">
        <uo k="s:originTrace" v="n:4313507821874104704" />
      </node>
      <node concept="3Tm1VV" id="Fu" role="1B3o_S">
        <uo k="s:originTrace" v="n:4313507821874104704" />
      </node>
      <node concept="3clFbS" id="Fv" role="3clF47">
        <uo k="s:originTrace" v="n:4313507821874104704" />
        <node concept="3cpWs8" id="Fy" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874104704" />
          <node concept="3cpWsn" id="FQ" role="3cpWs9">
            <property role="3TUv4t" value="true" />
            <property role="TrG5h" value="tgs" />
            <uo k="s:originTrace" v="n:4313507821874104704" />
            <node concept="3uibUv" id="FR" role="1tU5fm">
              <ref role="3uigEE" to="kpbf:~TextGenSupport" resolve="TextGenSupport" />
              <uo k="s:originTrace" v="n:4313507821874104704" />
            </node>
            <node concept="2ShNRf" id="FS" role="33vP2m">
              <uo k="s:originTrace" v="n:4313507821874104704" />
              <node concept="1pGfFk" id="FT" role="2ShVmc">
                <ref role="37wK5l" to="kpbf:~TextGenSupport.&lt;init&gt;(jetbrains.mps.text.rt.TextGenContext)" resolve="TextGenSupport" />
                <uo k="s:originTrace" v="n:4313507821874104704" />
                <node concept="37vLTw" id="FU" role="37wK5m">
                  <ref role="3cqZAo" node="Fw" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874104704" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="Fz" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874816890" />
          <node concept="2OqwBi" id="FV" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874816890" />
            <node concept="37vLTw" id="FW" role="2Oq$k0">
              <ref role="3cqZAo" node="FQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874816890" />
            </node>
            <node concept="liA8E" id="FX" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874816890" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="F$" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874108341" />
          <node concept="2OqwBi" id="FY" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874108341" />
            <node concept="37vLTw" id="FZ" role="2Oq$k0">
              <ref role="3cqZAo" node="FQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874108341" />
            </node>
            <node concept="liA8E" id="G0" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874108341" />
              <node concept="2OqwBi" id="G1" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821874108974" />
                <node concept="2OqwBi" id="G2" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874108393" />
                  <node concept="37vLTw" id="G4" role="2Oq$k0">
                    <ref role="3cqZAo" node="Fw" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="G5" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="G3" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMN" resolve="name" />
                  <uo k="s:originTrace" v="n:4313507821874109146" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="F_" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874817579" />
          <node concept="2OqwBi" id="G6" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874817579" />
            <node concept="37vLTw" id="G7" role="2Oq$k0">
              <ref role="3cqZAo" node="FQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874817579" />
            </node>
            <node concept="liA8E" id="G8" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874817579" />
              <node concept="Xl_RD" id="G9" role="37wK5m">
                <property role="Xl_RC" value=":\n" />
                <uo k="s:originTrace" v="n:4313507821874817579" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="FA" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874109724" />
          <node concept="2OqwBi" id="Ga" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874109724" />
            <node concept="2OqwBi" id="Gb" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874109724" />
              <node concept="2OqwBi" id="Gd" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874109724" />
                <node concept="37vLTw" id="Gf" role="2Oq$k0">
                  <ref role="3cqZAo" node="Fw" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874109724" />
                </node>
                <node concept="liA8E" id="Gg" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874109724" />
                </node>
              </node>
              <node concept="liA8E" id="Ge" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874109724" />
              </node>
            </node>
            <node concept="liA8E" id="Gc" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.increaseIndent()" resolve="increaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874109724" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="FB" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874836451" />
          <node concept="2OqwBi" id="Gh" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874836451" />
            <node concept="37vLTw" id="Gi" role="2Oq$k0">
              <ref role="3cqZAo" node="FQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874836451" />
            </node>
            <node concept="liA8E" id="Gj" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874836451" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="FC" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874153909" />
          <node concept="2OqwBi" id="Gk" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874153909" />
            <node concept="37vLTw" id="Gl" role="2Oq$k0">
              <ref role="3cqZAo" node="FQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874153909" />
            </node>
            <node concept="liA8E" id="Gm" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874153909" />
              <node concept="Xl_RD" id="Gn" role="37wK5m">
                <property role="Xl_RC" value="type: string" />
                <uo k="s:originTrace" v="n:4313507821874153909" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="FD" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874817653" />
          <node concept="2OqwBi" id="Go" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874817653" />
            <node concept="37vLTw" id="Gp" role="2Oq$k0">
              <ref role="3cqZAo" node="FQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874817653" />
            </node>
            <node concept="liA8E" id="Gq" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821874817653" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="FE" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874836055" />
          <node concept="2OqwBi" id="Gr" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874836055" />
            <node concept="37vLTw" id="Gs" role="2Oq$k0">
              <ref role="3cqZAo" node="FQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874836055" />
            </node>
            <node concept="liA8E" id="Gt" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
              <uo k="s:originTrace" v="n:4313507821874836055" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="FF" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874112060" />
          <node concept="2OqwBi" id="Gu" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874112060" />
            <node concept="37vLTw" id="Gv" role="2Oq$k0">
              <ref role="3cqZAo" node="FQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874112060" />
            </node>
            <node concept="liA8E" id="Gw" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874112060" />
              <node concept="Xl_RD" id="Gx" role="37wK5m">
                <property role="Xl_RC" value="description: " />
                <uo k="s:originTrace" v="n:4313507821874112060" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="FG" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874112250" />
          <node concept="2OqwBi" id="Gy" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874112250" />
            <node concept="37vLTw" id="Gz" role="2Oq$k0">
              <ref role="3cqZAo" node="FQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874112250" />
            </node>
            <node concept="liA8E" id="G$" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
              <uo k="s:originTrace" v="n:4313507821874112250" />
              <node concept="2OqwBi" id="G_" role="37wK5m">
                <uo k="s:originTrace" v="n:4313507821874112772" />
                <node concept="2OqwBi" id="GA" role="2Oq$k0">
                  <uo k="s:originTrace" v="n:4313507821874112302" />
                  <node concept="37vLTw" id="GC" role="2Oq$k0">
                    <ref role="3cqZAo" node="Fw" resolve="ctx" />
                  </node>
                  <node concept="liA8E" id="GD" role="2OqNvi">
                    <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                  </node>
                </node>
                <node concept="3TrcHB" id="GB" role="2OqNvi">
                  <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                  <uo k="s:originTrace" v="n:4313507821874114005" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="FH" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874820284" />
          <node concept="2OqwBi" id="GE" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874820284" />
            <node concept="37vLTw" id="GF" role="2Oq$k0">
              <ref role="3cqZAo" node="FQ" resolve="tgs" />
              <uo k="s:originTrace" v="n:4313507821874820284" />
            </node>
            <node concept="liA8E" id="GG" role="2OqNvi">
              <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
              <uo k="s:originTrace" v="n:4313507821874820284" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="FI" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600360534" />
          <node concept="3clFbS" id="GH" role="3clFbx">
            <uo k="s:originTrace" v="n:227067257600360535" />
            <node concept="3clFbF" id="GJ" role="3cqZAp">
              <uo k="s:originTrace" v="n:227067257600360536" />
              <node concept="2OqwBi" id="GK" role="3clFbG">
                <node concept="37vLTw" id="GL" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                </node>
                <node concept="liA8E" id="GM" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.reportError(java.lang.String)" resolve="reportError" />
                  <node concept="Xl_RD" id="GN" role="37wK5m">
                    <property role="Xl_RC" value="description is required" />
                    <uo k="s:originTrace" v="n:227067257600360537" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="GI" role="3clFbw">
            <uo k="s:originTrace" v="n:227067257600360538" />
            <node concept="2OqwBi" id="GO" role="2Oq$k0">
              <uo k="s:originTrace" v="n:227067257600360539" />
              <node concept="2OqwBi" id="GQ" role="2Oq$k0">
                <uo k="s:originTrace" v="n:227067257600360540" />
                <node concept="37vLTw" id="GS" role="2Oq$k0">
                  <ref role="3cqZAo" node="Fw" resolve="ctx" />
                </node>
                <node concept="liA8E" id="GT" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="GR" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMH" resolve="description" />
                <uo k="s:originTrace" v="n:227067257600360541" />
              </node>
            </node>
            <node concept="17RlXB" id="GP" role="2OqNvi">
              <uo k="s:originTrace" v="n:227067257600360542" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="FJ" role="3cqZAp">
          <uo k="s:originTrace" v="n:227067257600360026" />
        </node>
        <node concept="3clFbJ" id="FK" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874836652" />
          <node concept="3clFbS" id="GU" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874836654" />
            <node concept="3clFbF" id="GW" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874842478" />
              <node concept="2OqwBi" id="H0" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874842478" />
                <node concept="37vLTw" id="H1" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874842478" />
                </node>
                <node concept="liA8E" id="H2" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874842478" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="GX" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874111752" />
              <node concept="2OqwBi" id="H3" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874111752" />
                <node concept="37vLTw" id="H4" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874111752" />
                </node>
                <node concept="liA8E" id="H5" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874111752" />
                  <node concept="Xl_RD" id="H6" role="37wK5m">
                    <property role="Xl_RC" value="example: " />
                    <uo k="s:originTrace" v="n:4313507821874111752" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="GY" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874109783" />
              <node concept="2OqwBi" id="H7" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874109783" />
                <node concept="37vLTw" id="H8" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874109783" />
                </node>
                <node concept="liA8E" id="H9" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874109783" />
                  <node concept="2OqwBi" id="Ha" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874110305" />
                    <node concept="2OqwBi" id="Hb" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:4313507821874109835" />
                      <node concept="37vLTw" id="Hd" role="2Oq$k0">
                        <ref role="3cqZAo" node="Fw" resolve="ctx" />
                      </node>
                      <node concept="liA8E" id="He" role="2OqNvi">
                        <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="Hc" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:4favA_iBNMJ" resolve="example" />
                      <uo k="s:originTrace" v="n:4313507821874111503" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="GZ" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874820475" />
              <node concept="2OqwBi" id="Hf" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874820475" />
                <node concept="37vLTw" id="Hg" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874820475" />
                </node>
                <node concept="liA8E" id="Hh" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874820475" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="GV" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874839806" />
            <node concept="2OqwBi" id="Hi" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874837372" />
              <node concept="2OqwBi" id="Hk" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874836858" />
                <node concept="37vLTw" id="Hm" role="2Oq$k0">
                  <ref role="3cqZAo" node="Fw" resolve="ctx" />
                </node>
                <node concept="liA8E" id="Hn" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="Hl" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNMJ" resolve="example" />
                <uo k="s:originTrace" v="n:4313507821874838465" />
              </node>
            </node>
            <node concept="17RvpY" id="Hj" role="2OqNvi">
              <uo k="s:originTrace" v="n:4313507821874841980" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="FL" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874842820" />
          <node concept="3clFbS" id="Ho" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874842822" />
            <node concept="3clFbF" id="Hq" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874848038" />
              <node concept="2OqwBi" id="Hu" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874848038" />
                <node concept="37vLTw" id="Hv" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874848038" />
                </node>
                <node concept="liA8E" id="Hw" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874848038" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Hr" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874114343" />
              <node concept="2OqwBi" id="Hx" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874114343" />
                <node concept="37vLTw" id="Hy" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874114343" />
                </node>
                <node concept="liA8E" id="Hz" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874114343" />
                  <node concept="Xl_RD" id="H$" role="37wK5m">
                    <property role="Xl_RC" value="format: " />
                    <uo k="s:originTrace" v="n:4313507821874114343" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Hs" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874114345" />
              <node concept="2OqwBi" id="H_" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874114345" />
                <node concept="37vLTw" id="HA" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874114345" />
                </node>
                <node concept="liA8E" id="HB" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874114345" />
                  <node concept="2OqwBi" id="HC" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874114346" />
                    <node concept="2OqwBi" id="HD" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:4313507821874114347" />
                      <node concept="37vLTw" id="HF" role="2Oq$k0">
                        <ref role="3cqZAo" node="Fw" resolve="ctx" />
                      </node>
                      <node concept="liA8E" id="HG" role="2OqNvi">
                        <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="HE" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:3JsE8ainSvo" resolve="format" />
                      <uo k="s:originTrace" v="n:4313507821874114348" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Ht" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874822532" />
              <node concept="2OqwBi" id="HH" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874822532" />
                <node concept="37vLTw" id="HI" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874822532" />
                </node>
                <node concept="liA8E" id="HJ" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874822532" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="Hp" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874845610" />
            <node concept="2OqwBi" id="HK" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874843557" />
              <node concept="2OqwBi" id="HM" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874843043" />
                <node concept="37vLTw" id="HO" role="2Oq$k0">
                  <ref role="3cqZAo" node="Fw" resolve="ctx" />
                </node>
                <node concept="liA8E" id="HP" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="HN" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JsE8ainSvo" resolve="format" />
                <uo k="s:originTrace" v="n:4313507821874844269" />
              </node>
            </node>
            <node concept="17RvpY" id="HL" role="2OqNvi">
              <uo k="s:originTrace" v="n:4313507821874848003" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="FM" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874848889" />
          <node concept="3clFbS" id="HQ" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874848891" />
            <node concept="3clFbF" id="HS" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874901113" />
              <node concept="2OqwBi" id="HW" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874901113" />
                <node concept="37vLTw" id="HX" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874901113" />
                </node>
                <node concept="liA8E" id="HY" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874901113" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="HT" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874115269" />
              <node concept="2OqwBi" id="HZ" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874115269" />
                <node concept="37vLTw" id="I0" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874115269" />
                </node>
                <node concept="liA8E" id="I1" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874115269" />
                  <node concept="Xl_RD" id="I2" role="37wK5m">
                    <property role="Xl_RC" value="minLength: " />
                    <uo k="s:originTrace" v="n:4313507821874115269" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="HU" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874115271" />
              <node concept="2OqwBi" id="I3" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874115271" />
                <node concept="37vLTw" id="I4" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874115271" />
                </node>
                <node concept="liA8E" id="I5" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874115271" />
                  <node concept="3cpWs3" id="I6" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874826955" />
                    <node concept="Xl_RD" id="I7" role="3uHU7B">
                      <uo k="s:originTrace" v="n:4313507821874825425" />
                    </node>
                    <node concept="2OqwBi" id="I8" role="3uHU7w">
                      <uo k="s:originTrace" v="n:4313507821874119012" />
                      <node concept="3TrcHB" id="I9" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:3JsE8aiIcbJ" resolve="minLength" />
                        <uo k="s:originTrace" v="n:4313507821874119776" />
                      </node>
                      <node concept="2OqwBi" id="Ia" role="2Oq$k0">
                        <uo k="s:originTrace" v="n:4313507821874827006" />
                        <node concept="37vLTw" id="Ib" role="2Oq$k0">
                          <ref role="3cqZAo" node="Fw" resolve="ctx" />
                        </node>
                        <node concept="liA8E" id="Ic" role="2OqNvi">
                          <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="HV" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874825380" />
              <node concept="2OqwBi" id="Id" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874825380" />
                <node concept="37vLTw" id="Ie" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874825380" />
                </node>
                <node concept="liA8E" id="If" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874825380" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3eOSWO" id="HR" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874900741" />
            <node concept="3cmrfG" id="Ig" role="3uHU7w">
              <property role="3cmrfH" value="0" />
              <uo k="s:originTrace" v="n:4313507821874900745" />
            </node>
            <node concept="2OqwBi" id="Ih" role="3uHU7B">
              <uo k="s:originTrace" v="n:4313507821874891083" />
              <node concept="2OqwBi" id="Ii" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874890569" />
                <node concept="37vLTw" id="Ik" role="2Oq$k0">
                  <ref role="3cqZAo" node="Fw" resolve="ctx" />
                </node>
                <node concept="liA8E" id="Il" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="Ij" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JsE8aiIcbJ" resolve="minLength" />
                <uo k="s:originTrace" v="n:4313507821874892246" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="FN" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874902287" />
          <node concept="3clFbS" id="Im" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874902289" />
            <node concept="3clFbF" id="Io" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874908762" />
              <node concept="2OqwBi" id="Is" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874908762" />
                <node concept="37vLTw" id="It" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874908762" />
                </node>
                <node concept="liA8E" id="Iu" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874908762" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Ip" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874132705" />
              <node concept="2OqwBi" id="Iv" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874132705" />
                <node concept="37vLTw" id="Iw" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874132705" />
                </node>
                <node concept="liA8E" id="Ix" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874132705" />
                  <node concept="Xl_RD" id="Iy" role="37wK5m">
                    <property role="Xl_RC" value="maxLength: " />
                    <uo k="s:originTrace" v="n:4313507821874132705" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Iq" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874132707" />
              <node concept="2OqwBi" id="Iz" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874132707" />
                <node concept="37vLTw" id="I$" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874132707" />
                </node>
                <node concept="liA8E" id="I_" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874132707" />
                  <node concept="3cpWs3" id="IA" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874832672" />
                    <node concept="Xl_RD" id="IB" role="3uHU7B">
                      <uo k="s:originTrace" v="n:4313507821874831142" />
                    </node>
                    <node concept="2OqwBi" id="IC" role="3uHU7w">
                      <uo k="s:originTrace" v="n:4313507821874132710" />
                      <node concept="3TrcHB" id="ID" role="2OqNvi">
                        <ref role="3TsBF5" to="7thy:3JsE8aiIcbN" resolve="maxLength" />
                        <uo k="s:originTrace" v="n:4313507821874132711" />
                      </node>
                      <node concept="2OqwBi" id="IE" role="2Oq$k0">
                        <uo k="s:originTrace" v="n:4313507821874832998" />
                        <node concept="37vLTw" id="IF" role="2Oq$k0">
                          <ref role="3cqZAo" node="Fw" resolve="ctx" />
                        </node>
                        <node concept="liA8E" id="IG" role="2OqNvi">
                          <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Ir" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874831097" />
              <node concept="2OqwBi" id="IH" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874831097" />
                <node concept="37vLTw" id="II" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874831097" />
                </node>
                <node concept="liA8E" id="IJ" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874831097" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3eOSWO" id="In" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874907849" />
            <node concept="3cmrfG" id="IK" role="3uHU7w">
              <property role="3cmrfH" value="0" />
              <uo k="s:originTrace" v="n:4313507821874907884" />
            </node>
            <node concept="2OqwBi" id="IL" role="3uHU7B">
              <uo k="s:originTrace" v="n:4313507821874903038" />
              <node concept="2OqwBi" id="IM" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874902541" />
                <node concept="37vLTw" id="IO" role="2Oq$k0">
                  <ref role="3cqZAo" node="Fw" resolve="ctx" />
                </node>
                <node concept="liA8E" id="IP" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="IN" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JsE8aiIcbN" resolve="maxLength" />
                <uo k="s:originTrace" v="n:4313507821874904794" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="FO" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874909432" />
          <node concept="3clFbS" id="IQ" role="3clFbx">
            <uo k="s:originTrace" v="n:4313507821874909434" />
            <node concept="3clFbF" id="IS" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874914822" />
              <node concept="2OqwBi" id="IW" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874914822" />
                <node concept="37vLTw" id="IX" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874914822" />
                </node>
                <node concept="liA8E" id="IY" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.indent()" resolve="indent" />
                  <uo k="s:originTrace" v="n:4313507821874914822" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="IT" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874133583" />
              <node concept="2OqwBi" id="IZ" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874133583" />
                <node concept="37vLTw" id="J0" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874133583" />
                </node>
                <node concept="liA8E" id="J1" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874133583" />
                  <node concept="Xl_RD" id="J2" role="37wK5m">
                    <property role="Xl_RC" value="pattern: " />
                    <uo k="s:originTrace" v="n:4313507821874133583" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="IU" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874133585" />
              <node concept="2OqwBi" id="J3" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874133585" />
                <node concept="37vLTw" id="J4" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874133585" />
                </node>
                <node concept="liA8E" id="J5" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.append(java.lang.CharSequence)" resolve="append" />
                  <uo k="s:originTrace" v="n:4313507821874133585" />
                  <node concept="2OqwBi" id="J6" role="37wK5m">
                    <uo k="s:originTrace" v="n:4313507821874133588" />
                    <node concept="3TrcHB" id="J7" role="2OqNvi">
                      <ref role="3TsBF5" to="7thy:4favA_iBNS8" resolve="pattern" />
                      <uo k="s:originTrace" v="n:4313507821874133589" />
                    </node>
                    <node concept="2OqwBi" id="J8" role="2Oq$k0">
                      <uo k="s:originTrace" v="n:4313507821874133590" />
                      <node concept="37vLTw" id="J9" role="2Oq$k0">
                        <ref role="3cqZAo" node="Fw" resolve="ctx" />
                      </node>
                      <node concept="liA8E" id="Ja" role="2OqNvi">
                        <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="IV" role="3cqZAp">
              <uo k="s:originTrace" v="n:4313507821874835343" />
              <node concept="2OqwBi" id="Jb" role="3clFbG">
                <uo k="s:originTrace" v="n:4313507821874835343" />
                <node concept="37vLTw" id="Jc" role="2Oq$k0">
                  <ref role="3cqZAo" node="FQ" resolve="tgs" />
                  <uo k="s:originTrace" v="n:4313507821874835343" />
                </node>
                <node concept="liA8E" id="Jd" role="2OqNvi">
                  <ref role="37wK5l" to="kpbf:~TextGenSupport.newLine()" resolve="newLine" />
                  <uo k="s:originTrace" v="n:4313507821874835343" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="IR" role="3clFbw">
            <uo k="s:originTrace" v="n:4313507821874912761" />
            <node concept="2OqwBi" id="Je" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874910292" />
              <node concept="2OqwBi" id="Jg" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874909778" />
                <node concept="37vLTw" id="Ji" role="2Oq$k0">
                  <ref role="3cqZAo" node="Fw" resolve="ctx" />
                </node>
                <node concept="liA8E" id="Jj" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getPrimaryInput()" resolve="getPrimaryInput" />
                </node>
              </node>
              <node concept="3TrcHB" id="Jh" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:4favA_iBNS8" resolve="pattern" />
                <uo k="s:originTrace" v="n:4313507821874911420" />
              </node>
            </node>
            <node concept="17RvpY" id="Jf" role="2OqNvi">
              <uo k="s:originTrace" v="n:4313507821874914787" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="FP" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874109724" />
          <node concept="2OqwBi" id="Jk" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874109724" />
            <node concept="2OqwBi" id="Jl" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874109724" />
              <node concept="2OqwBi" id="Jn" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874109724" />
                <node concept="37vLTw" id="Jp" role="2Oq$k0">
                  <ref role="3cqZAo" node="Fw" resolve="ctx" />
                  <uo k="s:originTrace" v="n:4313507821874109724" />
                </node>
                <node concept="liA8E" id="Jq" role="2OqNvi">
                  <ref role="37wK5l" to="yfwt:~TextGenContext.getBuffer()" resolve="getBuffer" />
                  <uo k="s:originTrace" v="n:4313507821874109724" />
                </node>
              </node>
              <node concept="liA8E" id="Jo" role="2OqNvi">
                <ref role="37wK5l" to="ao3:~TextBuffer.area()" resolve="area" />
                <uo k="s:originTrace" v="n:4313507821874109724" />
              </node>
            </node>
            <node concept="liA8E" id="Jm" role="2OqNvi">
              <ref role="37wK5l" to="ao3:~TextArea.decreaseIndent()" resolve="decreaseIndent" />
              <uo k="s:originTrace" v="n:4313507821874109724" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="Fw" role="3clF46">
        <property role="TrG5h" value="ctx" />
        <property role="3TUv4t" value="true" />
        <uo k="s:originTrace" v="n:4313507821874104704" />
        <node concept="3uibUv" id="Jr" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenContext" resolve="TextGenContext" />
          <uo k="s:originTrace" v="n:4313507821874104704" />
        </node>
      </node>
      <node concept="2AHcQZ" id="Fx" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <uo k="s:originTrace" v="n:4313507821874104704" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="Js">
    <property role="TrG5h" value="TextGenAspectDescriptor" />
    <node concept="312cEg" id="Jt" role="jymVt">
      <property role="TrG5h" value="myIndex" />
      <property role="3TUv4t" value="true" />
      <node concept="3Tm6S6" id="JC" role="1B3o_S" />
      <node concept="2eloPW" id="JD" role="1tU5fm">
        <property role="2ely0U" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
        <ref role="3uigEE" to="tpcf:1OW7rNmnulT" resolve="LanguageConceptSwitch" />
      </node>
      <node concept="2ShNRf" id="JE" role="33vP2m">
        <node concept="xCZzO" id="JF" role="2ShVmc">
          <property role="xCZzQ" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
          <node concept="3uibUv" id="JG" role="xCZzL">
            <ref role="3uigEE" to="tpcf:1OW7rNmnulT" resolve="LanguageConceptSwitch" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="Ju" role="jymVt" />
    <node concept="3clFbW" id="Jv" role="jymVt">
      <node concept="3cqZAl" id="JH" role="3clF45" />
      <node concept="3clFbS" id="JI" role="3clF47" />
      <node concept="3Tm1VV" id="JJ" role="1B3o_S" />
    </node>
    <node concept="2tJIrI" id="Jw" role="jymVt" />
    <node concept="3Tm1VV" id="Jx" role="1B3o_S" />
    <node concept="3uibUv" id="Jy" role="1zkMxy">
      <ref role="3uigEE" to="yfwt:~TextGenAspectBase" resolve="TextGenAspectBase" />
    </node>
    <node concept="3clFb_" id="Jz" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getDescriptor" />
      <property role="DiZV1" value="false" />
      <node concept="3Tm1VV" id="JK" role="1B3o_S" />
      <node concept="3uibUv" id="JL" role="3clF45">
        <ref role="3uigEE" to="yfwt:~TextGenDescriptor" resolve="TextGenDescriptor" />
      </node>
      <node concept="37vLTG" id="JM" role="3clF46">
        <property role="TrG5h" value="concept" />
        <node concept="3bZ5Sz" id="JQ" role="1tU5fm" />
        <node concept="2AHcQZ" id="JR" role="2AJF6D">
          <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
        </node>
      </node>
      <node concept="2AHcQZ" id="JN" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
      </node>
      <node concept="3clFbS" id="JO" role="3clF47">
        <node concept="3KaCP$" id="JS" role="3cqZAp">
          <node concept="2OqwBi" id="JU" role="3KbGdf">
            <node concept="37vLTw" id="Kd" role="2Oq$k0">
              <ref role="3cqZAo" node="Jt" resolve="myIndex" />
            </node>
            <node concept="liA8E" id="Ke" role="2OqNvi">
              <ref role="37wK5l" to="tpcf:1OW7rNmnuDr" resolve="index" />
              <node concept="37vLTw" id="Kf" role="37wK5m">
                <ref role="3cqZAo" node="JM" resolve="concept" />
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="JV" role="3KbHQx">
            <node concept="1n$iZg" id="Kg" role="3Kbmr1">
              <property role="1n_iUB" value="AllOfSchema" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="Kh" role="3Kbo56">
              <node concept="3cpWs6" id="Ki" role="3cqZAp">
                <node concept="2ShNRf" id="Kj" role="3cqZAk">
                  <node concept="HV5vD" id="Kk" role="2ShVmc">
                    <ref role="HV5vE" node="0" resolve="AllOfSchema_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="JW" role="3KbHQx">
            <node concept="1n$iZg" id="Kl" role="3Kbmr1">
              <property role="1n_iUB" value="AnyOfSchema" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="Km" role="3Kbo56">
              <node concept="3cpWs6" id="Kn" role="3cqZAp">
                <node concept="2ShNRf" id="Ko" role="3cqZAk">
                  <node concept="HV5vD" id="Kp" role="2ShVmc">
                    <ref role="HV5vE" node="2C" resolve="AnyOfSchema_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="JX" role="3KbHQx">
            <node concept="1n$iZg" id="Kq" role="3Kbmr1">
              <property role="1n_iUB" value="ArraySchema" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="Kr" role="3Kbo56">
              <node concept="3cpWs6" id="Ks" role="3cqZAp">
                <node concept="2ShNRf" id="Kt" role="3cqZAk">
                  <node concept="HV5vD" id="Ku" role="2ShVmc">
                    <ref role="HV5vE" node="5g" resolve="ArraySchema_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="JY" role="3KbHQx">
            <node concept="1n$iZg" id="Kv" role="3Kbmr1">
              <property role="1n_iUB" value="BooleanSchema" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="Kw" role="3Kbo56">
              <node concept="3cpWs6" id="Kx" role="3cqZAp">
                <node concept="2ShNRf" id="Ky" role="3cqZAk">
                  <node concept="HV5vD" id="Kz" role="2ShVmc">
                    <ref role="HV5vE" node="97" resolve="BooleanSchema_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="JZ" role="3KbHQx">
            <node concept="1n$iZg" id="K$" role="3Kbmr1">
              <property role="1n_iUB" value="Content" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="K_" role="3Kbo56">
              <node concept="3cpWs6" id="KA" role="3cqZAp">
                <node concept="2ShNRf" id="KB" role="3cqZAk">
                  <node concept="HV5vD" id="KC" role="2ShVmc">
                    <ref role="HV5vE" node="a6" resolve="Content_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="K0" role="3KbHQx">
            <node concept="1n$iZg" id="KD" role="3Kbmr1">
              <property role="1n_iUB" value="DeleteOperationObject" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="KE" role="3Kbo56">
              <node concept="3cpWs6" id="KF" role="3cqZAp">
                <node concept="2ShNRf" id="KG" role="3cqZAk">
                  <node concept="HV5vD" id="KH" role="2ShVmc">
                    <ref role="HV5vE" node="bX" resolve="DeleteOperationObject_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="K1" role="3KbHQx">
            <node concept="1n$iZg" id="KI" role="3Kbmr1">
              <property role="1n_iUB" value="GetOperationObject" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="KJ" role="3Kbo56">
              <node concept="3cpWs6" id="KK" role="3cqZAp">
                <node concept="2ShNRf" id="KL" role="3cqZAk">
                  <node concept="HV5vD" id="KM" role="2ShVmc">
                    <ref role="HV5vE" node="fC" resolve="GetOperationObject_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="K2" role="3KbHQx">
            <node concept="1n$iZg" id="KN" role="3Kbmr1">
              <property role="1n_iUB" value="IntegerSchema" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="KO" role="3Kbo56">
              <node concept="3cpWs6" id="KP" role="3cqZAp">
                <node concept="2ShNRf" id="KQ" role="3cqZAk">
                  <node concept="HV5vD" id="KR" role="2ShVmc">
                    <ref role="HV5vE" node="hW" resolve="IntegerSchema_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="K3" role="3KbHQx">
            <node concept="1n$iZg" id="KS" role="3Kbmr1">
              <property role="1n_iUB" value="ObjectSchema" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="KT" role="3Kbo56">
              <node concept="3cpWs6" id="KU" role="3cqZAp">
                <node concept="2ShNRf" id="KV" role="3cqZAk">
                  <node concept="HV5vD" id="KW" role="2ShVmc">
                    <ref role="HV5vE" node="mS" resolve="ObjectSchema_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="K4" role="3KbHQx">
            <node concept="1n$iZg" id="KX" role="3Kbmr1">
              <property role="1n_iUB" value="ObjectSchemaProperty" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="KY" role="3Kbo56">
              <node concept="3cpWs6" id="KZ" role="3cqZAp">
                <node concept="2ShNRf" id="L0" role="3cqZAk">
                  <node concept="HV5vD" id="L1" role="2ShVmc">
                    <ref role="HV5vE" node="lw" resolve="ObjectSchemaProperty_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="K5" role="3KbHQx">
            <node concept="1n$iZg" id="L2" role="3Kbmr1">
              <property role="1n_iUB" value="OneOfSchema" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="L3" role="3Kbo56">
              <node concept="3cpWs6" id="L4" role="3cqZAp">
                <node concept="2ShNRf" id="L5" role="3cqZAk">
                  <node concept="HV5vD" id="L6" role="2ShVmc">
                    <ref role="HV5vE" node="rt" resolve="OneOfSchema_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="K6" role="3KbHQx">
            <node concept="1n$iZg" id="L7" role="3Kbmr1">
              <property role="1n_iUB" value="OpenApi" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="L8" role="3Kbo56">
              <node concept="3cpWs6" id="L9" role="3cqZAp">
                <node concept="2ShNRf" id="La" role="3cqZAk">
                  <node concept="HV5vD" id="Lb" role="2ShVmc">
                    <ref role="HV5vE" node="vZ" resolve="OpenApi_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="K7" role="3KbHQx">
            <node concept="1n$iZg" id="Lc" role="3Kbmr1">
              <property role="1n_iUB" value="OpenApiInfo" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="Ld" role="3Kbo56">
              <node concept="3cpWs6" id="Le" role="3cqZAp">
                <node concept="2ShNRf" id="Lf" role="3cqZAk">
                  <node concept="HV5vD" id="Lg" role="2ShVmc">
                    <ref role="HV5vE" node="u6" resolve="OpenApiInfo_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="K8" role="3KbHQx">
            <node concept="1n$iZg" id="Lh" role="3Kbmr1">
              <property role="1n_iUB" value="PathObject" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="Li" role="3Kbo56">
              <node concept="3cpWs6" id="Lj" role="3cqZAp">
                <node concept="2ShNRf" id="Lk" role="3cqZAk">
                  <node concept="HV5vD" id="Ll" role="2ShVmc">
                    <ref role="HV5vE" node="yZ" resolve="PathObject_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="K9" role="3KbHQx">
            <node concept="1n$iZg" id="Lm" role="3Kbmr1">
              <property role="1n_iUB" value="PostOperationObject" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="Ln" role="3Kbo56">
              <node concept="3cpWs6" id="Lo" role="3cqZAp">
                <node concept="2ShNRf" id="Lp" role="3cqZAk">
                  <node concept="HV5vD" id="Lq" role="2ShVmc">
                    <ref role="HV5vE" node="$d" resolve="PostOperationObject_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="Ka" role="3KbHQx">
            <node concept="1n$iZg" id="Lr" role="3Kbmr1">
              <property role="1n_iUB" value="PutOperationObject" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="Ls" role="3Kbo56">
              <node concept="3cpWs6" id="Lt" role="3cqZAp">
                <node concept="2ShNRf" id="Lu" role="3cqZAk">
                  <node concept="HV5vD" id="Lv" role="2ShVmc">
                    <ref role="HV5vE" node="Ax" resolve="PutOperationObject_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="Kb" role="3KbHQx">
            <node concept="1n$iZg" id="Lw" role="3Kbmr1">
              <property role="1n_iUB" value="Response" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="Lx" role="3Kbo56">
              <node concept="3cpWs6" id="Ly" role="3cqZAp">
                <node concept="2ShNRf" id="Lz" role="3cqZAk">
                  <node concept="HV5vD" id="L$" role="2ShVmc">
                    <ref role="HV5vE" node="CP" resolve="Response_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="Kc" role="3KbHQx">
            <node concept="1n$iZg" id="L_" role="3Kbmr1">
              <property role="1n_iUB" value="StringSchema" />
              <property role="1n_ezw" value="de.thb.openapidsl.copy.structure.LanguageConceptSwitch" />
            </node>
            <node concept="3clFbS" id="LA" role="3Kbo56">
              <node concept="3cpWs6" id="LB" role="3cqZAp">
                <node concept="2ShNRf" id="LC" role="3cqZAk">
                  <node concept="HV5vD" id="LD" role="2ShVmc">
                    <ref role="HV5vE" node="Fp" resolve="StringSchema_TextGen" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="JT" role="3cqZAp">
          <node concept="10Nm6u" id="LE" role="3cqZAk" />
        </node>
      </node>
      <node concept="2AHcQZ" id="JP" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="J$" role="jymVt" />
    <node concept="3clFb_" id="J_" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="breakdownToUnits" />
      <property role="DiZV1" value="false" />
      <node concept="3Tm1VV" id="LF" role="1B3o_S" />
      <node concept="3cqZAl" id="LG" role="3clF45" />
      <node concept="37vLTG" id="LH" role="3clF46">
        <property role="TrG5h" value="outline" />
        <node concept="3uibUv" id="LK" role="1tU5fm">
          <ref role="3uigEE" to="yfwt:~TextGenModelOutline" resolve="TextGenModelOutline" />
        </node>
        <node concept="2AHcQZ" id="LL" role="2AJF6D">
          <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
        </node>
      </node>
      <node concept="3clFbS" id="LI" role="3clF47">
        <node concept="1DcWWT" id="LM" role="3cqZAp">
          <node concept="3clFbS" id="LN" role="2LFqv$">
            <node concept="3clFbJ" id="LQ" role="3cqZAp">
              <node concept="3clFbS" id="LR" role="3clFbx">
                <node concept="3cpWs8" id="LT" role="3cqZAp">
                  <node concept="3cpWsn" id="LX" role="3cpWs9">
                    <property role="TrG5h" value="fname" />
                    <node concept="3uibUv" id="LY" role="1tU5fm">
                      <ref role="3uigEE" to="wyt6:~String" resolve="String" />
                    </node>
                    <node concept="1rXfSq" id="LZ" role="33vP2m">
                      <ref role="37wK5l" node="JA" resolve="getFileName_OpenApi" />
                      <node concept="37vLTw" id="M0" role="37wK5m">
                        <ref role="3cqZAo" node="LO" resolve="root" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs8" id="LU" role="3cqZAp">
                  <node concept="3cpWsn" id="M1" role="3cpWs9">
                    <property role="TrG5h" value="ext" />
                    <node concept="3uibUv" id="M2" role="1tU5fm">
                      <ref role="3uigEE" to="wyt6:~String" resolve="String" />
                    </node>
                    <node concept="1rXfSq" id="M3" role="33vP2m">
                      <ref role="37wK5l" node="JB" resolve="getFileExtension_OpenApi" />
                      <node concept="37vLTw" id="M4" role="37wK5m">
                        <ref role="3cqZAo" node="LO" resolve="root" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="LV" role="3cqZAp">
                  <node concept="2OqwBi" id="M5" role="3clFbG">
                    <node concept="37vLTw" id="M6" role="2Oq$k0">
                      <ref role="3cqZAo" node="LH" resolve="outline" />
                    </node>
                    <node concept="liA8E" id="M7" role="2OqNvi">
                      <ref role="37wK5l" to="yfwt:~TextGenModelOutline.registerTextUnit(java.lang.String,java.lang.String,java.nio.charset.Charset,org.jetbrains.mps.openapi.model.SNode...)" resolve="registerTextUnit" />
                      <node concept="3K4zz7" id="M8" role="37wK5m">
                        <node concept="1eOMI4" id="Ma" role="3K4GZi">
                          <node concept="3cpWs3" id="Md" role="1eOMHV">
                            <node concept="37vLTw" id="Me" role="3uHU7w">
                              <ref role="3cqZAo" node="M1" resolve="ext" />
                            </node>
                            <node concept="3cpWs3" id="Mf" role="3uHU7B">
                              <node concept="37vLTw" id="Mg" role="3uHU7B">
                                <ref role="3cqZAo" node="LX" resolve="fname" />
                              </node>
                              <node concept="1Xhbcc" id="Mh" role="3uHU7w">
                                <property role="1XhdNS" value="." />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="37vLTw" id="Mb" role="3K4E3e">
                          <ref role="3cqZAo" node="LX" resolve="fname" />
                        </node>
                        <node concept="3clFbC" id="Mc" role="3K4Cdx">
                          <node concept="10Nm6u" id="Mi" role="3uHU7w" />
                          <node concept="37vLTw" id="Mj" role="3uHU7B">
                            <ref role="3cqZAo" node="M1" resolve="ext" />
                          </node>
                        </node>
                      </node>
                      <node concept="37vLTw" id="M9" role="37wK5m">
                        <ref role="3cqZAo" node="LO" resolve="root" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3N13vt" id="LW" role="3cqZAp" />
              </node>
              <node concept="2OqwBi" id="LS" role="3clFbw">
                <node concept="2OqwBi" id="Mk" role="2Oq$k0">
                  <node concept="37vLTw" id="Mm" role="2Oq$k0">
                    <ref role="3cqZAo" node="LO" resolve="root" />
                  </node>
                  <node concept="liA8E" id="Mn" role="2OqNvi">
                    <ref role="37wK5l" to="mhbf:~SNode.getConcept()" resolve="getConcept" />
                  </node>
                </node>
                <node concept="liA8E" id="Ml" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.equals(java.lang.Object)" resolve="equals" />
                  <node concept="35c_gC" id="Mo" role="37wK5m">
                    <ref role="35c_gD" to="7thy:3JW8_axMW7I" resolve="OpenApi" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="LO" role="1Duv9x">
            <property role="TrG5h" value="root" />
            <node concept="3uibUv" id="Mp" role="1tU5fm">
              <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
            </node>
          </node>
          <node concept="2OqwBi" id="LP" role="1DdaDG">
            <node concept="2OqwBi" id="Mq" role="2Oq$k0">
              <node concept="37vLTw" id="Ms" role="2Oq$k0">
                <ref role="3cqZAo" node="LH" resolve="outline" />
              </node>
              <node concept="liA8E" id="Mt" role="2OqNvi">
                <ref role="37wK5l" to="yfwt:~TextGenModelOutline.getModel()" resolve="getModel" />
              </node>
            </node>
            <node concept="liA8E" id="Mr" role="2OqNvi">
              <ref role="37wK5l" to="mhbf:~SModel.getRootNodes()" resolve="getRootNodes" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="LJ" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2YIFZL" id="JA" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getFileName_OpenApi" />
      <node concept="3clFbS" id="Mu" role="3clF47">
        <node concept="3clFbF" id="My" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874333167" />
          <node concept="2OqwBi" id="Mz" role="3clFbG">
            <uo k="s:originTrace" v="n:4313507821874338662" />
            <node concept="2OqwBi" id="M$" role="2Oq$k0">
              <uo k="s:originTrace" v="n:4313507821874336244" />
              <node concept="2OqwBi" id="MA" role="2Oq$k0">
                <uo k="s:originTrace" v="n:4313507821874333817" />
                <node concept="37vLTw" id="MC" role="2Oq$k0">
                  <ref role="3cqZAo" node="Mx" resolve="node" />
                  <uo k="s:originTrace" v="n:4313507821874333166" />
                </node>
                <node concept="3TrEf2" id="MD" role="2OqNvi">
                  <ref role="3Tt5mk" to="7thy:3JW8_axMW7J" resolve="info" />
                  <uo k="s:originTrace" v="n:4313507821874335270" />
                </node>
              </node>
              <node concept="3TrcHB" id="MB" role="2OqNvi">
                <ref role="3TsBF5" to="7thy:3JW8_axMFqV" resolve="title" />
                <uo k="s:originTrace" v="n:4313507821874337570" />
              </node>
            </node>
            <node concept="liA8E" id="M_" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~String.toLowerCase()" resolve="toLowerCase" />
              <uo k="s:originTrace" v="n:4313507821874340707" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="Mv" role="1B3o_S" />
      <node concept="3uibUv" id="Mw" role="3clF45">
        <ref role="3uigEE" to="wyt6:~String" resolve="String" />
      </node>
      <node concept="37vLTG" id="Mx" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3uibUv" id="ME" role="1tU5fm">
          <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
        </node>
      </node>
    </node>
    <node concept="2YIFZL" id="JB" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getFileExtension_OpenApi" />
      <node concept="3clFbS" id="MF" role="3clF47">
        <node concept="3clFbF" id="MJ" role="3cqZAp">
          <uo k="s:originTrace" v="n:4313507821874304491" />
          <node concept="Xl_RD" id="MK" role="3clFbG">
            <property role="Xl_RC" value="yaml" />
            <uo k="s:originTrace" v="n:4313507821874304490" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="MG" role="1B3o_S" />
      <node concept="3uibUv" id="MH" role="3clF45">
        <ref role="3uigEE" to="wyt6:~String" resolve="String" />
      </node>
      <node concept="37vLTG" id="MI" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3uibUv" id="ML" role="1tU5fm">
          <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
        </node>
      </node>
    </node>
  </node>
</model>

