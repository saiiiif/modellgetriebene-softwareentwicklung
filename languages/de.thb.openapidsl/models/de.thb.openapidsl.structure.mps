<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:bacb7caf-e4bf-48cd-928a-a325cec4d119(de.thb.openapidsl.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="9" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="3348158742936976480" name="jetbrains.mps.lang.structure.structure.EnumerationMemberDeclaration" flags="ng" index="25R33">
        <property id="1421157252384165432" name="memberId" index="3tVfz5" />
      </concept>
      <concept id="3348158742936976479" name="jetbrains.mps.lang.structure.structure.EnumerationDeclaration" flags="ng" index="25R3W">
        <child id="3348158742936976577" name="members" index="25R1y" />
      </concept>
      <concept id="1082978164218" name="jetbrains.mps.lang.structure.structure.DataTypeDeclaration" flags="ng" index="AxPO6">
        <property id="7791109065626895363" name="datatypeId" index="3F6X1D" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="5u2bO_MPa$N">
    <property role="EcuMT" value="6305654420777773363" />
    <property role="TrG5h" value="OpenApiFile" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyi" id="5u2bO_MPa_m" role="1TKVEl">
      <property role="IQ2nx" value="6305654420777773398" />
      <property role="TrG5h" value="openapi" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="5u2bO_MPa_t" role="1TKVEi">
      <property role="IQ2ns" value="6305654420777773405" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="info" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="5u2bO_MPa_r" resolve="OpenApiInfo" />
    </node>
    <node concept="1TJgyj" id="5u2bO_MPa_v" role="1TKVEi">
      <property role="IQ2ns" value="6305654420777773407" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="paths" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="5u2bO_MPa_y" resolve="OpenApiPath" />
    </node>
  </node>
  <node concept="1TIwiD" id="5u2bO_MPa_r">
    <property role="EcuMT" value="6305654420777773403" />
    <property role="TrG5h" value="OpenApiInfo" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyi" id="5u2bO_MPa_Y" role="1TKVEl">
      <property role="IQ2nx" value="6305654420777773438" />
      <property role="TrG5h" value="title" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="5u2bO_MPaA0" role="1TKVEl">
      <property role="IQ2nx" value="6305654420777773440" />
      <property role="TrG5h" value="description" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="5u2bO_MPaA3" role="1TKVEl">
      <property role="IQ2nx" value="6305654420777773443" />
      <property role="TrG5h" value="version" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="5u2bO_MPa_y">
    <property role="EcuMT" value="6305654420777773410" />
    <property role="TrG5h" value="OpenApiPath" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyi" id="5u2bO_MPa_z" role="1TKVEl">
      <property role="IQ2nx" value="6305654420777773411" />
      <property role="TrG5h" value="endpoint" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="5u2bO_MPa_A" role="1TKVEi">
      <property role="IQ2ns" value="6305654420777773414" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="operations" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="5u2bO_MPa__" resolve="OpenApiPathItem" />
    </node>
  </node>
  <node concept="1TIwiD" id="5u2bO_MPa__">
    <property role="EcuMT" value="6305654420777773413" />
    <property role="TrG5h" value="OpenApiPathItem" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyi" id="5u2bO_MPa_C" role="1TKVEl">
      <property role="IQ2nx" value="6305654420777773416" />
      <property role="TrG5h" value="operation" />
      <ref role="AX2Wp" node="5u2bO_MPa_E" resolve="OperationType" />
    </node>
  </node>
  <node concept="25R3W" id="5u2bO_MPa_E">
    <property role="3F6X1D" value="6305654420777773418" />
    <property role="TrG5h" value="OperationType" />
    <node concept="25R33" id="5u2bO_MPa_F" role="25R1y">
      <property role="3tVfz5" value="6305654420777773419" />
      <property role="TrG5h" value="GET" />
    </node>
    <node concept="25R33" id="5u2bO_MPa_G" role="25R1y">
      <property role="3tVfz5" value="6305654420777773420" />
      <property role="TrG5h" value="PUT" />
    </node>
    <node concept="25R33" id="5u2bO_MPa_J" role="25R1y">
      <property role="3tVfz5" value="6305654420777773423" />
      <property role="TrG5h" value="DELETE" />
    </node>
    <node concept="25R33" id="5u2bO_MPa_N" role="25R1y">
      <property role="3tVfz5" value="6305654420777773427" />
      <property role="TrG5h" value="POST" />
    </node>
  </node>
  <node concept="1TIwiD" id="5u2bO_MPa_S">
    <property role="EcuMT" value="6305654420777773432" />
    <property role="TrG5h" value="OpenApiOperation" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyi" id="5u2bO_MPa_T" role="1TKVEl">
      <property role="IQ2nx" value="6305654420777773433" />
      <property role="TrG5h" value="summary" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="5u2bO_MPa_V" role="1TKVEl">
      <property role="IQ2nx" value="6305654420777773435" />
      <property role="TrG5h" value="description" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
</model>

